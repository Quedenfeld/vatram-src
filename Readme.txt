VATRAM
VAriant Tolerant ReAd Mapper


Compilation (qmake is needed):

mkdir build
cd build
qmake ../src
make


The code should compile on all common operating system (Windows, Linux, Mac)


Building index, see:
./bin/vatram index --help

Mapping and aligning reads, see:
./bin/vatram align --help

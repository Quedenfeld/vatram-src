TEMPLATE = subdirs

SUBDIRS += mapper
SUBDIRS += main
SUBDIRS += test

SUBDIRS += lsh
SUBDIRS += alignereval

SUBDIRS +=
SUBDIRS +=

DEPENDPATH = .

main.depends = mapper
test.depends = mapper

lsh.depends = mapper
alignereval.depends = mapper

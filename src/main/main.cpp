#include "module/IndexModule.h"
#include "module/AlignModule.h"
#include "module/MapModule.h"
#include "module/RealignModule.h"
#include "module/VariantStatisticsModule.h"
#include "module/BenchmarkModule.h"
#include "mapper/GenomeGenerator.h"

#include <seqan/arg_parse.h>

#include "SegFaultDebug.h"

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <functional>
#include <stdexcept>

int main(int argc, const char *argv[]) {

    setSegFaultAction();

    IndexModule index;
    AlignModule align;
    MapModule map;
    RealignModule realign;

    VariantStatisticsModule vcfStats;
    BenchmarkModule benchmark;

    std::vector<std::reference_wrapper<ApplicationModule>> modules{index, align, map, realign, vcfStats, benchmark};
    std::vector<std::string> moduleNames;
    for (ApplicationModule &module : modules) {
        moduleNames.push_back(module.getName());
    }

    std::string applicationName = "vatram";
    seqan::ArgumentParser argParser(applicationName);
    seqan::setShortDescription(argParser, "A variant tolerant read mapper and aligner.");
    seqan::addArgument(argParser, seqan::ArgParseArgument(seqan::ArgParseArgument::STRING));
    auto &moduleNameArg = seqan::getArgument(argParser, 0);
    seqan::setValidValues(moduleNameArg, moduleNames);

    for (ApplicationModule &module : modules) {
        seqan::addUsageLine(argParser, module.getName() + " [--help | \\fImodule-options\\fP]");
    }
//    seqan::addDescription(argParser, "description");
    std::ostringstream out, err;
    seqan::ArgumentParser::ParseResult parseRes = seqan::parse(argParser, argc, argv, out, err);
    if (!seqan::hasValue(moduleNameArg)) {
        std::cout << out.str();
        std::cerr << err.str();
        return parseRes == seqan::ArgumentParser::PARSE_ERROR;
    }
    auto itModuleName = std::find(moduleNames.begin(), moduleNames.end(), seqan::getArgumentValue(moduleNameArg));
    if (itModuleName == moduleNames.end()) {
        std::cout << out.str();
        std::cerr << err.str();
        return parseRes == seqan::ArgumentParser::PARSE_ERROR;
    }
    ApplicationModule &module = modules[itModuleName - moduleNames.begin()];
    seqan::ArgumentParser moduleArgParser(applicationName);
    seqan::addArgument(moduleArgParser, seqan::ArgParseArgument(seqan::ArgParseArgument::STRING, module.getName()));
    module.addOptions(moduleArgParser);
    seqan::addUsageLine(moduleArgParser, module.generateUsageLine(moduleArgParser));

    parseRes = seqan::parse(moduleArgParser, argc, argv);

    if (parseRes != seqan::ArgumentParser::PARSE_OK) {
        // outputs predefined functions like 'help', 'version' etc. if not PARSE_ERROR
        return parseRes == seqan::ArgumentParser::PARSE_ERROR;
    }

    try {
        module.readOptions(moduleArgParser);
    } catch(std::invalid_argument &e) {
        std::cerr << seqan::getAppName(moduleArgParser) << ": " << e.what() << std::endl;
        return 1;
    }

    return module.run();
}

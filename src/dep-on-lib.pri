win32-g++: TARGET = ../../bin/$$TARGET_FILE_NAME
else: TARGET = ../bin/$$TARGET_FILE_NAME

INCLUDEPATH += ../
INCLUDEPATH += ../mapper
INCLUDEPATH += ../mapper/vendor/seqan/include
DEPENDPATH = ../

LIB_MAPPER_NAME=mapper
# could / should be different when compiling with msvc ...
LIB_MAPPER_FILE=lib"$$LIB_MAPPER_NAME".a
LIB_MAPPER_SUB_DIR=
win32-g++ {
    CONFIG(release, release|debug): LIB_MAPPER_SUB_DIR=release
    else: CONFIG(debug, debug|release): LIB_MAPPER_SUB_DIR=debug
}
LIB_MAPPER_DIR=$$OUT_PWD/../$$LIB_MAPPER_NAME/$$LIB_MAPPER_SUB_DIR
LIBS += -L$$LIB_MAPPER_DIR -l$$LIB_MAPPER_NAME
PRE_TARGETDEPS += $$LIB_MAPPER_DIR/$$LIB_MAPPER_FILE

include(common.pri)

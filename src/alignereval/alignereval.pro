TARGET_FILE_NAME = alignereval
include(../dep-on-lib.pri)

TEMPLATE = app
CONFIG += console

QT += core
QT -= gui

SOURCES += main.cpp \
    AlignerNoRowSkip.cpp \
    AlignerNaive.cpp

HEADERS += \
    AlignerNoRowSkip.h \
    AlignerNaive.h

#include "AlignerNoRowSkip.h"
#include "align/Variant.h"
#include "PerformanceTimer.h"

#include <seqan/seq_io.h>

#include <iostream>
#include <sstream>
#include <algorithm>
#include <cmath>
#include <chrono>

using std::vector;

namespace { ReadString dummyForReferenceWrapper; }

AlignerNoRowSkip::AlignerNoRowSkip(ErrorCount maxError, ErrorCount maxClip) :
    b(maxError),
    maxError(maxError),
    maxClip(maxClip),
    maxCurClipFront(maxClip),
    maxCurClipBack(maxClip),
    pattern(dummyForReferenceWrapper),
    patternLength(0),
    text(),
    extendedText(),
    startPos(0),
    endPos(0),
    highestRow(0),
    bestMinimum(maxError+1),
    bestMinimumPosition(-1),
    lastRowCurrent(0),
    lastRowNext(0),
    nextVarPos(0),
    itVariant(),
    columns(3),
    lastColumnOfVariant(3)
    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    , numberOfHandledInsertions(0),
    numberOfHandledDeletions(0),
    numberOfComputedFields(0),
    numberOfFoundAlignments(0)
    #endif
{
}

inline ErrorCount AlignerNoRowSkip::charDistance(seqan::Iupac p, seqan::Iupac t) const {
    return 0 == (p.value & t.value);
}

ErrorCount AlignerNoRowSkip::getMaxErrors() const {
    return maxError;
}

ErrorCount AlignerNoRowSkip::getMaxClip() const {
    return maxClip;
}

Alignment AlignerNoRowSkip::align(const Chromosome &chromosome, size_t startPos, size_t endPos, const VariantIndex &variantIndex,
                         const ReadString &pattern, bool isInitialRun) {
    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    PerformanceTimer alignmentTimer;
    numberOfHandledInsertions = 0;
    numberOfHandledDeletions = 0;
    numberOfComputedFields = 0;
    numberOfFoundAlignments = 0;
    alignmentTimer.startCounter();
    #endif

    // Copy parameters and reset all members
    variantIndexEnd = variantIndex.getVariantList().end();
    this->startPos = startPos;
    this->endPos = endPos;
    this->pattern = pattern;

    // Calculate maximum clipping
    if (isInitialRun) {
        maxCurClipFront = std::min((size_t)maxClip, startPos);
        maxCurClipBack = std::min((size_t)maxClip, seqan::length(chromosome.data) - endPos);
        this->text = ReferenceInfix(chromosome.data, startPos, endPos);
        this->extendedText = ReferenceInfix(chromosome.data, startPos-maxCurClipFront, endPos+maxCurClipBack);
    }

    // Init
    patternLength = seqan::length(pattern);
    highestRow = 0;
    bestMinimum = maxError + 1;
    bestMinimumPosition = -1;
    b.init(patternLength);

    lastRowNext = std::min(((size_t)maxError)+1, patternLength);

    nextVarPos = endPos;
    itVariant = variantIndex.getNextVariant(startPos, endPos);
    if (itVariant != variantIndexEnd) {
        nextVarPos = itVariant->getPosition();
    }

    // Iterate over all reference positions
    for (size_t refPos = 0; refPos < seqan::length(text); refPos++) {
        lastRowCurrent = lastRowNext;
        lastRowNext = 1;

        if (startPos + refPos == nextVarPos) {
            // Variants at this position
            processVariants(refPos);
            lastRowCurrent = lastRowNext;
            lastRowNext = 1;
            alignColumn(refPos, 0, Variant::none, text[refPos]);
        } else {
            // No variants at this position
            alignColumn(refPos, 0, Variant::none, text[refPos], b.getLastColumn());
        }
    }

    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    double alignmentTime = alignmentTimer.getCounter();
    #endif

    #ifdef ALIGNER_DEBUG_OUTPUT
//    printMatrix();
    #endif

    Alignment alignment;
    if (highestRow >= patternLength - maxCurClipBack && bestMinimum <= maxError) {
        // aligned read from beginning, possible clipping at end
        alignment = backtrace(bestMinimumPosition);
    } else if (maxClip == 0) {
        // no alignment from beginning of read and no clipping allowed -> return no alignment
        alignment.setPosition(std::numeric_limits<size_t>::max());
    } else {
        if (isInitialRun) {
            // clipping beginning of read and retry to align
            alignment = align(chromosome, startPos, endPos, variantIndex, ReadInfix(pattern, maxCurClipFront, patternLength), false);
            if (alignment.isValid()) {
                // if end-clipping successful, add the front-clipped chars to front of alignment
                ColumnIndex posOfEndClippedAlignment = alignment.getPosition();
                bool stillMatching = true;
                for (RowIndex row = 1; row <= maxCurClipFront; row++) {
                    int index = (maxCurClipFront - row) + (posOfEndClippedAlignment - startPos);
                    seqan::Iupac t = extendedText[index];
                    if (stillMatching) {
                        seqan::Iupac p = pattern[maxCurClipFront-row];
                        if (charDistance(p, t) == 0) {
                            alignment.addCigarChar('M');
                        } else {
                            alignment.addCigarChar('S');
                            stillMatching = false;
                        }
                    } else {
                        alignment.addCigarChar('S');
                    }
                    alignment.addAlignmentChar(t);
                }
                alignment.setPosition(posOfEndClippedAlignment-maxCurClipFront);
            }
        } else {
            alignment.setPosition(std::numeric_limits<size_t>::max());
        }
    }

    #ifdef MEASURE_STATISTICS_IN_ALIGNER
    if (isInitialRun) {
        double backtracingTime = alignmentTimer.getCounter()-alignmentTime;
        numberOfFoundAlignments+=alignment.isValid();
        std::cout << "1;"
                << "0;"
                << alignmentTime << ";"
                << backtracingTime << ";"
                << numberOfComputedFields << ";"
                << numberOfFoundAlignments << ";"
                << numberOfHandledInsertions << ";"
                << numberOfHandledDeletions << "\n";
    }
    #endif

    return alignment;
}

Alignment AlignerNoRowSkip::align(const Chromosome &chromosome, size_t startPos, size_t endPos, const VariantIndex &variantIndex,
                         const ReadString &pattern) {

    // Copy parameters and reset all members
    b.maxError = maxError;
    return align(chromosome, startPos, endPos, variantIndex, pattern, true);
}

Alignment AlignerNoRowSkip::align(const Chromosome &chromosome, size_t startPos, size_t endPos, const VariantIndex &variantIndex,
                         const ReadString &pattern, ErrorCount maxError, ErrorCount maxClip) {

    // Copy parameters and reset all members
    this->maxError = maxError;
    this->maxClip = maxClip;
    b.maxError = maxError;
    return align(chromosome, startPos, endPos, variantIndex, pattern, true);
}

void AlignerNoRowSkip::alignColumn(size_t refPos, size_t indelPos,
                          const Variant &var, seqan::Iupac t, ColumnIndex previousColumn) {
    b.newColumn(refPos, var, indelPos);
    ColumnIndex currentColumn = b.getLastColumn();

    // iterate over rows
    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout << "column=" << b.getLastColumn() << " cancel=" << lastRowCurrent << std::endl;
    #endif
    for (RowIndex row = 1; row <= lastRowCurrent; row++) {
        seqan::Iupac p = pattern.get()[row-1];
        ColumnIndex backtracingColumn = previousColumn;
        #ifdef MEASURE_STATISTICS_IN_ALIGNER
        numberOfComputedFields++;
        #endif

        /*
         * The recursion rules have to be checked in this order: MATCH/MISMATCH, DELETION, INSERTION. Reason is the
         * same as stated in main alignment method.
         */
        ErrorCount dist = charDistance(p, t);
        // match (default minimum)
        RecursionType recursionDirection = (dist == 0) ? AL_MATCH : AL_MISMATCH; // match
        ErrorCount minimum = b.getScore(previousColumn, row - 1) + dist;
        if (b.getScore(previousColumn, row) + 1 < minimum) {
            // deletion
            minimum = b.getScore(previousColumn, row) + 1;
            recursionDirection = AL_DELETEDCHAR;
        }
        if (b.getScore(currentColumn, row - 1) + 1 < minimum) {
            // insertion
            minimum = b.getScore(currentColumn, row -1 ) + 1;
            recursionDirection = AL_INSERTEDCHAR;
            backtracingColumn = currentColumn;
        }

        // fill matrix
        b.setScore(currentColumn, row, minimum);
        b.setBtColumn(currentColumn, row, backtracingColumn);
        b.setDir(currentColumn, row, recursionDirection);

        // increment steps for next column
        if (minimum <= maxError) {
            lastRowNext = row + 1;
            /*
             * These scores can be false, but they will be overwritten if a value smaller than maxError+1 belongs
             * into them. If maxError+1 is correct, it is necessary to write this value for read access by a
             * neighbouring field.
             */
            b.defineScoreEntry(currentColumn, row + 1);
        } else {
            // do nothing -> do not refresh "jumpBackRow"
        }

        // Handle minimums
        if (row >= highestRow && minimum <= maxError) {
            if (row > highestRow) {
                bestMinimum = minimum;
                bestMinimumPosition = currentColumn;
                highestRow = row;
            } else if (minimum < bestMinimum) {
                bestMinimum = minimum;
                bestMinimumPosition = currentColumn;
            }
        }
    }

    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout << "endcol, lastRowNext=" << lastRowNext << std::flush;
    #endif

    // ensure that next column does not read into "undefined" area
    if (lastRowNext > patternLength) {
        lastRowNext = patternLength;
    } else {
        b.defineScoreEntry(currentColumn, lastRowNext);
    }
    b.setLastRowNext(currentColumn, lastRowNext);

    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout << std::endl;
    #endif
}

void AlignerNoRowSkip::alignColumn(size_t refPos, size_t indelPos, const Variant &var, seqan::Iupac t) {
    b.newColumn(refPos, var, indelPos);
    ColumnIndex currentColumn = b.getLastColumn();

    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout << "column=" << b.getLastColumn() << " cancel=" << lastRowCurrent << std::endl;
    #endif
    // iterate over rows
    for (RowIndex row = 1; row <= lastRowCurrent; row++) {
        /*
         * The recursion rules have to be checked in this order: MATCH/MISMATCH, DELETION, INSERTION. The reason is
         * that we want variant positions as far left as possible, so the alignment should contain "I"s and "D"s
         * as far left as possible.
         */
        #ifdef MEASURE_STATISTICS_IN_ALIGNER
        numberOfComputedFields++;
        #endif

        ColumnIndex recursionColumn = currentColumn;
        ErrorCount minimum = maxError + 1;
        RecursionType recursionDirection = AL_NONE;
        seqan::Iupac p = pattern.get()[row-1];

        // check MATCH/MISMATCH
        for (size_t k = 0; k < columns.size(); k++) {
            ErrorCount dist = charDistance(p, t);
            ErrorCount toCheck = b.getScore(columns[k], row-1) + dist;
            if (toCheck < minimum) {
                minimum = toCheck;
                recursionDirection = (dist == 0) ? AL_MATCH : AL_MISMATCH;
                recursionColumn = lastColumnOfVariant[k];
            }
        }
        // check DELETION
        for (size_t k = 0; k < columns.size(); k++) {
            ErrorCount toCheck = b.getScore(columns[k], row) + 1;
            if (toCheck < minimum) {
                minimum = toCheck;
                recursionDirection = AL_DELETEDCHAR;
                recursionColumn = lastColumnOfVariant[k];
            }
        }
        // check INSERTION
        ErrorCount toCheck = b.getScore(currentColumn, row-1) + 1;
        if (toCheck < minimum) {
            recursionColumn = currentColumn;
            minimum = toCheck;
            recursionDirection = AL_INSERTEDCHAR;
        }

        // fill matrix
        b.setScore(currentColumn, row, minimum);
        b.setBtColumn(currentColumn, row, recursionColumn);
        b.setDir(currentColumn, row, recursionDirection);

        // increment steps for next column
        if (minimum <= maxError) {
            lastRowNext = row + 1;
            /*
             * These scores can be false, but they will be overwritten if a value smaller than maxError+1 belongs
             * into them. If maxError+1 is correct, it is necessary to write this value for read access by a
             * neighbouring field.
             */
            b.defineScoreEntry(currentColumn, row + 1);
        } else {
            // do nothing -> do not refresh "jumpBackRow"
        }

        // Handle minimums
        if (row >= highestRow && minimum <= maxError) {
            if (row > highestRow) {
                bestMinimum = minimum;
                bestMinimumPosition = currentColumn;
                highestRow = row;
            } else if (minimum < bestMinimum) {
                bestMinimum = minimum;
                bestMinimumPosition = currentColumn;
            }
        }
    }

    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout << "endcol";
    #endif

    // ensure that next column does not read into "undefined" area
    if (lastRowNext > patternLength) {
        lastRowNext = patternLength;
    } else {
        b.defineScoreEntry(currentColumn, lastRowNext);
    }
    b.setLastRowNext(currentColumn, lastRowNext);

    #ifdef ALIGNER_DEBUG_OUTPUT
    std::cout << std::endl;
    #endif
}

void AlignerNoRowSkip::processVariants(size_t refPos) {
    // all columns, which have to be considered for dynamic programming will be stored here
    columns.assign(1, b.getLastColumn());
    lastColumnOfVariant.assign(1, b.getLastColumn()); // stored end position of each variant

    /*
     * We need to store this in case of multiple vatiants, so every variant knows, which columwas the last
     * "regular" column.
     */
    ColumnIndex lastColumnBeforeVariants = b.getLastColumn();

    /*
     * Row cancel has to be copied and re-used for each variant to avoid that they compute too far ahead.
     */
    RowIndex lastRowCurrentFinal = lastRowCurrent;

    while (startPos + refPos == nextVarPos) {
        // Each variant adds a new column to jump back for recursion. It is stored here and added to "columns"
        ColumnIndex newColumn = b.getLastColumn();

        const Variant &currentVar = *itVariant;

        switch(currentVar.getType()) {
        case(Variant::INSERTION):
            #ifdef MEASURE_STATISTICS_IN_ALIGNER
            numberOfHandledInsertions++;
            #endif
            #ifdef ALIGNER_DEBUG_OUTPUT
            #endif
            newColumn = processInsertion(refPos, currentVar, lastColumnBeforeVariants);
            break;
        case(Variant::DELETION):
            #ifdef MEASURE_STATISTICS_IN_ALIGNER
            numberOfHandledDeletions++;
            #endif
            newColumn = processDeletion(refPos, currentVar, lastColumnBeforeVariants);
            break;
        case(Variant::SUBSTITUTION):
            #ifdef MEASURE_STATISTICS_IN_ALIGNER
            numberOfHandledInsertions++;
            numberOfHandledDeletions++;
            #endif
            newColumn = processSubstitution(refPos, currentVar, lastColumnBeforeVariants);
            break;
        }

        /*
         * The variable "rowCancel" was determined by how far the last column was filled. Since insertions can
         * add whole submatrices in-position, we must ensure that all rows filled by an insertion variant are
         * considered in the next step.
         */
        lastRowCurrentFinal = std::max(lastRowCurrentFinal, std::max(lastRowNext, b.getScoreFill(newColumn)+1));
        lastRowCurrentFinal = std::min((RowIndex)patternLength, lastRowCurrentFinal);

        // store stuff
        columns.push_back(newColumn);
        lastColumnOfVariant.push_back(b.getLastColumn());
        if (++itVariant != variantIndexEnd) {
            nextVarPos = itVariant->getPosition();
        } else {
            nextVarPos = endPos;
        }
    }

    /*
     * Ensure that all column for dynamic programming are filled up to highest row of any insertion
     */
    for (size_t k = 0; k < columns.size(); k++) {
        b.ensureScoreColumnFillNoRowSkip(columns[k], lastRowCurrentFinal);
    }

    lastRowNext = lastRowCurrentFinal;
}

ColumnIndex AlignerNoRowSkip::processInsertion(size_t refPos, const Variant &insertion, ColumnIndex columnBefore) {
    /*
     * Insertions are handled by calculating new "insertion columns" to the matrix.
     */
    return alignInsertionString(refPos, insertion, columnBefore);
}

ColumnIndex AlignerNoRowSkip::processDeletion(size_t refPos, const Variant &deletion, ColumnIndex columnBefore) {
    /*
     * Check if deletion goes beyond the start of our reference.
     */
    if (deletion.getDeletionLength() < refPos) {
        // the last column, which is not skipped by this deletion
        ColumnIndex jumpBackColumn = b.getColumnOfRefInIndex(refPos - 1 - deletion.getDeletionLength());

        /*
         * Add new "dummy" column for deletions. These dummy columns have no influence on the score,
         * they are just needed to store the information, to which column the backtracer has to jump,
         * when he uses this deletion variant in an optimal alignment. The recursion direction is
         * always "NONE" in this column.
         */
        b.newColumn(refPos, deletion, 0);
        RowIndex rowFill = std::max(lastRowCurrent, b.getScoreFill(jumpBackColumn));
        for (RowIndex j = 0; j <= rowFill; j++) {
            b.setBtColumn(b.getLastColumn(), j, jumpBackColumn);
            b.setDir(b.getLastColumn(), j, AL_NONE);
        }

        /*
         * It could happen that the jumpBackColumn is less filled than the current one. To prevent
         * access into undefined area, we have to fill the jumpBackColumn sufficiently.
         */
        b.ensureScoreColumnFillNoRowSkip(jumpBackColumn, lastRowCurrent);

        return jumpBackColumn;
    } else {
        return columnBefore;
    }
}

ColumnIndex AlignerNoRowSkip::processSubstitution(size_t refPos, const Variant &substitution, ColumnIndex columnBefore) {
    if (substitution.getDeletionLength() < refPos) {
       ColumnIndex jumpBackColumn = b.getColumnOfRefInIndex(refPos - 1 - substitution.getDeletionLength());

       lastRowCurrent = b.getLastRowNext(jumpBackColumn);
       // same here as for deletion with the filled columns
       b.ensureScoreColumnFillNoRowSkip(jumpBackColumn, lastRowCurrent);
       return alignInsertionString(refPos, substitution, jumpBackColumn);
    } else {
        return columnBefore;
    }
}

ColumnIndex AlignerNoRowSkip::alignInsertionString(size_t refPos, const Variant &var, ColumnIndex lastColumnBeforeVariants) {
    const auto &varText = var.getInsertionString();
    size_t varTextLength = seqan::length(varText);

    /*
     * first column reads error values from column before variants, all others can read their lefthand neighbour
     */
    if (varTextLength > 0) {
        lastRowCurrent = b.getLastRowNext(lastColumnBeforeVariants);
        lastRowNext = 1;
        alignColumn(refPos, 0, var, varText[0], lastColumnBeforeVariants);
    }

    // iterate over insertion string (= columns)
    for (size_t indelPos = 1; indelPos < varTextLength; indelPos++) {
        lastRowCurrent = lastRowNext;
        lastRowNext = 1;
        alignColumn(refPos, indelPos, var, varText[indelPos], b.getLastColumn());
    }

    return b.getLastColumn();
}

Alignment AlignerNoRowSkip::backtrace(ColumnIndex position) {
    ColumnIndex currentColumn = position;
    ColumnIndex startColumn = currentColumn;
    RowIndex currentRow = highestRow;
    Alignment alignment;

    // fill "S" for all clipped read chars
    for (RowIndex row = patternLength; row > highestRow; row--) {
        alignment.addCigarChar('S');
        alignment.addAlignmentChar(extendedText[maxCurClipFront + row - highestRow + b.getIndexOfColumnInRef(currentColumn)]);
    }

    // actual backtracing
    while (currentRow > 0) {
        startColumn = currentColumn;
        alignment.addUsedVariant(b.getVariantAt(currentColumn));
        RecursionType recursionDirection = b.getDir(currentColumn, currentRow);
        RowIndex rowDecrease = 0;
        switch (recursionDirection) {
        case AL_DELETEDCHAR:
            alignment.addCigarChar('D');
            if(&b.getVariantAt(currentColumn) == &Variant::none) {
                alignment.addAlignmentChar(text[b.getIndexOfColumnInRef(currentColumn)]);
            } else {
                alignment.addAlignmentChar(b.getVariantAt(currentColumn).getInsertionString()[b.getOffset(currentColumn)]);
            }
            alignment.incrementErrorCount();
            break;
        case AL_INSERTEDCHAR:
            alignment.addCigarChar('I');
            alignment.incrementErrorCount();
            rowDecrease++;
            break;
        case AL_MATCH:
            alignment.addCigarChar('M');
            if(&b.getVariantAt(currentColumn) == &Variant::none) {
                alignment.addAlignmentChar(text[b.getIndexOfColumnInRef(currentColumn)]);
            } else {
                alignment.addAlignmentChar(b.getVariantAt(currentColumn).getInsertionString()[b.getOffset(currentColumn)]);
            }
            rowDecrease++;
            break;
        case AL_MISMATCH:
            alignment.addCigarChar('X');
            if(&b.getVariantAt(currentColumn) == &Variant::none) {
                alignment.addAlignmentChar(text[b.getIndexOfColumnInRef(currentColumn)]);
            } else {
                alignment.addAlignmentChar(b.getVariantAt(currentColumn).getInsertionString()[b.getOffset(currentColumn)]);
            }
            alignment.incrementErrorCount();
            rowDecrease++;
            break;
        case AL_NONE:
            break;
        default:
            break;
        }
        currentColumn = b.getBtColumn(currentColumn, currentRow);
        currentRow -= rowDecrease;
    }
    alignment.setPosition(startPos + b.getIndexOfColumnInRef(startColumn));
    alignment.setStartVariant(b.getVariantAt(currentColumn));
    alignment.setStartOffset(b.getOffset(currentColumn));
    return alignment;
}

void AlignerNoRowSkip::printMatrix() {

    const int maxIndex = b.getLastColumn();
    auto wout = [=]() -> std::ostream& { return std::cout << std::right << std::setw(2 + std::max(1, (int)std::log10(maxIndex))); };

    wout() << ' ' << "=== Scores (Columns) ===" << std::endl;
    wout() << ' ';
    wout() << ' ';
    //TODO: wout() macht bei Texten irgendwie Probleme
    for (size_t j = 1; j <= b.getLastColumn(); j++) {
        if (&b.getVariantAt(j) == &Variant::none) {
            wout() << text[b.getIndexOfColumnInRef(j)];
        } else if (b.getVariantAt(j).getType() == Variant::INSERTION || b.getVariantAt(j).getType() == Variant::SUBSTITUTION) {
            std::stringstream insertion;
            insertion << '(' << b.getVariantAt(j).getInsertionString()[b.getOffset(j)] << ')';
            wout() << insertion.str();
        } else {
            wout() << '.';
        }
    }
    std::cout << std::endl;

    for (size_t i = 0; i <= patternLength; i++) {
        if (i > 0) {
            wout() << pattern.get()[i-1];
        } else {
            wout() << ' ';
        }
        for (int j = 0; j <= maxIndex; j++) {
            wout() << (unsigned int) b.getScore(j,i);
        }
        std::cout << std::endl;
    }

    wout() << ' ' << "=== Backtracing (Columns) ===" << std::endl;
    wout() << ' ';
    wout() << ' ';
    for (int j = 1; j <= maxIndex; j++) {
        wout() << j;
    }
    std::cout << std::endl;

    for (size_t i = 0; i <= patternLength; i++) {
        wout() << i;
        for (int j = 0; j <= maxIndex; j++) {
            wout() << b.getBtColumn(j,i);
        }
        std::cout << std::endl;
    }

    wout() << ' ' << "=== Backtracing (Direction) ===" << std::endl;
    wout() << ' ';
    wout() << ' ';
    for (int j = 1; j <= maxIndex; j++) {
        wout() << j;
    }
    std::cout << std::endl;

    for (size_t i = 0; i <= patternLength; i++) {
        wout() << i;
        for (int j = 0; j <= maxIndex; j++) {
            wout() << int(b.getDir(j,i));
        }
        std::cout << std::endl;
    }
}

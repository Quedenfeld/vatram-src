#ifndef PG583_PERFORMANCETIMER_H
#define PG583_PERFORMANCETIMER_H

#include <chrono>
#include <iostream>

#ifdef _WIN32
#include <windows.h>
#else
#include "Timer.h"
#endif

/**
 * @brief Accurate timer to measure time in microseconds and works for both Windows and Linux.
 */
class PerformanceTimer {

public:
#ifdef _WIN32
    double PCFreq = 0.0;
    __int64 CounterStart = 0;
#else
    Timer<std::chrono::nanoseconds> timer;
    double time = 0.0;
#endif

    /**
     * @brief Starts the counter.
     */
    void startCounter() {
#ifdef _WIN32
        LARGE_INTEGER li;
        if(!QueryPerformanceFrequency(&li))
        std::cout << "QueryPerformanceFrequency failed!\n";

        PCFreq = double(li.QuadPart)/1000000.0;

        QueryPerformanceCounter(&li);
        CounterStart = li.QuadPart;
#else
        timer.startTimer();
#endif
    }

    /**
     * @brief Retrieves the current time from the timer
     * @return elapsed time in microseconds.
     */
    double getCounter() {
#ifdef _WIN32
        LARGE_INTEGER li;
        QueryPerformanceCounter(&li);
        return double(li.QuadPart-CounterStart)/PCFreq;
#else
        timer.stopTimer();
        return (double)(timer.getDuration().count())/1000.0;
#endif
    }
};

#endif // PG583_PERFORMANCETIMER_H

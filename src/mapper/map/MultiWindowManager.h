#ifndef PG583_MAP_MULTIWINDOWMANAGER_H
#define PG583_MAP_MULTIWINDOWMANAGER_H

#include "map/HashDataStructure.h"

#include <cstddef>
#include <array>
#include <vector>
#include <deque>
#include <limits>

/**
 * @brief This class manage multi window buckets
 * @details The MultiWindowManager does not have an array for storing single window indices.
 *      This is done by the classes that uses this class namely CollisionFreeBHT and SuperRank.
 *      The MultiWindowManager provides methods that takes a reference to the WindowIndexEntry (32-bit).
 *      This reference is changed and stores the information that is needed to determine, if we have
 *      a single window index or if there are more indices stored for the specific value.
 *      There are three data structure to store multiple window indices: The double window index buckets contain
 *      two window indices, the quadruple window index buckets contain three or four window indices and
 *      the multi window index buckets contains five or more window indices.
 */
class MultiWindowManager {
public:
    /// A pair of two pointers pointing to the begin and the end of an array with window indices
    typedef std::pair<const WindowIndex*, const WindowIndex*> WindowIndexPointerPair;

    /**
     * @brief This struct stores either one window index or meta information for a multiple window indices
     * @details The first two bits (most-significant) are used to store the meta information, the other 30 bits for the value.
     *      Window indices typically use only the low 26 bits, so this is not problematic.
     *      The stored window indices must be lower than 2^30-2.
     */
    struct
#ifdef __GNUC__
    __attribute__ ((__packed__)) // needed for packed BandHashEntry in CollisionFreeBHT
#endif
    WindowIndexEntry {
        /// 32-bit value storing the information
        WindowIndex value;

        /**
         * @brief Constructs an empty window index entry
         */
        inline WindowIndexEntry();

        /**
         * @brief Constructs a window index entry storing one window index
         * @param windowIndex The window index that should be stored in this entry
         */
        inline WindowIndexEntry(WindowIndex windowIndex);

        /**
         * @brief Checks if this entry stores no window index
         * @return true, if this entry is empty
         */
        inline bool isEmpty() const;
        inline bool isSingleEntry() const; /// Returns true, if this entry stores a single window index
        inline bool isDoubleWindow() const; /// Returns true, if this entry stores meta information for a double window index bucket
        inline bool isQuadrupleWindow() const; /// Returns true, if this entry stores meta information for a quadruple window index bucket
        inline bool isMultiWindow() const; /// Returns true, if this entry stores meta information for a multi window index bucket

        /**
         * @brief Returns the stored value without the meta information bits
         */
        inline WindowIndex getValue() const;

        /**
         * @brief Sets the value and the meta information for a single window index
         * @param windowIndex window index
         */
        inline void setWindowIndex(WindowIndex windowIndex);

        /**
         * @brief Sets the value and the meta information for a double window index bucket
         * @param doubleWindowIndex index of the double window index bucket
         */
        inline void setDoubleWindowIndex(WindowIndex doubleWindowIndex);

        /**
         * @brief Sets the value and the meta information for a quadruple window index bucket
         * @param doubleWindowIndex index of the quadruple window index bucket
         */
        inline void setQuadrupleWindowIndex(WindowIndex quadrupleWindowIndex);

        /**
         * @brief Sets the value and the meta information for a multi window index bucket
         * @param doubleWindowIndex index of the multi window index bucket
         */
        inline void setMultiWindowIndex(WindowIndex multiWindowIndex);

        static constexpr WindowIndex SINGLE_WINDOW = 0;                                      /// meta value denoting a single window index
        static constexpr WindowIndex DOUBLE_WINDOW = 1 << (sizeof(WindowIndex) * 8 - 1);     /// meta value denoting a double window bucket
        static constexpr WindowIndex QUADRUPLE_WINDOW = 1 << (sizeof(WindowIndex) * 8 - 2);  /// meta value denoting a quadruple window bucket
        static constexpr WindowIndex MULTI_WINDOW = DOUBLE_WINDOW | QUADRUPLE_WINDOW;        /// meta value denoting a multi window bucket

        /**
         * @brief The bits that are set in this constant are used to store the meta data (double, quadruple and multi window)
         */
        static constexpr WindowIndex META_MASK = DOUBLE_WINDOW | QUADRUPLE_WINDOW | MULTI_WINDOW;

        /**
         * @brief The bits that are set in this constant are used to store the value
         */
        static constexpr WindowIndex VALUE_MASK = ~META_MASK;

        /**
         * @brief This value denotes that the BandHashEntry is empty
         */
        static constexpr WindowIndex EMPTY = VALUE_MASK; // = 0011111...111
    };

    /**
     * @brief Adds another window index to a (maybe empty) window index entry
     * @param headEntry a reference to a window index entry where the meta information is stored
     * @param newWindowIndex the new window index
     * @details the headEntry is changed during this operation. It contains the necessary meta information to find the
     *      new window index
     */
    inline
    void add(WindowIndexEntry& headEntry, WindowIndex newWindowIndex);

    /**
     * @brief Finds all window indices stored in the specified window index entry
     * @param headEntry the window index entry that contains the meta information
     * @param operation for each window index wi that was found the function operation(wi) is called
     * @param maximumOfReturnedWindows The maximal number of window indices that is returned
     * @details If the number of window indices stored in the entry is bigger than maximumOfReturnedWindows,
     *      then the function operation() is not called.
     */
    template <class Function>
    void get(const WindowIndexEntry& headEntry, Function operation, size_t maximumOfReturnedWindows) const;

    /**
     * @brief Finds all window indices stored in the specified window index entry
     * @param headEntry the window index entry that contains the meta information
     * @param maximumOfReturnedWindows The maximal number of window indices that is returned
     * @return two pointers ("begin" and "end") pointing to the first window index
     * @details If the number of window indices stored in the entry is bigger than maximumOfReturnedWindows,
     *      then an empty pointer pair is returned.
     *      If the headEntry is emtpy, than two NULL pointers are returned.
     */
    inline
    WindowIndexPointerPair get(const WindowIndexEntry& headEntry, size_t maximumOfReturnedWindows) const;

    /**
     * @brief Clears all data structures, so the window manager contains no double, quadruple or multi buckets any more
     */
    inline
    void clear();

    /**
     * @brief Sort the double, quadruple and multi window index buckets.
     * @details The sorting is important for the new implementation of findReadOccurrences in LSH
     */
    inline
    void sort();

    /**
     * @brief current memory usage for this data structure
     * @return an approximation for the number of bytes used by this data structure
     */
    inline
    size_t memoryUsage() const;

    inline
    void shrinkToFit();

private:
    friend class LshIO;

    // Buckets for storing multiple window indices
    std::deque<std::array<WindowIndex, 2>> doubleWindowBuckets; /// Buckets for two window indices
    std::deque<std::array<WindowIndex, 4>> quadrupleWindowBuckets; /// Buckets for three or four window indices
    std::deque<std::vector<WindowIndex>> multiWindowBuckets; /// Buckets for more than five window indices

    // These vectors contains indices of empty buckets (gaps) in the respective vectors.
    std::vector<WindowIndex> freeDoubleWindowBuckets; /// A list of indices that denotes unused buckets in doubleWindowBuckets
    std::vector<WindowIndex> freeQuadrupleWindowBuckets; /// A list of indices that denotes unused buckets in quadrupleWindowBuckets
    std::vector<WindowIndex> freeMultiWindowBuckets; /// A list of indices that denotes unused buckets in multiWindowBuckets

    /**
     * @brief Converts a single window index entry to an entry with two window indices
     * @param singleEntry the original single index entry
     * @param secondIndex the new window index
     */
    inline void makeDoubleEntry(WindowIndexEntry& singleEntry, WindowIndex secondIndex);

    /**
     * @brief Converts a double window index entry to a quadruple entry containing three window indices
     * @param baseEntry the original double index entry
     * @param thirdIndex the new window index
     * @details the old double bucket that is not needed any more is added to the freeDoubleWindowBuckets
     */
    inline void makeQuadrupleEntry(WindowIndexEntry& baseEntry, WindowIndex thirdIndex);

    /**
     * @brief Inserts a window index to a quadruple entry that contains only three window indices
     * @param baseEntry the original entry pointing to quadruple bucket containing three window indices
     * @param nextIndex the new window index
     */
    inline void insertToQuadrupleEntry(WindowIndexEntry& baseEntry, WindowIndex nextIndex);

    /**
     * @brief Converts a quadruple window index entry to a multi entry
     * @param baseEntry the original quadruple entry containing four window indices
     * @param fifthIndex the new window index
     * @details the old quadruple bucket that is not needed any more is added to the freeQuadrupleWindowBuckets
     */
    inline void makeMultiEntry(WindowIndexEntry& baseEntry, WindowIndex fifthIndex);

    /**
     * @brief Inserts another window index to a multi bucket entry
     * @param baseEntry an entry pointing to a multi window index bucket
     * @param nextIndex the new window index
     */
    inline void insertToMultiEntry(WindowIndexEntry& baseEntry, WindowIndex nextIndex);

    /**
     * @brief Returns an index of a free double bucket.
     * @details First, it is checked, if there is an entry in freeDoubleWindowBuckets. If there is none, a new double bucket is created.
     */
    inline size_t getFreeDoubleBucket();

    /**
     * @brief Returns an index of a free quadruple bucket.
     * @details First, it is checked, if there is an entry in freeQuadrupleWindowBuckets. If there is none, a new quadruple bucket is created.
     */
    inline size_t getFreeQuadrupleBucket();

    /**
     * @brief Returns an index of a free multi bucket.
     * @details First, it is checked, if there is an entry in freeMultiWindowBuckets. If there is none, a new double multi is created.
     */
    inline size_t getFreeMultiBucket();
};

void MultiWindowManager::add(WindowIndexEntry& headEntry, WindowIndex newWindowIndex) {
    if (headEntry.isEmpty()) {
        headEntry.setWindowIndex(newWindowIndex);
    } else {
        switch (headEntry.value & WindowIndexEntry::META_MASK) {
        case WindowIndexEntry::SINGLE_WINDOW :
            makeDoubleEntry(headEntry, newWindowIndex);
            break;
        case WindowIndexEntry::DOUBLE_WINDOW :
            makeQuadrupleEntry(headEntry, newWindowIndex);
            break;
        case WindowIndexEntry::QUADRUPLE_WINDOW :
            insertToQuadrupleEntry(headEntry, newWindowIndex);
            break;
        case WindowIndexEntry::MULTI_WINDOW :
            insertToMultiEntry(headEntry, newWindowIndex);
            break;
        }

    }
}

template <class Function>
void MultiWindowManager::get(const WindowIndexEntry& headEntry, Function operation, size_t maximumOfReturnedWindows) const {
    if (!headEntry.isEmpty()) {
        switch (headEntry.value & WindowIndexEntry::META_MASK) {
        case WindowIndexEntry::SINGLE_WINDOW : {
            operation(headEntry.getValue());
            break;
        }
        case WindowIndexEntry::DOUBLE_WINDOW : {
            auto& doubleBucket = doubleWindowBuckets[headEntry.getValue()];
            operation(doubleBucket[0]);
            operation(doubleBucket[1]);
            break;
        }
        case WindowIndexEntry::QUADRUPLE_WINDOW : {
            auto& quadBucket = quadrupleWindowBuckets[headEntry.getValue()];
            operation(quadBucket[0]);
            operation(quadBucket[1]);
            operation(quadBucket[2]);
            if (quadBucket[3] != WindowIndexEntry::EMPTY) {
                operation(quadBucket[3]);
            }
            break;
        }
        case WindowIndexEntry::MULTI_WINDOW : {
            auto& multiBucket = multiWindowBuckets[headEntry.getValue()];
            if (multiBucket.size() <= maximumOfReturnedWindows) {
                for (WindowIndex wi: multiBucket) {
                    operation(wi);
                }
            }
            break;
        }
        }
    }
}

MultiWindowManager::WindowIndexPointerPair MultiWindowManager::get(const WindowIndexEntry& headEntry, size_t maximumOfReturnedWindows) const {
    if (!headEntry.isEmpty()) {
        switch (headEntry.value & WindowIndexEntry::META_MASK) {
        case WindowIndexEntry::SINGLE_WINDOW : {
            return std::make_pair(&headEntry.value, &headEntry.value + 1);
        }
        case WindowIndexEntry::DOUBLE_WINDOW : {
            auto& doubleBucket = doubleWindowBuckets[headEntry.getValue()];
            return std::make_pair(&doubleBucket[0], &doubleBucket[2]);
        }
        case WindowIndexEntry::QUADRUPLE_WINDOW : {
            auto& quadBucket = quadrupleWindowBuckets[headEntry.getValue()];
            ptrdiff_t size = quadBucket[3] != WindowIndexEntry::EMPTY ? 4 : 3;
            return std::make_pair(&quadBucket[0], &quadBucket[size]);
        }
        case WindowIndexEntry::MULTI_WINDOW : {
            auto& multiBucket = multiWindowBuckets[headEntry.getValue()];
            if (multiBucket.size() <= maximumOfReturnedWindows) {
                return std::make_pair(&multiBucket[0], &multiBucket[multiBucket.size()]);
            }
        }
        }

    }
    return WindowIndexPointerPair(NULL, NULL);
}

void MultiWindowManager::clear() {
    doubleWindowBuckets.clear();
    quadrupleWindowBuckets.clear();
    multiWindowBuckets.clear();
    freeDoubleWindowBuckets.clear();
    freeQuadrupleWindowBuckets.clear();
    freeMultiWindowBuckets.clear();
}

void MultiWindowManager::sort() {
    for (auto& db: doubleWindowBuckets) {
        std::sort(db.begin(), db.end());
    }
    for (auto& qb: quadrupleWindowBuckets) {
        ptrdiff_t size = qb[3] != WindowIndexEntry::EMPTY ? 4 : 3;
        std::sort(qb.begin(), qb.begin() + size);
    }
    for (auto& mb: multiWindowBuckets) {
        std::sort(mb.begin(), mb.end());
    }
}

void MultiWindowManager::shrinkToFit() {
    for (auto& mb: multiWindowBuckets) {
        mb.shrink_to_fit();
    }
    freeDoubleWindowBuckets.shrink_to_fit();
    freeQuadrupleWindowBuckets.shrink_to_fit();
    freeMultiWindowBuckets.shrink_to_fit();
    // Deques should need to be shrinked, but it does not hurt...
    doubleWindowBuckets.shrink_to_fit();
    quadrupleWindowBuckets.shrink_to_fit();
    multiWindowBuckets.shrink_to_fit();
}

size_t MultiWindowManager::memoryUsage() const {
    size_t sum = 0;
    sum += sizeof doubleWindowBuckets + doubleWindowBuckets.size() * sizeof doubleWindowBuckets[0];
    sum += sizeof quadrupleWindowBuckets + quadrupleWindowBuckets.size() * sizeof quadrupleWindowBuckets[0];
    sum += sizeof multiWindowBuckets;
    for (const std::vector<WindowIndex>& bucket: multiWindowBuckets) {
        sum += sizeof bucket + bucket.capacity() * sizeof(WindowIndex);
    }
    sum += sizeof freeDoubleWindowBuckets + freeDoubleWindowBuckets.capacity() * sizeof(WindowIndex);
    sum += sizeof freeQuadrupleWindowBuckets + freeQuadrupleWindowBuckets.capacity() * sizeof(WindowIndex);
    sum += sizeof freeMultiWindowBuckets + freeMultiWindowBuckets.capacity() * sizeof(WindowIndex);
    return sum;
}

void MultiWindowManager::makeDoubleEntry(WindowIndexEntry& singleEntry, WindowIndex secondIndex) {
    size_t doubleBucketIdx = getFreeDoubleBucket();
    auto& doubleBucket = doubleWindowBuckets[doubleBucketIdx];
    doubleBucket[0] = singleEntry.getValue();
    doubleBucket[1] = secondIndex;
    singleEntry.setDoubleWindowIndex(doubleBucketIdx);
}

void MultiWindowManager::makeQuadrupleEntry(WindowIndexEntry& baseEntry, WindowIndex thirdIndex) {
    size_t quadBucketIdx = getFreeQuadrupleBucket();
    auto& quadBucket = quadrupleWindowBuckets[quadBucketIdx];
    freeDoubleWindowBuckets.push_back(baseEntry.getValue()); // mark old double entry as free
    quadBucket[0] = doubleWindowBuckets[baseEntry.getValue()][0];
    quadBucket[1] = doubleWindowBuckets[baseEntry.getValue()][1];
    quadBucket[2] = thirdIndex;
    quadBucket[3] = WindowIndexEntry::EMPTY;
    baseEntry.setQuadrupleWindowIndex(quadBucketIdx);
}

void MultiWindowManager::insertToQuadrupleEntry(WindowIndexEntry& baseEntry, WindowIndex nextIndex) {
    if (quadrupleWindowBuckets[baseEntry.getValue()][3] == WindowIndexEntry::EMPTY) {
        quadrupleWindowBuckets[baseEntry.getValue()][3] = nextIndex;
    } else {
        makeMultiEntry(baseEntry, nextIndex);
    }
}

void MultiWindowManager::makeMultiEntry(WindowIndexEntry& baseEntry, WindowIndex fifthIndex) {
    size_t multiBucketIdx = getFreeMultiBucket(); // the new entry
    std::vector<WindowIndex>& multiBucket = multiWindowBuckets[multiBucketIdx];
    multiBucket.reserve(8);// initial size is 8 (so it can contain twice as many as a quadruple bucket)
    freeQuadrupleWindowBuckets.push_back(baseEntry.getValue()); // mark old double entry as free
    for (size_t i = 0; i < 4; i++) {
        multiBucket.push_back(quadrupleWindowBuckets[baseEntry.getValue()][i]);
    }
    multiBucket.push_back(fifthIndex);
    baseEntry.setMultiWindowIndex(multiBucketIdx);
}

void MultiWindowManager::insertToMultiEntry(WindowIndexEntry& baseEntry, WindowIndex nextIndex) {
    multiWindowBuckets[baseEntry.getValue()].push_back(nextIndex);
}

size_t MultiWindowManager::getFreeDoubleBucket() {
    if (freeDoubleWindowBuckets.size()) {
        // there is a free double window bucket
        size_t idx = freeDoubleWindowBuckets.back();
        freeDoubleWindowBuckets.pop_back();
        return idx;
    } else {
        doubleWindowBuckets.emplace_back();
        return doubleWindowBuckets.size() - 1;
    }
}

size_t MultiWindowManager::getFreeQuadrupleBucket() {
    if (freeQuadrupleWindowBuckets.size()) {
        // there is a free double window bucket
        size_t idx = freeQuadrupleWindowBuckets.back();
        freeQuadrupleWindowBuckets.pop_back();
        return idx;
    } else {
        quadrupleWindowBuckets.emplace_back();
        return quadrupleWindowBuckets.size() - 1;
    }
}

size_t MultiWindowManager::getFreeMultiBucket() {
    if (freeMultiWindowBuckets.size()) {
        // there is a free double window bucket
        size_t idx = freeMultiWindowBuckets.back();
        freeMultiWindowBuckets.pop_back();
        return idx;
    } else {
        multiWindowBuckets.emplace_back();
        return multiWindowBuckets.size() - 1;
    }
}

MultiWindowManager::WindowIndexEntry::WindowIndexEntry() :
    value(EMPTY) {
}

MultiWindowManager::WindowIndexEntry::WindowIndexEntry(WindowIndex windowIndex):
    value(windowIndex & VALUE_MASK) {
}

bool MultiWindowManager::WindowIndexEntry::isEmpty() const {
    return value == EMPTY;
}

bool MultiWindowManager::WindowIndexEntry::isSingleEntry() const {
    return (value & META_MASK) == 0 /*&& value != EMPTY*/;
}

bool MultiWindowManager::WindowIndexEntry::isDoubleWindow() const {
    return (value & META_MASK) == DOUBLE_WINDOW;
}

bool MultiWindowManager::WindowIndexEntry::isQuadrupleWindow() const {
    return (value & META_MASK) == QUADRUPLE_WINDOW;
}

bool MultiWindowManager::WindowIndexEntry::isMultiWindow() const {
    return (value & META_MASK) == MULTI_WINDOW;
}

WindowIndex MultiWindowManager::WindowIndexEntry::getValue() const {
    return value & VALUE_MASK;
}

void MultiWindowManager::WindowIndexEntry::setWindowIndex(WindowIndex windowIndex) {
    value = windowIndex & VALUE_MASK;
}

void MultiWindowManager::WindowIndexEntry::setDoubleWindowIndex(WindowIndex doubleWindowIndex) {
    value = (doubleWindowIndex & VALUE_MASK) | DOUBLE_WINDOW;
}

void MultiWindowManager::WindowIndexEntry::setQuadrupleWindowIndex(WindowIndex quadrupleWindowIndex) {
    value = (quadrupleWindowIndex & VALUE_MASK) | QUADRUPLE_WINDOW;
}

void MultiWindowManager::WindowIndexEntry::setMultiWindowIndex(WindowIndex multiWindowIndex) {
    value = (multiWindowIndex & VALUE_MASK) | MULTI_WINDOW;
}


#endif // PG583_MAP_MULTIWINDOWMANAGER_H

#ifndef PG583_MAP_HASHDATASTRUCTURE_H
#define PG583_MAP_HASHDATASTRUCTURE_H

#include <seqan/basic.h>

#include <cstdint>
#include <cstddef>

typedef uint32_t WindowIndex;

/**
 * Base class for all data structures that are used in LSH instances
 */
template <class TBandHash = uint32_t>
class HashDataStructure {
public:
    using BandHash = TBandHash;
    /**
     * @brief Adds a new bandHash windowsIndex pair to the hash table
     * @param bandHash band hash value
     * @param windowIndex index of the winodw
     */
    void add(BandHash bandHash, WindowIndex windowIndex) {
        seqan::ignoreUnusedVariableWarning(bandHash);
        seqan::ignoreUnusedVariableWarning(windowIndex);
    }

    /**
     * @brief Finds all window indices with the specified band hash value
     * @param bandHash band hash value
     * @param maximumOfReturnedWindows The maximal number of window indices that is returned
     * @param operation for each window index wi that was found the function operation(wi) is called
     * @details If the number of window indices stored for the specified band hash value is bigger than maximumOfReturnedWindows,
     *      then the function operation() is not called.
     */
    template <class Function>
    void find(BandHash bandHash, size_t maximumOfReturnedWindows, Function operation) {
        seqan::ignoreUnusedVariableWarning(bandHash);
        seqan::ignoreUnusedVariableWarning(operation);
        seqan::ignoreUnusedVariableWarning(maximumOfReturnedWindows);
    }

    /**
     * @brief Finds all window indices with the specified band hash value
     * @param bandHash band hash value
     * @param maximumOfReturnedWindows The maximal number of window indices that is returned
     * @return two pointers ("begin" and "end") pointing to the first window index
     * @details If the number of window indices stored for the specified band hash value is bigger than maximumOfReturnedWindows,
     *      then an empty pointer pair is returned.
     */
    std::pair<const WindowIndex*, const WindowIndex*> findIterators(BandHash bandHash, size_t maximumOfReturnedWindows) {
        seqan::ignoreUnusedVariableWarning(bandHash);
        seqan::ignoreUnusedVariableWarning(maximumOfReturnedWindows);
        return std::pair<const WindowIndex*, const WindowIndex*>(NULL, NULL);
    }

    /**
     * @brief Removes a bandHash-windowIndex-Pair from the band hash table
     * @details DOES NOT WORK ANY MORE!
     */
    void remove(BandHash bandHash, WindowIndex windowIndex) {
        seqan::ignoreUnusedVariableWarning(bandHash);
        seqan::ignoreUnusedVariableWarning(windowIndex);
    }

    /**
     * @brief Clears the hash table and sets its size to the specified value
     * @param initialSize the new size of the hash table
     */
    void clearAndSetSize(size_t initialSize) {
        seqan::ignoreUnusedVariableWarning(initialSize);
    }

    /**
     * @brief current memory usage for this data structure
     * @return an approximation for the number of bytes used by this data structure
     */
    size_t memoryUsage() const {
        return 0;
    }

    /**
     * @brief This method is called after all values have been inserted in the data structure and before build().
     */
    void sort() {}

    /**
     * @brief This method is called after all values have been inserted in the data structure.
     * This is necessary for the SuperRank data structure, that can not be modified dynamically.
     */
    void build() {}

    /**
     * @brief Prints some statistics to std::cout
     */
    void printHashTableStatistics() {}
};

#endif // PG583_MAP_HASHDATASTRUCTURE_H

#ifndef PG583_MAP_SUPERRANK_H
#define PG583_MAP_SUPERRANK_H

#include "HashDataStructure.h"
#include "MultiWindowManager.h"

#include <seqan/basic.h>
#include <seqan/misc/misc_bit_twiddling.h>
#include <sstream>

template <size_t BITS>
struct uint_t {};
template<> struct uint_t<1> { typedef uint8_t Type; };
template<> struct uint_t<2> { typedef uint8_t Type; };
template<> struct uint_t<4> { typedef uint8_t Type; };
template<> struct uint_t<8> { typedef uint8_t Type; };
template<> struct uint_t<16> { typedef uint16_t Type; };
template<> struct uint_t<32> { typedef uint32_t Type; };
template<> struct uint_t<64> { typedef uint64_t Type; };

// Retrieves the smallest unsigned int type that holds at least BITS many bits.
template <unsigned BITS>
using SmallestUnsignedType = typename uint_t<seqan::Power<2, seqan::Log2<BITS>::VALUE>::VALUE>::Type;

template <class TBandHash = uint32_t, unsigned SUPER_BLOCK_SIZE = 64, unsigned BLOCK_SIZE = 64>
class SuperRank : public HashDataStructure<TBandHash> {
public:
    using typename HashDataStructure<TBandHash>::BandHash;

    SuperRank(size_t windowCount);

    template <class Function>
    void find(BandHash bandHash, size_t maximumOfReturnedWindows, Function operation);

    std::pair<const WindowIndex*, const WindowIndex*> findIterators(BandHash bandHash, size_t maximumOfReturnedWindows);

    size_t memoryUsage() const;

    void add(const BandHash bandHash, const WindowIndex windowIndex);

    void sort();
    void build();

    static constexpr
    size_t superBlockCount = 1 + std::numeric_limits<BandHash>::max() / (SUPER_BLOCK_SIZE * BLOCK_SIZE);

private:
    friend class LshIO;

    size_t uniqueBandHashCount;
    size_t blockCount;

    // This vector buffers all input data until build() is called.
    // Its memory is reused for bandHashEntryList.
    std::vector<std::pair<BandHash, WindowIndex>> bandHashBuffer;
    MultiWindowManager::WindowIndexEntry *bandHashEntryList;

    static_assert(sizeof bandHashEntryList[0] <= sizeof bandHashBuffer[0],
                  "Cannot reuse memory if buffer entries are smaller than final band hash entries!");

    // Layer 1: Bit vector for all existing band hashes (2^32) -> (2^32)/64 elements (super blocks) in vector
    typedef SmallestUnsignedType<SUPER_BLOCK_SIZE> SuperBlockType;
    std::vector<SuperBlockType> blockBitVector;
    std::vector<uint32_t> listBlockPositions;

    // Layer 0: Bit vector for all 8-bit blocks in 1. layer -> (2^32)/(64*8) elements (block) in vector
    typedef SmallestUnsignedType<BLOCK_SIZE> BlockType;
    std::vector<BlockType> bandHashBitVector;
    std::vector<uint32_t> listBandHashPositions;

    MultiWindowManager windowManager;

    MultiWindowManager::WindowIndexEntry* findBucket(BandHash position);
    void shrinkToFit();

public:
    size_t getUniqueBandHashCount() const {
        return uniqueBandHashCount;
    }
    size_t getBlockCount() const {
        return blockCount;
    }

    class BandHashIterator {
        const SuperRank *superRank;
        BandHash superBlockIndex = 0;
        SuperBlockType superBlockOffset = 0;
        BandHash blockIndex = 0;
        BlockType blockOffset = 0;
        bool proceedToValidBlockOffset() {
            while (blockOffset < BLOCK_SIZE) {
                if ((superRank->bandHashBitVector[blockIndex] >> ((BLOCK_SIZE - 1) - blockOffset)) & 1) {
                    return true;
                }
                ++blockOffset;
            }
            return false;
        }
        bool proceedToValidSuperBlockOffset() {
            while (superBlockOffset < SUPER_BLOCK_SIZE) {
                if ((superRank->blockBitVector[superBlockIndex] >> ((SUPER_BLOCK_SIZE - 1) - superBlockOffset)) & 1) {
                    proceedToValidBlockOffset();
                    return true;
                }
                ++superBlockOffset;
            }
            return false;
        }
        bool proceedToValidSuperBlockIndex() {
            while (superBlockIndex < SuperRank::superBlockCount) {
                if (superRank->blockBitVector[superBlockIndex] != 0) {
                    proceedToValidSuperBlockOffset();
                    return true;
                }
                ++superBlockIndex;
            }
            return false;
        }
    public:
        BandHashIterator(const SuperRank &superRank) : superRank(&superRank) {
        }
        static BandHashIterator getBegin(const SuperRank &superRank) {
            BandHashIterator it(superRank);
            it.proceedToValidSuperBlockIndex();
            return it;
        }
        static BandHashIterator getEnd(const SuperRank &superRank) {
            BandHashIterator it(superRank);
            it.superBlockIndex = SuperRank::superBlockCount;
            it.blockIndex = superRank.blockCount;
            return it;
        }
        BandHashIterator& operator++() {
            ++blockOffset;
            if (!proceedToValidBlockOffset()) {
                ++blockIndex;
                blockOffset = 0;
                ++superBlockOffset;
                if (!proceedToValidSuperBlockOffset()) {
                    ++superBlockIndex;
                    superBlockOffset = 0;
                    blockOffset = 0;
                    proceedToValidSuperBlockIndex();
                }
            }
            return *this;
        }
        BandHash operator*() const {
            return (((superBlockIndex * SUPER_BLOCK_SIZE) + superBlockOffset) * BLOCK_SIZE) + blockOffset;
        }
        bool operator==(const BandHashIterator &rhs) const {
            return rhs.superRank == superRank && (
                        (rhs.superBlockIndex == superBlockIndex && rhs.superBlockOffset == superBlockOffset
                            && rhs.blockIndex == blockIndex && rhs.blockOffset == blockOffset)
                        || (rhs.blockIndex >= superRank->blockCount && blockIndex >= superRank->blockCount)
                        );
        }
        bool operator!=(const BandHashIterator &rhs) const {
            return !operator==(rhs);
        }

    };
    BandHashIterator begin() const {
        return BandHashIterator::getBegin(*this);
    }
    BandHashIterator end() const {
        return BandHashIterator::getEnd(*this);
    }
};

template <class TBandHash, unsigned SUPER_BLOCK_SIZE, unsigned BLOCK_SIZE>
template <class Function>
void SuperRank<TBandHash, SUPER_BLOCK_SIZE, BLOCK_SIZE>::find(BandHash bandHash, size_t maximumOfReturnedWindows,
                                                             Function operation) {
    MultiWindowManager::WindowIndexEntry* bucket = findBucket(bandHash);
    if (bucket) {
        windowManager.get(*bucket, operation, maximumOfReturnedWindows);
    }
}

template <class TBandHash, unsigned SUPER_BLOCK_SIZE, unsigned BLOCK_SIZE>
std::pair<const WindowIndex*, const WindowIndex*>
SuperRank<TBandHash, SUPER_BLOCK_SIZE, BLOCK_SIZE>::findIterators(BandHash bandHash, size_t maximumOfReturnedWindows) {
    MultiWindowManager::WindowIndexEntry* bucket = findBucket(bandHash);
    if (bucket) {
        return windowManager.get(*bucket, maximumOfReturnedWindows);
    } else {
        return std::pair<const WindowIndex*, const WindowIndex*>(nullptr, nullptr);
    }
}

template <class TBandHash, unsigned SUPER_BLOCK_SIZE, unsigned BLOCK_SIZE>
MultiWindowManager::WindowIndexEntry*
SuperRank<TBandHash, SUPER_BLOCK_SIZE, BLOCK_SIZE>::findBucket(BandHash position) {
    // The super window in Layer 0 is at position / (SUPER_BLOCK_SIZE * BLOCK_SIZE)
    size_t block = position / BLOCK_SIZE;
    size_t blockOffset = position % BLOCK_SIZE;
    size_t superBlockIndex = block / SUPER_BLOCK_SIZE;
    size_t superBlockOffset = block % SUPER_BLOCK_SIZE;
    SuperBlockType blockBits = blockBitVector[superBlockIndex];

    // Check whether there is a 1 in this super window.
    SuperBlockType bitsUpToAndIncludingSuperBlockOffset = (blockBits >> ((SUPER_BLOCK_SIZE - 1) - superBlockOffset) );
    if (bitsUpToAndIncludingSuperBlockOffset & 1) {
        // Block index is stored in listBlockPositions for every entry in blockBitVector.
        // Count how many 1s are before the given position and add them to the position read from listBlockPositions
        // to get the final block index.
        size_t blockIndex = listBlockPositions[superBlockIndex] +
                            seqan::popCount(bitsUpToAndIncludingSuperBlockOffset >> 1);

        BlockType bandHashBits = bandHashBitVector[blockIndex];

        BlockType bitsUpToAndIncludingBlockOffset = (bandHashBits >> ((BLOCK_SIZE - 1) - blockOffset) );
        if (bitsUpToAndIncludingBlockOffset & 1) {
            size_t finalListIndex = listBandHashPositions[blockIndex]
                                    + seqan::popCount(bitsUpToAndIncludingBlockOffset >> 1);
            return &bandHashEntryList[finalListIndex];
        }
    }
    return nullptr;
}

template <class TBandHash, unsigned SUPER_BLOCK_SIZE, unsigned BLOCK_SIZE>
SuperRank<TBandHash, SUPER_BLOCK_SIZE, BLOCK_SIZE>::SuperRank(size_t windowCount) {
    bandHashBuffer.reserve(windowCount);
}

template <class TBandHash, unsigned SUPER_BLOCK_SIZE, unsigned BLOCK_SIZE>
size_t SuperRank<TBandHash, SUPER_BLOCK_SIZE, BLOCK_SIZE>::memoryUsage() const {
    size_t size = 0;
    size += sizeof bandHashEntryList + sizeof uniqueBandHashCount;
    size += sizeof bandHashBuffer + bandHashBuffer.capacity() * sizeof bandHashBuffer[0];
    size += sizeof bandHashBitVector + bandHashBitVector.capacity() * sizeof bandHashBitVector[0];
    size += sizeof listBandHashPositions + listBandHashPositions.capacity() * sizeof listBandHashPositions[0];
    size += sizeof blockBitVector + blockBitVector.capacity() * sizeof blockBitVector[0];
    size += sizeof listBlockPositions + listBlockPositions.capacity() * sizeof listBlockPositions[0];

    size += windowManager.memoryUsage();

    return size;
}

template <class TBandHash, unsigned SUPER_BLOCK_SIZE, unsigned BLOCK_SIZE>
inline void SuperRank<TBandHash, SUPER_BLOCK_SIZE, BLOCK_SIZE>::add(BandHash bandHash, WindowIndex windowIndex) {
    bandHashBuffer.emplace_back(bandHash, windowIndex);
}

template <class TBandHash, unsigned SUPER_BLOCK_SIZE, unsigned BLOCK_SIZE>
void SuperRank<TBandHash, SUPER_BLOCK_SIZE, BLOCK_SIZE>::sort() {
    // SuperRank can only be built once.
    if (!blockBitVector.empty() || bandHashBuffer.empty()) {
        return;
    }
    std::sort(bandHashBuffer.begin(), bandHashBuffer.end(),
              [](const std::pair<BandHash, WindowIndex> &a,
                 const std::pair<BandHash, WindowIndex> &b) {
                    return a.first < b.first || (a.first == b.first && a.second < b.second); });

    uniqueBandHashCount = 0;
    blockCount = 0;
    BandHash previousBandHash = bandHashBuffer[0].first + 1;
    BandHash previousBlock = std::numeric_limits<BandHash>::max();
    // TODO: Through collision counting we should be able to preallocate all memory for multi window buckets,
    //       thus evading the need for deques and free*buckets etc..
    std::array<size_t, 6> bandHashCollisions;
    size_t collisionCount = 0;
    for (const std::pair<BandHash, WindowIndex> &entry : bandHashBuffer) {
        const BandHash bandHash = entry.first;
        const size_t block = bandHash / BLOCK_SIZE;

        if (bandHash == previousBandHash) {
            if (collisionCount < bandHashCollisions.size() - 1) {
                --bandHashCollisions[collisionCount];
                ++collisionCount;
            }
        } else {
            previousBandHash = bandHash;
            collisionCount = 0;
            ++uniqueBandHashCount;
            if (block != previousBlock) {
                previousBlock = block;
                ++blockCount;
            }
        }
        ++bandHashCollisions[collisionCount];
    }
}

template <class TBandHash, unsigned SUPER_BLOCK_SIZE, unsigned BLOCK_SIZE>
void SuperRank<TBandHash, SUPER_BLOCK_SIZE, BLOCK_SIZE>::build() {
    // SuperRank can only be built once.
    if (!blockBitVector.empty()) {
        return;
    }
    // The super block count is constant, so we can initialize it with zeros.
    blockBitVector = std::vector<SuperBlockType>(superBlockCount, 0);
    listBlockPositions = std::vector<uint32_t>(superBlockCount);

    // Buffer must not be empty, because position 0 is always accessed.
    if (bandHashBuffer.empty()) {
        return;
    }

    // sort() should be called before build(). If it has not been called already, do it now.
    if (uniqueBandHashCount == 0) {
        sort();
    }

    bandHashBitVector = std::vector<BlockType>(blockCount, 0);
    listBandHashPositions = std::vector<uint32_t>(blockCount);

    // This ensures that the first previousBandHash is always different from the actual element.
    BandHash previousBandHash = bandHashBuffer[0].first + 1;
    size_t uniqueBandHashIndex = -1;
    size_t previousBlock = std::numeric_limits<size_t>::max();
    size_t blockIndex = -1;
    size_t previousSuperBlockIndex = 0;

    bandHashEntryList = reinterpret_cast<decltype(bandHashEntryList)>(&bandHashBuffer[0]);
    for (const std::pair<BandHash, WindowIndex> entry : bandHashBuffer) {
        const BandHash bandHash = entry.first;
        const WindowIndex windowIndex = entry.second;

        if (bandHash != previousBandHash) {
            previousBandHash = bandHash;
            ++uniqueBandHashIndex;
            // Add a new single entry
            bandHashEntryList[uniqueBandHashIndex] = windowIndex;
        } else {
            // If the band hash is the same, add the WindowIndex to the existing entry.
            windowManager.add(bandHashEntryList[uniqueBandHashIndex], windowIndex);
            continue;
        }

        size_t block = bandHash / BLOCK_SIZE;
        size_t blockOffset = bandHash % BLOCK_SIZE;
        size_t superBlockIndex = block / SUPER_BLOCK_SIZE;
        size_t superBlockOffset = block % SUPER_BLOCK_SIZE;

        if (block != previousBlock) {
            previousBlock = block;
            ++blockIndex;
            // listBandHashPositions at the current window index should hold the amount of bandHashes
            // that are stored in the bandHashEntryList until now (i.e. uniqueBandHashIndex).
            listBandHashPositions[blockIndex] = uniqueBandHashIndex;
        }

        if (superBlockIndex != previousSuperBlockIndex) {
            previousSuperBlockIndex = superBlockIndex;
            listBlockPositions[superBlockIndex] = blockIndex;
        }

        blockBitVector[superBlockIndex] |= SuperBlockType(1) << ((SUPER_BLOCK_SIZE - 1) - superBlockOffset);
        bandHashBitVector[blockIndex] |= BlockType(1) << ((BLOCK_SIZE - 1) - blockOffset);
    }

    // Set allocated out-of-bounds entries to EMPTY. Unnecessary, but in case of off-by-one errors better than garbage.
    size_t sizeOfEntryType = sizeof bandHashEntryList[0];
    size_t sizeOfBufferType = sizeof bandHashBuffer[0];
    size_t usedBytes = uniqueBandHashCount* sizeOfEntryType;
    size_t excessBytes = usedBytes % sizeOfBufferType;
    for (size_t i = uniqueBandHashCount; (i + 1) * sizeOfEntryType <= (usedBytes + excessBytes); ++i) {
        bandHashEntryList[i] = MultiWindowManager::WindowIndexEntry::EMPTY;
    }

    // windowManager.sort(); // should not be neccessary since windows are already sorted.

    shrinkToFit();
}

template <class TBandHash, unsigned SUPER_BLOCK_SIZE, unsigned BLOCK_SIZE>
void SuperRank<TBandHash, SUPER_BLOCK_SIZE, BLOCK_SIZE>::shrinkToFit() {
//    std::ostringstream oss; oss << "_SuperRank::shrinkToFit(): " << omp_get_thread_num();

    size_t sizeOfEntryType = sizeof bandHashEntryList[0];
    size_t sizeOfBufferType = sizeof bandHashBuffer[0];
    size_t usedBytes = uniqueBandHashCount * sizeOfEntryType;
    size_t excessBytes = usedBytes % sizeOfBufferType;
    // Free the surplus memory allocated by bandHashBuffer.
    bandHashBuffer.resize((usedBytes + excessBytes) / sizeOfBufferType);
//    std::cout << (oss.str() + " begins with realloc (1): " + std::to_string(bandHashBuffer.size() * sizeof(bandHashBuffer[0]) / 1000. / 1000. ) + "\n") << std::flush;
    bandHashBuffer.shrink_to_fit();
//    std::cout << (oss.str() + " finished realloc (1)" + std::to_string(bandHashBuffer.size() * sizeof(bandHashBuffer[0]) / 1000. / 1000. ) + "\n") << std::flush;
    bandHashEntryList = reinterpret_cast<decltype(bandHashEntryList)>(&bandHashBuffer[0]);

//    std::cout << (oss.str() + " begins with realloc (2)" + std::to_string(windowManager.memoryUsage() / 1000. / 1000. ) + "\n") << std::flush;
    windowManager.shrinkToFit();
//    std::cout << (oss.str() + " finished realloc (2)" + std::to_string(windowManager.memoryUsage() / 1000. / 1000. ) + "\n") << std::flush;
}

#endif // PG583_MAP_SUPERRANK_H

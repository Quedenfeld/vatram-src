#ifndef PG583_MAP_LSHSIMHASH_H
#define PG583_MAP_LSHSIMHASH_H

#include "map/CollisionFreeBHT.h"
#include "map/SuperRank.h"
#include "map/QGramHashFunctions.h"
#include "Chromosome.h"
#include "parallel.h"
#include "Logger.h"

#include <seqan/modifier.h>
#include <seqan/misc/misc_bit_twiddling.h>

#include <vector>
#include <random>F
#include <ctime>
#include <cstdint>
#include <cmath>
#include <numeric>
#include <limits>
#include <functional>
#include <type_traits>
#include <utility>
#include <cstddef>
#include <unordered_map>

using std::size_t;

struct LshSimHashMapOptions {
    const static size_t DEFAULT_MIN_COUNT = 2;
    const static size_t DEFAULT_MAX_SELECT = 3;
    constexpr static double DEFAULT_NEXT_FACTOR = 1.75;
    constexpr static double DEFAULT_CONTRACT_BAND_MULTIPLICATOR = 0.3;
    constexpr static double DEFAULT_EXTEND_BAND_MULTIPLICATOR = 0.3;
    const static size_t DEFAULT_EXTEND_NUMBER = 48;
    const static size_t DEFAULT_MAXIMUM_OF_RETURNED_WINDOWS = 100;

    size_t minCount        = DEFAULT_MIN_COUNT; // findInterval() returns only intervals with hits in at least minCound band hash tables for one read.
    size_t maxSelect       = DEFAULT_MAX_SELECT; // findInterval() returns at most maxSelect different intervals for one read
    double nextFactor      = DEFAULT_NEXT_FACTOR; // in findInteral() the number of hits for the second, third (and so one) window sequence must not be smaller than the first window sequence by a factor of nextFactor
    double contractBandMultiplicator = DEFAULT_CONTRACT_BAND_MULTIPLICATOR; // for the interval calculation the number of band hash hits is multiplied with this attribute
    double extendBandMultiplicator   = DEFAULT_EXTEND_BAND_MULTIPLICATOR;
    size_t extendNumber              = DEFAULT_EXTEND_NUMBER; // a single window is extended by this number of bases in findInterval()
    /**
     * @brief Maximum number of window indices returned by HashDataStructure::find()
     * @details  If there are more window indices stored for a specific band hash value than the value of this variable,
     *      then no window indices are returned.
     */
    size_t maximumOfReturnedWindows  = DEFAULT_MAXIMUM_OF_RETURNED_WINDOWS;
};

/** If this macro is defined, all methods of the LSH class are virtual */
//#define LSH_MAKE_VIRTUAL

#ifdef LSH_MAKE_VIRTUAL
#define LSH_VIRTUAL virtual
#else
#define LSH_VIRTUAL
#endif

/**
 * @brief Generic LSH class
 * @details If the macro LSH_MAKE_VIRTUAL is defined, several methods are declared as virtual.
 * @param TBandHash BandHash-Type, usually a 32bit (or 64 bit) unsigned integer
 * @param TQGramHash QGramHash-Type, usually a 32bit (or 64 bit) unsigned integer
 * @param BHT band hash table, for example the CollisionFreeBHT or the BandHashTable. The class has to implement
 *      add(BandHash_, WindowIndex), find(BandHash_, std::vector<WindowIndex>&) and a constructor taking the intial size
 * @param QGramHashFunction, a class template that overloads the operator() taking two iterator objects (called begin and end)
 *      pointing to a part of a Dna- or Dna5-String. The function calculates a hash value for the q-gram
 *      between begin (inclusive) and end (exclusive). See DefaultQGramHashFunction for an example
 */
template <class TBandHash,
          class TQGramHash,
          class BHT,
          class QGramHashFunction>
class LSHSimHash_Generic {
public:
    typedef uint32_t ChromosomeIndex;
    typedef uint32_t WindowPosition;
    typedef uint32_t WindowIndex;
    typedef TQGramHash HashFunction;  // sollte kleiner gleich decltype(rngHashFunctions()) sein
    typedef TQGramHash QGramHash;     // sollte gleich HashFunction sein
    typedef TQGramHash SignatureHash; // sollte gleich QGramHash und HashFunction sein
    typedef TBandHash BandHash;

    /**
     * @brief Represents a window of the genome
     * @details This struct stores a pointer to the respective chromosome and the begin position of the window (aka document)
     */
    struct ReferenceWindow {
        ChromosomeIndex chromosomeIndex;
        WindowPosition beginPosition;
        ReferenceWindow(ChromosomeIndex chromosomeIndex, WindowPosition beginPosition) :
            chromosomeIndex(chromosomeIndex),
            beginPosition(beginPosition) {
        }
    };

    /**
     * @brief Represents a possible location for a read
     * @details The aligner can calculate the alignment between both given positions. There is no reason to expand the specified interval
     */
    struct Result {
        ChromosomeIndex chromosome;
        WindowPosition startPosition;
        WindowPosition endPosition;
        bool inverted; /// denotes that the reversed complement of a read was found
    };

    const static size_t DEFAULT_WINDOW_SIZE = 140;
    const static size_t DEFAULT_WINDOW_OFFSET = 125;
    const static size_t DEFAULT_QGRAM_SIZE = 16;
    const static size_t DEFAULT_SIGNATURE_LENGTH = 40;
    const static size_t DEFAULT_BAND_SIZE = 1;
    const static size_t DEFAULT_LIMIT = 3;
    const static size_t DEFAULT_LIMIT_SKIP = 8;
    const static size_t DEFAULT_PERMUTATION_COUNT = 64;
    const static size_t DEFAULT_PERMUTATION_SKIP = 0;
    const static size_t DEFAULT_MAX_HAMMING_DISTANCE = 3;
    

    size_t windowSize      = DEFAULT_WINDOW_SIZE;
    size_t windowOffset    = DEFAULT_WINDOW_OFFSET;
    size_t qGramSize       = DEFAULT_QGRAM_SIZE;
//    size_t signatureLength = DEFAULT_SIGNATURE_LENGTH; // In other words: hash function count
    size_t bandSize        = DEFAULT_BAND_SIZE; // For Locality-Sensitive Hashing
    size_t limit           = DEFAULT_LIMIT; // The number of combinations that is at most added
    size_t limit_skip      = DEFAULT_LIMIT_SKIP; // If the number of combinations in a q-gram is bigger than this value, no combination is added
    // SimHash:
    size_t permutationCount = DEFAULT_PERMUTATION_COUNT;
    // How many bits should be skipped between two cyclic bit shifts
    size_t permutationSkip = DEFAULT_PERMUTATION_SKIP;
    size_t maxHammingDistance = DEFAULT_MAX_HAMMING_DISTANCE;

    size_t &signatureLength = permutationCount;
    
    const unsigned int dimensions = sizeof(BandHash) * 8;

    // TODO: This member is actually not used, apart from in the test / experiment classes.
    // The sound method to resolve this is to split LSH into LshIndex and LshMap / LshMapper or the like (which should
    // hold the find methods). LshMapOptions could then reside in the LshMap* class.
    LshMapOptions _mapOptions;

    // This random generator is used to select some q-grams if there are more qgrams than "limit".
    // The generator should be fast, so don't use std::mt19937
    std::minstd_rand::result_type initialRandomSeed;
    std::vector<std::minstd_rand> rngLimits;

    std::mt19937_64 rngHashFunctions;

    std::vector<HashFunction> hashFunctions;
    ChromosomeReferences chromosomes;
    std::vector<ReferenceWindow> referenceWindows;

//    // Our New custom data structure
//    std::vector<BHT> bandHashTables;
    std::vector<std::vector<std::pair<BandHash, WindowIndex>>> bandHashTables;

    QGramHashFunction qGramHashObject;
    
    // The vector for SimHashing
    std::vector<int> bitCounter;


    /**
     * @brief Default constructor. Uses the current time as seed.
     */
    LSHSimHash_Generic(QGramHashFunction qghf = QGramHashFunction());
    LSHSimHash_Generic(unsigned long long initialRandomSeed, QGramHashFunction qghf = QGramHashFunction());



    /**
     * @brief initialize the hash functions needed by LSH and sets
     * @param genomeSize a estimation (respectively upper value) for genome size
     */
    void initialize(size_t genomeSize);

    void buildIndex();


    /**
     * @brief Clears all data structures such that the state of the lsh object is the same as before calling initialize.
     */
    void clear();


    /**
     * @brief Adds all chromosomes between begin_it and end_it.
     */
    template <class Iterator>
    void addGenome(Iterator begin_it, Iterator end_it);


    /**
     * @brief
     * @details Creates overlapping windows of the chromosome (using the attributes windowSize and windowOffset).
     *      Each window is stored as a Reference object and is added to the hash table via addReference().
     * @param chromosome
     */
    void addChromosome(const Chromosome &chromosome);

    void addReferenceWindows(ChromosomeIndex chromosomeIndex);
    void createReferenceWindowBandHashes(WindowIndex firstWindowIndex, WindowIndex lastWindowIndex);
    void createWindowBandHashes(ChromosomeIndex chromosomeIndex, WindowPosition beginPosition, std::vector<BandHash> &out_bandHashes);

    /**
     * @brief Adds a pair consisting of a band hash value and a window index to the specified band hash table
     * @param bandIndex the index of the band hash function respecively band hash table
     * @param bucketHash the band hash value
     * @param referenceWindowIndex the window index
     */
    void addReferenceWindowHash(size_t bandIndex, BandHash bucketHash, WindowIndex referenceWindowIndex);

    WindowIndex addReferenceWindow(ChromosomeIndex chromosomeIndex, WindowPosition beginPosition,
                                   const std::vector<BandHash> &bandHashes);

    /**
     * @brief Finds the read and its reverse complement in the genome.
     * @param out_referenceIDs Positions where the read was found.
     * @param out_referenceIDs_revComplement Positions where the reverse complement of the read was found.     */
    LSH_VIRTUAL void findRead(const ReadString &read, const ReadString &reversedRead,
                              const LshMapOptions &mapOptions,
                              std::vector<WindowIndex> &out_referenceWindowIndices,
                              std::vector<WindowIndex> &out_referenceWindowIndices_revComplement);

    /**
     * @brief Finds the read and its reverse complement in the genome.
     * @param read a reference to the read string
     * @param reversedRead a reference to the reverse complement of the read string
     * @param out_result The intervals where the read was found
     */
    LSH_VIRTUAL void findInterval(const ReadString &read, const ReadString &reversedRead,
                                  const LshMapOptions &mapOptions, std::vector<Result>& out_result);

    /**
     * @brief creates the q-gram hashes of a DnaString or Dna5String
     * @param window_begin
     * @param window_end
     * @param out_hashes After execution of this function, this variable contains the q-grams (respectively their hash values)
     */
    template <class Iterator>
    void createQGramHashes(Iterator window_begin, Iterator window_end, std::vector<QGramHash> &out_hashes);

    /**
     * @brief creates the q-gram hashes of a IupacString
     * @param window_begin
     * @param window_end
     * @param out_hashes After execution of this function, this variable contains the q-grams (respectively their hash values)
     */
    template <class Iterator>
    void createQGramHashes_iupac(Iterator window_begin, Iterator window_end, std::vector<QGramHash> &out_hashes);

    template <class Iterator>
    void createSignatures(Iterator qGramHashes_begin, Iterator qGramHashes_end, std::vector<SignatureHash> &out_signatures);

    template <class Iterator>
    void createBandHashes(Iterator signature_begin, Iterator signature_end, std::vector<BandHash> &out_bandHashes);

    template <class Iterator> // an iterator pointing to seqan::Dna or seqan::Dna5
    QGramHash qGramHashFunction(Iterator qgram_begin, Iterator qgram_end);

    template <class Iterator>
    BandHash bandHashFunction(Iterator signature_begin, Iterator signature_end);

    // This function is virtual because it is overloaded in the sub project "lsh" to do some experiments
    // If you measure runtime, you can remove the "virtual", however don't commit this chance or tell me (Jens), if you commit it.
    LSH_VIRTUAL BandHash bandHashFunction(typename std::vector<SignatureHash>::iterator sig_begin,
                                          typename std::vector<SignatureHash>::iterator sig_end);

    /** Find all variant combinations of the substring "iupacString[beginPos...beginPos+qGramSize]"
     *  and execute the function operation on the founds.
     *  @param curQGram arbitrary space of length qGramSize
     *  @param curVariant arbitrary space of length qGramSize+1
     *  @param operation a object that overloads operator()(Iterator, Iterator) with Iterator = std::vector<seqan::DNA>::iterator
     */
    template <class Iterator, class Function>
    void findCombinationsOfQGram(Iterator iupacQgram_begin, Function &operation);

// DEPRECATED
    /**
     * @brief Finds the given string in the genome. The reverse complement is not taken into account
     * @param read_begin
     * @param read_end
     * @param out_referenceIDs Positions where the read was found.
     * @param out_referenceIDs_revComplement Positions where the reverse complement of the read was found.     */
    template <class Iterator>
    void findReadDirected(Iterator read_begin, Iterator read_end, std::vector<WindowIndex> &out_referenceWindowIndices);

    /**
     * @brief removes duplicate window indices
     * @param windowIndices Input and output parameter
     */
    LSH_VIRTUAL void filterResults(std::vector<WindowIndex>& windowIndices);
// -----------------------------
    template <class Function>
    void findReadInPermutations
        (BandHash bandHash, size_t maximumOfReturnedWindows, Function operation);


    template <class Iterator>
    void calcSimHash (Iterator qGramHashes_begin, Iterator qGramHashes_end, std::vector<BandHash> &out_bandHashes);
    /**
     * @brief current memory usage for this data structure
     * @return an approximation for the number of bytes used by this data structure
     */
    LSH_VIRTUAL size_t memoryUsage() const;

    void printHashTableStatistics();


//protected:
    /**
     * @brief This window index offset denotes that the reversed complement
     */
    const static WindowIndex INVERTED_WI_OFFSET = 1 << 31;

    /**
     * @brief A sequence of window indices
     */
    struct WindowSequence {
        WindowIndex begin;
        WindowIndex end;
        unsigned count; /// the sum of the hits in the band hash tables for all window indices
    };

    /**
     * @brief Calculates the band hashes of a read
     * @param read_begin
     * @param read_end
     * @param out_bandHashes
     */
    template <class Iterator>
    void calcReadBandHashes(Iterator read_begin, Iterator read_end, std::vector<BandHash>& out_bandHashes);

    /**
     * @brief Finds the read and its reverse complement in the genome.
     * @param read a reference to the read string
     * @param reversedRead a reference to the reverse complement of the read string
     * @return a map with the window indices and the corresponding numbers of band hash tables where the read was found
     * @details The reverse complement is represented by a WindowIndex where INVERTED_WI_OFFSET is added. */
    std::vector<std::pair<WindowIndex, unsigned>> findReadOccurrences(const ReadString &read, const ReadString &reversedRead,
                                                                      const LshMapOptions &mapOptions);

    /**
     * @brief Compresses the window indices where a read was found
     * @param occurrences usually the map returned by findReadOccurrences
     * @details Compresses a sequence of window indices to one WindowSequence object
     * @return a vector with window sequences     */
    std::vector<WindowSequence> compressOccurrences(const std::vector<std::pair<WindowIndex, unsigned>>& occurrences);

};

typedef LSHSimHash_Generic<uint32_t, uint32_t, CollisionFreeBHT<uint32_t>, GappedQGramHashFunction<uint32_t>> LSHSimHash;

// =====================================================================================
// ===== I M P L E M E N T A T I O N S
// =====================================================================================

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::LSHSimHash_Generic(QGramHashFunction qghf) :
    LSHSimHash_Generic(std::time(nullptr), qghf) {
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::LSHSimHash_Generic(unsigned long long initialRandomSeed, QGramHashFunction qghf) :
//    windowSize(125),
//    windowOffset(100),
//    qGramSize(16),
//    signatureLength(32),
//    bandSize(1),
//    limit(3),
//    limit_skip(8),
    initialRandomSeed(initialRandomSeed),
    rngHashFunctions(initialRandomSeed),
    qGramHashObject(qghf) {
    rngLimits.emplace_back(initialRandomSeed);
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::buildIndex() {
    for (auto &bht : bandHashTables) {
        // Sort the permutations for SimHash lookup
        std::sort(bht.begin(), bht.end(),
                  [](std::pair<BandHash, WindowIndex> a, std::pair<BandHash, WindowIndex> b)
        { return a.first < b.first; });
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::initialize(size_t genomeSize){
    // create hash functions
    hashFunctions.clear();
    rngHashFunctions.seed(initialRandomSeed);
    for (size_t i = 0; i < signatureLength; i++) {
        hashFunctions.push_back(rngHashFunctions());
    }
    for (auto &rng : rngLimits) {
        rng.seed(initialRandomSeed);
    }

    // Set initial size of SimHash permutations
    size_t bandHashTableCount = permutationCount;
    bandHashTables.clear();
    bandHashTables.reserve(bandHashTableCount);
    for (size_t i = 0; i < bandHashTableCount; i++) {
        bandHashTables.emplace_back(std::max<size_t>(1, 2.01 * genomeSize / windowOffset));
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::clear() {
    for (auto &rng : rngLimits) {
        rng.seed(initialRandomSeed);
    }

    rngHashFunctions.seed(initialRandomSeed);

    chromosomes.clear();
    referenceWindows.clear();
    bandHashTables.clear();
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::addGenome(Iterator begin_it, Iterator end_it) {
    for (; begin_it != end_it; ++begin_it) {
//        std::cout << "_add" << begin_it->name << "," << seqan::length(begin_it->data) << std::endl;
        addChromosome(*begin_it);
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::addChromosome(const Chromosome &chromosome) {
    if (hashFunctions.size() == 0) {
        // Ihr glaubt gar nicht, wie oft ich schon vergessen hab, initialize() aufzurufen und mich dann über den Seg-Fault gewundert hab...
        std::cout << "hey, you forgot to initialize me" << std::endl;
        throw std::runtime_error("LSH is not initialized");
    }
    size_t chromosomeIndex = chromosomes.size();
    chromosomes.push_back(chromosome);

    const WindowIndex firstWindowIndex = referenceWindows.size();
    addReferenceWindows(chromosomeIndex);
    const WindowIndex lastWindowIndex = referenceWindows.size();
    createReferenceWindowBandHashes(firstWindowIndex, lastWindowIndex);
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::addReferenceWindows(ChromosomeIndex chromosomeIndex) {
    const Chromosome &chromosome = chromosomes[chromosomeIndex];
    if (seqan::length(chromosome.data) >= windowSize) {
        WindowPosition lastWindowPos = seqan::length(chromosome.data) - windowSize + 1;
        for (WindowPosition windowStartPos = 0; windowStartPos < lastWindowPos; windowStartPos += windowOffset) {
            auto windowStartPosIt = seqan::begin(chromosome.data) + windowStartPos;
            auto isOnlyNs = [windowStartPosIt] (WindowPosition first, WindowPosition last ) {
                return std::all_of(windowStartPosIt + first, windowStartPosIt + last, [](ReferenceChar c) { return c == 'N'; });
            };
            if (!isOnlyNs(0, windowOffset) && !isOnlyNs(windowSize - windowOffset, windowSize)) {
                referenceWindows.emplace_back(chromosomeIndex, windowStartPos);
            }
        }
    } else {
        logger.warn("LSH: Length of chromosome ", chromosome.name, " is ", seqan::length(chromosome.data));
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::createReferenceWindowBandHashes(WindowIndex first, WindowIndex last) {
    std::vector<Mutex> bandHashTableMutex(bandHashTables.size());
    size_t chunkSize = 1000; // process chunkSize many windows in one thread
    #pragma omp parallel
    {
        #pragma omp single
        {
            int numThreads = omp_get_num_threads();
            while (int(rngLimits.size()) < numThreads) {
                rngLimits.emplace_back(initialRandomSeed);
            }
        }
        #pragma omp for schedule(dynamic, 1)
        for (WindowIndex chunkStartIndex = first; chunkStartIndex < last; chunkStartIndex += chunkSize) {
            WindowIndex chunkEndIndex = std::min<WindowIndex>(chunkStartIndex + chunkSize, last);
            // In SimHash context this is a vector with permutations of 
            // all the inner BandHash vectors
            std::vector<std::vector<BandHash>> bandHashesPerWindow;
            bandHashesPerWindow.reserve(chunkEndIndex - chunkStartIndex);
            for (WindowIndex windowIndex = chunkStartIndex; windowIndex < chunkEndIndex; ++windowIndex) {
                bandHashesPerWindow.emplace_back();
                createWindowBandHashes(referenceWindows[windowIndex].chromosomeIndex,
                                       referenceWindows[windowIndex].beginPosition, bandHashesPerWindow.back());
            }
            for (size_t t = 0; t < bandHashTables.size(); ++t) {
                LockGuard lock(bandHashTableMutex[t]);
                for (WindowIndex windowIndex = chunkStartIndex; windowIndex < chunkEndIndex; ++windowIndex) {
                    addReferenceWindowHash(t, bandHashesPerWindow[windowIndex - chunkStartIndex][t], windowIndex);
                }
            }
        }
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::
        calcSimHash
    (Iterator qGramHashes_begin, Iterator qGramHashes_end, std::vector<BandHash> &out_bandHashes) {
    
    // The amount of dimensions in the bitCounter vector equals the amount
    // of bits in the BandHash
//    const unsigned int dimensions = sizeof(BandHash) * 8;
    bitCounter.resize(dimensions, 0);
    
    for (; qGramHashes_begin != qGramHashes_end; ++qGramHashes_begin) {
        // TODO: if less than 64 should be generated, then use different shifting widths
        QGramHash qGramHash = *qGramHashes_begin;
        for (unsigned int bit = 0; bit < dimensions; bit++) {
            bitCounter[bit] += (qGramHash & (uint32_t)1) ? 1 : -1;
            qGramHash >>= 1;
        }
    }
    
    BandHash finalBitVector = 0;
    
    for (unsigned int bit = 0; bit < dimensions; bit++) {
        bitCounter[bit] = bitCounter[bit] > 0;
        finalBitVector |= bitCounter[bit];
        finalBitVector <<= bit <= dimensions;
    }
    
    out_bandHashes.emplace_back(finalBitVector);
    
    for (unsigned int hashFuncIndex = 1; hashFuncIndex < permutationCount; ++hashFuncIndex) {

        // SimHash cyclic bit shift

        // Take the first bit and shift it to the left end
        QGramHash hashFirstBit = (finalBitVector & 1) << ((sizeof(QGramHash) * 8) - 1);
        // Right shift the rest once
        QGramHash hashShifted = finalBitVector >> 1;
        // Add the first bit to the left end of the shifted hash
        out_bandHashes.emplace_back(hashFirstBit | hashShifted);
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::
         createWindowBandHashes
            (ChromosomeIndex chromosomeIndex, WindowPosition beginPosition, std::vector<BandHash> &out_bandHashes) {
//    seqan::String<ReferenceChar> window = seqan::infixWithLength(chromosomes[chromosomeIndex].get().data, beginPosition, windowSize);
    
    // Use the original reference string without variants for SimHashing 
    seqan::String<ReferenceChar> window =
            seqan::infixWithLength(chromosomes[chromosomeIndex].get().noVariantsString, beginPosition, windowSize);
    std::vector<QGramHash> qGramHashes;
    // Alternative windows erstellen


    createQGramHashes_iupac(seqan::begin(window), seqan::end(window), qGramHashes);

    std::vector<SignatureHash> signatures;
    // SimHash
    calcSimHash(qGramHashes.begin(), qGramHashes.end(), out_bandHashes);

//    createSignatures(qGramHashes.begin(), qGramHashes.end(), signatures);
//    createBandHashes(signatures.begin(), signatures.end(), out_bandHashes);
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::
    addReferenceWindowHash(size_t bandIndex, BandHash bucketHash, WindowIndex referenceWindowIndex) {
    bandHashTables[bandIndex].emplace_back(bucketHash, referenceWindowIndex);
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
typename LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::WindowIndex LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::addReferenceWindow(
        ChromosomeIndex chromosomeIndex, WindowPosition beginPosition, const std::vector<BandHash> &bandHashes) {
    WindowIndex windowIndex = referenceWindows.size();
    referenceWindows.emplace_back(chromosomeIndex, beginPosition);

    for (size_t i = 0; i < bandHashes.size(); i++) {
        addReferenceWindowHash(i, bandHashes[i], windowIndex);
    }

    return windowIndex;
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::findRead(const ReadString &read, const ReadString &reversedRead,
                   const LshMapOptions &mapOptions,
                   std::vector<WindowIndex> &out_referenceWindowIndices,
                   std::vector<WindowIndex> &out_referenceWindowIndices_revComplement) {
//    findReadDirected(seqan::begin(read), seqan::end(read), out_referenceWindowIndices);
//    findReadDirected(seqan::begin(reversedRead), seqan::end(reversedRead), out_referenceWindowIndices_revComplement);
    std::vector<std::pair<WindowIndex, unsigned>> occ = findReadOccurrences(read, reversedRead, mapOptions);
    if (occ.size() == 0) {
        return;
    }
    std::vector<WindowSequence> wseqs = compressOccurrences(occ);
    for (WindowSequence& ws: wseqs) {
        bool inverted = false;
        if (ws.begin >= INVERTED_WI_OFFSET) {
            ws.begin -= INVERTED_WI_OFFSET;
            ws.end -= INVERTED_WI_OFFSET;
            inverted = true;
        }
        WindowIndex wi = (ws.end + ws.begin) / 2;
        if ((ws.end + ws.begin) % 2 == 1) { // there is a division rest
            if (occ[wi] < occ[wi+1]) { // ceil is bigger than floor
                wi++; // use ceil
            }
        }
        if (inverted) {
            out_referenceWindowIndices_revComplement.push_back(wi);
        } else {
            out_referenceWindowIndices.push_back(wi);
        }
    }
}

//extern std::unordered_map<unsigned, unsigned> lsh_debug_wsSize;

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::findInterval(const ReadString &read, const ReadString &reversedRead,
                                                                            const LshMapOptions &mapOptions,
                                                                            std::vector<Result>& out_result) {
    std::vector<std::pair<WindowIndex, unsigned>> map = findReadOccurrences(read, reversedRead, mapOptions);
    if (map.size() == 0) {
        return;
    }

    std::vector<WindowSequence> compressed = compressOccurrences(map);
    if (compressed.front().count < mapOptions.minCount) {
        // no relevant window found
        return;// out_result;
    }

    // select interesting windows
    std::vector<WindowSequence> selected;
    selected.push_back(compressed[0]);
    for (size_t i = 1; i < std::min<size_t>(mapOptions.maxSelect, compressed.size()); i++) {
        if (compressed[i].count < mapOptions.minCount) {
            break;
        } else if (compressed[i].count * mapOptions.nextFactor < compressed[0].count) {
            break;
        } else {
            // the next interval is added, if the count value is significant large
            selected.push_back(compressed[i]);
        }
    }


    const double bandNumber = signatureLength / bandSize; // double because we divide by this value

    const double extendMult = mapOptions.extendBandMultiplicator / bandNumber * windowSize;
    const double contractReadOverlappingLength = (seqan::length(read) - qGramSize) / 2.0;
    const double contractMult = mapOptions.contractBandMultiplicator / bandNumber * contractReadOverlappingLength;

    // calculate begin and end where the aligner will search for the read
    for (WindowSequence ws: selected) {
        Result res;
        res.inverted = false;
        if (ws.begin >= INVERTED_WI_OFFSET) {
            ws.begin -= INVERTED_WI_OFFSET;
            ws.end -= INVERTED_WI_OFFSET;
            res.inverted = true;
        }

        res.chromosome = referenceWindows[ws.begin].chromosomeIndex;


//        lsh_debug_wsSize[ws.end - ws.begin + 1]++;

        // count_begin replaces map[ws.begin] if map were an unordered_map
        // count_end replaces map[ws.end] if map were an unordered_map
        unsigned count_begin = std::lower_bound(map.begin(), map.end(), ws.begin, [](std::pair<WindowIndex, unsigned>& a, WindowIndex b){return a.first < b;})->second;
        unsigned count_end = std::lower_bound(map.begin(), map.end(), ws.end, [](std::pair<WindowIndex, unsigned>& a, WindowIndex b){return a.first < b;})->second;

        // switch for the number of windows in the sequence
        int64_t left, right;
        switch(ws.end - ws.begin + 1) {
        case 1: {
            left = referenceWindows[ws.begin].beginPosition - std::max<int64_t>(0, mapOptions.extendNumber - count_begin * extendMult);
            right = referenceWindows[ws.end].beginPosition + windowSize + std::max<int64_t>(0, mapOptions.extendNumber - count_end * extendMult);
            break;
        }
        case 2: {
            left = referenceWindows[ws.end].beginPosition - seqan::length(read) + qGramSize + count_end * contractMult;
            right = referenceWindows[ws.begin].beginPosition + windowSize + seqan::length(read) - (int64_t) qGramSize - count_begin * contractMult;
            break;
        }
        default: {
            left = referenceWindows[ws.begin].beginPosition;
            right = referenceWindows[ws.end].beginPosition + windowSize;
            break;
        }
        }

        res.startPosition = std::max<int64_t>(0, left);
        res.endPosition = std::min<int64_t>(right, seqan::length(chromosomes[res.chromosome].get().noVariantsString));

        // int64_t, because the calculated values can be negative (WindowPosition is unsigned)
        // this heuristic is designed for single windows (we extend the window by extendNumber)
//        int64_t leftExtend = referenceWindows[ws.begin].beginPosition - (int64_t) mapOptions.extendNumber;
//        int64_t rightExtend = referenceWindows[ws.end].beginPosition + windowSize + mapOptions.extendNumber;

//        // this heuristic is designed for window sequences (we look at last window and go left (for the first window analogous))
//        int64_t leftMax = referenceWindows[ws.end].beginPosition - seqan::length(read) + qGramSize + map[ws.end] * bandMultiplicator;
//        int64_t rightMax = referenceWindows[ws.begin].beginPosition + windowSize + seqan::length(read) - (int64_t) qGramSize - (int64_t) map[ws.begin] * bandMultiplicator;

//        res.startPosition = std::max<int64_t>(0, std::max(leftExtend, leftMax));
//        res.endPosition = std::min(rightExtend, rightMax);
        if (res.startPosition > res.endPosition || res.endPosition - res.startPosition + 1 < seqan::length(read)) {
            if (res.startPosition > res.endPosition) {
                std::cout << "___so goes that but not:" << res.startPosition << "," << res.endPosition << std::endl;
            } else {
                std::cout << "___sense - this result makes none" << std::endl;
            }
            std::cout << "ws: " << ws.begin << " - " << ws.end << ", #=" << ws.count << std::endl;
            std::cout << "case: " << (ws.end - ws.begin + 1) << std::endl;
            std::cout << "l/r: " << left << " - " << right << std::endl;
            std::cout << "read: " << seqan::length(read) << std::endl;
            std::cout << "mult (c,e): " << contractMult << "," << extendMult << std::endl;
            std::cout << "windowSize, qGramSize: " << windowSize << "," << qGramSize << std::endl;
            std::cout << "count (b,e): " << count_begin << "," << count_end << std::endl;
            std::cout << "map: [";
            for (size_t i = ws.begin; i <= ws.end; i++) {
                unsigned count = std::lower_bound(map.begin(), map.end(), i, [](std::pair<WindowIndex, unsigned>& a, WindowIndex b){return a.first < b;})->second;
                std::cout << "(" << i << ": " << count << "), ";
            }
            std::cout << "]" << std::endl;
            std::cout << "begin: " << referenceWindows[ws.begin].beginPosition << " - " << referenceWindows[ws.end].beginPosition << std::endl;
            std::cout << "chr: " << referenceWindows[ws.begin].chromosomeIndex << ", " << referenceWindows[ws.end].chromosomeIndex << std::endl;
//            std::cout << "_" << leftExtend << "," << leftMax << "," << std::max(leftExtend, leftMax) << ";" <<std::max<int64_t>(0, std::max(leftExtend, leftMax)) << std::endl;
//            std::cout << "_" << rightExtend << "," << rightMax << std::endl;
//            std::cout << referenceWindows[ws.begin].beginPosition << ";" << referenceWindows[ws.end].beginPosition << std::endl;
//            std::cout << map[ws.begin] << ";" << map[ws.end] << std::endl;
        } else {
            // only add the result if start and end position are valid
            out_result.push_back(res);
        }
    }
}



template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::createQGramHashes(Iterator window_begin, Iterator window_end, std::vector<QGramHash> &out_hashes) {
    out_hashes.clear();
    if (window_begin + qGramSize <= window_end) { // if the window is bigger than the q-gram
        out_hashes.reserve(window_end - window_begin - qGramSize + 1);
        for (auto qgram_end = window_begin + qGramSize; qgram_end <= window_end; ++window_begin, ++qgram_end) {
            // window_begin is the start position of the current q-gram.
            out_hashes.push_back(qGramHashFunction(window_begin, qgram_end));
        }
//        // Remove duplicates.
//        std::sort(out_hashes.begin(), out_hashes.end());
//        out_hashes.erase(std::unique(out_hashes.begin(), out_hashes.end()), out_hashes.end());
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::createQGramHashes_iupac(Iterator window_begin, Iterator window_end, std::vector<QGramHash> &out_hashes) {
    // number of variants for a iupac symbol
    const static auto nv = [](unsigned x) {return (x & 1)
                                                + ((x & 2) >> 1)
                                                + ((x & 4) >> 2)
                                                + ((x & 8) >> 3);};
    //const static unsigned long long NUMBER_OF_VARIANTS[16] = {nv(0), nv(1), nv(2), nv(3), nv(4), nv(5), nv(6), nv(7),
    //                                                          nv(8), nv(9), nv(10),nv(11),nv(12),nv(13),nv(14),nv(15)};

    out_hashes.clear();
//    out_hashes.reserve(3 * qGramSize);
    // For SimHashing we don't need combinations
    out_hashes.reserve(qGramSize);

    typedef typename std::array<seqan::Dna, MAX_Q_GRAM_SIZE>::iterator OpIt;

    /*
    uint_fast16_t combinationCount[5] = {0,0,0,0,0}; // we count the number of bases with 0,1,2,3,4 variants
    const double lb3 = log2(3);
    const double lb_limit = log2(limit + 0.001);
    const double lb_limit_skip = log2(limit_skip + 0.001);
    auto func_lb_combinationNumber = [&combinationCount, lb3]() { // calculates the binary logarithm of the number of combinations for the current q-gram
        return combinationCount[2] + lb3 * combinationCount[3] + 2 * combinationCount[4];
    };
    auto func_combinationNumber = [&combinationCount]() {
        unsigned long long combinations = 1ull << (combinationCount[2] + 2*combinationCount[4]);
        for (uint_fast16_t i = 0; i < combinationCount[3]; i++) {
            combinations *= 3;
        }
        return combinations;
    };

    auto qgram_end = window_begin + qGramSize;
    for (auto it = window_begin; it < qgram_end && it < window_end; ++it) {
        combinationCount[NUMBER_OF_VARIANTS[unsigned(seqan::Iupac(*it))]]++;
    }
    */

    // the normal operation to add all combinations
    auto operation = [&out_hashes, this](OpIt beginIt, OpIt endIt) {
        out_hashes.emplace_back(this->qGramHashFunction(beginIt, endIt));
    };

    // the operation that adds on average only "limit" combinations
    auto &rngLimit = rngLimits[omp_get_thread_num()];
    const double rngMax = rngLimit.max();
    const double limitAsFloat = limit;
    unsigned long long combinations; // must be set before executing operation_limit
    auto operation_limit = [&out_hashes, this, &rngLimit, rngMax, &combinations, limitAsFloat](OpIt beginIt, OpIt endIt) {
        if (double(rngLimit()) / rngMax < limitAsFloat / combinations) {
            out_hashes.emplace_back(this->qGramHashFunction(beginIt, endIt));
        }
    };

    auto lastQgram_begin = window_end - qGramSize;
    for (auto curQgram_begin = window_begin; curQgram_begin <= lastQgram_begin; ++curQgram_begin) {
        findCombinationsOfQGram(curQgram_begin, operation);
//        double lb_combinations = func_lb_combinationNumber();
//        if (lb_combinations <= lb_limit) {
//            // add all combinations
//            findCombinationsOfQGram(curQgram_begin, operation);
//        } else if (lb_combinations <= lb_limit_skip) {
//            // add a random set of (on average) "limit" combinations
//            combinations = func_combinationNumber(); // combinations is used by findCombinationsOfQGram!
//            findCombinationsOfQGram(curQgram_begin, operation_limit);
//        } else {
//            // skip this q-gram (because there are too many combinations)
//        }

//        // update the number of combinations for the next q-gram
//        if (curQgram_begin < lastQgram_begin) { // true, if this is not the last iteration
//            combinationCount[NUMBER_OF_VARIANTS[unsigned(seqan::Iupac(*curQgram_begin))]]--;
//            combinationCount[NUMBER_OF_VARIANTS[unsigned(seqan::Iupac(*(curQgram_begin + qGramSize)))]]++;
//        }
    }

    // No unique hashes for SimHashing
//    std::sort(out_hashes.begin(), out_hashes.end());
//    out_hashes.erase(std::unique(out_hashes.begin(), out_hashes.end()), out_hashes.end());
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::createSignatures(Iterator qGramHashes_begin, Iterator qGramHashes_end, std::vector<SignatureHash> &out_signatures) {
    out_signatures.assign(signatureLength, std::numeric_limits<SignatureHash>::max());
    for (; qGramHashes_begin != qGramHashes_end; ++qGramHashes_begin) {
        QGramHash qGramHash = *qGramHashes_begin;
        for (size_t hashFuncIndex = 0; hashFuncIndex < signatureLength; hashFuncIndex++) {
            // Create a normal hash value and XOR it with one of the randomly generated integers.
            // This will serve well enough as "another" hash function.
            SignatureHash hash = qGramHash ^ hashFunctions[hashFuncIndex];
            out_signatures[hashFuncIndex] = hash < out_signatures[hashFuncIndex] ? hash : out_signatures[hashFuncIndex];
        }
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::createBandHashes(Iterator sig_begin, Iterator sig_end, std::vector<BandHash> &out_bandHashes) {
    out_bandHashes.clear();
    out_bandHashes.reserve((sig_end - sig_begin) / bandSize);
    // FIXME: signatureLength should be a multiple of bandSize.
    //        If not, the last 'signatureLength mod bandSize' signatures will be ignored!
    //        Should be enforced in constructor and / or CLI etc.
    for (; sig_begin < (sig_end - bandSize + 1); sig_begin = sig_begin + bandSize) {
        out_bandHashes.push_back(bandHashFunction(sig_begin, sig_begin + bandSize));
    }
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator> // an iterator pointing to seqan::Dna or seqan::Dna5
typename LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::QGramHash LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::qGramHashFunction(Iterator begin, Iterator end) {
    static_assert(std::is_same<const typename std::remove_reference<decltype(*begin)>::type, const seqan::Dna>::value ||
                  std::is_same<const typename std::remove_reference<decltype(*begin)>::type, const seqan::Dna5>::value,
                  "Iterator is pointing to the wrong type");
    return qGramHashObject(begin, end);
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator>
BandHash LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::bandHashFunction(Iterator begin, Iterator end) {
    // TODO: diese Hashfunction könnte möglichweise zu schwach sein!
    // 33767 is a prime number
    return std::accumulate(begin + 1, end, BandHash(*begin), [](BandHash ret, SignatureHash c) { return ret * 33767 + c; });
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
BandHash LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::bandHashFunction(typename std::vector<SignatureHash>::iterator begin,
                                                                                    typename std::vector<SignatureHash>::iterator end) {
    // TODO: diese Hashfunction könnte möglichweise zu schwach sein!
    // 33767 is a prime number
    return std::accumulate(begin + 1, end, BandHash(*begin), [](BandHash ret, SignatureHash c) { return ret * 33767 + c; });
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator, class Function>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::findCombinationsOfQGram(Iterator iupacQgram_begin, Function &operation) {
    std::array<char, sizeof(seqan::Dna) * MAX_Q_GRAM_SIZE> _curQGram;
    // goddamn ugly, but prevents default initialization of non-plain data type. Other applicable suggestions welcome!
    auto &curQGram = (std::array<seqan::Dna, MAX_Q_GRAM_SIZE>&) _curQGram;
    operation(curQGram.begin(), curQGram.begin() + qGramSize);
    
    // Don't use any variants for SimHashing at this time
    return;
    std::array<char, MAX_Q_GRAM_SIZE + 1> curVariant;

    int curIndex = 0; // current index in the current q-gram
    curVariant[0] = 0; // begin with variant id 0 at position 0

    const int qGramSizeMinusOne = (int) qGramSize - 1;

    while (true) {
        while (curVariant[curIndex] == 4) {
            // Alle Varianten wurden für das aktuelle Zeichen abgearbeitet
            curIndex--;
            if (curIndex < 0) {
                // Alle Varianten des aktuellen Q-Gramms wurden berücksichtigt -> break und nächstes Q-Gramm
                return;
            }
        }

        // extract the current iupac-base
        seqan::Iupac iupacSymbol = *(iupacQgram_begin + curIndex);
        seqan::Iupac dnaChar = iupacSymbol.value & (1 << curVariant[curIndex]);
        curVariant[curIndex]++; // next variant for the current position
        if (dnaChar.value != 0) {
            // an der Position "curIndex" im Q-Gramm kann die Base mit ID "curVariant[curIndex]" stehen
            curQGram[curIndex] = dnaChar;
            if (curIndex == qGramSizeMinusOne) {
                // we are at the end of the q-gram, so add the current q-gram to the document-q-gram-set
                operation(curQGram.begin(), curQGram.begin() + qGramSize);
           } else {
                curIndex++; // next position
                curVariant[curIndex] = 0; // current variant at the new position is set to zero
            }
        }
    }

}

// DEPRECATED
template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::findReadDirected(Iterator read_begin, Iterator read_end, std::vector<WindowIndex> &out_referenceWindowIndices) {
    std::vector<QGramHash> qGramHashes;
    createQGramHashes(read_begin, read_end, qGramHashes);
    std::vector<SignatureHash> signatures;
    createSignatures(qGramHashes.begin(), qGramHashes.end(), signatures);
    std::vector<BandHash> bandHashes;
    createBandHashes(signatures.begin(), signatures.end(), bandHashes);

    for (size_t i = 0; i < bandHashes.size(); i++) {
        // Find the bandHash in the bandHashTable and add all found rows to out_referenceWindowIndices
        bandHashTables[i].find(bandHashes[i], out_referenceWindowIndices);
    }
    // Remove duplicates.
    filterResults(out_referenceWindowIndices);
}

// DEPRECATED
template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::filterResults(std::vector<WindowIndex> &windowIndices) {
    std::sort(windowIndices.begin(), windowIndices.end());
    windowIndices.erase(std::unique(windowIndices.begin(), windowIndices.end()), windowIndices.end());
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
size_t LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::memoryUsage() const {
    size_t sum = 0;
    sum += sizeof(*this);
    for (const auto & bht: bandHashTables) {
       // sum += bht.memoryUsage();
    }
    sum += referenceWindows.capacity() * sizeof(ReferenceWindow);
    return sum;
}


// TODO: DEPRECATED
//
template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::printHashTableStatistics() {
    for (auto& bht: bandHashTables) {
        bht.printHashTableStatistics();
    }
    std::cout << "reference windows: " << referenceWindows.size() << std::endl;
    std::cout << "genome size: "
              << std::accumulate(chromosomes.begin(), chromosomes.end(), (size_t) 0,
                                 [&](size_t s, const Chromosome &c) { return s + seqan::length(c.data); }) << std::endl;
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Iterator>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::calcReadBandHashes(Iterator read_begin, Iterator read_end, std::vector<BandHash>& out_bandHashes) {
    std::vector<QGramHash> qGramHashes;
    createQGramHashes(read_begin, read_end, qGramHashes);
    // TODO SimHash
    // If we want to use variants, we have to create different SimHashes for
    // a variety of combinations at this point
    calcSimHash(qGramHashes.begin(), qGramHashes.end(), out_bandHashes);
//    createSignatures(qGramHashes.begin(), qGramHashes.end(), signatures);
//    createBandHashes(signatures.begin(), signatures.end(), out_bandHashes);
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
template <class Function>
void LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::
findReadInPermutations
(BandHash bandHash, size_t maximumOfReturnedWindows, Function operation) {
    unsigned int left = 0;
    unsigned int right = 0;
    unsigned int middle = 0;
    for (size_t i = 0; i < permutationCount; i++) {
        std::vector<std::pair<BandHash, WindowIndex>> &bht = bandHashTables[i];
        // Binary search in permutation
        left = 0;
        right = bht.size();
        while (right > left) {
            middle = (left + right) >> 1; // (left + right) / 2
            if (bht[middle].first == bandHash) {
                break;
            } else if (bht[middle].first < bandHash) {
                left = middle + 1;
            } else {
                right = middle;
            }
        }
        BandHash nearestBandHash = bht[middle].first;

        unsigned int returnedWindows = 0;
        if (seqan::popCount(bandHash ^ nearestBandHash) <= maxHammingDistance) {
            returnedWindows++;
            operation(middle);
        }

        // Scan through
        // the permutations in both directions until too many windows were found
        // or the hamming distance is too large
        //
        // Search downwards
        unsigned int index = middle - 1;
        if (middle > 0) {
            while ((index > 0) && (seqan::popCount(bandHash ^ bht[index].first) <= maxHammingDistance)) {
                if (returnedWindows++ >= maximumOfReturnedWindows) {
                break;
                }
                operation(index--);
            }
        }
        // And upwards
        index = middle + 1;
        unsigned int size = bht.size();
        if (middle < size) {
            while ((index < size) && (seqan::popCount(bandHash ^ bht[index].first) <= maxHammingDistance)) {
                if (returnedWindows++ >= maximumOfReturnedWindows) {
                break;
                }
                operation(index++);
            }
        }
    }
}
template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
//std::unordered_map<typename LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::WindowIndex, unsigned>
std::vector<std::pair<typename LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::WindowIndex, unsigned>>
LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::findReadOccurrences(
        const ReadString &read, const ReadString &reversedRead, const LshMapOptions &mapOptions) {
    std::vector<WindowIndex> occurrences;
    occurrences.reserve(permutationCount); // reserve enough space
    std::vector<BandHash> bandHashes;

    // Search for the read
    calcReadBandHashes(seqan::begin(read), seqan::end(read), bandHashes);
    
    for (auto bandHash : bandHashes) {
        findReadInPermutations(bandHash, mapOptions.maximumOfReturnedWindows,
                           [&occurrences](WindowIndex wi){occurrences.push_back(wi);}
        );
    }

    // Search for the reversed complement
    calcReadBandHashes(seqan::begin(reversedRead), seqan::end(reversedRead), bandHashes);
    
    for (auto bandHash : bandHashes) {
        findReadInPermutations(bandHash, mapOptions.maximumOfReturnedWindows,
                           [&occurrences](WindowIndex wi){occurrences.push_back(wi + INVERTED_WI_OFFSET);}
        );
    }

    // sort the window indices
    std::sort(occurrences.begin(), occurrences.end());
    std::vector<std::pair<WindowIndex, unsigned>> result;
    if (occurrences.size() == 0) {
        // return an empty vector if nothing was found
        return result;
    }

    // lets count the number of hits for each window
    WindowIndex curIndex = occurrences.front();
    unsigned curCount = 1;
    for (size_t i = 1; i < occurrences.size(); i++) {
        if (occurrences[i] == curIndex) {
            curCount++;
        } else {
            if (curCount >= mapOptions.minCount) {
                // we only add the window if there are enough occurences
                result.emplace_back(curIndex, curCount);
            }
            curIndex = occurrences[i];
            curCount = 1;
        }
    }
    // add the last index that was not added yet
    result.emplace_back(curIndex, curCount);

    return result;
//    std::unordered_map<WindowIndex, unsigned> occurrences;
//    std::vector<BandHash> bandHashes;
//    calcReadBandHashes(seqan::begin(read), seqan::end(read), bandHashes);
//    for (size_t i = 0; i < bandHashes.size(); i++) {
//        bandHashTables[i].find(bandHashes[i],
//                               [&occurrences](WindowIndex wi){++occurrences[wi];}
//        );
//    }
//    calcReadBandHashes(seqan::begin(reversedRead), seqan::end(reversedRead), bandHashes);
//    for (size_t i = 0; i < bandHashes.size(); i++) {
//        bandHashTables[i].find(bandHashes[i],
//                               [&occurrences](WindowIndex wi){++occurrences[wi + INVERTED_WI_OFFSET];}
//        );
//    }
//    return occurrences;
}

template <class BandHash, class QGramHash, class BHT, class QGramHashFunction>
std::vector<typename LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::WindowSequence>
                            LSHSimHash_Generic<BandHash, QGramHash, BHT, QGramHashFunction>::compressOccurrences(const std::vector<std::pair<WindowIndex, unsigned> > &occurrences) {
    typedef std::pair<WindowIndex, unsigned> WICPair; // WindowIndex-Count-Pair
    std::vector<WICPair> wiSorted(occurrences.begin(), occurrences.end());
    std::sort(wiSorted.begin(), wiSorted.end(), [](const WICPair& a, const WICPair& b){return a.first < b.first;});

    // compress a sequence of windows to one Compressed object
    std::vector<WindowSequence> compressed;
    compressed.push_back(WindowSequence{wiSorted.front().first,
                                            wiSorted.front().first,
                                            wiSorted.front().second});
    for (size_t i = 1; i < wiSorted.size(); i++) {
        WICPair& entry = wiSorted[i];
        WindowIndex curWI = entry.first;
        WindowIndex lastWI = compressed.back().end;
        ReferenceWindow& curRefWindow = referenceWindows[curWI >= INVERTED_WI_OFFSET ? curWI - INVERTED_WI_OFFSET : curWI];
        ReferenceWindow& lastRefWindow = referenceWindows[lastWI >= INVERTED_WI_OFFSET ? lastWI - INVERTED_WI_OFFSET : lastWI];
        if (curWI == lastWI + 1
                    && curRefWindow.chromosomeIndex == lastRefWindow.chromosomeIndex
                    && curRefWindow.beginPosition == lastRefWindow.beginPosition + windowOffset) {
            compressed.back().count += entry.second;
            compressed.back().end = entry.first;
        } else {
            compressed.push_back(WindowSequence{entry.first,
                                                    entry.first,
                                                    entry.second});
        }
    }

    // sort the compressed windows by count
    std::sort(compressed.begin(), compressed.end(),
              [](const WindowSequence& a, const WindowSequence& b){
        double aScore = a.count / (a.end == a.begin ? 1. : (a.end - a.begin + 1) / 2.);
        double bScore = b.count / (b.end == b.begin ? 1. : (b.end - b.begin + 1) / 2.);
        return aScore > bScore;
    });

    return compressed;
}

#endif // LSHSIMHASH_H


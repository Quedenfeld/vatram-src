#ifndef PG583_MAP_COLLISIONFREEBHT_H
#define PG583_MAP_COLLISIONFREEBHT_H

#include "map/HashDataStructure.h"
#include "map/MultiWindowManager.h"

#include <seqan/parallel.h>

#include <memory>
#include <vector>
#include <cstddef>
#include <cstdint>
#include <array>
#include <deque>
#include <functional>
#include <iostream>
#include <utility>


/**
 * @brief Collision Free Band Hash Table
 * @details If there is a collision in this hash table, the next entry is used.
 */
template <class TBandHash = uint32_t>
class CollisionFreeBHT : public HashDataStructure<TBandHash> {
public:
    using typename HashDataStructure<TBandHash>::BandHash;

    /**
     * @brief Creates an empty band hash table
     * @param windowCount number of windows which will be added (may be estimated, preferably exact)
     */
    CollisionFreeBHT(size_t windowCount);

    /**
     * @brief Adds a new bandHash windowsIndex pair to the hash table
     * @param bandHash band hash value
     * @param windowIndex index of the winodw
     */
    void add(BandHash bandHash, WindowIndex windowIndex);

    /**
     * @brief Finds all window indices with the specified band hash value
     * @param bandHash band hash value
     * @param maximumOfReturnedWindows The maximal number of window indices that is returned
     * @return a vector of window indices that are associated
     * @details If the number of window indices stored for the specified band hash value is bigger than maximumOfReturnedWindows,
     *      then the returned vector will be empty.
     */
    std::vector<WindowIndex> find(BandHash bandHash, size_t maximumOfReturnedWindows);

    /**
     * @brief Finds all window indices with the specified band hash value
     * @param bandHash band hash value
     * @param maximumOfReturnedWindows The maximal number of window indices that is returned
     * @param out_windows the resulting window indices are added to this empty vector
     * @details If the number of window indices stored for the specified band hash value is bigger than maximumOfReturnedWindows,
     *      then nothing will be add to out_windows.
     */
    void find(BandHash bandHash, size_t maximumOfReturnedWindows, std::vector<WindowIndex>& out_windows);

    /**
     * @brief Finds all window indices with the specified band hash value
     * @param bandHash band hash value
     * @param maximumOfReturnedWindows The maximal number of window indices that is returned
     * @param operation for each window index wi that was found the function operation(wi) is called
     * @details If the number of window indices stored for the specified band hash value is bigger than maximumOfReturnedWindows,
     *      then the function operation() is not called.
     */
    template <class Function>
    void find(BandHash bandHash, size_t maximumOfReturnedWindows, Function operation);

    /**
     * @brief Finds all window indices with the specified band hash value
     * @param bandHash band hash value
     * @param maximumOfReturnedWindows The maximal number of window indices that is returned
     * @return two pointers ("begin" and "end") pointing to the first window index
     * @details If the number of window indices stored for the specified band hash value is bigger than maximumOfReturnedWindows,
     *      then an empty pointer pair is returned.
     */
    std::pair<const WindowIndex*, const WindowIndex*> findIterators(BandHash bandHash, size_t maximumOfReturnedWindows);

    /**
     * @brief Clears the hash table and sets its size to the specified value
     * @param initialSize the new size of the hash table
     */
    void clearAndSetSize(size_t initialSize);

    /**
     * @brief Creates a new hash table with the same data stored in it, but a different size.
     * @param newSize the new size of the hash table
     * @details This function temporarily needs memory for the old hash table and the new one at the same time,
     *      so be sure, that there is enough free memory, when this method is executed.
     */
    void rehash(size_t newSize);

    /**
     * @brief Current memory usage for this data structure
     * @return an approximation for the number of bytes used by this data structure
     */
    size_t memoryUsage() const;


    /**
     * @brief Removes a bandHash-windowIndex-Pair from the band hash table
     * @param bandHash
     * @param windowIndex
     */
    void remove(BandHash bandHash, WindowIndex windowIndex);

    /**
     * @brief Starts the post processing for the window manager
     * @details The multi window buckets are sorted
     */
    void sort();


    /**
     * @brief Struct for storing a band hash and a window index entry
     * @details If 64-bit band hashes are used, then the struct needs 12 bytes memory (if compiled with the gcc).
     */
    struct
#ifdef __GNUC__
    __attribute__ ((__packed__))
#endif
    BandHashEntry {
        /**
         * @brief Checks if this entry is empty
         * @return true, if no window index is stored in this entry
         */
        bool isEmpty() const;

        BandHash key;
        MultiWindowManager::WindowIndexEntry value;
    };

    /**
     * @brief The BandHashIterator iterates over the band hash values stored in the hash table.
     * @details If you want to find the corresponding window indices for one band hash value, you can
     *      simply use find()
     */
    struct BandHashIterator {
        CollisionFreeBHT* cfbht;
        size_t curIdx;

        bool operator==(BandHashIterator bhi) {return cfbht == bhi.cfbht && curIdx == bhi.curIdx;}
        bool operator!=(BandHashIterator bhi) {return cfbht != bhi.cfbht || curIdx != bhi.curIdx;}
        BandHash operator*() {return cfbht->hashTable[curIdx].key;}
        BandHashIterator& operator++() {
            do {
                ++curIdx;
            } while (curIdx < cfbht->hashTable.size() && cfbht->hashTable[curIdx].isEmpty());
            return *this;
        }
    };

    /**
     * @brief iterator pointing to the first band hash
     * @return the begin iterator for the band hashes
     */
    BandHashIterator begin() {
        BandHashIterator bhi{this, 0};
        if (hashTable[0].isEmpty()) {
            ++bhi; // search for the next non-empty entry
        }
        return bhi;
    }

    /**
     * @brief iterator pointing to the end of the hash table
     * @return the end iterator for the band hashes
     */
    BandHashIterator end() {
        return BandHashIterator{this, hashTable.size()};
    }

    /**
     * @brief if the ratio of the non-empty buckets in the hash table is bigger than this variable, the size of the hash table is doubled.
     */
    double maximalLoadFactor = 0.5;

    // NOTE: This factor should be chosen in conjunction with the input's hash functions,
    //       e.g. 1.5-2.0 seems reasonable for the shifting q-gram hash function (no gaps),
    //       though "r = r * 33767 + i" (no gaps) works better with 1.0.
    /**
     * @brief the initial size of the hash table
     */
    double initialSizeMultiplicator = 2.01;

//private:
    /**
     * @brief Array that supported parallel initialization
     */
    class ParallelInitializingArray {
        // Hash tables are quite big => parallel initialization and parallel copy on resize causes noticeable speedup.
        // This simple implementation omits destructing the elements => only use for fixed size datatypes!
        size_t arraySize = 0;
        size_t arrayCapacity = 0;
        BandHashEntry *array = nullptr;
    public:
        ParallelInitializingArray(size_t count = 0) { resize(count); }
        ParallelInitializingArray(const ParallelInitializingArray &rhs) { assign(rhs.array, rhs.arraySize); }
        ParallelInitializingArray(ParallelInitializingArray &&rhs) { swap(rhs); }
        ParallelInitializingArray& operator=(ParallelInitializingArray&& rhs) {
            if (this != &rhs) {
                swap(rhs);
            }
            return *this;
        }

        ParallelInitializingArray& operator=(const ParallelInitializingArray& rhs) {
            if (this != &rhs) {
                assign(rhs.array, rhs.arraySize);
            }
            return *this;
        }

        ~ParallelInitializingArray() {
            if (array != nullptr) {
                operator delete(array);
            }
        }

        size_t size() const { return arraySize; }
        size_t capacity() const { return arrayCapacity; }
        void clear() { resize(0); }

        BandHashEntry& operator [](size_t pos) {
            assert(pos < arraySize);
            return array[pos];
        }
        const BandHashEntry& operator [](size_t pos) const {
            assert(pos < arraySize);
            return array[pos];
        }

        BandHashEntry* begin() { return array; }
        const BandHashEntry* begin() const { return array; }
        BandHashEntry* end() { return array + arraySize; }
        const BandHashEntry* end() const { return array + arraySize; }

        void swap(ParallelInitializingArray &rhs) {
            std::swap(arraySize, rhs.arraySize);
            std::swap(arrayCapacity, rhs.arrayCapacity);
            std::swap(array, rhs.array);
        }

        void resize(size_t count) {
            if (count > arrayCapacity) {
                assign(array, arraySize, count);
            }
            if (count > arraySize) {
                seqan::arrayFill(array + arraySize, array + count, BandHashEntry(), seqan::Parallel());
            }
            arraySize = count;
        }

        void assign(BandHashEntry *source, size_t count, size_t newCapacity = 0) {
            BandHashEntry *target = array;
            newCapacity = std::max(count, newCapacity);
            if (newCapacity > arrayCapacity) {
                target = static_cast<BandHashEntry*>(::operator new(newCapacity * sizeof(BandHashEntry)));
            }
            seqan::Splitter<BandHashEntry*> splitter(source, source + count, seqan::Parallel());
            SEQAN_OMP_PRAGMA(parallel for if (count >= 10000))
            for (size_t job = 0; job < seqan::length(splitter); ++job) {
                seqan::arrayCopyForward(splitter[job], splitter[job + 1], target + (splitter[job] - source));
            }
            if (target != array) {
                if (array != nullptr) {
                    operator delete(array);
                }
                arrayCapacity = newCapacity;
                array = target;
            }
            arraySize = count;
        }
    } hashTable;

    /**
     * @brief The window manager stores the multi window buckets
     */
    MultiWindowManager windowManager;

    /// The number of unique band hash values stored in the band hash table
    size_t uniqueBandHashCount;

    size_t getUniqueBandHashCount() const {
        return uniqueBandHashCount;
    }

    /// Hash function (currently std::hash)
    size_t hash(BandHash bandHash);

    /**
     * @brief Finds the index for a given band hash
     * @param bandHash the band hash searched for
     * @return the index, where the band hash is saved, or the index for the next free empty entry
     */
    size_t getHashTableIndex(BandHash bandHash);
};

// =====================================================================================
// ===== I M P L E M E N T A T I O N S
// =====================================================================================

template <class TBandHash>
CollisionFreeBHT<TBandHash>::CollisionFreeBHT(size_t windowCount) :
    hashTable(std::max<size_t>(1, initialSizeMultiplicator * windowCount)),
    uniqueBandHashCount(0) {
}

template <class TBandHash>
void CollisionFreeBHT<TBandHash>::add(BandHash bandHash, WindowIndex windowIndex) {
    BandHashEntry& bucket = hashTable[getHashTableIndex(bandHash)];
    bucket.key = bandHash;
    uniqueBandHashCount += bucket.isEmpty();
    windowManager.add(bucket.value, windowIndex);

    // Check for rehash
    if (uniqueBandHashCount > maximalLoadFactor * hashTable.size()) {
        rehash(2 * hashTable.size());
    }
}

template <class TBandHash>
std::vector<WindowIndex> CollisionFreeBHT<TBandHash>::find(BandHash bandHash, size_t maximumOfReturnedWindows) {
    std::vector<WindowIndex> result;
    find(bandHash, maximumOfReturnedWindows, result);
    return result;
}

template <class TBandHash>
void CollisionFreeBHT<TBandHash>::find(BandHash bandHash, size_t maximumOfReturnedWindows, std::vector<WindowIndex> &out_windows) {
    find(bandHash, maximumOfReturnedWindows, [&out_windows](WindowIndex wi){out_windows.push_back(wi);});
}

template <class TBandHash>
template <class Function>
void CollisionFreeBHT<TBandHash>::find(BandHash bandHash, size_t maximumOfReturnedWindows, Function operation) {
    BandHashEntry& bucket = hashTable[getHashTableIndex(bandHash)];
    windowManager.get(bucket.value, operation, maximumOfReturnedWindows);
}

template <class TBandHash>
std::pair<const WindowIndex*, const WindowIndex *>
CollisionFreeBHT<TBandHash>::findIterators(CollisionFreeBHT::BandHash bandHash, size_t maximumOfReturnedWindows) {
    BandHashEntry& bucket = hashTable[getHashTableIndex(bandHash)];
    return windowManager.get(bucket.value, maximumOfReturnedWindows);
}

template <class TBandHash>
void CollisionFreeBHT<TBandHash>::clearAndSetSize(size_t initialSize) {
    uniqueBandHashCount = 0;
    hashTable.clear();
    hashTable.resize(initialSize);
    windowManager.clear();
}

template <class TBandHash>
void CollisionFreeBHT<TBandHash>::rehash(size_t newSize) {
    ParallelInitializingArray oldHashTable;
    oldHashTable.swap(hashTable);
    // mutliWindowsBuckets does not need to be changed.

    hashTable.clear();
    hashTable.resize(newSize);
    for (BandHashEntry& entry: oldHashTable) {
        if (!entry.isEmpty()) {
            size_t index = getHashTableIndex(entry.key);
            hashTable[index] = entry;
        }
    }
}

template <class TBandHash>
size_t CollisionFreeBHT<TBandHash>::memoryUsage() const {
    size_t sum = 0;
    sum += sizeof(hashTable) + hashTable.capacity() * sizeof(BandHashEntry);
    sum += windowManager.memoryUsage();
    sum += sizeof(uniqueBandHashCount);
    return sum;
}

template <class TBandHash>
void CollisionFreeBHT<TBandHash>::remove(BandHash /*bandHash*/, WindowIndex /*windowIndex*/) {
    /*
    size_t hashTableIndex = getHashTableIndex(bandHash);
    BandHashEntry& bucket = hashTable[hashTableIndex];
    if (!bucket.isEmpty()) {
        switch (bucket.value & BandHashEntry::META_MASK) {
        case BandHashEntry::SINGLE_WINDOW : {
            if (bucket.getValue() == windowIndex) {

                // hashTableIndex = current empty index
                // curIdx = current index for lookup
                size_t htSize = hashTable.size();
                size_t curIdx = (hashTableIndex + 1) % htSize;
                while (!hashTable[curIdx].isEmpty()) {
                    // get the real index (without collision moving)
                    size_t realIdx = hash(hashTable[curIdx].key) % htSize;
                    // Check if the current bucket has to be moved
                    // There can be an overflow, so the comparison is more complicated than "realIdx < hashTableIndex"
                    if ((realIdx <= hashTableIndex && hashTableIndex < curIdx)
                            || (hashTableIndex < curIdx && curIdx < realIdx)
                            || (curIdx < realIdx && realIdx <= hashTableIndex)) {
                        // the entry was a collision entry and must be moved
                        hashTable[hashTableIndex] = hashTable[curIdx];
                        // the current empty index is updated
                        hashTableIndex = curIdx;
                    }
                    curIdx = (curIdx + 1) % htSize;
                }
                hashTable[hashTableIndex].value = BandHashEntry::EMPTY;
            }
            break;
        }
        case BandHashEntry::DOUBLE_WINDOW : {
            auto& doubleBucket = doubleWindowBuckets[bucket.getValue()];
            // Convert Double-Window to Single-Window (if windowIndex is found)
            if (doubleBucket[0] == windowIndex) {
                freeDoubleWindowBuckets.push_back(bucket.getValue());
                bucket.setWindowIndex(doubleBucket[1]);
            } else if (doubleBucket[1] == windowIndex) {
                freeDoubleWindowBuckets.push_back(bucket.getValue());
                bucket.setWindowIndex(doubleBucket[0]);
            }
            break;
        }
        case BandHashEntry::QUADRUPLE_WINDOW : {
            auto& quadBucket = quadrupleWindowBuckets[bucket.getValue()];
            size_t pos = 0;
            // Search for the windowIndex
            while (pos < 4 && quadBucket[pos] != windowIndex) {
                pos++;
            }
            if (pos != 4) {
                // windowIndex was found at position pos
                if (quadBucket[3] == BandHashEntry::EMPTY) {
                    // Convert Quad-Window to Double-Window (because there are only two windows left)
                    freeQuadrupleWindowBuckets.push_back(bucket.getValue());
                    quadBucket[pos] = quadBucket[2]; // the non-deleted window indices can be found at position 0 and 1
                    // create the double window bucket
                    size_t doubleBucketIdx = getFreeDoubleBucket();
                    auto& doubleBucket = doubleWindowBuckets[doubleBucketIdx];
                    doubleBucket[0] = quadBucket[0];
                    doubleBucket[1] = quadBucket[1];
                    bucket.setDoubleWindowIndex(doubleBucketIdx);
                } else {
                    // just delete the windowIndex such that the last position is empty
                    quadBucket[pos] = quadBucket[3];
                    quadBucket[3] = BandHashEntry::EMPTY;
                }
            }
            break;
        }
        case BandHashEntry::MULTI_WINDOW : {
            auto& multiBucket = multiWindowBuckets[bucket.getValue()];
            auto it = std::find(multiBucket.begin(), multiBucket.end(), windowIndex);
            if (it != multiBucket.end()) {
                // delete windowIndex by overwriting with last entry
                *it = multiBucket.back();
                if (multiBucket.size() == 5) {
                    // Convert Multi-Window to Quad-Window
                    freeMultiWindowBuckets.push_back(bucket.getValue());
                    // create a quad window bucket
                    size_t quadBucketIdx = getFreeQuadrupleBucket();
                    auto& quadBucket = quadrupleWindowBuckets[quadBucketIdx];
                    quadBucket[0] = multiBucket[0];
                    quadBucket[1] = multiBucket[1];
                    quadBucket[2] = multiBucket[2];
                    quadBucket[3] = multiBucket[3];
                    bucket.setQuadrupleWindowIndex(quadBucketIdx);
                } else {
                    // just delete last entry
                    multiBucket.pop_back();
                }
            }
            break;
        }
        }
    }
    */
}

template <class TBandHash>
void CollisionFreeBHT<TBandHash>::sort() {
    windowManager.sort();
}

template <class TBandHash>
size_t CollisionFreeBHT<TBandHash>::hash(BandHash bandHash) {
    std::hash<BandHash> hashFunction;
    return hashFunction(bandHash);
}

template <class TBandHash>
size_t CollisionFreeBHT<TBandHash>::getHashTableIndex(BandHash bandHash) {
    size_t index = hash(bandHash) % hashTable.size();

    // find free or correct position
    while (!hashTable[index].isEmpty() && hashTable[index].key != bandHash) {
        index = (index + 1) % hashTable.size();
    }
    return index;
}

template <class TBandHash>
bool CollisionFreeBHT<TBandHash>::BandHashEntry::isEmpty() const {
    return value.isEmpty();
}

#endif // PG583_MAP_COLLISIONFREEBHT_H

#include "align/Variant.h"

#include <seqan/sequence.h>

#include <limits>

Variant::Variant() :
    position(std::numeric_limits<decltype(position)>::max()) {
}

const Variant Variant::none;

Variant::Variant(size_t position, const ReferenceString &insertionString) ://, double probability) :
    position(position),
    insertionString(insertionString)/*,
    probability(probability)*/ {
}

Variant::Variant(size_t position, size_t deletionLength) ://, double probability) :
    position(position),
    deletionLength(deletionLength)/*,
    probability(probability)*/ {
}

Variant::Variant(size_t position, const ReferenceString &insertionString, size_t deletionLength) ://, double probability) :
    position(position),
    deletionLength(deletionLength),
    insertionString(insertionString)/*,
    probability(probability)*/ {
}

size_t Variant::getPosition() const {
    return position;
}

const ReferenceString& Variant::getInsertionString() const {
    return insertionString;
}

size_t Variant::getDeletionLength() const {
    return deletionLength;
}

Variant::Type Variant::getType() const {
    if (seqan::length(insertionString) == 0) {
        return DELETION;
    }
    return (deletionLength == 0) ? INSERTION : SUBSTITUTION;
}

//double  Variant::getProbability() const {
//    return probability;
//}

bool Variant::operator<(const Variant &rhs) const {
    if (position < rhs.position) {
        return true;
    } else if (position > rhs.position) {
        return false;
    }
    if (deletionLength < rhs.deletionLength) {
        return true;
    } else if (deletionLength > rhs.deletionLength) {
        return false;
    }
    auto insertionLength = seqan::length(insertionString);
    auto rhsInsertionLength = seqan::length(rhs.insertionString);
    if (insertionLength < rhsInsertionLength) {
        return true;
    } else if (insertionLength > rhsInsertionLength) {
        return false;
    }
    return false; // *this == rhs
}

bool Variant::operator==(const Variant &rhs) const {
    return this == &rhs;
    // bitte nicht löschen
//    return (position == rhs.position)
//            && (deletionLength == rhs.deletionLength)
//            && (insertionString == rhs.insertionString);
}

std::ostream& operator<<(std::ostream& stream, const Variant &var) {
    if (var == Variant::none) {
        stream << "None";
    } else {
        seqan::CharString insertionString(var.getInsertionString());
        stream << "Type=" << var.getType() << " Pos=" << var.getPosition() << " Del=" <<
                   var.getDeletionLength() << " Ins=" << seqan::toCString(insertionString);
    }
    return stream;
}

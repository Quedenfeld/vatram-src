#include "align/Alignment.h"
#include "align/Variant.h"

#include <seqan/sequence.h>
#include <seqan/file.h>

Alignment::Alignment() :
    cigar(),
    alignmentString(),
    usedVariants(),
    startVariant(Variant::none),
    startOffset(0),
    errorCount(0),
    cigarInvalid(false),
    reversedCigar(),
    reversedAlignmentString() {
}

std::string toString(const CigarString &cigarString) {
    std::ostringstream oss;
    for (const seqan::CigarElement<> &c : cigarString) {
        oss << c.operation << c.count;
    }
    return oss.str();
}

CigarString& Alignment::getCigar() {
    if (cigarInvalid) {
        // rebuild cigar, if it has been changed
        seqan::assign(cigar, seqan::reverseString(reversedCigar));
        cigarInvalid = false;
    }
    return cigar;
}

const seqan::String<ReferenceChar>& Alignment::getAlignmentString() {
    if (alignmentStringInvalid) {
        // rebuild alignment string, if it has been changed
        seqan::assign(alignmentString, seqan::reverseString(reversedAlignmentString));
        alignmentStringInvalid = false;
    }
    return alignmentString;
}

const std::set<std::reference_wrapper<const Variant>>& Alignment::getUsedVariants() const {
    return usedVariants;
}

size_t Alignment::getPosition() const {
    return position;
}

const Variant& Alignment::getStartVariant() const {
    return startVariant;
}

int Alignment::getStartOffset() const {
    return startOffset;
}

size_t Alignment::getErrorCount() {
    return errorCount;
}

void Alignment::addCigarChar(char c) {
    if (!seqan::empty(reversedCigar) && c == seqan::back(reversedCigar).operation) {
        // if added char is equal to last inserted, then just increment counter
        ++seqan::back(reversedCigar).count;
    } else {
        // register new char
        seqan::appendValue(reversedCigar, seqan::CigarElement<>(c, 1));
    }
    cigarInvalid = true;
}

void Alignment::addAlignmentChar(ReferenceChar c) {
    seqan::appendValue(reversedAlignmentString, c);
    alignmentStringInvalid = true;
}

void Alignment::addUsedVariant(const Variant &var) {
    if (!(var == Variant::none)) {
        usedVariants.insert(var);
    }
}

void Alignment::setCigarString(const CigarString &newCigar) {
    seqan::assign(cigar, newCigar);
    seqan::assign(reversedCigar, seqan::reverseString(newCigar));
    cigarInvalid = false;
}

void Alignment::setCigar(const std::string &newCigar) {
    // Parse the cigar string into seqAn's representation.
    seqan::clear(cigar);
    std::istringstream is(newCigar);
    while (is.good()) {
        char op;
        unsigned count;
        is >> count >> op;
        if (is.eof())
            break;
        seqan::appendValue(cigar, seqan::CigarElement<>(op, count));
    }

    seqan::assign(reversedCigar, seqan::reverseString(cigar));
    cigarInvalid = false;
}

void Alignment::setAlignmentString(const std::string &align) {
    seqan::assign(alignmentString, align);
    seqan::assign(reversedAlignmentString, seqan::reverseString(align));
    alignmentStringInvalid = false;
}

void Alignment::setPosition(size_t pos) {
    position = pos;
}

void Alignment::setStartVariant(const Variant &var) {
    startVariant = var;
}

void Alignment::setStartOffset(int off) {
    startOffset = off;
}

void Alignment::incrementErrorCount() {
    errorCount++;
}

bool Alignment::isValid() const {
    return !seqan::empty(reversedCigar);
}

const CigarString& Alignment::getReversedCigar() const {
    return reversedCigar;
}

const seqan::String<ReferenceChar>& Alignment::getReversedAlignmentString() const {
    return reversedAlignmentString;
}

bool Alignment::operator==(const Alignment &a2) const {
    const Alignment &a1 = *this;

//    std::cout << "Compare alignmetns (exp vs real)" << std::endl;
//    std::cout << "rCigar   : " << cigarToString(a1.getReversedCigar()) << " : " << cigarToString(a2.getReversedCigar()) << std::endl;
//    std::cout << "Position: " << a1.getPosition() << " : " << a2.getPosition() << std::endl;
//    std::cout << "rAlign   : " << seqan::reverseString(a1.getReversedAlignmentString()) << " : " << seqan::reverseString(a2.getReversedAlignmentString()) << std::endl;
//    std::cout << "StartOff: " << a1.getStartOffset() << " : " << a2.getStartOffset() << std::endl;
//    std::cout << "StartVar: " << a1.getStartVariant() << " : " << a2.getStartVariant() << std::endl;
//    std::cout << "UssedVar: " << a1.getUsedVariants() << " : " << a2.getUsedVariants() << std::endl;

    if (a1.getReversedCigar() != a2.getReversedCigar())
        return false;
    if (a1.getPosition() != a2.getPosition())
        return false;
    if (a1.getReversedAlignmentString() != a2.getReversedAlignmentString())
        return false;
    if (a1.getStartOffset() != a1.getStartOffset())
        return false;
    if (!(a1.getStartVariant() == a2.getStartVariant()))
        return false;
    if (a1.getUsedVariants() != a2.getUsedVariants())
        return false;
    return true;
}

std::string Alignment::cigarToString(const CigarString &cs) const{
    std::string s;
    for (int i = seqan::length(cs) - 1; i >= 0; i--) {
        s.append(std::to_string(cs[i].count));
        char c = (char)(cs[i].operation);
        s.push_back(c);
    }
    return s;
}

bool Alignment::operator<(const Alignment &a2) const {
    const Alignment &a1 = *this;

    if (a1.getPosition() < a2.getPosition())
        return true;
    else if (a2.getPosition() < a1.getPosition())
        return false;

    if (a2.getReversedCigar() < a1.getReversedCigar())
        return true;
    else if (a1.getReversedCigar() < a2.getReversedCigar())
        return false;

    if (a2.getReversedAlignmentString() < a1.getReversedAlignmentString())
        return true;
    else if (a1.getReversedAlignmentString() < a2.getReversedAlignmentString())
        return false;

    if (a1.getStartOffset() < a1.getStartOffset())
        return true;
    else if (a2.getStartOffset() < a1.getStartOffset())
        return false;

    if (a1.getStartVariant() < a2.getStartVariant())
        return true;
    else if (a2.getStartVariant() < a1.getStartVariant())
        return false;

    if (a1.getUsedVariants() < a2.getUsedVariants())
        return false;
    return true;
}

bool operator==(const std::vector<Alignment> &vec1, const std::vector<Alignment> &vec2) {
    for (Alignment a1 : vec1) {
        bool contained = false;
        for (Alignment a2 : vec2) {
            if (a1 == a2) {
                contained = true;
                break;
            }
        }
        if (!contained) {
            return false;
        }
    }
    for (Alignment a2 : vec2) {
        bool contained = false;
        for (Alignment a1 : vec1) {
            if (a1 == a2) {
                contained = true;
                break;
            }
        }
        if (!contained) {
            return false;
        }
    }

    return true;
}

#ifndef ALIGNOPTIONS_H
#define ALIGNOPTIONS_H

#include "types.h"
#include "Chromosome.h"
#include "Aligner.h"
#include "Read.h"

struct AlignerOptions {
    ErrorCount maxError_low;
    ErrorCount maxClip_low;

    ErrorCount maxError_high;
    ErrorCount maxClip_high;

    size_t maxSelect;
    double nextFactor;

    AlignerOptions(ErrorCount maxError_low, ErrorCount maxClip_low, ErrorCount maxError_high, ErrorCount maxClip_high, size_t maxSelect, double nextFactor) :
        maxError_low(maxError_low),
        maxClip_low(maxClip_low),
        maxError_high(maxError_high),
        maxClip_high(maxClip_high),
        maxSelect(maxSelect),
        nextFactor(nextFactor) {
    }
};

class AlignerDriver {
public:
    ChromosomeReferences genome;
    std::vector<std::reference_wrapper<const VariantIndex>> variantIndexes;

    AlignerDriver(const Genome& genome, const std::vector<VariantIndex>& variantIndexes) {
        for (const Chromosome& chr: genome) {
            this->genome.emplace_back(chr);
        }
        for (const VariantIndex& vidx: variantIndexes) {
            this->variantIndexes.emplace_back(vidx);
        }
    }

    AlignerDriver(const ChromosomeReferences& genome, const std::vector<VariantIndex>& variantIndexes) :
        genome(genome) {
        for (const VariantIndex& vidx: variantIndexes) {
            this->variantIndexes.emplace_back(vidx);
        }
    }

    struct Result {
        Alignment alignment;
        MapInterval bestInterval = NO_INTERVAL;
        size_t alignerCalls = 0;
        size_t alignments = 0;
    };

    Result align(Aligner& aligner, const ReadString& read, const ReadString& reversedRead,
                    const std::vector<MapInterval>& intervals, const AlignerOptions& options) {
        Alignment curAlignment;
        Result bestResult;
        if (intervals.size()) {
            double firstScore = intervals.front().score;
            // try low error
            ErrorCount err = options.maxError_low + 1;
            for (size_t i = 0; i < intervals.size() &&
                               i < options.maxSelect &&
                               intervals[i].score * options.nextFactor >= firstScore; i++) {
                curAlignment = align(aligner, read, reversedRead, intervals[i], err - 1, options.maxClip_low);
                bestResult.alignerCalls++;
                if (curAlignment.isValid()) {
                    bestResult.alignments++;
                    bestResult.alignment = curAlignment;
                    bestResult.bestInterval = intervals[i];
                    err = curAlignment.getErrorCount();
                    if (err == 0) {
                        break;
                    }
                }

            }

            // try high error
            if (!bestResult.alignment.isValid()) {
                err = options.maxError_high + 1;
                for (size_t i = 0; i < intervals.size(); i++) {
                    curAlignment = align(aligner, read, reversedRead, intervals[i], err - 1, options.maxClip_high);
                    bestResult.alignerCalls++;
                    if (curAlignment.isValid()) {
                        bestResult.alignments++;
                        bestResult.alignment = curAlignment;
                        bestResult.bestInterval = intervals[i];
                        err = curAlignment.getErrorCount();
                        if (err == 0) {
                            break;
                        }
                    }
                }
            }
        }
        return bestResult;
    }

private:
    Alignment align(Aligner& aligner, const ReadString& read, const ReadString& reversedRead, const MapInterval& ival, ErrorCount err, ErrorCount maxClip) {
        const ReadString& pattern = ival.inverted ? reversedRead : read;

//        const std::vector<SnpVariant>& lowProbSnps = genome[ival.chromosome].get().lowProbabilitySnps;
//        auto it = std::lower_bound(lowProbSnps.begin(), lowProbSnps.end(), ival.startPosition, [](const SnpVariant& var, size_t val){return var.position < val;});
//        auto itEnd = lowProbSnps.end();
//        if (it != itEnd && it->position < ival.endPosition) {
//            // temporary ref-copy with low-probability-SNPs
//            std::vector<SnpVariant> originalBases;
//            Chromosome& chr = const_cast<Chromosome&>(genome[ival.chromosome].get());
//            for (; it != itEnd && it->position < ival.endPosition; ++it) {
//                originalBases.push_back(SnpVariant{it->position, chr.data[it->position]});
//                chr.addSNP(it->position, it->snp);
//            }
//            Alignment alignment = aligner.align(chr, ival.startPosition, ival.endPosition, variantIndexes[ival.chromosome], pattern, err, maxClip);
//            for (SnpVariant& snp: originalBases) {
//                chr.data[snp.position] = snp.snp;
//            }
//            return alignment;
//        } else {
//            // there are no low-probability-SNPs
//            return aligner.align(genome[ival.chromosome], ival.startPosition, ival.endPosition, variantIndexes[ival.chromosome], pattern, err, maxClip);
//        }
        return aligner.align(genome[ival.chromosome], ival.startPosition, ival.endPosition, variantIndexes[ival.chromosome], pattern, err, maxClip);
    }




};

#endif // ALIGNOPTIONS_H

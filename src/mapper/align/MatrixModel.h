#ifndef PG583_ALIGN_MATRIXMODEL_H
#define PG583_ALIGN_MATRIXMODEL_H

#include <vector>
#include <cstddef>
#include <cstdint>
#include <cassert>
#include <functional>

using std::size_t;

enum RecursionType : uint16_t {
    AL_MATCH,          // read and reference chars match
    AL_DELETEDCHAR,    // char from reference is not in read
    AL_INSERTEDCHAR,   // char from read is not in reference
    AL_MISMATCH,       // read and referece chars do not match
    AL_NONE            // special case, used for deletion variants
};

typedef uint16_t ErrorCount;
typedef uint32_t RowIndex;
typedef uint32_t ColumnIndex;

class Variant;

/**
 * @class MatrixStorage
 * @brief A container for memory allocations in the BacktracingMatix. This is used to allow reusing memory across
 * multiple matrix instances.
 */
class MatrixModel {

public:
    MatrixModel(size_t h, size_t w);

    struct Cell {
        /**
         * @brief columns from where the recursion came.
         * This information is needed for the backtracing in variants, because a backtrace step can not only be done into
         * the column before, but it can skip many columns, if there has been an insertion variant in between, for example.
         */
        ColumnIndex columnForRecursion;

        /**
         * @brief next row for every cell
         * Stores for every matrix cell, which row is the next to calculate.
         */
        RowIndex nextRow;

        /**
         * @brief type or direction of recursion for this field.
         */
        RecursionType recDir;

        /**
         * @brief score for dynamic programming recursion
         * Is actually the number of errors, which is actually the edit distance between the part of read and reference,
         * which have been read so far.
         */
        ErrorCount score;

        Cell();
    };
    std::vector<Cell> cells;

    struct ColumnHeader {
        /**
         * @brief the variant, which is present in each column
         * For every column of the matrix we have to store the variant, which belongs to this column. In most cases this is
         * just Variant::none.
         */
        std::reference_wrapper<const Variant> variant;

        /**
         * @brief the index of the reference part, where this column belongs to.
         * For every column of the matrix we have to store to which index of the reference it belongs. Because of insertion
         * variants the column index is not necessarily equal to the actual index of the reference.
         */
        ColumnIndex indexOfColumnInRef;

        /**
         * @brief the offset inside an insertion variant
         */
        ColumnIndex offset;

        /**
         * @brief the size for each column which has been filled with a calculated score
         */
        RowIndex scoreFill;

        /**
         * @brief the last row, which has to be computed for the _next_ column (not for the current one).
         */
        RowIndex lastRowNext;

        ColumnHeader();
    };
    std::vector<ColumnHeader> headers;

    /**
     * @brief allocated height of this matrix
     */
    size_t height;

    /**
     * @brief the initial number of columns, for which space is allocated
     */
    size_t width;

    /**
     * @brief the column id of the given position (index) in the reference
     * For every column of the matrix we have to store to which index of the reference it belongs. Because of insertion
     * variants the column index is not necessarily equal to the actual index of the reference.
     */
    std::vector<ColumnIndex> columnOfRefInIndex;

    /**
     * @brief resize Enlarges the matrix storage to accomodate height.
     */
    void resizeHeight(size_t newHeight);

    /**
     * @brief resize Enlarges the matrix storage to accomodate width.
     */
    void resizeWidth(size_t newWidth);
};

#endif // PG583_ALIGN_MATRIXMODEL_H

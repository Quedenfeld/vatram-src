#include "align/MatrixModel.h"
#include "align/Variant.h"

MatrixModel::MatrixModel(size_t h, size_t w) :
    cells(h*w),
    headers(w),
    height(h),
    width(w),
    columnOfRefInIndex(w)
{}

MatrixModel::Cell::Cell() :
    columnForRecursion(0),
    nextRow(0),
    recDir(RecursionType::AL_INSERTEDCHAR),
    score(0) {
}

MatrixModel::ColumnHeader::ColumnHeader() :
    variant(Variant::none),
    indexOfColumnInRef(0),
    offset(0),
    scoreFill(0),
    lastRowNext(0){
}

void MatrixModel::resizeHeight(size_t height) {
    if (height <= this->height)
        return;

    this->height = height;
    cells.resize(height*width);
    headers.resize(width);
    columnOfRefInIndex.resize(width);
}

void MatrixModel::resizeWidth(size_t width) {
    if (width <= this->width) {
        return;
    }

    this->width = width;
    cells.resize(height*width);
    headers.resize(width);
    columnOfRefInIndex.resize(width);
}

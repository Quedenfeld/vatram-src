#include "align/RankMatrixModel.h"
#include "align/Variant.h"

RankMatrixModel::RankMatrixModel(size_t h, size_t w) :
    superBlocks(10+h*w/(64*64)),
    superOffset(10+h*w/(64*64)),
    blocks(0),
    offset(0),
    cells(0),
    cellCache(h*MATRIX_MODEL_CACHE_WIDTH),
    headers(w),
    height(h),
    width(w),
    columnOfRefInIndex(w) {
    defaultCell.columnForRecursion = 0;
    defaultCell.nextRow = 0;
    defaultCell.recDir = AL_NONE;
    defaultCell.score = 255;
}

void RankMatrixModel::resizeHeight(size_t height) {
    if (height <= this->height)
        return;

    this->height = height;
    cellCache.resize(height*MATRIX_MODEL_CACHE_WIDTH);
    superBlocks.resize(10+height*width/(64*64));
    superOffset.resize(10+height*width/(64*64));
}

void RankMatrixModel::resizeWidth(size_t width) {
    if (width <= this->width) {
        return;
    }

    this->width = width;
    superBlocks.resize(10+height*width/(64*64));
    superOffset.resize(10+height*width/(64*64));
    headers.resize(width);
    columnOfRefInIndex.resize(width);
}

void RankMatrixModel::writeCell(ColumnIndex col, RowIndex row, MatrixModel::Cell cell) {
    size_t index = col*height + row;
    size_t superBlockIndex = index/(64*64);
    size_t offsetInSuperBlock = (index%(64*64))/64;
    if (superBlocks[superBlockIndex] == 0) {
        // if super block is empty, set superblock bit, add super block start offset and everything below
        superOffset[superBlockIndex] = blocks.size();
        superBlocks[superBlockIndex] |= (((size_t)1) << (63 - offsetInSuperBlock));
        size_t offsetInBlock = index % 64;
        blocks.push_back(((size_t)1) << (63 - offsetInBlock));
        offset.push_back(cells.size());
        cells.push_back(cell);
    } else {
        if (superBlocks[superBlockIndex] & (((size_t)1) << (63 - offsetInSuperBlock))) {
            // superblock already has bit set -> investigate block
            size_t blockIndex = superOffset[superBlockIndex] + seqan::popCount((superBlocks[superBlockIndex] >> 1) >> (63 - offsetInSuperBlock));
            size_t offsetInBlock = index % 64;
            if (blocks[blockIndex] & (((size_t)1) << (63 - offsetInBlock))) {
                // block already set -> do nothing
                return;
            } else {
                // set block bit and add cell
                blocks[blockIndex] |= (((size_t)1) << (63 - offsetInBlock));
                cells.push_back(cell);
            }
        } else {
            // superblock is zero at position -> set bit and create new block
            superBlocks[superBlockIndex] |= (((size_t)1) << (63 - offsetInSuperBlock));
            size_t offsetInBlock = index % 64;
            blocks.push_back(((size_t)1) << (63 - offsetInBlock));
            offset.push_back(cells.size());
            cells.push_back(cell);
        }
    }
}

MatrixModel::Cell &RankMatrixModel::readCell(ColumnIndex col, RowIndex row) {
    size_t index = col*height + row;
    size_t superBlockIndex = index/(64*64);
    size_t offsetInSuperBlock = (index%(64*64))/64;
    if (superBlocks[superBlockIndex] & (((size_t)1) << (63 - offsetInSuperBlock))) {
        size_t blockIndex = superOffset[superBlockIndex] + seqan::popCount((superBlocks[superBlockIndex] >> 1) >> (63 - offsetInSuperBlock));
        size_t offsetInBlock = index % 64;
        if (blocks[blockIndex] & (((size_t)1) << (63 - offsetInBlock))) {
            size_t cellIndex = offset[blockIndex] + seqan::popCount((blocks[blockIndex] >> 1) >> (63 - offsetInBlock));
            return cells[cellIndex];
        } else {
            return defaultCell;
        }
    } else {
        return defaultCell;
    }
}

void RankMatrixModel::clear() {
    superBlocks.assign(superBlocks.size(), 0);
    blocks.clear();
    offset.clear();
    cells.clear();
}

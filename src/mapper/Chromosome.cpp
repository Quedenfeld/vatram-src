#include "Chromosome.h"

#include <seqan/sequence.h>

#include <algorithm>
#include <cctype>
#include <cassert>

Chromosome::Chromosome(Chromosome &&rhs) noexcept :
    id(std::move(rhs.id)),
    name(std::move(rhs.name)),
    variants(std::move(rhs.variants)),
    lowProbabilitySnps(std::move(rhs.lowProbabilitySnps)){
    // Swapping seqan::host(...) since seqan::swap on packed string uses std::swap instead of seqan::swap for whatever reason.
    seqan::swap(seqan::host(data), seqan::host(rhs.data));
    seqan::swap(seqan::host(noVariantsString), seqan::host(rhs.noVariantsString));
}

Chromosome& Chromosome::operator=(Chromosome &&rhs) {
    std::swap(id, rhs.id);
    std::swap(name, rhs.name);
    std::swap(variants, rhs.variants);
    std::swap(lowProbabilitySnps, rhs.lowProbabilitySnps);
    // Swapping seqan::host(...) since seqan::swap on packed string uses std::swap instead of seqan::swap for whatever reason.
    seqan::swap(seqan::host(data), seqan::host(rhs.data));
    seqan::swap(seqan::host(noVariantsString), seqan::host(rhs.noVariantsString));
    return *this;
}

Chromosome::Chromosome(const std::string &id) :
    Chromosome(id, "") {
}

Chromosome::Chromosome(const std::string &id, const ReferenceString &data) :
    Chromosome(id, data, std::vector<Variant>()) {
}

Chromosome::Chromosome(const std::string &id, const ReferenceString &data, const std::vector<Variant> &variants) :
    id(id),
    name(generateName(id)),
    data(data),
    variants(variants) {
}

void Chromosome::addSNP(size_t pos, ReferenceChar snp) {
    assert(seqan::length(data) > pos);
    data[pos] = seqan::Iupac(snp).value | seqan::Iupac(data[pos]).value;
    highProbabilitySnps.push_back(SnpVariant{pos, snp});
}

void Chromosome::addLowProbabilitySNP(size_t pos, ReferenceChar snp) {
    assert(seqan::length(data) > pos);
    if (!(seqan::Iupac(data[pos]).value & snp.value)) {
        lowProbabilitySnps.push_back(SnpVariant{pos, snp});
    }
}

//void Chromosome::insertHighProbabilitySnps() {
//    for (const SnpVariant& snp: highProbabilitySnps) {
//        data[pos] = seqan::Iupac(snp).value | seqan::Iupac(data[pos]).value;
//    }
//}

void Chromosome::insertLowProbabilitySnps() {
    for (const SnpVariant& snp: lowProbabilitySnps) {
        data[snp.position] = seqan::Iupac(snp.snp).value | seqan::Iupac(data[snp.position]).value;
    }
}

void Chromosome::removeLowProbabilitySnps() {
    for (const SnpVariant& snp: lowProbabilitySnps) {
        data[snp.position] = noVariantsString[snp.position];
    }
    for (const SnpVariant& snp: highProbabilitySnps) {
        data[snp.position] = seqan::Iupac(snp.snp).value | seqan::Iupac(data[snp.position]).value;
    }
}

void Chromosome::addInsertion(size_t pos, const ReferenceString &insertionString) {
    assert(seqan::length(data) > pos);
    variants.emplace_back(pos, insertionString);
}

void Chromosome::addDeletion(size_t posAfterDeletion, size_t deletionLength) {
    assert(seqan::length(data) > posAfterDeletion);
    assert(posAfterDeletion >= deletionLength);
    variants.emplace_back(posAfterDeletion, deletionLength);
}

void Chromosome::addVariant(size_t posAfterDeletion, size_t deletionLength, const ReferenceString &insertionString) {
    assert(seqan::length(data) > posAfterDeletion);
    assert(posAfterDeletion >= deletionLength);
    variants.emplace_back(posAfterDeletion, insertionString, deletionLength);
}

std::string Chromosome::getOriginalName() const {
    return id.substr(0, id.find_first_of(' '));
}

// TODO: either remove this functionality (i.e. require same IDs in FASTA and VCF files) or make it user configurable.
std::string Chromosome::generateName(const std::string &id) {
    if (id.empty()) {
        return "";
    }

    // TODO: Drop GCC 4.8 support and use std::regex sometime...

    static constexpr const char *MITOCHONDRION = "MT";
    using SplitString = seqan::StringSet<seqan::Segment<const std::string, seqan::InfixSegment>>;
    std::string ret;

    auto charString = seqan::infix(id, 0, id.size());
    SplitString splitByPipe;
    seqan::strSplit(splitByPipe, charString, '|', true);
    if (seqan::length(splitByPipe) > 2) {
        auto idString = seqan::back(splitByPipe);
        SplitString splitByComma;
        seqan::strSplit(splitByComma, idString, ',', true, 1);
        SplitString splitBySpace;
        seqan::strSplit(splitBySpace, seqan::front(splitByComma), ' ', false);

        const size_t splitBySpaceLength = seqan::length(splitBySpace);
        if (splitBySpaceLength > 1 && splitBySpace[splitBySpaceLength - 2] == "chromosome") {
            seqan::assign(ret, seqan::back(splitBySpace));
            return ret;
        }
        if (splitBySpaceLength > 0 && seqan::back(splitBySpace) == "mitochondrion") {
            ret = MITOCHONDRION;
            return ret;
        }

        for (const auto idType : {"gb", "ref"}) {
            for (int i = seqan::length(splitByPipe) - 2; i > 0; --i) {
                if (splitByPipe[i-1] == idType) {
                    seqan::assign(ret, splitByPipe[i]);
                    return ret;
                }
            }
        }
    }

    // assume FASTA-IDs as described in
    // ftp://ftp.ncbi.nih.gov/genbank/genomes/Eukaryotes/vertebrates_mammals
    // /Homo_sapiens/GRCh37.p13/seqs_for_alignment_pipelines/README_ANALYSIS_SETS

    SplitString splitBySpace;
    seqan::strSplit(splitBySpace, charString, ' ', false);
    auto idString = seqan::front(splitBySpace);

    SplitString splitByUnderscore;
    seqan::strSplit(splitByUnderscore, idString, '_', true);

    if (seqan::length(splitByUnderscore) == 1) {
        static constexpr const char *chrPrefix = "chr";
        if (seqan::startsWith(idString, chrPrefix)) {
            seqan::assign(ret, seqan::suffix(idString, 3)); // remove "chr" prefix
        } else {
            seqan::assign(ret, idString);
        }
        if (ret == "M") {
            ret = MITOCHONDRION;
        }
        return ret;
    }

    for (size_t i = 1; i < seqan::length(splitBySpace); ++i) {
        static constexpr const char *acPrefix = "AC:";
        if (seqan::startsWith(splitBySpace[i], acPrefix)) {
            seqan::assign(ret, seqan::suffix(splitBySpace[i], 3));
            return ret;
        }
    }

    return "";
}

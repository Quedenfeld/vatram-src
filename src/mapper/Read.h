#ifndef READ_H
#define READ_H

#include "types.h"
#include "seqan/bam_io.h"

/**
 * @brief Represents a possible location for a read
 * @details The aligner can calculate the alignment between both given positions. There is no reason to expand the specified interval
 */



struct MapInterval {
    ChromosomeIndex chromosome;
    Position startPosition;
    Position endPosition;
    bool inverted; /// denotes that the reversed complement of a read was found
    double score;

    double getScore() const {return score;}

    bool operator==(const MapInterval& o) {
        return std::memcmp(this, &o, sizeof(MapInterval)) == 0;
    }
    bool operator!=(const MapInterval& o) {
        return std::memcmp(this, &o, sizeof(MapInterval)) != 0;
    }
};

constexpr MapInterval NO_INTERVAL = MapInterval{ChromosomeIndex(-1), Position(-1), Position(-1), false, 0.};

struct PairedRead {
    seqan::CharString id[2];
    ReadString seq[2];
    ReadString revSeq[2];
    std::vector<MapInterval> intervals[2];
    seqan::BamAlignmentRecord record[2];
    bool isPairedEnd;

    PairedRead() {
        record[0].flag = 0;
        record[1].flag = 0;
        isPairedEnd = false;
    }

    void setRevCompl() {
        for (size_t i = 0; i <= isPairedEnd; i++) {
            revSeq[i] = seq[i];
            seqan::reverseComplement(revSeq[i], seqan::Serial());
        }
    }
};


#endif // READ_H

#ifndef PG583_IO_INDEXREADER_H
#define PG583_IO_INDEXREADER_H

#include "io/IndexWriter.h"
#include "Logger.h"

#include <algorithm>

class IndexReadError : public IndexFileError {
public:
    explicit IndexReadError(const std::string &msg) :
        IndexFileError("Reading index file", msg) {
    }
    explicit IndexReadError(const std::string &expected, const std::string &str,
                                const std::string &prefix = "", const std::string &postfix = "") :
        IndexReadError(prefix + "Expected \"" + expected + "\" got \"" + str + "\"." + postfix) {
    }
};

/// @brief Reads an index file into a @p TIndex instance.
/// @details An index file stores the following:
///   - header section:
///     - all parameters with which the corresponding index has been created (see ::IndexOptions)
///     - IDs of the used reference chromosomes
///   - index section:
///     - reference window positions
///     - band hashes
/// @tparam TIndex Type of the index the index file is loaded into (usually LSH or IndexWriter).
template <class TIndex>
class IndexReader {
public:
    /// @brief IndexReader
    /// @param index The associated index to which the index file's data is loaded into.
    IndexReader(TIndex &index);

    /// @brief Opens an index file and resets internal state variables.
    /// @param filePath The path of the index file.
    void open(const std::string &filePath);

    /// @brief Reads an index file's header and applies stored index options to #index.
    /// @param genome Index will only loaded for chromosomes which are present in @p genome (identified by ID).
    void readHeader(const Genome &genome);

    /// @brief Reads index file into #index.
    /// @details Reads index data for all chromosomes which are retured by readHeader().
    /// @remarks readHeader() must have been called beforehand!
    void readIndex();
private:
    /// Reference to the TIndex instance which receives the indexed data.
    TIndex &index;
    /// Input stream of the index file.
    std::unique_ptr<std::istream> is;
    /// Used to check whether readHeader() has been called before readIndex().
    bool isHeaderRead = false;
    /// Provides a mapping from index file's chromosome order to the currently loaded chromosomes' order.
    std::vector<size_t> chromosomeIndexMapping;

    /// @brief Helper function to restore escaped characters in chromosome IDs.
    /// @details Restores escape sequences with IndexFileConstants::ESCAPE for '\0' and members of ::IndexFileConstants.
    static std::string unescape(const std::string &str);
};

template <class TIndex>
IndexReader<TIndex>::IndexReader(TIndex &index) :
    index(index) {
}

template <class TIndex>
void IndexReader<TIndex>::open(const std::string &filePath) {
    is.reset(new std::ifstream (filePath, std::ifstream::in | std::ifstream::binary));
    isHeaderRead = false;
    chromosomeIndexMapping.clear();
}

template <class TIndex>
void IndexReader<TIndex>::readHeader(const Genome &genome) {
    if (!is) {
        throw IndexReadError("No index file opened.");
    }

    std::string bufferLine;
    if (!std::getline(*is, bufferLine, IndexFileConstants::NEWLINE)) {
        throw IndexReadError("Unable to read file header.");
    }
    if (bufferLine != IndexFileIdentifiers::FILE) {
        throw IndexReadError(IndexFileIdentifiers::FILE, bufferLine);
    }

    IndexOptions indexOptions;
    auto &intOptions = indexOptions.getOptions();
    for (Option &option : intOptions) {
        option.unset();
    }
    for (size_t i = 0; i < intOptions.size() && std::getline(*is, bufferLine, IndexFileConstants::NEWLINE); ++i) {
        std::istringstream line(bufferLine);
        std::string identifier;
        if (!std::getline(line, identifier, IndexFileConstants::DELIM)) {
            throw IndexReadError("Could not read option identifier.");
        }
        auto itOption = std::find_if(intOptions.begin(), intOptions.end(),
                                     [&](const Option &opt) { return identifier == opt.getArgOptName(); });
        if (itOption == intOptions.end()) {
            throw IndexReadError("Unknown option identifier: \"" + identifier + "\".");
        }
        if (!itOption->get().readOption(line)) {
            throw IndexReadError("Could not read value for option \"" + identifier + "\".");
        }
    }
    try {
        indexOptions.checkValues();
    } catch (std::invalid_argument &e) {
        throw IndexReadError(e.what());
    }

    if (!std::getline(*is, bufferLine, IndexFileConstants::NEWLINE)) {
        throw IndexReadError("Could not read chromosome section.");
    }
    if (bufferLine != IndexFileIdentifiers::CHROMOSOME_SECTION) {
        throw IndexReadError(IndexFileIdentifiers::CHROMOSOME_SECTION, bufferLine);
    }
    ChromosomeReferences indexedChromosomes;
    chromosomeIndexMapping.clear();
    while (std::getline(*is, bufferLine, IndexFileConstants::NEWLINE)
           && bufferLine != IndexFileIdentifiers::INDEX_SECTION) {
        auto it = std::find(bufferLine.begin(), bufferLine.end(), IndexFileConstants::DELIM);
        std::string identifier(bufferLine.begin(), it);

        if (identifier.empty()) {
            throw IndexReadError("Could not read chromosome entry.");
        }
        if (identifier != IndexFileIdentifiers::CHROMOSOME_ENTRY) {
            throw IndexReadError(IndexFileIdentifiers::CHROMOSOME_ENTRY, identifier);
        }

        if (it == bufferLine.end()) {
            throw IndexReadError("Could not read chromosome id.");
        }
        std::string id = unescape(std::string(++it, bufferLine.end()));

        auto itChromosome = std::find_if(genome.begin(), genome.end(),
                                         [&](const Chromosome &chromosome) { return chromosome.id == id; });
        if (itChromosome == genome.end()) {
            logger.warn("Reading Index: ", "Chromosome not in reference files, ignored. Chromosome id: \"", id, '"');
            chromosomeIndexMapping.emplace_back(std::numeric_limits<size_t>::max());
        } else {
            if (indexedChromosomes.end() != std::find_if(indexedChromosomes.begin(), indexedChromosomes.end(),
                                                         [&](const Chromosome &chr) { return chr.id == id; })) {
                throw IndexReadError("Multiple entries for chromosome with id \"" + id + "\".");
            }
            chromosomeIndexMapping.emplace_back(indexedChromosomes.size());
            indexedChromosomes.emplace_back(*itChromosome);
        }
    }

    if(bufferLine != IndexFileIdentifiers::INDEX_SECTION) {
        throw IndexReadError(IndexFileIdentifiers::INDEX_SECTION, bufferLine);
    }

    isHeaderRead = true;
    indexOptions.apply(index);
    index.chromosomes.assign(indexedChromosomes.begin(), indexedChromosomes.end());
}

template <class TIndex>
void IndexReader<TIndex>::readIndex() {
    if (!isHeaderRead) {
        throw IndexReadError("Header has not been read before attempting to load index data.");
    }

    const size_t numChromosomes = index.chromosomes.size();
    size_t estimatedRefWindowCount = 0;
    for (const Chromosome &chromosome : index.chromosomes) {
        estimatedRefWindowCount += (seqan::length(chromosome.data) + index.windowOffset - 1)
                                   / index.windowOffset;
    }
    index.referenceWindows.clear();

    std::vector<std::vector<typename TIndex::BandHash>> bandHashes(index.bandHashTables.size());
    std::vector<Mutex> bandHashesMutex(bandHashes.size());

    std::string buffer;

    #pragma omp parallel
    #pragma omp single
    for (size_t entry = 0; std::getline(*is, buffer, IndexFileConstants::NEWLINE); ++entry) {
        auto addIndexEntryToErrMsg = [&](const std::string &msg) {
            return msg + " Index entry: " + std::to_string(entry) + "."; };

        std::stringstream line(buffer);

        std::string identifier;
        if (!std::getline(line, identifier, IndexFileConstants::DELIM)) {
            throw IndexReadError(addIndexEntryToErrMsg("Could not read identifier."));
        }
        if (identifier != IndexFileIdentifiers::INDEX_ENTRY) {
            throw IndexReadError(IndexFileIdentifiers::INDEX_ENTRY, identifier, "Invalid index entry identifier.",
                                 addIndexEntryToErrMsg(""));
        }

        ChromosomeIndex chromosomeIndex;
        if (!(line >> chromosomeIndex)) {
            throw IndexReadError(addIndexEntryToErrMsg("Could not read chromosome index."));
        }
        if (chromosomeIndex >= chromosomeIndexMapping.size()) {
            throw IndexReadError(addIndexEntryToErrMsg(
                                     "Invalid chromosome index \"" + std::to_string(chromosomeIndex) + "\"."));
        }

        chromosomeIndex = chromosomeIndexMapping[chromosomeIndex];
        size_t chromosomeSize;
        if (chromosomeIndex >= numChromosomes) {
            chromosomeSize = std::numeric_limits<size_t>::max();
        } else {
            chromosomeSize = seqan::length(index.chromosomes[chromosomeIndex].get().data);
        }

        WindowIndex windowCount;
        if (!(line >> windowCount)) {
            throw IndexReadError(addIndexEntryToErrMsg("Could not read number of reference windows."));
        }

        std::vector<Position> beginPositions(windowCount);
        if (!is->read(reinterpret_cast<char *>(&beginPositions[0]), windowCount * sizeof beginPositions[0])) {
            throw IndexReadError(addIndexEntryToErrMsg("Could not read reference window positions"));
        }
        const WindowIndex firstWindowIndex = index.referenceWindows.size();
        for (auto beginPosition : beginPositions) {
            if (beginPosition >= chromosomeSize) {
                throw IndexReadError(addIndexEntryToErrMsg("Window position outside of chromosome bounds."));
            }
            index.referenceWindows.emplace_back(chromosomeIndex, beginPosition);
        }
        const size_t bandCount = index.bandHashTables.size();
        for (size_t bandIndex = 0; bandIndex < bandCount; ++bandIndex) {
            bandHashesMutex[bandIndex].lock();
            bandHashes[bandIndex].resize(windowCount);
            if (!is->read(reinterpret_cast<char *>(&bandHashes[bandIndex][0]),
                          windowCount * sizeof bandHashes[bandIndex][0])) {
                throw IndexReadError(addIndexEntryToErrMsg("Could not band hashes. Band hash table index: "
                                                            + std::to_string(bandIndex) + "."));
            }
            #pragma omp task
            {
                WindowIndex windowIndex = firstWindowIndex;
                for (auto bandHash : bandHashes[bandIndex]) {
                    index.addReferenceWindowHash(bandIndex, bandHash, windowIndex++);
                }
                bandHashesMutex[bandIndex].unlock();
            }
        }
        is->get(); // consume NEWLINE
        addIndexSection(index, chromosomeIndex);
    }

    index.referenceWindows.shrink_to_fit();
    if (!is->eof()) {
        throw IndexReadError("Failed before reaching end of file");
    }
}

template <class TIndex>
std::string IndexReader<TIndex>::unescape(const std::string &str) {
    std::string ret;
    ret.reserve(str.size());

    for (auto it = str.begin(), end = str.end(); it != end; ++it) {
        char c = *it;
        if (c == IndexFileConstants::ESCAPE) {
            if (++it != end) {
                c = *it;
                switch (c) {
                case '0':
                    c = '\0';
                    break;
                case 'n':
                    c = IndexFileConstants::NEWLINE;
                    break;
                case 't':
                    c = IndexFileConstants::DELIM;
                    break;
                case IndexFileConstants::ESCAPE:
                    break;
                default:
                    ret.push_back(IndexFileConstants::ESCAPE);
                    logger.warn("IndexFile::unescape: unknown escape sequence \"", IndexFileConstants::ESCAPE, c, '"');
                }
            } else {
                logger.warn("IndexFile::unescape: empty escape sequence \"", IndexFileConstants::ESCAPE, '"');
            }
        }
        ret.push_back(c);
    }

    return ret;
}

#endif // PG583_IO_INDEXREADER_H

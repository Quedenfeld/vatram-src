#include "io/VariantsReader.h"
#include "Chromosome.h"
#include "types.h"

#include <type_traits>
#include <iostream>

namespace {
/// Tag used for the seqan::readRecord() specialization down below.
struct ExtractOnlyChromPosIdRefAlt {};
} // namespace
namespace seqan {
/// @brief Specialization of seqan::readRecord() which accelerates parsing of VCF files.
/// @details Only required fields (CHROM, POS, ID, REF, ALT) are being read. Furthermore integer parsing is sped up.
template <typename TStream>
int readRecord(VcfRecord & record, RecordReader<TStream, SinglePass<>> &reader, VcfIOContext &context,
               ExtractOnlyChromPosIdRefAlt const &);
} // namespace seqan

VariantsReader::VariantsReader(Genome &genome, const std::unordered_set<std::string> &chromosomeNameFilter, double probabilityThreshold) :
    genome(genome),
    chromosomeNameFilter(chromosomeNameFilter),
    probabilityThreshold(probabilityThreshold) {
}
VariantsReader::VariantsReader(Genome &genome, const std::unordered_set<std::string> &chromosomeNameFilter) :
    VariantsReader(genome, chromosomeNameFilter, 0.0) {

}

VariantsReader::VariantsReader(Genome &genome, double probabilityThreshold) :
    VariantsReader(genome, std::unordered_set<std::string>(), probabilityThreshold) {
}

int VariantsReader::readVariants(const std::vector<std::string> &variantFiles) {
    int retval = readVariants(variantFiles, [this](Chromosome& chromosome, size_t pos,
                                             const seqan::Infix<const seqan::CharString&>::Type& infixRef,
                                             const seqan::Infix<seqan::CharString&>::Type& infixAlt,
                                             double probability) {
        this->addVariant(chromosome, pos, infixRef, infixAlt, probability);
    });

    for (Chromosome &chromosome : genome) {
        auto &variants = chromosome.variants;
        std::sort(variants.begin(), variants.end());
        variants.erase(std::unique(variants.begin(), variants.end(), [](const Variant &v1, const Variant &v2) {
            return v1.getPosition() == v2.getPosition()
                    && v1.getDeletionLength() == v2.getDeletionLength()
                    && v1.getInsertionString() == v2.getInsertionString();
        }), variants.end());
//        auto& lowProbSnps = chromosome.lowProbabilitySnps;
//        std::sort(lowProbSnps.begin(), lowProbSnps.end());
    }

    return retval;
}

int VariantsReader::readVariants(const std::vector<std::string> &variantFiles, AddVariantFunction function) {
    for (size_t i = 0; i < genome.size(); ++i) {
        chromosomeNameToIndex[genome[i].name] = i;
    }

    chromosomesMutexes = std::vector<Mutex>(genome.size());
    #pragma omp parallel for schedule(dynamic)
    for (size_t i = 0; i < variantFiles.size(); ++i) {
        readVariantsFromReader(variantFiles[i], function);
    }

    return 0;
}

int VariantsReader::readVariantsFromReader(const std::string &variantFile, AddVariantFunction function) {
    const size_t bufferSize = 65536;

    if (seqan::endsWith(variantFile, ".gz")) {
    #if SEQAN_HAS_ZLIB
        seqan::Stream<seqan::GZFile> f;
        if (!seqan::open(f, variantFile.c_str(), "rb")) {
            logger.err(LOG_PREFIX, "GZip file has the wrong format! At file: ", variantFile);
            return 1;
        }

        seqan::RecordReader<seqan::Stream<seqan::GZFile>, seqan::SinglePass<>> reader(f, bufferSize);
        readVariantsFromReader(reader, function);
    #else // SEQAN_HAS_ZLIB
        logger.err(LOG_PREFIX, "ZLIB not available! At file: ", variantFile);
        return 1;
    #endif // SEQAN_HAS_ZLIB
    } else {
        std::ifstream f(variantFile, std::ios::in | std::ios::binary);
        if (!f) {
            logger.err(LOG_PREFIX, "Could not open \"", variantFile, '"');
            return 1;
        }

        seqan::RecordReader<std::ifstream, seqan::SinglePass<>> reader(f, bufferSize);
        readVariantsFromReader(reader, function);
    }
    return 0;
}

template<class TReader>
int VariantsReader::readVariantsFromReader(TReader &reader, AddVariantFunction function) {
    seqan::VcfHeader header;
    seqan::VcfIOContext context(header.sequenceNames, header.sampleNames);

    int res = seqan::read(header, reader, context, seqan::Vcf());
    if (res != 0) {
        logger.err(LOG_PREFIX, "Failed reading header. Error code: ", res);
        return 1;
    }

    size_t recordCount = 0;
    while (!seqan::atEnd(reader)) {
        ++recordCount;
        seqan::VcfRecord record;
        int res = seqan::readRecord(record, reader, context, ExtractOnlyChromPosIdRefAlt());
        if (res != 0 && !seqan::atEnd(reader)) {
            logger.err(LOG_PREFIX, "Failed reading record. Error code: ", res, ", Record: ", recordCount);
            return 1;
        }

        processRecord(record, context, function);
    }
    return 0;
}

int VariantsReader::processRecord(seqan::VcfRecord &record, const seqan::VcfIOContext &context, AddVariantFunction function) {
    std::string chromosomeName = seqan::toCString((*context.sequenceNames)[record.rID]);

    if (!chromosomeNameFilter.empty() && chromosomeNameFilter.find(chromosomeName) == chromosomeNameFilter.end()) {
        return 0;
    }

    auto chromosomeIt = chromosomeNameToIndex.find(chromosomeName);
    if (chromosomeIt == chromosomeNameToIndex.end()) {
        logger.warn(LOG_PREFIX, "Skipping entry. Chromosome not in reference: \"", chromosomeName, '"');
        return 1;
    }
    LockGuard lock(chromosomesMutexes[chromosomeIt->second]);
    Chromosome &chromosome = genome[chromosomeIt->second];

    seqan::toUpper(record.ref);
    seqan::StringSet<seqan::CharString> alts;
    seqan::splitString(alts, record.alt, ',');

    // extract probability
    seqan::StringSet<seqan::CharString> infos;
    seqan::splitString(infos, record.info, ';');
    seqan::CharString probInfo;
    for (const auto& info: infos) {
        if (seqan::startsWith(info, "CAF")) {
            probInfo = info;
            break;
        }
    }
    std::vector<double> probabilities;
    if (probInfo != "") {
        const size_t NOT_FOUND = -1;
        size_t startIdx = NOT_FOUND, endIdx = NOT_FOUND;
        for (size_t i = 0; i < seqan::length(probInfo); i++) {
            char c = probInfo[i];
            if (c == '[') {
                startIdx = i+1;
            } else if (c == ']') {
                endIdx = i;
            }
        }
        if (startIdx != NOT_FOUND && endIdx != NOT_FOUND) {
            seqan::StringSet<seqan::CharString> probs;
            seqan::splitString(probs, seqan::infix(probInfo, startIdx, endIdx), ',');
            for (size_t i = 1; i < seqan::length(probs); i++) {//first value is reference probability! -> begin with 1
                probabilities.push_back(std::atof(seqan::toCString(probs[i])));
            }
        }
    }
    while (probabilities.size() < seqan::length(alts)) {
        probabilities.push_back(0.0); // add values if something goes wrong
    }

    // probabilityThreshold
    for (size_t i = 0; i < seqan::length(alts); i++) {
//        const auto &orgAlt : alts
        seqan::CharString alt = alts[i];
        AltType ret = processVariant(chromosome, record.beginPos, record.ref, alt, probabilities[i], function);
        if (ret != AltType::SUPPORTED && ret != AltType::LOWPROBABILITY) {
            const char *logMessage = "";
            switch(ret) {
            case AltType::SUPPORTED: case AltType::LOWPROBABILITY:
                assert(false); // not reachable
                break;
            case AltType::MISSING_ALLELE:
                logMessage = "Missing allele '*' (due to upstream deletion) unsupported";
                break;
            case AltType::BREAKEND:
                logMessage = "Breakend variants unsupported";
                break;
            case AltType::STRUCTURAL:
                logMessage = "Structural variants / symbolic alleles unsupported";
                break;
            case AltType::UNKNOWN:
                logMessage = "Unknown ALT-format";
                break;
            case AltType::SAME_AS_REFERENCE:
                logMessage = "No variation, alt == ref";
                break;
//            case AltType::LOWPROBABILITY:
//                logMessage = "Low Probability";
//                break;
            }   
            logger.warn(LOG_PREFIX, logMessage, ", ignored. At",
                        " Chromosome: \"", chromosomeName, '"',
                        " Position: ", record.beginPos,
                        " Ref: \"",  record.ref, '"',
                        " Alt: \"", alts[i], '"',
                        " Prob: ", probabilities[i]);
        }
    }
    return 0;
}

namespace {
template<class TSeq1, class TSeq2>
size_t commonPrefixLength(const TSeq1 &seq1, const TSeq2 &seq2, size_t maxLength) {
    assert(maxLength <= seqan::length(seq1));
    assert(maxLength <= seqan::length(seq2));
    auto beginSeq1 = seqan::begin(seq1);
    return std::mismatch(beginSeq1, beginSeq1 + maxLength, seqan::begin(seq2)).first - beginSeq1;
}

template<class TSeq1, class TSeq2>
std::pair<typename seqan::Infix<TSeq1>::Type, typename seqan::Infix<TSeq2>::Type>
trimCommonPrefixAndSuffix(TSeq1 &seq1, TSeq2 &seq2) {
    size_t maxLength = std::min(seqan::length(seq1), seqan::length(seq2));
    size_t prefixLength = commonPrefixLength(seq1, seq2, maxLength);
    maxLength -= prefixLength;
    size_t suffixLength = commonPrefixLength(seqan::reverseString(seq1), seqan::reverseString(seq2), maxLength);
    return std::make_pair(seqan::infix(seq1, prefixLength, seqan::length(seq1) - suffixLength),
                          seqan::infix(seq2, prefixLength, seqan::length(seq2) - suffixLength));
}
} // namespace

VariantsReader::AltType VariantsReader::processVariant(Chromosome &chromosome, size_t beginPos,
        const seqan::CharString &ref, seqan::CharString &alt, double probability, AddVariantFunction addVariantFunction) {
    // TODO: check if dbSNP RV-entries are not reversed:
    //       ##INFO=<ID=RV,Number=0,Type=Flag,Description="RS orientation is reversed">
    // TODO: handle telomeres (pos = 0, pos = N+1
    // TODO: maybe support: VCFv4.2.pdf: 'If any of the ALT alleles is a symbolic allele (an angle-bracketed
    //                                   ID String "<ID>") then the padding base is required and POS denotes the
    //                                   coordinate of the base preceding the polymorphism.'

//    if (probability >= probabilityThreshold) {
        AltType ret = checkAndConvertAltToUpper(alt);
        if (ret != AltType::SUPPORTED) {
            return ret;
        }
        seqan::Infix<decltype(ref)>::Type infixRef;
        seqan::Infix<decltype(alt)>::Type infixAlt;
        std::tie(infixRef, infixAlt) = trimCommonPrefixAndSuffix(ref, alt);
        const size_t refLength = seqan::length(infixRef);
        const size_t altLength = seqan::length(infixAlt);
        size_t pos = beginPos + seqan::beginPosition(infixRef);
        if (refLength == altLength && refLength == 0) {
            return AltType::SAME_AS_REFERENCE;
        }
        addVariantFunction(chromosome, pos, infixRef, infixAlt, probability);
        return AltType::SUPPORTED;
//    } else {
//        return AltType::LOWPROBABILITY;
//    }
}

void VariantsReader::addVariant(Chromosome& chromosome,
                                size_t pos,
                                const seqan::Infix<const seqan::CharString&>::Type& infixRef,
                                const seqan::Infix<seqan::CharString&>::Type& infixAlt,
                                double probability) {
    const size_t refLength = seqan::length(infixRef);
    const size_t altLength = seqan::length(infixAlt);
    if (refLength == altLength) {
        if (probability >= probabilityThreshold) {
            chromosome.addSNP(pos, infixAlt[0]);
        } else {
            chromosome.addLowProbabilitySNP(pos, infixAlt[0]);
        }
    } else {
        ReferenceString insertionString;
        seqan::assign(insertionString, infixAlt);
        chromosome.addVariant(refLength + pos, refLength, insertionString);
    }
}

VariantsReader::AltType VariantsReader::checkAndConvertAltToUpper(seqan::CharString &alt) {
    for (auto &c : alt) {
        switch (c) {
        case 'a':
        case 'c':
        case 'g':
        case 't':
        case 'n':
            c = std::toupper(c);
        case 'A':
        case 'C':
        case 'G':
        case 'T':
        case 'N':
            continue;
        case '*':
            return AltType::MISSING_ALLELE;
        case '[':
        case ']':
            return AltType::BREAKEND;
        case '>':
        case '<':
            return AltType::STRUCTURAL;
        default:
            return AltType::UNKNOWN;
        }
    }
    return AltType::SUPPORTED;
}

template <typename TStream>
int seqan::readRecord(VcfRecord & record,
                      RecordReader<TStream, SinglePass<> > & reader,
                      VcfIOContext & context,
                      ExtractOnlyChromPosIdRefAlt const & /*tag*/)
{
    clear(record);
    CharString buffer;
    int res = 0;

    // CHROM
    res = readUntilWhitespace(buffer, reader);
    if (res != 0)
        return res;  // Could be EOF_BEFORE_SUCCESS.
    if (!getIdByName(*context.sequenceNames, buffer, record.rID, context.sequenceNamesCache))
    {
        record.rID = length(*context.sequenceNames);
        appendName(*context.sequenceNames, buffer, context.sequenceNamesCache);
    }

    res = skipWhitespaces(reader);
    if (res != 0)
        return res;  // Could be EOF_BEFORE_SUCCESS.

    // POS
    clear(buffer);
    res = readUntilWhitespace(buffer, reader);
    if (res != 0)
        return res;  // Could be EOF_BEFORE_SUCCESS.
////////////////////////////////////////////////
//    if (!lexicalCast2(record.beginPos, buffer))
//        return 1;  // Could not cast number.
    try {
        long res = std::stoul(seqan::toCString(buffer));
        if (res > seqan::MaxValue<decltype(record.beginPos)>::VALUE) {
            logger.err(VariantsReader::LOG_PREFIX, "POS is too big! ", "Value: ", res);
            return 100;
        }
        record.beginPos = res;
    } catch (const std::logic_error &) {
        logger.err(VariantsReader::LOG_PREFIX, "POS is no unsigned integer! ", "Content: \"", buffer, "\"");
        return 101;
    }
////////////////////////////////////////////////
    record.beginPos -= 1;  // Translate from 1-based to 0-based.

    res = skipWhitespaces(reader);
    if (res != 0)
        return res;  // Could be EOF_BEFORE_SUCCESS.

    // ID
    res = readUntilWhitespace(record.id, reader);
    if (res != 0)
        return res;  // Could be EOF_BEFORE_SUCCESS.

    res = skipWhitespaces(reader);
    if (res != 0)
        return res;  // Could be EOF_BEFORE_SUCCESS.

    // REF
    res = readUntilWhitespace(record.ref, reader);
    if (res != 0)
        return res;  // Could be EOF_BEFORE_SUCCESS.

    res = skipWhitespaces(reader);
    if (res != 0)
        return res;  // Could be EOF_BEFORE_SUCCESS.

    // ALT
    res = readUntilWhitespace(record.alt, reader);
    if (res != 0)
        return res;  // Could be EOF_BEFORE_SUCCESS.


    res = skipWhitespaces(reader);
    if (res != 0)
        return res;  // Could be EOF_BEFORE_SUCCESS.

    // QUAL
    clear(buffer);
    res = readUntilWhitespace(buffer, reader);
    if (res != 0)
        return res;  // Could be EOF_BEFORE_SUCCESS.

    if (buffer == ".")
        record.qual = VcfRecord::MISSING_QUAL();
    else if (!lexicalCast2(record.qual, buffer))
        return 1;  // Could not cast.

    res = skipWhitespaces(reader);
    if (res != 0)
        return res;  // Could be EOF_BEFORE_SUCCESS.

    // FILTER
    res = readUntilWhitespace(record.filter, reader);
    if (res != 0)
        return res;  // Could be EOF_BEFORE_SUCCESS.

    res = skipWhitespaces(reader);
    if (res != 0)
        return res;  // Could be EOF_BEFORE_SUCCESS.

    // INFO
    res = readUntilWhitespace(record.info, reader);
    if (res != 0)
        return res;  // Could be EOF_BEFORE_SUCCESS.

    res = skipWhitespaces(reader);
    if (res != 0)
        return res;  // Could be EOF_BEFORE_SUCCESS.

////////////////////////////////////////////////
    res = skipLine(reader); // skip unused fields
#if 0 // skip unused fields

    // FORMAT
    if (length(*context.sampleNames) > 1)
    {
        res = readUntilWhitespace(record.format, reader);
        if (res != 0)
            return res;  // Could be EOF_BEFORE_SUCCESS.
    }

    res = skipWhitespaces(reader);
    if (res != 0)
        return res;  // Could be EOF_BEFORE_SUCCESS.

    // The samples.
    for (unsigned i = 0; i < length(*context.sampleNames); ++i)
    {
        clear(buffer);
        res = readUntilWhitespace(buffer, reader);
        if (res != 0 && res != EOF_BEFORE_SUCCESS)
            return 1;  // Could not read sample information.
        appendValue(record.genotypeInfos, buffer);
        if (atEnd(reader))
        {
            if ((i + 1) != length(*context.sampleNames))
                return 1;  // Not enough fields.
            else
                break;  // Done
        }
        if (value(reader) == '\r' || value(reader) == '\n')
        {
            res = skipLine(reader);
            if (res != 0 && res != EOF_BEFORE_SUCCESS)
                return res;  // Error skipping beyond the end of the line.
        }
        else
        {
            res = skipWhitespaces(reader);
            if (res != 0)
                return res;  // Could be EOF_BEFORE_SUCCESS.
        }
    }

#endif // skip unused fields
////////////////////////////////////////////////

    // Skip empty lines, necessary for getting to EOF if there is an empty line at the ned of the file.
    while (!atEnd(reader) && (value(reader) == '\r' || value(reader) == '\n'))
        if ((res = skipLine(reader)) != 0)
            return res;

    return 0;
}

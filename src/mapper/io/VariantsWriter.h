#ifndef VARIANTSWRITER_H
#define VARIANTSWRITER_H

#include <seqan/vcf_io.h>
#include "types.h"
#include "VcfVariant.h"
#include "Chromosome.h"

class VariantsWriter {
public:

    VariantsWriter(std::ostream& vcfstrm, const Genome& genome) :
        vcfstrm(vcfstrm),
        genome(genome) {
    }

    void writeHeader() {
        // Fill header records.
//        resize(header.records, 2 + genome.size());
//        // @HD header.
//        header.records[0].type = seqan::VCF_HE;
//        resize(header.records[0].tags, 1);
//        // @HD header, tag/value: VN:1.4.
//        header.records[0].tags[0].i1 = "VN";
//        header.records[0].tags[0].i2 = "1.4";
//        // @PG header
//        header.records[1].type = seqan::BAM_HEADER_PROGRAM;
//        resize(header.records[1].tags, 1);
//        header.records[1].tags[0].i1 = "ID";
//        header.records[1].tags[0].i2 = "PG583";
        // @SQ header.
        resize(header.headerRecords, genome.size());
        resize(header.sequenceNames, genome.size());
        for (size_t i = 0; i < genome.size(); ++i) {
            header.sequenceNames[i] = genome[i].name;
            seqan::VcfHeaderRecord& hrec = header.headerRecords[i];
            hrec.key = "contig";
            hrec.value = ("<ID=" + genome[i].name + ",length=" + std::to_string(seqan::length(genome[i].data)) + ">").c_str();
        }

        std::cout << "vcf-head-write" << std::endl;
//        vcfstrm.header = std::move(header);
        context.reset(new seqan::VcfIOContext(header.sequenceNames, header.sampleNames));
        seqan::write(vcfstrm, header, *context, seqan::Vcf());
    }

    void writeVariant(const std::string& name, const VcfVariant& variant) {
        seqan::VcfRecord record;
        record.rID = variant.chrIdx;
        record.beginPos = variant.position;
        record.id = name;
        record.ref = seqan::infix(genome[variant.chrIdx].data, variant.position, variant.position + variant.refLength);
        record.alt = variant.altString;
        record.qual = record.MISSING_QUAL();
        record.info = "CAF=[" + std::to_string(1 - variant.probability) + "," + std::to_string(variant.probability) + "]";
        seqan::writeRecord(vcfstrm, record, *context, seqan::Vcf());
    }

private:
    std::ostream& vcfstrm;
    const Genome& genome;
    seqan::VcfHeader header;
    std::unique_ptr<seqan::VcfIOContext> context;
};


#endif // VARIANTSWRITER_H

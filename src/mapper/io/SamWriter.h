#ifndef PG583_IO_SAMWRITER_H
#define PG583_IO_SAMWRITER_H

#include "align/Alignment.h"
#include "Chromosome.h"
#include "types.h"

#include <seqan/bam_io.h>

class SamWriter {
public:
    SamWriter(seqan::BamStream &bamStream);

    /**
     * @brief writeHeader Emit a header to the stream.
     * @param chromosomes Reference genome(s).
     */
    void writeHeader(const ChromosomeReferences &chromosomes);

    /**
     * @brief writeRead Write a record to the bam stream.
     * @return non-zero on error.
     */
    int writeRead(const seqan::CharString &readName, const ReadString &read, const ReadString &reversedRead,
                  seqan::BamAlignmentRecord &record);
    int writeRead(const seqan::CharString &readName, const ReadString &read, const ReadString &reversedRead,
                  bool isReverseComplement, size_t chromosomeIndex, size_t beginPos, CigarString &cigar);

    static void setAttributes(seqan::BamAlignmentRecord &record, bool isReverseComplement, size_t chromosomeIndex,
                              size_t beginPos, CigarString &cigar);
private:
    seqan::BamStream &bamStream;
};

#endif // PG583_IO_SAMWRITER_H

#ifndef PG583_IO_INDEXWRITER_H
#define PG583_IO_INDEXWRITER_H

#include "module/IndexOptions.h"
#include "module/ModuleOptions.h"
#include "map/LSHimpl.h"
#include "map/HashDataStructure.h"

#include <sstream>
#include <limits>
#include <memory>
#include <stdexcept>
#include <iostream>
#include <string>
#include <utility>
#include <functional>
#include <vector>
#include <cstdint>
#include <algorithm>

namespace IndexFileIdentifiers {
    constexpr const char *FILE               = "PG583MapperIndex";
    constexpr const char *HASH_FUNCTIONS     = "hash-functions";
    constexpr const char *CHROMOSOME_SECTION = "chromosome-section";
    constexpr const char *CHROMOSOME_ENTRY   = "chromosome-entry";
    constexpr const char *INDEX_SECTION      = "index-section";
    constexpr const char *INDEX_ENTRY        = "index-entry";
}

namespace IndexFileConstants {
    constexpr char DELIM = '\t';
    constexpr char NEWLINE = '\n';
    constexpr char ESCAPE = '\\';
}

class IndexFileError : public std::invalid_argument {
public:
    explicit IndexFileError(const std::string &prefix, const std::string &msg) :
        std::invalid_argument(prefix + ": " + msg) {
    }
};

class IndexWriteError : public IndexFileError {
public:
    explicit IndexWriteError(const std::string &msg) :
        IndexFileError("Saving index file", msg) {
    }
};

/// Simple buffer for band hashes used by ::IndexWriter.
template <class TBandHash>
class WindowIndexToBandHashMap : public HashDataStructure<TBandHash>{
public:
    using typename HashDataStructure<TBandHash>::BandHash;
    std::vector<BandHash> map;
    WindowIndexToBandHashMap(size_t windowCount) {
        seqan::ignoreUnusedVariableWarning(windowCount);
    }

    void add(BandHash bandHash, WindowIndex windowIndex) {
        if (windowIndex >= map.size()) {
            map.resize(windowIndex + 1);
        }
        map[windowIndex] = bandHash;
    }
    size_t memoryUsage() const {
        return map.capacity() * sizeof map[0] + sizeof map;
    }
};

/// @brief Writes an LSH index directly to a file while creating the index.
/// @details Specializes ::LSH_Generic by writing the band hashes to file after a chromosome has been indexed by
/// addChromosome().
/// An index file stores the following:
///   - header section:
///     - all parameters with which the corresponding index has been created (see ::IndexOptions)
///     - IDs of the used reference chromosomes
///   - index section:
///     - reference window positions
///     - band hashes
template <class TBandHash = uint32_t,
          class TQGramHash = uint32_t,
          class TQGramHashFunction = GappedQGramHashFunction<uint32_t>>
class IndexWriter : public LSH_Generic<TBandHash,
                                       TQGramHash,
                                       WindowIndexToBandHashMap<TBandHash>,
                                       TQGramHashFunction> {
public:
    using BandHash = typename WindowIndexToBandHashMap<TBandHash>::BandHash;

    /// @brief Opens a file for writing the index to it.
    /// @param filePath Path to the output file.
    void open(const std::string &filePath);

    /// @brief Writes ::IndexOptions and chromsome IDs to the index file.
    /// @param newlyIndexedChromosomes Chromosomes which will be indexed in addition to #chromosomes.
    void writeHeader(const ChromosomeReferences &newlyIndexedChromosomes);

    /// @brief Indexes @chromsome and writes it to the index file.
    /// @param chromosome Chromosome which should be processed.
    void addChromosome(const Chromosome &chromosome);

    /// @brief Writes a chromosome's index data to the index file.
    /// @details Called by addChromosome() and IndexReader::readIndex() since ::IndexReader circumvents addChromosome().
    /// @param newChromosomeIndex Index of the chromosome in the index file.
    void addIndexSection(size_t newChromosomeIndex);
private:
    /// Output stream to the index file.
    std::unique_ptr<std::ostream> os;

    using Base = LSH_Generic<TBandHash, TQGramHash, WindowIndexToBandHashMap<TBandHash>, TQGramHashFunction>;

    /// @brief Helper function to mask special characters in chromosome IDs.
    /// @details Adds escape sequences with IndexFileConstants::ESCAPE for '\0' and members of ::IndexFileConstants.
    static std::string escape(const std::string &str);
};

template <class TIndex>
void addIndexSection(TIndex &index, size_t chromosomeIndex) {
    seqan::ignoreUnusedVariableWarning(index);
    seqan::ignoreUnusedVariableWarning(chromosomeIndex);
}

template <class TBandHash, class TQGramHash, class TQGramHashFunction>
void addIndexSection(IndexWriter<TBandHash, TQGramHash, TQGramHashFunction> &index, size_t chromosomeIndex) {
#pragma omp taskwait
    index.addIndexSection(chromosomeIndex);
}

template <class TBandHash, class TQGramHash, class TQGramHashFunction>
void IndexWriter<TBandHash, TQGramHash, TQGramHashFunction>::open(const std::string &filePath) {
    os.reset(new std::ofstream(filePath, std::ofstream::out | std::ofstream::binary));
}

template <class TBandHash, class TQGramHash, class TQGramHashFunction>
void IndexWriter<TBandHash, TQGramHash, TQGramHashFunction>::writeHeader(
        const ChromosomeReferences &newlyIndexedChromosomes) {
    *os << IndexFileIdentifiers::FILE << IndexFileConstants::NEWLINE;

    IndexOptions indexOptions;
    indexOptions.load(*this);
    for (const Option &intOption : indexOptions.getOptions()) {
        *os << intOption.getArgOptName() << IndexFileConstants::DELIM;
        intOption.printOption(*os) << IndexFileConstants::NEWLINE;
    }

    *os << IndexFileIdentifiers::CHROMOSOME_SECTION << IndexFileConstants::NEWLINE;
    for (const Chromosome &chromosome : Base::chromosomes) {
        *os << IndexFileIdentifiers::CHROMOSOME_ENTRY << IndexFileConstants::DELIM
            << escape(chromosome.id) << IndexFileConstants::NEWLINE;
    }
    for (const Chromosome &chromosome : newlyIndexedChromosomes) {
        *os << IndexFileIdentifiers::CHROMOSOME_ENTRY << IndexFileConstants::DELIM
            << escape(chromosome.id) << IndexFileConstants::NEWLINE;
    }
    *os << IndexFileIdentifiers::INDEX_SECTION << IndexFileConstants::NEWLINE;
    *os << std::flush;
    if (!*os) {
        throw IndexWriteError("Failed to save index header.");
    }
}

template <class TBandHash, class TQGramHash, class TQGramHashFunction>
void IndexWriter<TBandHash, TQGramHash, TQGramHashFunction>::addChromosome(const Chromosome &chromosome) {
    Base::addChromosome(chromosome);

    addIndexSection(Base::chromosomes.size() - 1);
}

template <class TBandHash, class TQGramHash, class TQGramHashFunction>
void IndexWriter<TBandHash, TQGramHash, TQGramHashFunction>::addIndexSection(size_t chromosomeIndex) {
    const size_t windowCount = Base::referenceWindows.size();
    *os << IndexFileIdentifiers::INDEX_ENTRY << IndexFileConstants::DELIM
        << chromosomeIndex << IndexFileConstants::DELIM
        << windowCount << IndexFileConstants::NEWLINE;
    if (!*os) {
        throw IndexWriteError("Failed to save index at chromosome index " + std::to_string(chromosomeIndex) + ".");
    }

    std::vector<Position> beginPositions;
    beginPositions.reserve(windowCount);
    std::transform(Base::referenceWindows.begin(), Base::referenceWindows.end(), std::back_inserter(beginPositions),
                   [](const typename Base::ReferenceWindow &window) { return window.beginPosition; });
    Base::referenceWindows.clear();
    if (!os->write(reinterpret_cast<const char *>(&beginPositions[0]), windowCount * sizeof beginPositions[0])) {
        throw IndexWriteError("Failed to save window position at chromosome index "
                              + std::to_string(chromosomeIndex) + ".");
    }
    for (auto &bandHashTable : Base::bandHashTables) {
        assert(bandHashTable.map.size() == windowCount);
        if (!os->write(reinterpret_cast<const char *>(&bandHashTable.map[0]),
                       windowCount * sizeof bandHashTable.map[0])) {
            throw IndexWriteError("Failed to save band hashes at chromosome index "
                                  + std::to_string(chromosomeIndex) + ".");
        }
        bandHashTable.map.clear();
    }

    *os << IndexFileConstants::NEWLINE << std::flush;
}

template <class TBandHash, class TQGramHash, class TQGramHashFunction>
std::string IndexWriter<TBandHash, TQGramHash, TQGramHashFunction>::escape(const std::string &str) {
    std::string ret;
    ret.reserve(str.size() * 2);

    for (char c : str) {
        switch (c) {
        case '\0':
            ret.push_back(IndexFileConstants::ESCAPE);
            ret.push_back('0');
            break;
        case IndexFileConstants::NEWLINE:
            ret.push_back(IndexFileConstants::ESCAPE);
            ret.push_back('n');
            break;
        case IndexFileConstants::DELIM:
            ret.push_back(IndexFileConstants::ESCAPE);
            ret.push_back('t');
            break;
        case IndexFileConstants::ESCAPE:
            ret.push_back(IndexFileConstants::ESCAPE);
        default:
            ret.push_back(c);
        }
    }

    return ret;
}

#endif // PG583_IO_INDEXWRITER_H

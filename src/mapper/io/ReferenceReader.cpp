#include "io/ReferenceReader.h"
#include "Chromosome.h"
#include "Logger.h"
#include "types.h"

#include <seqan/sequence.h>
#include <seqan/seq_io.h>

ReferenceReader::ReferenceReader(Genome &genome, const std::unordered_set<std::string> &chromosomeNameFilter) :
    genome(genome),
    chromosomeNameFilter(chromosomeNameFilter) {
}

ReferenceReader::ReferenceReader(Genome &genome) :
    ReferenceReader(genome, std::unordered_set<std::string>()) {
}

int ReferenceReader::readReferences(const std::vector<std::string> &referenceFiles) {
    for (size_t i = 0; i < genome.size(); ++i) {
        chromosomeNameToIndex[genome[i].name] = i;
    }

    #pragma omp parallel for schedule(dynamic)
    for (size_t i = 0; i < referenceFiles.size(); ++i) {
        readReferenceFile(referenceFiles[i]);
    }

    for (size_t i = 0; i < genome.size(); i++) {
        Chromosome& chr = genome[i];
        chr.noVariantsString = chr.data;
    }

    return 0;
}

int ReferenceReader::readReferenceFile(const std::string &referenceFile) {
    seqan::SequenceStream refStream(referenceFile.c_str());
    if (!isGood(refStream)) {
        logger.err(LOG_PREFIX, "Could not open reference file: ", referenceFile);
        return 1;
    }
    size_t recordCounter = 0;
    while (!seqan::atEnd(refStream)) {
        if (!chromosomeNameFilter.empty() && genome.size() == chromosomeNameFilter.size()) {
            logger.info(LOG_PREFIX, "Chromosome filter set, all filtered chromosomes read"
                         ", omitting subsequent reference entries");
            break;
        }
        ++recordCounter;

        Chromosome chromosome("");
        int res = seqan::readRecord(chromosome.id, chromosome.data, refStream);
        if (res != 0) {
            logger.err(LOG_PREFIX, "Could not read record number ", recordCounter,
                       " in reference file: ", referenceFile);
            return 1;
        }

        chromosome.name = Chromosome::generateName(seqan::toCString(chromosome.id));
        if (chromosome.name.empty()) {
            logger.err(LOG_PREFIX, "Could not deduce chromosome id for reference: \"", chromosome.id, '"');
            continue;
        }
        if (!chromosomeNameFilter.empty() && chromosomeNameFilter.find(chromosome.name) == chromosomeNameFilter.end()) {
            logger.info(LOG_PREFIX, "Chromosome filter set. ",
                        "Omitting chromosome: \"", chromosome.name, "\", id: \"", chromosome.id, '"');
            continue;
        }
        {
            LockGuard lock(genomeMutex);
            auto chromosomeIndex = chromosomeNameToIndex.find(chromosome.name);
            if (chromosomeIndex != chromosomeNameToIndex.end()) {
                auto warn = [&](const std::string &type, const std::string &value) {
                    logger.warn(LOG_PREFIX, "Duplicate reference chromosome ignored. ", type, ": \"", value, '"'); };
                warn("Chromosome", chromosome.name);
                warn("Previous id", genome[chromosomeIndex->second].id);
                warn("Current id", seqan::toCString(chromosome.id));
                continue;
            }
            if (seqan::length(chromosome.data) == 0) {
                logger.warn(LOG_PREFIX, "Chromosome with zero length ignored. ", "id: ", chromosome.id);
                continue;
            } else {
                chromosomeNameToIndex.emplace(chromosome.name, genome.size());
                logger.info(LOG_PREFIX, "Done reading", " length=", seqan::length(chromosome.data),
                            " name=", chromosome.name, " id=", chromosome.id);
                genome.emplace_back(std::move(chromosome));
            }
        }
    }
    return 0;
}

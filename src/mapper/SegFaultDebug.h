#ifndef PG583_SEGFAULTDEBUG_H
#define PG583_SEGFAULTDEBUG_H

#ifdef __linux__
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#endif

#include <unordered_map>
#include <vector>

struct DebugInfos {
    struct ThreadInfo {
        bool used = false;
        bool in_createWindowBandHashes = false;
        bool in_createQGramHashes_iupac = false;
        bool in_addReferenceWindowHash = false;
        size_t bht_num = -1;
        int pos_bht_add = 0;
    };

    bool in_addChromsome = false;
    bool in_addReferenceWindows = false;
    bool in_createReferenceWindowBandHashes = false;
    int chrIdx = -1;
    int lastWinPos = -1;
    int winStartPos = -1;
    int pos_addReferenceWindows = 0;
    int first = -1;
    int last = -1;

    std::vector<ThreadInfo> thread;

    DebugInfos() : thread(1000) {}
};

extern DebugInfos debugInfos;

void setSegFaultAction();

#ifdef __linux__
void segfault_sigaction(int signal, siginfo_t *si, void *arg);
#endif

void printDebugInfo();

#endif // PG583_SEGFAULTDEBUG_H

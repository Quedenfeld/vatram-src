#ifndef PG583_LOGGER_H
#define PG583_LOGGER_H

#include <sstream>
#include <iostream>

class Logger {
    std::ostream &infoStream;
    std::ostream &warnStream;
    std::ostream &errStream;
public:
    Logger(std::ostream &os = std::cerr);
    Logger(std::ostream &infoStream, std::ostream &warnStream, std::ostream &errStream);
    template <class... Args>
    std::ostream& info(Args... args) {
        return write(infoStream, "INFO", args...);
    }
    template <class... Args>
    std::ostream& warn(Args... args) {
        return write(warnStream, "WARNING", args...);
    }
    template <class... Args>
    std::ostream& err(Args... args) {
        return write(errStream, "ERROR", args...);
    }

private:
    template <class... Args>
    std::ostream& write(std::ostream &os, const char *prefix, Args... args) {
        std::ostringstream oss;
        append(oss, prefix, ": ", args...);
        return os << oss.str() << std::flush;
    }

    template <class T, class... Args>
    void append(std::ostream &os, T t, Args... args) {
        append(os << t, args...);
    }

    void append(std::ostream &os) {
        os << '.' << std::endl;
    }
};

extern Logger logger;

#endif // PG583_LOGGER_H

#ifndef VCFVARIANT_H
#define VCFVARIANT_H

#include "types.h"

//struct SnpVariant {
//    double probability;
//    Position position;
//    ChromosomeIndex chrIdx;
//    ReadChar base;

//    SnpVariant(ChromosomeIndex chrIdx, Position position, double probability, ReadChar base) :
//        probability(probability),
//        position(position),
//        chrIdx(chrIdx),
//        base(base) {
//    }

//    bool operator< (const VcfVariant& o) const {
//        if (chrIdx == o.chrIdx) {
//            return position < o.position;
//        } else {
//            return chrIdx < o.chrIdx;
//        }
//    }
//};

//struct VariantOperation {
//    typedef uint32_t Length;
//    virtual Length getLength() const = 0;
//    virtual ReferenceString getString() const = 0;
//};

//struct VariantOperation_Stanadard : public VariantOperation {
//    Length refLength;
//    ReferenceString altString;
//    virtual Length getLength() const {return refLength;}
//    virtual ReferenceString getString() const {return altString;}

//    VariantOperation_Stanadard(Length refLenth = 0, const ReferenceString& altString) :
//        refLength(refLength),
//        altString(altString) {
//    }
//};

struct VcfVariant {
    typedef uint32_t Length;

    double probability;
    Position position;
    ChromosomeIndex chrIdx;
    Length refLength;
    ReferenceString altString;

    VcfVariant(ChromosomeIndex chrIdx, Position pos, double probability = 0.0, Length refLenth = 0, ReferenceString altString = ReferenceString()) :
        probability(probability),
        position(pos),
        chrIdx(chrIdx),
        refLength(refLenth),
        altString(altString) {
    }

//    VcfVariant(const SnpVariant& snp) :
//        probability(snp.probability),
//        position(snp.position),
//        chrIdx(snp.chrIdx),
//        refLength(1),
//        altString(snp.base) {
//    }

    bool operator< (const VcfVariant& o) const {
        if (chrIdx == o.chrIdx) {
            return position < o.position;
        } else {
            return chrIdx < o.chrIdx;
        }
    }
};



#endif // VCFVARIANT_H

#include "GenomeGenerator.h"
#include "io/SamWriter.h"
#include "align/Variant.h"
#include "Chromosome.h"
#include "Logger.h"

#include <seqan/seq_io.h>
#include <seqan/bam_io.h>
#include <seqan/sequence.h>

#include <ctime>
#include <cmath>

    /**
     * @brief GenomeGenerator::GenomeGenerator Default-Constructor with random seed
     */
    GenomeGenerator::GenomeGenerator() :
        GenomeGenerator(std::time(nullptr)) {
    }

    /**
     * @brief GenomeGenerator::GenomeGenerator Default-Constructor with fixed seed
     * @param seed fixed value, used for the seed
     */
    GenomeGenerator::GenomeGenerator(size_t seed){
        rand.seed(seed);
    }

    /**
     * @brief GenomeGenerator::GenomeGenerator Constructor with parameters for different properties for the generated Genome
     * @param id for the chromosome
     * @param length of the created chromosome
     * @param numberReads to be generated
     * @param seed value for random values
     */
    GenomeGenerator::GenomeGenerator(const std::string &id, const size_t &length, const size_t &numberReads, size_t seed):
        id(id),
        length(length),
        numberReads(numberReads) {
        rand.seed(seed);
    }

    /**
     * @brief GenomeGenerator::probability auxiliary function for easy use of probabilities
     * @param prob given probability for the event.
     * @return
     * This auxiliary function is built on a random number generator in the interval [0, 1]
     * It returns true, if the given double number is greater than the generated number.
     */
    bool GenomeGenerator::probability(double prob){
        double randDouble = (double)rand() / rand.max();
        return randDouble < prob;
    }

    /**
     * @brief GenomeGenerator::iupacToDna5 Conversation between iupac-alphabet and dna5-alphabet.
     * @param string referecence of sequence to be converted.
     * @return seqan::Dna5String of the given seqan::IupacString
     * This function helps with the conversation from the iupac-alphabet and the dna5-alphabet.
     * Since there are only 5 different characters possible in the dna5-alphabet, there is no deterministic result.
     * One Iupac-character could be translated to a range of different dna5-characters. If there is a choice,
     * randomly one suitable dna5-character is chosen.
     */
    seqan::Dna5String GenomeGenerator::iupacToDna5(const seqan::IupacString& string){
        seqan::Dna5String dnaString;
        for (size_t i = 0; i < seqan::length(string); i++){
//            std::cout << "," << int(string[i]) << std::flush;
            // Just copy the Nucleotid, if it is 'A', 'C', 'G', 'T'
            if ((size_t)seqan::ordValue(string[i]) == 1 || (size_t)seqan::ordValue(string[i]) == 2 || (size_t)seqan::ordValue(string[i]) == 4 || (size_t)seqan::ordValue(string[i]) == 8){
                dnaString += string[i];
            }
            // Otherwise choose randomly a Nucleotid, covered by the IupacChar
            else {
                size_t random = rand() % 4;
                if (seqan::ordValue(string[i]) == 0) {
                    logger.err("U contained in generated read String ", string);
                    continue;
                }
                while ((seqan::ordValue(string[i]) & (1 << random)) == 0){
                    random = rand() % 4;
                }
                dnaString += seqan::Dna(seqan::Iupac(1 << random));
            }
        }
        return dnaString;
    }

    /**
     * @brief GenomeGenerator::generateReference generates a synthetic reference to a given filepath
     * @param length of the reference to be generated
     * @param filepath of the FASTA-file to be generated
     */
    void GenomeGenerator::generateReference(size_t length, std::string filepath){
        std::ofstream file_genome(filepath);
        seqan::Dna5String sequence;

        for (size_t i = 0; i < length; i++) {
            sequence += rand() % 4;
        }

        seqan::writeRecord(file_genome, "random-generated genome", sequence, seqan::Fasta());
        file_genome.flush();
        seqan::clear(sequence);
    }

    /**
     * @brief GenomeGenerator::createChromosome creates a synthetic reference in a Chromosome object.
     * @param length of the Chromosome to be created
     * @return created Chromosome-object
     */
    Chromosome GenomeGenerator::createChromosome(size_t length){
        seqan::IupacString sequence;

        for (size_t i = 0; i < length; i++) {
            sequence += 1 << (rand() % 4);
        }

        Chromosome chromosome("RandomGeneratedChromosome", sequence);
        return chromosome;
    }

    /**
     * @brief GenomeGenerator::addSNP adds a Single Nucleotid Polymorphism to the Chromosome at a specified position
     * @param chromosome to be altered
     * @param pos to be changed
     */
    void GenomeGenerator::addSNP(Chromosome &chromosome, size_t pos){
        seqan::Iupac currentNucleotid = chromosome.data[pos];
        seqan::Iupac randomNucleotid(1 << (rand() % 4));

        while (currentNucleotid == randomNucleotid){
            randomNucleotid = 1 << (rand() % 4);
        }

        seqan::Iupac combinedNucleotid(currentNucleotid.value | randomNucleotid.value);
        chromosome.data[pos] = combinedNucleotid;
    }

    /**
     * @brief GenomeGenerator::addSNPbyIupac combines a nucleotid in a Chromosome-object with a given Iupac-character
     * @param chromosome to be altered
     * @param pos to be chanced
     * @param base to be combined with nucleotid on specified position
     */
    void GenomeGenerator::addSNPbyIupac(Chromosome &chromosome, size_t pos, seqan::Iupac base){
        seqan::Iupac currentNucleotid = chromosome.data[pos];
        seqan::Iupac combinedNucleotid(currentNucleotid.value | base.value);
        chromosome.data[pos] = combinedNucleotid;
    }

    /**
     * @brief GenomeGenerator::addSNPs adds a given number of SNPs with a specified length to the Chromosome
     * @param chromosome to be altered
     * @param numberOfSNPs to be added
     * @param lengthOfSNP
     */
    void GenomeGenerator::addSNPs(Chromosome &chromosome, size_t numberOfSNPs, size_t lengthOfSNP){
        for (size_t i = 0; i < numberOfSNPs; i++){
            size_t randomPosition = rand() % (seqan::length(chromosome.data) - lengthOfSNP);
            for (size_t offset = 0; offset < lengthOfSNP; offset++){
                addSNP(chromosome, randomPosition + offset);
            }
        }
    }

    /**
     * @brief GenomeGenerator::addSNPwithProbabilty adds SNPs with a specified length to the Chromosome, if the random-event is satisfied
     * @param chromosome to be altered
     * @param snpProbability probability of the event
     * @param lengthOfSNP
     */
    void GenomeGenerator::addSNPwithProbabilty(Chromosome &chromosome, double snpProbability, size_t lengthOfSNP){
        if (probability(snpProbability)){
            size_t randomPosition = rand() % (seqan::length(chromosome.data) - lengthOfSNP);
            for (size_t offset = 0; offset < lengthOfSNP; offset++){
                addSNP(chromosome, randomPosition + offset);
            }
        }
    }

    /***********************************************************************************
     * Following Functions create Reads in the format defined in GenomeGenerator::Read *
     ***********************************************************************************/

    /**
     * @brief GenomeGenerator::createRead Creates a single read with given properties in the GenomeGenerator::Read Struct
     * @param chromosome source of the read to be created
     * @param readLength length of the read to be created
     * @param substitutionProbability probabilty of substituion of single bases
     * @param indelProbability probability of InDels of single bases
     * @return Read in the Struct: GenomeGenerator::Read
     */
    GenomeGenerator::Read GenomeGenerator::createRead(const Chromosome &chromosome, size_t readLength, double substitutionProbability, double indelProbability){
        const ReferenceString &reference = chromosome.data;

        seqan::IupacString currentRead;

        // We do not want to construct a read i region with too many 'N's in the reference
        bool tooManyNs = true;
        size_t startPos;
        do {
            startPos = rand() % (seqan::length(reference) - readLength); // choose a random start position
            // Count the 'N's in the reference beginning at startPos and ending where the read ends
            size_t countN = 0;
            size_t end = startPos + readLength;
            for (size_t i = startPos; i < end; i++) {
                if (reference[i] == 'N') {
                    countN++;
                }
            }
            // The while-loop is left, if there are only a few 'N's in the string.
            if (countN < MAXIMAL_N_RATIO_IN_READ * readLength) {
                tooManyNs = false;
            }
        } while (tooManyNs);

        GenomeGenerator::Read read;

        for (size_t j = 0; j < readLength; j++){
            // Indels
            if (probability(indelProbability)){
                if (probability(0.5)){
                    currentRead += seqan::Iupac(reference[startPos + j]);
                    currentRead += 1 << (rand() % 4);
//                    read.insertions.push_back(GenomeGenerator::Insertion({j, 1}));
                }
                else {
//                    read.deletions.push_back(GenomeGenerator::Deletion({j, 1}));
                }
            }
            // Substitutions
            else if (probability(substitutionProbability)){
                ReferenceChar currentNucleotid = reference[startPos + j];
                ReferenceChar randomNucloetid = 1 << (rand() % 4);

                while (currentNucleotid == randomNucloetid){
                    randomNucloetid = 1 << (rand() % 4);
                }
                currentRead += randomNucloetid;
//                read.substitution.push_back(GenomeGenerator::Substitution({j, 1}));
            }
            // Else take the Nucleotid from the reference without change
            else {
                currentRead += seqan::Iupac(reference[startPos + j]);
            }
        }
        currentRead = iupacToDna5(currentRead);
        read.readString = currentRead;
        read.startPos = startPos;
        read.chromosome = &chromosome;
        read.reversedComplement = false;
        return read;
    }


    GenomeGenerator::Read GenomeGenerator::createReadFromVariantAndWriteSam(const Chromosome &chromosome, const VariantIndex &varIndex, size_t readLength, double varProb, double insProb, double delProb, double subsProb, double snpProb, std::string filename, std::string read_id) {
        GenomeGenerator::Read r = createReadWithVariants(chromosome, varIndex, readLength, varProb, insProb, delProb, subsProb, snpProb);

        //CigarString cs(seqan::CigarElement<>('M', readLength));
        CigarString cs;
        seqan::CigarElement<> manyElements('M', seqan::length(r.readString));
        seqan::append(cs, manyElements);

        /*for(int i = 0; i < readLength; i++) {
            cs += 'M';
        }*/

        seqan::BamStream bamstream(filename.c_str(), seqan::BamStream::WRITE);

        ChromosomeReferences cr({chromosome});

        SamWriter sw(bamstream);
        sw.writeHeader(cr);
        ReadString rs = r.readString;
        sw.writeRead(read_id, r.readString, seqan::reverseComplementString(rs), r.reversedComplement, 0, r.startPos, cs);

        return r;
    }

    std::vector<GenomeGenerator::Read> GenomeGenerator::createReadsAndSamWithVariants(const Genome & genome,
                                                                 size_t readLength,
                                                                 double varProb,
                                                                 double insProb,
                                                                 double delProb,
                                                                 double subsProb,
                                                                 double snpProb,
                                                                 int numberOfReads,
                                                                 std::string filename) {

        seqan::BamStream bamstream(filename.c_str(), seqan::BamStream::WRITE);

        ChromosomeReferences cr;
        for (const Chromosome& chr: genome) {
            cr.push_back(chr);
        }



        SamWriter sw(bamstream);
        sw.writeHeader(cr);

        size_t length = 0;
        for (const Chromosome& chr: genome) {
            length += seqan::length(chr.data);
        }

        size_t id_cnt = 0;

        std::vector<GenomeGenerator::Read> reads;
        for (size_t i = 0; i < genome.size(); i++) {

            const Chromosome& chromosome = genome[i];
            size_t chromLength = seqan::length(chromosome.data);
            VariantIndex varIndex(chromosome.variants, chromLength);

            size_t num = numberOfReads * double(double(chromLength) / double(length));
            std::vector<GenomeGenerator::Read> reads_chr = createReadsWithVariants(chromosome, varIndex, num, readLength,
                                                                               varProb, insProb, delProb, subsProb, snpProb);
            for (auto& read: reads_chr)  {
                reads.push_back(read);
            }

            for(size_t k=0; k < reads_chr.size(); k++) {
                Read& r = reads_chr[k];
                ReadString rs = r.readString;
                std::string read_id = std::string("read") + std::to_string(id_cnt);

                CigarString cs;
                seqan::CigarElement<> manyElements('M', seqan::length(r.readString));
                seqan::append(cs, manyElements);

                seqan::reverseComplementString(rs);
                sw.writeRead(read_id, r.readString, rs, r.reversedComplement, i, r.startPos, cs);

                id_cnt++;
            }
        }



        return reads;
    }

    std::vector<GenomeGenerator::Read> GenomeGenerator::createReads(const Chromosome &chromosome, size_t numberOfReads, size_t readLength, double substitutionProbability, double indelProbability) {
        std::vector<GenomeGenerator::Read> reads;
        for (size_t i = 0; i < numberOfReads; i++) {
            reads.push_back(createRead(chromosome, readLength, substitutionProbability, indelProbability));
        }
        return reads;
    }

    /* Gibt eine List mit gültigen Varianten wieder.
     * Deletions, die beispielsweise in eine vorherige Variante ragen werden ignoriert
     */
    std::vector<Variant> GenomeGenerator::getVariants(const Chromosome &chromosome, const VariantIndex &varIndex, size_t pos, size_t lastEnd) {

        const size_t chromosomeLength = seqan::length(chromosome.data);
        std::vector<Variant>::const_iterator itVariant = varIndex.getNextVariant(pos, chromosomeLength);
//        size_t nextVarPos = std::numeric_limits<size_t>::max();
        std::vector<Variant> variants;
        if (itVariant != varIndex.getVariantList().end()) {
//            nextVarPos = itVariant->getPosition();
        } else {
            return variants;
        }

        for (; itVariant != varIndex.getVariantList().end() && itVariant->getPosition() == pos; ++itVariant) {
            const Variant &var = *itVariant;

            if(var.getPosition() - var.getDeletionLength() + seqan::length(var.getInsertionString()) > lastEnd) {
                variants.push_back(var);
            }
        }

        return variants;

    }

    void GenomeGenerator::handleVar(const Chromosome &chromosome, const VariantIndex &varIndex, seqan::IupacString &readString,
                                    const size_t &refIndex, size_t &readIndex, size_t &lastEnd, size_t readLength, double prob) {

        if(!probability(prob)) return;

        // get variants at current position and choose one uniformly at random (or abort if no variant exists)
        std::vector<Variant> variants = getVariants(chromosome, varIndex, refIndex, lastEnd);
        if (variants.size() == 0) return;


        int rPos = rand() % (variants.size());
        Variant &chosen = variants[rPos];


        // Wenn an der Stelle refI ( index für deie Referenz) eine Variante ist, dann bearbeiten wir diese
        //  Im falle einer Insertion bleiben wir mit refI an der Stelle stehen
        //  Im falle einer Deletion setzen wir readI um Deletion.length zurück (da wir schon kopiert haben),

        if(chosen.getDeletionLength() > 0) {
            readIndex -= std::min(chosen.getDeletionLength(), readIndex);
            seqan::resize(readString, readIndex);
        }

        ReferenceString insString = chosen.getInsertionString();
        size_t insLength = seqan::length(insString);

        if(insLength > 0) {;

            // Gesamte Insertion kopieren, kappen, wenn länger als readLength!!
            for(size_t i = 0; i < insLength && readIndex < readLength; i++) {
                readString += seqan::Iupac(insString[i]);
                readIndex++;
            }
        }

        // merken welche Variante wir bearbeitet haben
        lastEnd = chosen.getPosition() - chosen.getDeletionLength() + insLength;
    }

    void GenomeGenerator::doRand(double in, double del, double subs, double snp, seqan::IupacString &readString, size_t readLength) {
        seqan::IupacString randomInsertion;
        size_t randomInsertionLength = 1;

        for(size_t i = 0; i < readLength; i++) {
            if(probability(in)) {
                // an der Stelle i Insertion

                randomInsertionLength = 1;

                for (size_t j = 0; j < randomInsertionLength; j++){
                    randomInsertion += (1 << rand() % 4);
                }
                // TODO: index ende :-)
                if (i < seqan::length(readString))
                    seqan::replace(readString, i, i, randomInsertion);

            }

            if(probability(del)) {;
                if (i < seqan::length(readString))
                    seqan::erase(readString, i); // default deletion 1

            }

            if(probability(subs)) {
                randomInsertionLength = 1;

                for (size_t j = 0; j < randomInsertionLength; j++){
                    randomInsertion += (1 << rand() % 4);
                }

                // TODO: index :-)
                if (i+randomInsertionLength < seqan::length(readString))
                    seqan::replace(readString, i, i+randomInsertionLength, randomInsertion);

            }

            if(probability(snp)) {
                // TODO: SNP inflicted
            }

        }

    }

    /*
     * Generiert aus der Referenz und seinen Varianten einen Zufallsread
     */
    GenomeGenerator::Read GenomeGenerator::createReadWithVariants(const Chromosome &chromosome,
                                                                 const VariantIndex &varIndex,
                                                                 size_t readLength,
                                                                 double varProb,
                                                                 double insProb,
                                                                 double delProb,
                                                                 double subsProb,
                                                                 double snpProb) {
        const ReferenceString &reference = chromosome.data;
        const size_t referenceLength = seqan::length(reference);
        GenomeGenerator::Read readContainer;
        seqan::IupacString readString;

        // Index holen.
        // Variante rausholen
        // Ab hier kann man durchiterieren.

        // We do not want to construct a read i region with too many 'N's in the reference
        bool tooManyNs = true;
        size_t startPos;
        do {
            startPos = rand() % (referenceLength - readLength); // choose a random start position

            // checken ob startPos in einem deletion liegt
            // ans Ende der Deletion Springen

            // Count the 'N's in the reference beginning at startPos and ending where the read ends
            size_t countN = 0;
            size_t end = startPos + readLength;
            for (size_t i = startPos; i < end; i++) {
                if (reference[i] == 'N') {
                    countN++;
                }
            }
            // The while-loop is left, if there are only a few 'N's in the string.
            if (countN < MAXIMAL_N_RATIO_IN_READ * readLength) {
                tooManyNs = false;
            }
        } while (tooManyNs);

        /*--------------------------------------------------------------------------*/

        size_t refIndex = startPos;
        size_t readIndex = 0;
        size_t lastEnd = 0;

        // Wir erstellen einen Read der (readLength)
        while(readIndex < readLength && refIndex < referenceLength) {
            handleVar(chromosome, varIndex, readString, refIndex, readIndex, lastEnd, readLength, varProb);
            readString += seqan::Iupac(reference[refIndex++]);
            readIndex++;
        }

        // Das hab ich nicht verstanden.
        if (readIndex < readLength) {
            // TODO: Handle reads that are smaller than readLength (i.e. re-execute the current function or the like).
        } else if (readIndex > readLength) {
            seqan::resize(readString, readLength);
            // TODO: If an insertion string is cut off by resize, read.insertions (read.substitutions) may need to be fixed.
        }

        doRand(insProb, delProb, subsProb, snpProb, readString, seqan::length(readString));

        // Pack things into read
        readContainer.readString = iupacToDna5(readString);
        readContainer.startPos = startPos;
        readContainer.chromosome = &chromosome;
        readContainer.reversedComplement = false;
        return readContainer;
    }

    std::vector<GenomeGenerator::Read> GenomeGenerator::createReadsWithVariants(const Chromosome &chromosome,
                                                                 const VariantIndex &varIndex,
                                                                 size_t numberOfReads,
                                                                 size_t readLength,
                                                                 double varProb,
                                                                 double insProb,
                                                                 double delProb,
                                                                 double subsProb,
                                                                 double snpProb) {
        std::vector<GenomeGenerator::Read> reads;
        for (unsigned int i = 0; i < numberOfReads; i++) {
            reads.push_back(createReadWithVariants(chromosome, varIndex, readLength, varProb, insProb, delProb, subsProb, snpProb));
        }
        return reads;
    }

    /**********************************************************************
     * Following functions generate Reads with output in the FASTQ-Format *
     **********************************************************************/

    /**
     * @brief GenomeGenerator::createReads Creates reads of a given FASTA-File with specified properties. Output is a FASTQ-File
     * @param input_path FASTA-File with reference
     * @param numberOfReads amount of reads to be created
     * @param readLength length of the reads to be created
     * @param substitutionProbability probability of substitution of single bases
     * @param indelProbability probability of InDels of single bases
     * @param output_path output for FASTQ-Format
     */
    void GenomeGenerator::createReads(std::string input_path, size_t numberOfReads, size_t readLength, double substitutionProbability, double indelProbability, std::string output_path){
        seqan::Dna5String reference;
        Chromosome chr("autogenerated-chromosome");

        seqan::SequenceStream refStream(input_path.c_str());
        if (!isGood(refStream)){
            std::cout << "Fehler beim Einlesen der Daten" << std::endl;
        }

        seqan::CharString id = chr.id;
        seqan::CharString refGenome;

        seqan::readRecord(id, refGenome, refStream);
        seqan::assign(chr.data, refGenome);

        reference = chr.data;

        createReads(chr, numberOfReads, readLength, substitutionProbability, indelProbability, output_path);
        }

    /**
     * @brief GenomeGenerator::createReads Creates reads of a given Chromosome-object with specified properties. Output is a FASTQ-File
     * @param chromosome Chromosome-object with reference
     * @param numberOfReads amount of reads to be created
     * @param readLength length of the reads to be created
     * @param substitutionProbability probability of substitution of single bases
     * @param indelProbability probability of InDels of single bases
     * @param output_path output for FASTQ-Format
     */
    void GenomeGenerator::createReads(const Chromosome &chromosome, size_t numberOfReads, size_t readLength, double substitutionProbability, double indelProbability, std::string output_path){
        std::vector<GenomeGenerator::Read> reads = createReads(chromosome, numberOfReads, readLength, substitutionProbability, indelProbability);

        std::ofstream file(output_path);
        for (size_t i = 0; i < reads.size(); i++) {
            ReadString read = reads[i].readString;
            seqan::writeRecord(file, std::to_string(i), read, seqan::Fastq());
        }
        file.flush();
    }

    /**
     * @brief GenomeGenerator::createReads Creates reads of a given FASTA-File with specified properties. Output is a GenomeGenerator::Read Struct
     * @param input_path
     * @param numberOfReads amount of reads to be created
     * @param readLength length of the reads to be created
     * @param substitutionProbability probability of substitution of single bases
     * @param indelProbability probability of InDels of single bases
     * @return Struct of GenomeGenerator::Read
     */
    std::vector<GenomeGenerator::Read> GenomeGenerator::createReads(std::string input_path, size_t numberOfReads, size_t readLength, double substitutionProbability, double indelProbability){
        seqan::Dna5String reference;
        Chromosome chr("autogenerated-chromosome");

        seqan::SequenceStream refStream(input_path.c_str());
        if (!isGood(refStream)){
            std::cout << "Fehler beim Einlesen der Daten" << std::endl;
        }

        seqan::CharString id = chr.id;
        seqan::CharString refGenome;

        seqan::readRecord(id, refGenome, refStream);
        seqan::assign(chr.data, refGenome);

        reference = chr.data;
        return createReads(chr, numberOfReads, readLength, substitutionProbability, indelProbability);
    }

    /**
     * @brief GenomeGenerator::generateIndels Generates InDels in a given Chromosome-objects
     * @param chromosome as source for the reference
     * @param numberOfIndels amount of indels to be generated
     * @param maxLength maximum length of indels
     * @return vector of Variant-objects
     */
    std::vector<Variant> GenomeGenerator::generateIndels(Chromosome &chromosome, size_t numberOfIndels, size_t maxLength){
        size_t chrLength = seqan::length(chromosome.data);

        std::vector<Variant> variants;
        for (size_t i = 0; i < numberOfIndels; i++){
            Variant::Type type = static_cast<Variant::Type>(rand() % 3);
            size_t randomInsertionLength = 0;
            size_t randomDeletionLength = 0;
            seqan::IupacString randomInsertion;
            switch(type) {
            case Variant::Type::INSERTION: {
                randomInsertionLength = 1 + (rand() % (maxLength-1));
                for (size_t j = 0; j < randomInsertionLength; j++){
                    randomInsertion += (1 << rand() % 4);
                }
                break;
            }
            case Variant::Type::DELETION: {
                randomDeletionLength = 1 + (rand() % (maxLength-1));
                break;
            }
            case Variant::Type::SUBSTITUTION: {
                randomInsertionLength = 1 + (rand() % (maxLength-1));
                for (size_t j = 0; j < randomInsertionLength; j++){
                    randomInsertion += (1 << rand() % 4);
                }
                randomDeletionLength = 1 + (rand() % (maxLength-1));
                break;
            }
            default:
                std::cout<<type<<std::endl;
            }
            size_t randomPos = rand() % (chrLength - randomDeletionLength);
            variants.push_back(addIndel(chromosome, randomPos, randomInsertion, randomDeletionLength));
        }
        std::sort(variants.begin(), variants.end());
        return variants;
    }

    // TODO: Testen
    VariantIndex GenomeGenerator::generateVariantIndex(const Chromosome &chromosome, std::vector<Variant> variants){
        VariantIndex varIndex(variants, seqan::length(chromosome.data));
        return varIndex;
    }

    /**
     * @brief GenomeGenerator::addIndel Generation of a single variant with given insertionString or deletionLength.
     * If length of the insertionString is equal to the deletionLength, a substitution is executed.
     * @param chromosome as source for the reference
     * @param pos of the Indel
     * @param insertionString sequence to be inserted
     * @param deletionLength number of bases to be deleted
     * @return
     */
    Variant GenomeGenerator::addIndel(Chromosome &chromosome, size_t pos, seqan::IupacString &insertionString, size_t deletionLength){

        if (seqan::length(chromosome.data) < pos || (seqan::length(chromosome.data) < (pos + deletionLength))){
            std::cerr << "given positions is out of bounds" << std::endl;
            Variant indel;
            return indel;
        }
        else {
            // Insertion
            if (seqan::length(insertionString) > 0 && deletionLength == 0){
                Variant insertion(pos, insertionString);
                return insertion;
            }
            // Deletion
            else if (seqan::length(insertionString) == 0 && deletionLength > 0){
                Variant deletion(pos, deletionLength);
                return deletion;
            }
            // Substitution
            else {
                Variant substitution(pos, insertionString, deletionLength);
                return substitution;
            }
        }
    }

// TODO: Delete if not needed.
// Old generate* functions from Mapper.cpp.
// UniformIntRng: Convenience class for generating uniformly distributed random numbers using seqan::MinValue / MaxValue.
namespace {

template<class T, class Engine = std::minstd_rand>
class UniformIntRng {
public:
    typedef std::uniform_int_distribution<T> Distribution;
    Distribution distribution;
    Engine engine;

    UniformIntRng(Distribution distribution, Engine engine = Engine(std::time(nullptr))) :
        distribution(distribution),
        engine(engine) {
    }

    template<class ...DistributionArgs>
    UniformIntRng(DistributionArgs... distributionArgs) :
        UniformIntRng(Distribution(distributionArgs...)) {
    }

    T operator()() {
        return distribution(engine);
    }
};

template<typename TValue, typename TSpec, class Engine>
class UniformIntRng<seqan::SimpleType<TValue, TSpec>, Engine> {
    typedef seqan::SimpleType<TValue, TSpec> T;
public:
    typedef std::uniform_int_distribution<typename seqan::Value<T>::Type> Distribution;
    Distribution distribution;
    Engine engine;

    UniformIntRng(Distribution distribution, Engine engine = Engine(std::time(nullptr))) :
        distribution(distribution),
        engine(engine) {
    }

    template<class ...DistributionArgs>
    UniformIntRng(DistributionArgs... distributionArgs) :
        UniformIntRng(Distribution(distributionArgs...)) {
    }

    UniformIntRng() : UniformIntRng(seqan::MinValue<T>::VALUE, seqan::MaxValue<T>::VALUE) {
    }

    T operator()() {
        return distribution(engine);
    }
};

#if 0
void generateRandomChromosome(size_t genomeLength = 1000 /* 1000*1000*30 */,
                              const std::string &filename = "./genome.fasta") {
    std::ofstream genomeOutputFile(filename);

    typedef seqan::Dna RefChar;
    UniformIntRng<RefChar> baseRng;
    seqan::String<RefChar> genome;
    seqan::resize(genome, genomeLength);
    for (auto &c : genome) {
        seqan::assign(c, baseRng());
    }
    seqan::writeRecord(genomeOutputFile, "0", genome, seqan::Fasta());
    genomeOutputFile.flush();
}
#endif

template<class TString>
void generateReads(const TString &genome, size_t numReads, size_t maxErrors = 4, size_t readLength = 100,
                   const std::string &filename = "./reads.fasta") {
    if(seqan::length(genome) < readLength) {
        return;
    }

    typedef seqan::Dna ReadChar;

    UniformIntRng<size_t> startPosRng(0, length(genome) - readLength);
    UniformIntRng<size_t> errorPosRng(0, readLength - 1);
    UniformIntRng<ReadChar> baseRng;

    std::ofstream readsOutputFile(filename);
    for (size_t readIndex = 0; readIndex < numReads; readIndex++) {
        seqan::String<ReadChar> read = seqan::infixWithLength(genome, startPosRng(), readLength);
        for (size_t errorCounter = 0; errorCounter < maxErrors; ++errorCounter) {
            read[errorPosRng()].value ^= baseRng().value;
        }
        seqan::writeRecord(readsOutputFile, std::to_string(readIndex), read, seqan::Fasta());
    }
    readsOutputFile.flush();
}

} // namespace

void GenomeGenerator::structReadToFastq(std::vector<GenomeGenerator::Read> reads, std::string output_path){

    std::ofstream file(output_path);
    for (size_t i = 0; i < reads.size(); i++) {
        ReadString read = reads[i].readString;
        seqan::writeRecord(file, std::to_string(i), read, seqan::Fastq());
    }
    file.flush();

}


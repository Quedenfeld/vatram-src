#ifndef PG583_GENOMEGENERATOR_H
#define PG583_GENOMEGENERATOR_H

#include <align/VariantIndex.h>
#include <Chromosome.h>

#include <string>
#include <random>

class GenomeGenerator {
public:
    constexpr static double MAXIMAL_N_RATIO_IN_READ = 0.1;

    std::string id;
    size_t length;
    size_t numberReads;
    std::mt19937 rand;

    struct Read {
        ReadString readString;
        size_t startPos;
        const Chromosome* chromosome;
        bool reversedComplement;
        std::unique_ptr<Read> next; // used for paired end reads
        size_t errorCount;

        Read(const Read& r) :
            readString(r.readString),
            startPos(r.startPos),
            chromosome(r.chromosome),
            reversedComplement(r.reversedComplement),
            next(r.next.get() ? new Read(*r.next) : NULL),
            errorCount(r.errorCount) {
        }
        Read() :
            errorCount(0) {
        }
        Read(const ReadString& readString, size_t startPos, const Chromosome* chromosome, bool reversedComplement, size_t errors) :
            readString(readString),
            startPos(startPos),
            chromosome(chromosome),
            reversedComplement(reversedComplement),
            next(nullptr),
            errorCount(errors) {
        }

        Read& operator=(const Read& r) {
            if (this != &r) {
                readString = r.readString;
                startPos = r.startPos;
                chromosome = r.chromosome;
                reversedComplement = r.reversedComplement;
                next.reset(r.next.get() ? new Read(*r.next) : NULL);
                errorCount = r.errorCount;
            }
            return *this;
        }
    };


    GenomeGenerator();
    GenomeGenerator(size_t seed);
    GenomeGenerator(const std::string &id, const size_t &length, const size_t &numberReads, size_t seed);

    bool probability(double prob);
    seqan::Dna5String iupacToDna5(const seqan::IupacString &string);

    void generateReference(size_t length, std::string filepath);
    Chromosome createChromosome(size_t length);

    void addSNP(Chromosome &chromosome, size_t pos);
    void addSNPbyIupac(Chromosome &chromosome, size_t pos, seqan::Iupac base);
    void addSNPs(Chromosome &chromosome, size_t numberOfSNPs, size_t lengthOfSNP);
    void addSNPwithProbabilty(Chromosome &chromosome, double snpProbability, size_t lengthOfSNP);


    Read createRead(const Chromosome &chromosome, size_t readLength, double substitutionProbability, double indelProbability);

    std::vector<Variant> getVariants(const Chromosome &chromosome, const VariantIndex &varIndex, size_t pos, size_t lastEnd);
    void handleVar(const Chromosome &chromosome, const VariantIndex &varIndex, seqan::IupacString &readString, const size_t &refIndex, size_t &readIndex, size_t &lastEnd, size_t readLength, double prob);
    void doRand(double in, double del, double subs, double snp, seqan::IupacString &readString, size_t readLength);
    GenomeGenerator::Read createReadFromVariantAndWriteSam(const Chromosome &chromosome, const VariantIndex &varIndex, size_t readLength, double varProb, double insProb, double delProb, double subsProb, double snpProb, std::string Filename, std::string read_id);

    std::vector<GenomeGenerator::Read> createReadsAndSamWithVariants(const Genome &genome,
                                                                 size_t readLength,
                                                                 double varProb,
                                                                 double insProb,
                                                                 double delProb,
                                                                 double subsProb,
                                                                 double snpProb,
                                                                 int numberOfReads,
                                                                 std::string filename);

    GenomeGenerator::Read createReadWithVariants(const Chromosome &chromosome, const VariantIndex &varIndex, size_t readLength, double varProb, double insProb, double delProb, double subsProb, double snpProb);
    std::vector<GenomeGenerator::Read> createReadsWithVariants(const Chromosome &chromosome, const VariantIndex &varIndex, size_t numberOfReads, size_t readLength, double varProb, double insProb, double delProb, double subsProb, double snpProb);

    std::vector<GenomeGenerator::Read> createReads(const Chromosome &chromosome, size_t numberOfReads, size_t readLength, double substitutionProbability, double indelProbability);
    void createReads(std::string input_path, size_t numberOfReads, size_t readLength, double substitutionProbability, double indelProbability, std::string output_path);
    void createReads(const Chromosome &chromosome, size_t numberOfReads, size_t readLength, double substitutionProbability, double indelProbability, std::string output_path);
    std::vector<GenomeGenerator::Read> createReads(std::string input_path, size_t numberOfReads, size_t readLength, double substitutionProbability, double indelProbability);

    std::vector<Variant> generateIndels(Chromosome &chromosome, size_t numberOfIndels, size_t maxLength);
    VariantIndex generateVariantIndex(const Chromosome &chromosome, std::vector<Variant> variants);
    Variant addIndel(Chromosome &chromosome, size_t pos, seqan::IupacString &insertionString, size_t deletionLength);

    void structReadToFastq(std::vector<GenomeGenerator::Read> reads, std::string output_path);
};

#endif // PG583_GENOMEGENERATOR_H

#ifndef VARIANTGENERATOR_H
#define VARIANTGENERATOR_H

#include "Chromosome.h"
#include "VcfVariant.h"
#include "io/VariantsWriter.h"

class VariantGenerator {
public:

    VariantGenerator(const Genome& genome, uint64_t seed) :
        genome(genome),
        rnd(seed) {

        size_t sum = 0;
        for (const Chromosome& chr: genome) {
            sum += seqan::length(chr.data);
            cumulativeChrLng.push_back(sum);
        }
        chrLngDistr = std::uniform_int_distribution<size_t>(0, sum-1);
    }

    ChromosomeIndex sampleChromosome() {
        size_t random = chrLngDistr(rnd);
        for (size_t i = 0; i < genome.size(); i++) {
            if (cumulativeChrLng[i] > random) {
                return i;
            }
        }
        return genome.size() - 1;
    }

    seqan::Dna randomNucleotidUnequal(seqan::Dna x) {
        seqan::Dna r;
        do {
            r = rnd() & 0x3;
        } while (r == x);
        return r;
    }

    void setSeed(uint64_t seed) {
        rnd.seed(seed);
    }

    VcfVariant createSNP(double probability) {
        ChromosomeIndex chrIdx;
        Position pos;
        do {
            chrIdx = sampleChromosome();
            std::uniform_int_distribution<Position> posDistr(0, seqan::length(genome[chrIdx].data) - 1);
            pos = posDistr(rnd);
        } while(genome[chrIdx].data[pos] == 'N');

        return VcfVariant(chrIdx, pos, probability, 1, randomNucleotidUnequal(genome[chrIdx].noVariantsString[pos]));
    }

    template <class ProbabilityDistribution>
    std::vector<VcfVariant> createSNPs(size_t count, ProbabilityDistribution pDistr) {
        std::cout << "createSNPs()" << std::endl;
        std::vector<VcfVariant> variants;
        for (size_t i = 0; i < count; i++) {
            variants.push_back(createSNP(pDistr()));
        }
        std::sort(variants.begin(), variants.end());
        return variants;
    }

//    template <class ProbabilityDistribution>
//    std::vector<SnpVariant> createSNPs_asSnpVariants(size_t count, ProbabilityDistribution pDistr) {
//        std::cout << "createSNPs()" << std::endl;
//        std::vector<SnpVariant> variants;
//        for (size_t i = 0; i < count; i++) {
//            variants.push_back(createSNP(pDistr()));
//        }
//        std::sort(variants.begin(), variants.end());
//        return variants;
//    }

    void writeVariants(const std::vector<VcfVariant>& variants, const std::string& vcfPath) {
        std::cout << "writeVariants()" << std::endl;
//        seqan::VcfStream vcfstrm(vcfPath.c_str(), seqan::VcfStream::WRITE);
        std::ofstream vcfstrm(vcfPath);

        VariantsWriter vw(vcfstrm, genome);
        vw.writeHeader();

        size_t idCount = 0;
        for (const VcfVariant& var: variants) {
            vw.writeVariant("v" + std::to_string(idCount++), var);
        }
    }

private:
    const Genome& genome;
    std::vector<size_t> cumulativeChrLng;
    std::mt19937 rnd;
    std::uniform_int_distribution<size_t> chrLngDistr;

};

#endif // VARIANTGENERATOR_H

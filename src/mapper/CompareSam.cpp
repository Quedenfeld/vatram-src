#include "CompareSam.h"

#include <seqan/bam_io.h>

#include <unordered_map>

CompareSam::CompareSam() {
}

CompareSam::CompareSam(std::string perfectSamPath, std::string compareSamPath):
    perfectSamPath(perfectSamPath),
    compareSamPath(compareSamPath){
}

void CompareSam::startComparison(){

    unmappedThreshold = 0;

    std::cout << "Start comparison of SAM-Files " << perfectSamPath << " and " << compareSamPath << std::endl;
    std::cout << "Threshold for correct positionioning: " << unmappedThreshold << std::endl;

    std::vector<CompareSam::Row> perfectSamData;
    std::unordered_map<std::string, CompareSam::Row> compareSamData;

    // Read lines of "perfect" SAM-File
    seqan::BamStream perfectStream(perfectSamPath.c_str());
    if (!seqan::isGood(perfectStream)) {
        std::cout << "Error while reading " << perfectSamPath << std::endl;
    }
    seqan::BamAlignmentRecord perfectRecord;
    while (!seqan::atEnd(perfectStream)){
        if (seqan::readRecord(perfectRecord, perfectStream) != 0) {
            std::cout << "Error while reading line of " << perfectSamPath << std::endl;
        }
        CompareSam::Row r;
        r.qName = seqan::toCString(perfectRecord.qName);
        r.flag = perfectRecord.flag;
        r.pos = perfectRecord.beginPos;
        r.length = perfectRecord.tLen;
       // r.cigar = record.cigar;
        r.sequence = perfectRecord.seq;
        r.alignmentCopy = perfectRecord;
        perfectSamData.push_back(r);
    }
    std::cout << "read " << perfectSamData.size() << " lines of the perfect SAM-file" << std::endl;

    // read lines of comparison SAM-File
    seqan::BamStream compareStream(compareSamPath.c_str());
    if (!seqan::isGood((compareStream))){
        std::cout << "Error while reading " << perfectSamPath << std::endl;
    }
    seqan::BamAlignmentRecord compareRecord;
    while (!seqan::atEnd(compareStream)){
        if (seqan::readRecord(compareRecord, compareStream) != 0) {
            std::cout << "Error while reading line of " << compareSamPath << std::endl;
        }
        CompareSam::Row r;
        r.qName = seqan::toCString(compareRecord.qName);
        r.flag = compareRecord.flag;
        r.pos = compareRecord.beginPos;
        r.length = compareRecord.tLen;
       // r.cigar = compareRecord.cigar;
        r.sequence = compareRecord.seq;
        r.alignmentCopy = compareRecord;

        std::string name = r.qName;
        std::pair<std::string, CompareSam::Row> pair(name, r);
        compareSamData.insert(pair);
    }
    std::cout << "read " << compareSamData.size() << " lines of the compare SAM-file" << std::endl;


    /*
     * Start Comparison of SAM-File
     * 1. Iterate over PerfectSamData
     * 2. Find matching read in CompareSamData
     * 3. Compare attributes
     */

    for (CompareSam::Row currentPerfectRow : perfectSamData){
        totalNum++;
        std::string currentReadName = currentPerfectRow.qName;
        CompareSam::Row &compareRow = compareSamData[currentReadName];

        // Calculate Hamming-Distance. This is only possible, if both seqences have the same length;
        if (seqan::length(currentPerfectRow.sequence) == seqan::length(compareRow.sequence)){
            compareRow.hamming_distance = calculateHammingDistance(currentPerfectRow.sequence, compareRow.sequence);
            hammingDistances.push_back(compareRow.hamming_distance);
            // std::cout << "Hamming distance for read " << compareRow.qName << ": " << compareRow.hamming_distance << std::endl;

        }
        else {
            compareRow.hamming_distance = -1;
        }
        compareRow.edit_distance = calculateEditDistance(currentPerfectRow.sequence, compareRow.sequence);
        editDistances.push_back(compareRow.edit_distance);
        // std::cout << "Edit distance for read " << compareRow.qName << ": " << compareRow.edit_distance << std::endl;


        // If PerfectSam has an Unmapped Read (only possible in real world data), increment true negative.
        if (seqan::hasFlagUnmapped(currentPerfectRow.alignmentCopy)){
            numIncorrectByGoldStandard++;
        }

        // TODO: else if oder if

        // read is unmapped, if the unmapped-flag within the SAM-file is set.
        if (seqan::hasFlagUnmapped(compareRow.alignmentCopy)){
            numUnmapped++;
        }
        // if both given position are equal, read is mapped correctly
        else if (currentPerfectRow.pos == compareRow.pos){
            numCorrectMapped++;
        }
        // if the position differs by unmappedThreshold, read is mapped correctly in the threshold-interval
        else if ((currentPerfectRow.pos - compareRow.pos <= unmappedThreshold) && (compareRow.pos - currentPerfectRow.pos >= 3)) {
            numCorrectMappedThreshold++;
        }
        // otherwise read is mapped incorrectly.
        else {
            numIncorrectMapped++;
        }
    }
    // printStats();

}

size_t CompareSam::calculateEditDistance(seqan::CharString seq1, seqan::CharString seq2){
    std::vector<std::vector<size_t>> distanceMatrix;

    std::vector<size_t> initRow;
    for (size_t i = 0; i <= seqan::length(seq1); i++){
        initRow.push_back(i);
    }
    distanceMatrix.push_back(initRow);

    for (size_t j = 1; j <= seqan::length(seq2); j++){
        std::vector<size_t> initColumn(seqan::length(seq1), 0);
        initColumn[0] = j;
        distanceMatrix.push_back(initColumn);
    }

    for (size_t j = 1; j < seqan::length(seq2); j++){
        for (size_t i = 1; i < seqan::length(seq1); i++){
            if (seq1[i] == seq2[j]){
                distanceMatrix[i][j] = distanceMatrix[i-1][j-1];
            }
            else {
                distanceMatrix[i][j] = std::min({distanceMatrix[i-1][j]+1, distanceMatrix[i][j-1]+1, distanceMatrix[i-1][j-1]+1});
            }
        }
    }
    return distanceMatrix[seqan::length(seq1)][seqan::length(seq2)];
}

size_t CompareSam::calculateHammingDistance(seqan::CharString seq1, seqan::CharString seq2){
    size_t distanceScore = 0;
    if (seqan::length(seq1) != seqan::length(seq2)){
        std::cout << "Cannot calculate Hamming-Distance on two sequences with different length" << std::endl;
        return seqan::length(seq1);
    }
    for (size_t i = 0; i < seqan::length(seq1); i++){
        if (seq1[i] != seq2[i]){
            distanceScore++;
        }
    }
    return distanceScore;
}

double CompareSam::calculateAccuracy(){
    // (true_positives + true_negatives) / (all)
    return ((double)(numCorrectMapped + numIncorrectMapped) / (totalNum));
}


double CompareSam::calculateSensitivity(){
    // true_positive / (true_positive + false_negative)
    return ((double)(numCorrectMapped) / (numCorrectMapped + numUnmapped));
}

double CompareSam::calculateSpecitivity(){
    // false_positives / (true_negatives + false_positves)
    // TODO: Überhaupt anwendbar, da keine Information über nicht mapbare Reads exisitieren?
    // return ((double)(numIncorrectMapped) / (numIncorrectMapped + numUnmapped));
    return ((double)(numIncorrectMapped) / (numIncorrectByGoldStandard + numIncorrectMapped));
}

double CompareSam::calculatePrecision(){
    // true_positives / (true_positives + false_positives)
    return ((double)(numCorrectMapped) / (numCorrectMapped + numIncorrectMapped));
}

double CompareSam::calculateRecall(){
    return calculateSensitivity();
}

double CompareSam::calculateFalseDiscoveryRate(){
    // false_positives / (true_positves + false positives)
    return ((double)(numIncorrectMapped) / (numCorrectMapped + numIncorrectMapped));
}

double CompareSam::calculateFalseOmissionRate(){
    // false_negatives / (false_negatives + true_negatives)
    return ((double)(numUnmapped) / (numUnmapped + numIncorrectByGoldStandard));
}

double CompareSam::calculateNegativePredictionValue(){
    // true_negatives / (false_negatives + true_negatives)
    return ((double)(numIncorrectByGoldStandard) / (numUnmapped + numIncorrectByGoldStandard));
}

double CompareSam::calculateF1(){
    return ((2*calculateRecall()*calculatePrecision())/(calculateRecall()+calculatePrecision()));
}

double CompareSam::calculateAverageDistance(std::vector<size_t> distances){
    size_t sum = 0;
    for (size_t current : distances){
        sum += current;
    }
    return (double)sum / distances.size();
}

void CompareSam::printStats() {
    std::cout << "Correct Mappings: " << numCorrectMapped << std::endl;
    std::cout << "Incorrect Mappings " << numIncorrectMapped << std::endl;
    std::cout << "Correct Mappings within threshold interval: " << numCorrectMappedThreshold << std::endl;
    std::cout << "Unmapped: " << numUnmapped << std::endl << std::endl;

    std::cout << "Average Hamming Distance: " << calculateAverageDistance(hammingDistances) << std::endl;
    std::cout << "Average Edit Distance: " << calculateAverageDistance(editDistances) << std::endl << std::endl;

    std::cout << "Accuracy: " << calculateAccuracy() << std::endl;
    std::cout << "Precision: " << calculatePrecision() << std::endl;
    std::cout << "Recall / Sensitivity: " << calculateRecall() << std::endl;
    std::cout << "Specitivity: " << calculateSpecitivity() << std::endl << std::endl;

    std::cout << "False Discovery Rate: " << calculateFalseDiscoveryRate() << std::endl;
    std::cout << "False Omission Rate: " << calculateFalseOmissionRate() << std::endl;
    std::cout << "Negative Prediction Value " << calculateNegativePredictionValue() << std::endl;
    std::cout << "F1: " << calculateF1() << std::endl;
}

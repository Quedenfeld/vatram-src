#include "module/ApplicationModule.h"

#include <sstream>
#include <iterator>

std::string ApplicationModule::generateUsageLine(seqan::ArgumentParser &argParser) const {
    std::ostringstream oss;
    auto itArg = argParser.argumentList.begin();
    if (itArg != argParser.argumentList.end()) {
        oss << " \\fB" << seqan::getArgumentLabel(*itArg) << "\\fP";
    }

    for (auto &option : argParser.optionMap) {
        if (option.shortName != "" && option.shortName != "-") {
            oss << " ";
            if (!seqan::isRequired(option)) {
                oss << "[";
            }

            oss << "\\fB-" << option.shortName << "\\fP";

            std::istringstream iss(seqan::getArgumentLabel(option));
            for (std::istream_iterator<std::string> it(iss); it != std::istream_iterator<std::string>(); ++it) {
                oss << " \\fI" << *it << "\\fP";
            }

            if (seqan::isListArgument(option)) {
                oss << "...";
            }
            if (!seqan::isRequired(option)) {
                oss << "]";
            }
        }
    }

    if (itArg != argParser.argumentList.end()) {
        while (++itArg != argParser.argumentList.end()) {
            oss << " \\fI" << seqan::getArgumentLabel(*itArg) << "\\fP";
        }
        if (seqan::isListArgument(*--itArg)) {
            oss << "...";
        }
    }
    return oss.str();
}

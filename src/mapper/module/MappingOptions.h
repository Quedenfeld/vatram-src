#ifndef PG583_MODULE_MAPPINGOPTIONS_H
#define PG583_MODULE_MAPPINGOPTIONS_H

#include "module/ModuleOptions.h"
#include "map/LSH.h"

class MappingOptions : public ModuleOptions {
public:
    IntOption minCountSelection;
    IntOption maxSelect;
    FloatOption nextFactor;
    FloatOption contractBandMultiplicator;
    FloatOption extendBandMultiplicator;
    FloatOption extendFactor;
    IntOption maximumOfReturnedWindows;

    IntOption splitLength;
    FloatOption splitFactor;
    IntOption pairedEndDistanceMin;
    IntOption pairedEndDistanceMax;

    MappingOptions();

    virtual void checkValues() const;
    void apply(LshMapOptions &lshMapOptions) const;

    /**
     * @brief replaceUnsetValues replaces unset option values by values from \p source
     * @param source
     * @return true iff all options were either unset or identical to \p source before call
     */
    bool replaceUnsetValues(const MappingOptions &source);
};

#endif // PG583_MODULE_MAPPINGOPTIONS_H

#ifndef PG583_MODULE_REFERENCELOADINGMODULE_H
#define PG583_MODULE_REFERENCELOADINGMODULE_H

#include "module/ApplicationModule.h"
#include "module/ModuleOptions.h"
#include "Chromosome.h"

class ReferenceLoadingModule : virtual public ApplicationModule {
protected:
    FloatOption variantsProbabilityThreshold;
    InputFilesOptions inputFilesOptions;
    Genome genome;
public:
    ReferenceLoadingModule();
    virtual void addOptions(seqan::ArgumentParser &argParser);
    virtual void readOptions(seqan::ArgumentParser &argParser);
    void readReference();
};

#endif // PG583_MODULE_REFERENCELOADINGMODULE_H

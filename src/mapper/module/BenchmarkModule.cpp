#include "module/BenchmarkModule.h"
#include "GenomeGenerator.h"
#include "CompareSam.h"
#include "Chromosome.h"
#include "Logger.h"
#include "types.h"
#include <seqan/seq_io.h>
#include "io/ReferenceReader.h"
#include "io/VariantsReader.h"

#include <seqan/arg_parse.h>

#include <ctime>

BenchmarkModule::BenchmarkModule() {
}

std::string BenchmarkModule::getName() const {
    return "benchmark";
}

std::string BenchmarkModule::getDescription() const {
    return "Module for benchmarking tasks";
}

void BenchmarkModule::addOptions(seqan::ArgumentParser &argParser) {
    using namespace seqan;

    inputFileOptions.addToParser(argParser);

    addSection(argParser, getName() + " Options");

    ArgParseOption genomeLengthOpt("gl", "genome-length", "Length of the generated genome.", ArgParseOption::INTEGER, "INT");
    setDefaultValue(genomeLengthOpt, 100);
    addOption(argParser, genomeLengthOpt);

    ArgParseOption numberOfReadsOpt("nr", "number-reads", "Number of the generated reads.", ArgParseOption::INTEGER, "INT");
    setDefaultValue(numberOfReadsOpt, 10);
    addOption(argParser, numberOfReadsOpt);

    ArgParseOption readLengthOpt("rl", "read-length", "Length of the generated reads.", ArgParseOption::INTEGER, "INT");
    setDefaultValue(readLengthOpt, 10);
    addOption(argParser, readLengthOpt);

    ArgParseOption indelProbabilityOpt("iprob", "indel-probabilty", "Probabilty wheather a generated position contains indels.", ArgParseOption::DOUBLE, "DOUBLE");
    setDefaultValue(indelProbabilityOpt, 0.0);
    addOption(argParser, indelProbabilityOpt);

    ArgParseOption substitutionProbabilityOpt("sprob", "substitution-probability", "Probabilty wheather a generated position contains a substitution.", ArgParseOption::DOUBLE, "DOUBLE");
    setDefaultValue(substitutionProbabilityOpt, 0.0);
    addOption(argParser, substitutionProbabilityOpt);

    ArgParseOption genomeOutputFile("gf", "genome-filepath", "writes genome to the specified filepath.", ArgParseOption::OUTPUTFILE, "genome.fasta");
    addOption(argParser, genomeOutputFile);

    ArgParseOption readsOutputFile("rf", "reads-filepath", "writes reads to the specified filepath.", ArgParseOption::OUTPUTFILE, "reads.fastq");
    addOption(argParser, readsOutputFile);

    ArgParseOption readsOutputSamOpt("sam", "read-as-sam", "write reads as SAM-File to specified filepath.", ArgParseOption::OUTPUTFILE, "reads.sam");
    addOption(argParser, readsOutputSamOpt);

    ArgParseOption perfectSamInput("psam", "perfect-sam", "file-path to the perfect sam-file", ArgParseOption::INPUTFILE, "perfect.sam");
    addOption(argParser, perfectSamInput);

    ArgParseOption compareSamInput("csam", "compare-sam", "file-path to the compare sam-file", ArgParseOption::INPUTFILE, "compare.sam");
    addOption(argParser, compareSamInput);
}

void BenchmarkModule::readOptions(seqan::ArgumentParser &argParser) {
    seqan::getOptionValue(genomeLength, argParser, "gl");
    seqan::getOptionValue(numberOfReads, argParser, "nr");
    seqan::getOptionValue(readLength, argParser, "rl");
    seqan::getOptionValue(indelProbability, argParser, "iprob");
    seqan::getOptionValue(substitutionProbability, argParser, "sprob");
    seqan::getOptionValue(inputGenomeFilePath, argParser, "r");
    seqan::getOptionValue(inputVariantFilePath, argParser, "v");
    seqan::getOptionValue(outputGenomeFilePath, argParser, "gf");
    seqan::getOptionValue(outputReadsFilePath, argParser, "rf");
    seqan::getOptionValue(outputReadsAsSam, argParser, "sam");
    seqan::getOptionValue(inputPerfectSam, argParser, "psam");
    seqan::getOptionValue(inputCompareSam, argParser, "csam");
    inputFileOptions.read(argParser);
}

int BenchmarkModule::run() {
    Chromosome chr("id");
    std::vector<GenomeGenerator::Read> reads; // TODO: somehow strange...

    GenomeGenerator gen(std::time(nullptr));

    if (!inputPerfectSam.empty() && !inputCompareSam.empty()){
        CompareSam comp(inputPerfectSam, inputCompareSam);
        comp.startComparison();
        comp.printStats();
    }

    if (!outputGenomeFilePath.empty()){
        gen.generateReference(genomeLength, outputGenomeFilePath);
        if (!outputReadsFilePath.empty()){
            gen.createReads(outputGenomeFilePath, numberOfReads, readLength, substitutionProbability, indelProbability, outputReadsFilePath);
        }
    }
    else if ((!outputReadsAsSam.empty()) && !inputGenomeFilePath.empty()){
        Genome genome;

        std::vector<std::string> refPath;
        refPath.push_back(inputGenomeFilePath);

        std::cout << "Read Reference.." << std::endl;

        std::unordered_set<std::string> filter;
        ReferenceReader rr(genome, filter);
        rr.readReferences(refPath);

        std::cout << "Read Reference ... done" << std::endl;

        if (!inputVariantFilePath.empty()){
            std::vector<std::string> varPath;
            varPath.push_back(inputVariantFilePath);
            std::cout << "Read variants.." << std::endl;

            VariantsReader vr(genome, filter);
            vr.readVariants(varPath);
            std::cout << "Read variants.. done" << std::endl;
        }

        std::vector<GenomeGenerator::Read> reads = gen.createReadsAndSamWithVariants(genome, readLength, 0.25, 0.001, 0.001, 0.018, 0.0, numberOfReads, outputReadsAsSam);
        if (!outputReadsFilePath.empty()){
            gen.structReadToFastq(reads, outputReadsFilePath);
            std::cout << "Fastq created" << std::endl;
        }
        std::cout << "Create Reads" << std::endl;

//                std::cout << "Create Reads... done" << std::endl;
    }
    else if (!outputReadsFilePath.empty() && !inputGenomeFilePath.empty()) {
        gen.createReads(inputGenomeFilePath, numberOfReads, readLength, substitutionProbability, indelProbability, outputReadsFilePath);
    }
    else if (!inputGenomeFilePath.empty()) {
        reads = gen.createReads(inputGenomeFilePath, numberOfReads, readLength, substitutionProbability, indelProbability);
    }
    else {
        chr = gen.createChromosome(genomeLength);
        reads = gen.createReads(chr, numberOfReads, readLength, 0.0, 0.0);
   }

    return 0;
}

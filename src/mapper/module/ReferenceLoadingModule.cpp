#include "module/ReferenceLoadingModule.h"
#include "io/ReferenceReader.h"
#include "io/VariantsReader.h"
#include "Timer.h"
#include "Logger.h"

ReferenceLoadingModule::ReferenceLoadingModule() :
    variantsProbabilityThreshold("V", "variants-probability-threshold", 0.2,  0.0, 1.0   ) {

}

void ReferenceLoadingModule::addOptions(seqan::ArgumentParser &argParser) {
    inputFilesOptions.addToParser(argParser);
    seqan::addOption(argParser, variantsProbabilityThreshold.getArgOpt());
    seqan::setRequired(seqan::getOption(argParser, "R"), true);
}

void ReferenceLoadingModule::readOptions(seqan::ArgumentParser &argParser) {
    variantsProbabilityThreshold.getValue(argParser);
    inputFilesOptions.read(argParser);
}

void ReferenceLoadingModule::readReference() {
    genome.clear();

    ReferenceReader referenceReader(genome, inputFilesOptions.chromosomeFilter);
    VariantsReader variantsReader(genome, inputFilesOptions.chromosomeFilter, variantsProbabilityThreshold.value);

    Timer<> ioTimer;
    logger.info("Reading reference files..");
    referenceReader.readReferences(inputFilesOptions.referenceFiles);
    logger.info("Done reading references. ", "Elapsed time: ", ioTimer);

    ioTimer.resetTimer();
    logger.info("Reading variant files..");
    variantsReader.readVariants(inputFilesOptions.variantFiles);
    logger.info("Done reading variants. ", "Elapsed time: ", ioTimer);
}

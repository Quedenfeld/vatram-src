#include "module/MappingOptions.h"
#include "map/LSH.h"

MappingOptions::MappingOptions() :
    minCountSelection        ("m",  "min-count",            LshMapOptions::DEFAULT_MIN_COUNT_SELECTION,  1     ),
    maxSelect                ("n",  "max-select",           LshMapOptions::DEFAULT_MAX_SELECT,  1     ),
    nextFactor               ("F",  "next-factor",          LshMapOptions::DEFAULT_NEXT_FACTOR,  1.0   ),
    contractBandMultiplicator("B",  "contract-band-mult",   LshMapOptions::DEFAULT_CONTRACT_BAND_MULTIPLICATOR,  0.0, 1.0),
    extendBandMultiplicator  ("b",  "extend-band-mult",     LshMapOptions::DEFAULT_EXTEND_BAND_MULTIPLICATOR,  0.0, 1.0   ),
    extendFactor             ("a",  "extend-factor",        LshMapOptions::DEFAULT_EXTEND_FACTOR,  0     ),
    maximumOfReturnedWindows ("r",  "max-returned-windows", LshMapOptions::DEFAULT_MAXIMUM_OF_RETURNED_WINDOWS,  1     ),
    splitLength              ("",  "split-length",          LshMapOptions::DEFAULT_LONG_READ_SPLIT_LENGTH,  1     ),
    splitFactor              ("",  "split-factor",          LshMapOptions::DEFAULT_LONG_READ_SPLIT_FACTOR,  0     ),
    pairedEndDistanceMin     ("",  "pe-distance-min",       LshMapOptions::DEFAULT_PAIRED_END_DISTANCE_MIN),
    pairedEndDistanceMax     ("",  "pe-distance-max",       LshMapOptions::DEFAULT_PAIRED_END_DISTANCE_MAX) {
    options.assign({minCountSelection,
                    maxSelect,
                    extendFactor,
                    maximumOfReturnedWindows,
                    nextFactor,
                    contractBandMultiplicator,
                    extendBandMultiplicator,
                    splitLength,
                    splitFactor,
                    pairedEndDistanceMin,
                    pairedEndDistanceMax});
    minCountSelection        .setHelpText("Minimal number of hits for window.");
    maxSelect                .setHelpText("Maximal number of window sequences that are selected.");
    nextFactor               .setHelpText("The score values of all selected window sequences are at most by a factor of this parameter smaller than the highest score value.");
    contractBandMultiplicator.setHelpText("Contraction of window sequences consisting of two windows. The number of collisions is mulitplied with this value to determine how many bases are removed.");
    extendBandMultiplicator  .setHelpText("Extension of window sequences consisting of only one window. The number of collisions is mulitplied with this value to determine how many bases are added.");
    extendFactor             .setHelpText("Extension of window sequences consisting of only one window. The parameter is multiplied with the window size.");
    maximumOfReturnedWindows .setHelpText("If the number of window indices stored for one signature value is greater than this value, it will be ignored.");
    splitLength              .setHelpText("A long read is splitted into fragments of approximately this size.");
    splitFactor              .setHelpText("A read is splitted, if it is longer than the window size multiplied with this parameter.");
    pairedEndDistanceMin     .setHelpText("The distance between the middle points of two paired end sequences is greater than this value.");
    pairedEndDistanceMax     .setHelpText("The distance between the middle points of two paired end sequences is less than this value.");
}

void MappingOptions::checkValues() const {
    try {
        ModuleOptions::checkValues();
    } catch(const std::invalid_argument &ex) {
        throw std::invalid_argument(std::string("Invalid mapping options: ") + ex.what());
    }
}

void MappingOptions::apply(LshMapOptions &lshMapOptions) const {
    checkValues();

    lshMapOptions.minCountSelection = minCountSelection.value;
    lshMapOptions.minCountPrefiltering = minCountSelection.value; // TODO: weiterer Parameter?
    lshMapOptions.maxSelect = maxSelect.value;
    lshMapOptions.nextFactor = nextFactor.value;
    lshMapOptions.contractBandMultiplicator = contractBandMultiplicator.value;
    lshMapOptions.extendBandMultiplicator = extendBandMultiplicator.value;
    lshMapOptions.extendFactor = extendFactor.value;
    lshMapOptions.maximumOfReturnedWindows = maximumOfReturnedWindows.value;
    lshMapOptions.longReadSplitLength = splitLength.value;
    lshMapOptions.longReadSplitFactor = splitFactor.value;
    lshMapOptions.pairedEndDistanceMin = pairedEndDistanceMin.value;
    lshMapOptions.pairedEndDistanceMax = pairedEndDistanceMax.value;
}

namespace {
template <class TOpt>
bool assignIfUnSet(TOpt &target, const TOpt &source) {
    if (!target.isSet()) {
        target.value = source.value;
        return true;
    }
    return target.value == source.value;
}
} // namepsace

bool MappingOptions::replaceUnsetValues(const MappingOptions &source) {
    return assignIfUnSet(minCountSelection, source.minCountSelection)
         & assignIfUnSet(maxSelect, source.maxSelect)
         & assignIfUnSet(nextFactor, source.nextFactor)
         & assignIfUnSet(contractBandMultiplicator, source.contractBandMultiplicator)
         & assignIfUnSet(extendBandMultiplicator, source.extendBandMultiplicator)
         & assignIfUnSet(extendFactor, source.extendFactor)
         & assignIfUnSet(maximumOfReturnedWindows, source.maximumOfReturnedWindows)
         & assignIfUnSet(splitLength, source.splitLength)
         & assignIfUnSet(splitFactor, source.splitFactor)
         & assignIfUnSet(pairedEndDistanceMin, source.pairedEndDistanceMin)
         & assignIfUnSet(pairedEndDistanceMax, source.pairedEndDistanceMax);
}

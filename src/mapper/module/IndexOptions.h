#ifndef PG583_MODULE_INDEXOPTIONS_H
#define PG583_MODULE_INDEXOPTIONS_H

#include "module/ModuleOptions.h"
#include "map/LSH.h"

class IndexOptions : public ModuleOptions {
public:
    IntOption qGramSize;
    IntOption signatureLength;
    IntOption bandSize;
    IntOption windowSize;
    IntOption windowOffset;
    IntOption snpCombinationLimit;
    IntOption snpCombinationLimitSkip;
    IntOption randomSeed;
    StringOption qGramForm;

    std::string indexFile;

    IndexOptions();

    virtual void read(seqan::ArgumentParser &argParser);
    virtual void addToParser(seqan::ArgumentParser &argParser);
    template <class TLsh>
    void apply(TLsh &lsh) const;
    template <class TLsh>
    void load(const TLsh &lsh);
    virtual void checkValues() const;
};

template <class TLsh>
void IndexOptions::apply(TLsh &lsh) const {
    checkValues();

    lsh.qGramSize = qGramSize.value;
    lsh.signatureLength = signatureLength.value;
    lsh.bandSize = bandSize.value;
    lsh.windowSize = windowSize.value;
    lsh.windowOffset = windowOffset.value;
    lsh.limit = snpCombinationLimit.value;
    lsh.limit_skip = snpCombinationLimitSkip.value;
    lsh.initialRandomSeed = randomSeed.value;
    lsh.qGramHashObject.setGapVector(qGramForm.value);
}

template <class TLsh>
void IndexOptions::load(const TLsh &lsh) {
    qGramSize.value = lsh.qGramSize;
    signatureLength.value = lsh.signatureLength;
    bandSize.value = lsh.bandSize;
    windowSize.value = lsh.windowSize;
    windowOffset.value = lsh.windowOffset;
    snpCombinationLimit.value = lsh.limit;
    snpCombinationLimitSkip.value = lsh.limit_skip;
    randomSeed.value = lsh.initialRandomSeed;
    qGramForm.value = lsh.qGramHashObject.getGapVector();
}

#endif // PG583_MODULE_INDEXOPTIONS_H

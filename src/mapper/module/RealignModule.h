#ifndef PG583_MODULE_REALIGNMODULE_H
#define PG583_MODULE_REALIGNMODULE_H

#include "module/AligningModule.h"

class RealignModule : public AligningModule {
public:
    RealignModule();
    virtual void addOptions(seqan::ArgumentParser &argParser);
    virtual void readOptions(seqan::ArgumentParser &argParser);
    virtual int run();
    virtual std::string getName() const;
    virtual std::string getDescription() const;

private:
    virtual AlignmentStats processRead(Aligner &aligner, PairedRead& read) override;
};

#endif // PG583_MODULE_REALIGNMODULE_H

#ifndef PG583_MODULE_INDEXINGMODULE_H
#define PG583_MODULE_INDEXINGMODULE_H

#include "module/ReferenceLoadingModule.h"
#include "module/IndexOptions.h"
#include "io/IndexReader.h"
#include "map/LSH.h"
#include "Chromosome.h"
#include "Timer.h"

#include <stdexcept>
#include <memory>
#include <iostream>
#include <fstream>
#include <cstddef>
using std::size_t;

template <class TIndex>
class IndexingModule : virtual public ReferenceLoadingModule {
protected:
    IndexOptions indexOptions;
    TIndex index;
    ChromosomeReferences newlyIndexedChromosomes;

    std::unique_ptr<IndexReader<TIndex>> indexReader;

    void initializeIndex();

    void readIndex();

    void createIndex();
public:
    using Index = TIndex;
    virtual void addOptions(seqan::ArgumentParser &argParser);
    virtual void readOptions(seqan::ArgumentParser &argParser);
    virtual int run() = 0;
    virtual std::string getName() const = 0;
    virtual std::string getDescription() const = 0;
};

template <class TIndex>
void IndexingModule<TIndex>::addOptions(seqan::ArgumentParser &argParser) {
    ReferenceLoadingModule::addOptions(argParser);
    indexOptions.addToParser(argParser);
}

template <class TIndex>
void IndexingModule<TIndex>::readOptions(seqan::ArgumentParser &argParser) {
    ReferenceLoadingModule::readOptions(argParser);
    if (indexOptions.indexFile.empty()) {
        indexOptions.read(argParser);
    } else {
        const auto &intOptions = indexOptions.getOptions();
        if (std::any_of(intOptions.begin(), intOptions.end(),
                        [&](const Option &opt) { return seqan::isSet(argParser, opt.getArgOptName()); })) {
            throw std::invalid_argument("Index file as well additional index options given. "
                                        "An index file already provides all necessary index options.");
        }
    }
}

template <class TIndex>
void IndexingModule<TIndex>::initializeIndex() {
    if (indexOptions.indexFile.empty()) {
        indexReader.reset(nullptr);
        indexOptions.apply(index);
    } else {
        logger.info("Reading index file header..");
        indexReader.reset(new IndexReader<TIndex>(index));
        indexReader->open(indexOptions.indexFile);
        indexReader->readHeader(genome);
        indexOptions.load(index);
    }

    const size_t windowSize = indexOptions.windowSize.value;
    const size_t windowOffset = indexOptions.windowOffset.value;
    // TODO: Don't calculate windowCount here, instead postpone LSH::initialize until after LSH::addReferenceWindows.
    //       Index loading has to be altered accordingly beforehand...
    // The following is the same code as in LSH::addReferenceWindows.
    size_t windowCount = 0;
    for (const Chromosome &chromosome : genome) {
        if (seqan::length(chromosome.data) >= windowSize) {
            Position lastWindowPos = seqan::length(chromosome.data) - windowSize + 1;
            for (Position windowStartPos = 0; windowStartPos < lastWindowPos; windowStartPos += windowOffset) {
                auto windowStartPosIt = seqan::begin(chromosome.data) + windowStartPos;
                auto isOnlyNs = [windowStartPosIt] (Position first, Position last) {
                    return std::all_of(windowStartPosIt + first, windowStartPosIt + last,
                                       [](ReferenceChar c) { return c == 'N'; });
                };
                if (!isOnlyNs(0, windowOffset) && !isOnlyNs(windowSize - windowOffset, windowSize)) {
                    ++windowCount; // count only those windows which will actually be indexed
                }
            }
        } else {
            logger.warn("LSH: Length of chromosome ", chromosome.name, " is ", seqan::length(chromosome.data));
        }
    }

    index.initialize(windowCount);

    newlyIndexedChromosomes.clear();
    std::remove_copy_if(genome.begin(), genome.end(), std::back_inserter(newlyIndexedChromosomes),
                        [this](const Chromosome &chr) {
                        return std::any_of(this->index.chromosomes.begin(), this->index.chromosomes.end(),
                                           [&](const Chromosome &indexedChr) { return &indexedChr== &chr; }); });

    // NOTE: accomplishes only lexicographic sort, i.e. chr1 < chr10 < chr2 etc.
    std::sort(newlyIndexedChromosomes.begin(), newlyIndexedChromosomes.end(),
              [](const Chromosome &c1, const Chromosome &c2) { return c1.name < c2.name; });
}

template <class TIndex>
void IndexingModule<TIndex>::readIndex() {
    if (indexReader) {
        logger.info("Loading index from file..");
        Timer<> timer;

        indexReader->readIndex();
        indexReader.reset(nullptr);

        logger.info("Done loading index. ", "Elapsed time: ", timer);
    }
}

template <class TIndex>
void IndexingModule<TIndex>::createIndex() {
    logger.info("Creating index..");
    Timer<> indexTimer;

    for (const Chromosome &chromosome : newlyIndexedChromosomes) {
        logger.info("LSH: ", "Indexing ", '"', chromosome.id, '"', "..");
        Timer<> chromosomeIndexTimer;
        index.addChromosome(chromosome);
        logger.info("LSH: ", "Done indexing ", '"', chromosome.id, '"', ". ",
                    "Elapsed time: ", chromosomeIndexTimer);
    }
    index.buildIndex();
    for (Chromosome& chr: genome) {
        chr.insertLowProbabilitySnps(); // insert low probability snps for aligner. The genome is form herenot accessed by lsh any more
    }
    logger.info("LSH: ", "Done creating index. ", "Elapsed time: ", indexTimer);
}

#endif // PG583_MODULE_INDEXINGMODULE_H

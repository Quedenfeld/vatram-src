#include "module/ModuleOptions.h"

std::string StringOption::UNSET;

void ModuleOptions::addToParser(seqan::ArgumentParser &argParser) {
    for (const Option &option : getOptions()) {
        seqan::addOption(argParser, option.getArgOpt());
    }
}

void ModuleOptions::read(seqan::ArgumentParser &argParser) {
    for (Option &option : options) {
        option.getValue(argParser);
    }
}

void ModuleOptions::checkValues() const {
    std::ostringstream err;
    err << _checkValues<int64_t>();
    err << _checkValues<double>();
    std::string errors(err.str());
    if (!errors.empty()) {
        throw std::invalid_argument(errors);
    }
}

Option &ModuleOptions::addOption(Option &option) {
    auto it = getOption(option.getArgOptName());
    if (it != options.end()) {
        return *it;
    }
    options.emplace_back(option);
    return options.back();
}

ModuleOptions::OptionRefs::const_iterator ModuleOptions::getOption(const std::string &name) const {
    auto &options = getOptions();
    if (seqan::empty(name)) {
        return options.end();
    }
    return std::find_if(options.begin(), options.end(), [&](const Option &opt) {
        return opt.getArgOpt().longName == name || opt.getArgOpt().shortName == name; });
}

void InputFilesOptions::addToParser(seqan::ArgumentParser &argParser) {
    seqan::ArgParseOption filter("f", "filter", "Filter reference for chromosome.",
                                 seqan::ArgParseOption::STRING, "chromosome", true);
    seqan::ArgParseOption vars("v", "variants", "VCF variant files.",
                               seqan::ArgParseOption::INPUTFILE, "variants.vcf.gz", true);
    seqan::ArgParseOption ref("R", "reference", "FASTA reference files.",
                              seqan::ArgParseOption::INPUTFILE, "reference.fa.gz", true);

    seqan::addOption(argParser, filter);
    seqan::addOption(argParser, vars);
    seqan::addOption(argParser, ref);
}

void InputFilesOptions::read(seqan::ArgumentParser &argParser) {
    auto chromosomes = seqan::getOptionValues(argParser, "f");
    chromosomeFilter.insert(chromosomes.begin(), chromosomes.end());
    referenceFiles = seqan::getOptionValues(argParser, "R");
    variantFiles = seqan::getOptionValues(argParser, "v");
}

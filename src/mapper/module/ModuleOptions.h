#ifndef PG583_MODULE_MODULEOPTIONS_H
#define PG583_MODULE_MODULEOPTIONS_H

#include <seqan/arg_parse.h>

#include <sstream>
#include <unordered_set>
#include <string>
#include <vector>
#include <functional>
#include <limits>

template <typename T>
struct ArgParseType;
template <>
struct ArgParseType<int64_t> {
    static constexpr seqan::ArgParseOption::ArgumentType type = seqan::ArgParseOption::INT64;
};
template <>
struct ArgParseType<double> {
    static constexpr seqan::ArgParseOption::ArgumentType type = seqan::ArgParseOption::DOUBLE;
};

class Option {
protected:
    seqan::ArgParseOption argOpt;

    template <typename... Ts>
    Option(Ts... args) : argOpt(std::forward<Ts>(args)...) {}
public:
    virtual void unset() = 0;
    virtual bool isSet() const = 0;
    virtual bool getValue(const seqan::ArgumentParser &argParser) = 0;

    void setHelpText(const std::string &text) {
        seqan::setHelpText(argOpt, text);
    }

    const seqan::ArgParseOption& getArgOpt() const {
        return argOpt;
    }

    seqan::ArgParseOption& getArgOpt() {
        return argOpt;
    }

    const std::string& getArgOptName() const {
        if (!seqan::empty(argOpt.longName)) {
            return argOpt.longName;
        }
        return argOpt.shortName;
    }

    virtual std::ostream &printOption(std::ostream &os) const = 0;
    virtual std::istream &readOption(std::istream &is) = 0;
};

template <class T>
class NumOption : public Option {
public:
    typedef T ValueType;
    static constexpr ValueType UNSET = std::numeric_limits<ValueType>::max();

    ValueType value;
    const ValueType min, max;

    NumOption(const std::string &shortName,
              const std::string &longName,
              const std::string &argumentLabel,
              ValueType value = UNSET,
              ValueType min = UNSET,
              ValueType max = UNSET) :
        Option(shortName, longName, "", ArgParseType<T>::type, argumentLabel),
        value(value),
        min(min),
        max(max) {
        if (value != UNSET) {
            seqan::setDefaultValue(argOpt, value);
        }
        if (min != UNSET) {
            seqan::setMinValue(argOpt, std::to_string(min));
        }
        if (max != UNSET) {
            seqan::setMaxValue(argOpt, std::to_string(max));
        }
    }

    NumOption(const std::string &shortName,
              const std::string &longName,
              ValueType value = UNSET,
              ValueType min = UNSET,
              ValueType max = UNSET) :
        NumOption(shortName, longName, "N", value, min, max) {
    }

    void unset() override {
        value = UNSET;
    }

    bool isSet() const override {
        return value != UNSET;
    }
    bool hasMin() const {
        return min != UNSET;
    }
    bool hasMax() const {
        return max != UNSET;
    }

    bool getValue(const seqan::ArgumentParser &argParser) override {
        if (!seqan::getOptionValue(value, argParser, getArgOptName())) {
            unset();
            return false;
        }
        return true;
    }

    std::ostream &printOption(std::ostream &os) const override {
        return os << value;
    }
    std::istream &readOption(std::istream &is) override {
        return is >> value;
    }
};

class StringOption : public Option {
public:
    typedef std::string ValueType;
    static ValueType UNSET;

    ValueType value;

    StringOption(const std::string &shortName,
              const std::string &longName,
              const std::string &argumentLabel,
              ValueType value = UNSET) :
        Option(shortName, longName, "", seqan::ArgParseOption::STRING, argumentLabel),
        value(value) {
        if (value != UNSET) {
            seqan::setDefaultValue(argOpt, value);
        }
    }

    void unset() override {
        value = UNSET;
    }

    bool isSet() const override {
        return value != UNSET;
    }

    bool getValue(const seqan::ArgumentParser &argParser) override {
        if (!seqan::getOptionValue(value, argParser, getArgOptName())) {
            unset();
            return false;
        }
        return true;
    }

    std::ostream &printOption(std::ostream &os) const override {
        return os << value;
    }
    std::istream &readOption(std::istream &is) override {
        return std::getline(is, value);
    }
};

typedef NumOption<int64_t> IntOption;
typedef NumOption<double> FloatOption;

class ModuleOptions {
public:
    typedef std::vector<std::reference_wrapper<Option>> OptionRefs;

    virtual void addToParser(seqan::ArgumentParser &argParser);
    virtual void read(seqan::ArgumentParser &argParser);
    virtual void checkValues() const;

    const OptionRefs& getOptions() const { return options; }

    OptionRefs::const_iterator getOption(const std::string &name) const;
protected:
    template <class T>
    std::string _checkValues() const;
    Option& addOption(Option &option);

    OptionRefs options;
};

template <class T>
std::string ModuleOptions::_checkValues() const {
    std::ostringstream err;
    for (const Option &opt : getOptions()) {
        auto option = dynamic_cast<const NumOption<T> *>(&opt);
        if (!option) {
            continue;
        }

        const std::string &name = option->getArgOptName();
        if (!option->isSet()) {
            err << name << " is not set." << std::endl;
        } else {
            const auto &value = option->value;
            if (option->hasMin() && value < option->min) {
                err << name << " is too small (value: " << value << ", minimum: " << option->min << ")." << std::endl;
            }
            if (option->hasMax() && value > option->max) {
                err << name << " is too large (value: " << value << ", maximum: " << option->max << ")." << std::endl;
            }
        }
    }
    return err.str();
}

class InputFilesOptions : public ModuleOptions {
public:
    std::unordered_set<std::string> chromosomeFilter;
    std::vector<std::string> variantFiles;
    std::vector<std::string> referenceFiles;

    virtual void addToParser(seqan::ArgumentParser &argParser);
    virtual void read(seqan::ArgumentParser &argParser);
};

#endif // PG583_MODULE_MODULEOPTIONS_H

#ifndef PG583_MODULE_INDEXMODULE_H
#define PG583_MODULE_INDEXMODULE_H

#include "module/IndexingModule.h"
#include "io/IndexWriter.h"

class IndexModule : public IndexingModule<IndexWriter<>> {
    std::string outputFilePath;
public:
    virtual void addOptions(seqan::ArgumentParser &argParser);
    virtual void readOptions(seqan::ArgumentParser &argParser);
    virtual int run();
    virtual std::string getName() const;
    virtual std::string getDescription() const;
};

#endif // PG583_MODULE_INDEXMODULE_H

#ifndef PG583_MODULE_VARIANTSTATISTICSMODULE_H
#define PG583_MODULE_VARIANTSTATISTICSMODULE_H

#include "module/ApplicationModule.h"

#include <vector>
#include <utility>
#include <cstddef>
using std::size_t;

class VariantStatisticsModule : public ApplicationModule {
private:
    std::string variantsFilePath;

    int readVariantFile();
    void createStatistics();
public:
    std::vector<std::pair<size_t, size_t>> snpCountPerPos;
    std::vector<std::pair<size_t, size_t>> indelCountPerPos;
    std::vector<std::pair<size_t, std::ptrdiff_t>> indelLen;
    std::vector<std::pair<std::string, size_t>> chromosomeStartPositions;

    const static size_t SPACE_BETWEEN_CHROMOSOMS = 10000;

    VariantStatisticsModule();
    void addOptions(seqan::ArgumentParser &argParser);
    void readOptions(seqan::ArgumentParser &argParser);
    int run();
    std::string getName() const;
    std::string getDescription() const;
};

#endif // PG583_MODULE_VARIANTSTATISTICSMODULE_H

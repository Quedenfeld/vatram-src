#ifndef PG583_MODULE_ALIGNMODULE_H
#define PG583_MODULE_ALIGNMODULE_H

#include "module/AligningModule.h"
#include "module/MappingModule.h"

class AlignModule : public MappingModule, public AligningModule {
private:
    MappingOptions mappingOptions2ndTry;
    LshMapOptions mapOptions2ndTry;
    bool shouldUse2ndTryMapping;
public:
    AlignModule();
    virtual void addOptions(seqan::ArgumentParser &argParser);
    virtual void readOptions(seqan::ArgumentParser &argParser);
    virtual int run();
    virtual std::string getName() const;
    virtual std::string getDescription() const;

private:
    virtual AlignmentStats processRead(Aligner &aligner, PairedRead& read) override;
};

#endif // PG583_MODULE_ALIGNMODULE_H

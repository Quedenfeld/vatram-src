#include "module/AligningModule.h"

void AligningModule::addOptions(seqan::ArgumentParser &argParser) {
    alignOptions.addToParser(argParser);
    seqan::addOption(argParser, seqan::ArgParseOption("", "no-realign",
            "If set, an already aligned read in the SAM file will not be realigned."));
}

void AligningModule::readOptions(seqan::ArgumentParser &argParser) {
    alignOptions.read(argParser);
    noRealign = seqan::isSet(argParser, "no-realign");
}

void AligningModule::prepareAligning(const ChromosomeReferences &chromosomes) {
    this->chromosomes = chromosomes;
    variantIndexes.clear();
    variantIndexes.reserve(this->chromosomes.size());
    for (const Chromosome &chromosome : this->chromosomes) {
        variantIndexes.emplace_back(chromosome.variants, seqan::length(chromosome.data) - 1);
    }
}

void AligningModule::alignRead(Aligner &aligner, const ReadString &read, const ReadString &reversedRead,
                               const std::vector<MapInterval> &intervals, seqan::BamAlignmentRecord &record,
                               AlignmentStats &readStats) {
    for (size_t i = 0; i < intervals.size() && readStats.minErrors > 0; ++i) {
        ChromosomeIndex chrIdx = intervals[i].chromosome;
        const ReadString &pattern = intervals[i].inverted ? reversedRead : read;

//        const std::vector<SnpVariant>& lowProbSnps = chromosomes[chrIdx].get().lowProbabilitySnps;
        size_t startPos = intervals[i].startPosition;
        size_t endPos = intervals[i].endPosition;
//        auto it = std::lower_bound(lowProbSnps.begin(), lowProbSnps.end(), startPos, [](const SnpVariant& var, size_t val){return var.position < val;});
//        auto itEnd = lowProbSnps.end();
//        Alignment alignment;
//        if (it != itEnd && it->position < endPos) {
//            // temporary ref-copy with low-probability-SNPs
//            std::vector<SnpVariant> originalBases;
//            Chromosome& chr = const_cast<Chromosome&>(chromosomes[chrIdx].get());
//            for (; it != itEnd && it->position < endPos; ++it) {
//                originalBases.push_back(SnpVariant{it->position, chr.data[it->position]});
//                chr.addSNP(it->position, it->snp);
//            }
//            alignment = aligner.align(chr, startPos,
//                                      endPos, variantIndexes[chrIdx], pattern,
//                                      readStats.minErrors - 1, alignOptions.maxClip.value);
//            for (SnpVariant& snp: originalBases) {
//                chr.data[snp.position] = snp.snp;
//            }
//        } else {
            // there are no low-probability-SNPs
        Alignment alignment = aligner.align(chromosomes[chrIdx], startPos,
                                            endPos, variantIndexes[chrIdx], pattern,
                                            readStats.minErrors - 1, alignOptions.maxClip.value);
//        }

        ++readStats.alignerCalls;
        if (alignment.isValid()) {
            ++readStats.alignments;
            readStats.minErrors = alignment.getErrorCount();
            readStats.alignedIntervalSize = intervals[i].endPosition - intervals[i].startPosition;
            SamWriter::setAttributes(record, intervals[i].inverted, chrIdx,
                                     /*intervals[i].startPosition +*/ alignment.getPosition(), alignment.getCigar());
        }
    }
}

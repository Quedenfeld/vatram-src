#ifndef PG583_MODULE_ALIGNINGMODULE_H
#define PG583_MODULE_ALIGNINGMODULE_H

#include "module/ReferenceLoadingModule.h"
#include "module/ReadProcessingModule.h"
#include "module/AlignOptions.h"

class AligningModule : virtual public ReferenceLoadingModule, virtual public ReadProcessingModule {
protected:
    AlignOptions alignOptions;

    ChromosomeReferences chromosomes;
    std::vector<VariantIndex> variantIndexes;

    void prepareAligning(const ChromosomeReferences &chromosomes);
    void alignRead(Aligner &aligners, const ReadString &read, const ReadString &reversedRead,
                   const std::vector<MapInterval> &intervals, seqan::BamAlignmentRecord &record,
                   AlignmentStats &readStats);
public:
    virtual void addOptions(seqan::ArgumentParser &argParser);
    virtual void readOptions(seqan::ArgumentParser &argParser);
    virtual int run() = 0;
    virtual std::string getName() const = 0;
    virtual std::string getDescription() const = 0;
};

#endif // PG583_MODULE_ALIGNINGMODULE_H

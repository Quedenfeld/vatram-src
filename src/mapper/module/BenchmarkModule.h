#ifndef PG583_MODULE_BENCHMARKMODULE_H
#define PG583_MODULE_BENCHMARKMODULE_H

#include "module/ApplicationModule.h"
#include "module/ModuleOptions.h"

class BenchmarkModule : public ApplicationModule {
    InputFilesOptions inputFileOptions;
    int genomeLength;
    int numberOfReads;
    int readLength;
    double indelProbability;
    double substitutionProbability;
    std::string inputGenomeFilePath;
    std::string inputVariantFilePath;
    std::string outputGenomeFilePath;
    std::string outputReadsFilePath;
    std::string outputReadsAsSam;
    std::string inputPerfectSam;
    std::string inputCompareSam;
public:
    BenchmarkModule();
    void addOptions(seqan::ArgumentParser &argParser);
    void readOptions(seqan::ArgumentParser &argParser);
    int run();
    std::string getName() const;
    std::string getDescription() const;
};

#endif // PG583_MODULE_BENCHMARKMODULE_H

#ifndef PG583_TEST_SEMI_GLOBAL_ALIGNER_TEST_H
#define PG583_TEST_SEMI_GLOBAL_ALIGNER_TEST_H

#include <string>

class SemiGlobalAlignerTest {
public:
    bool runTest();

    bool test1();

    bool test2();

    bool test3();

    bool test4();

    static bool testJQ();

    bool denseTest(std::string id, std::string reference, std::string read, std::string cigar);
};

#endif // PG583_TEST_SEMI_GLOBAL_ALIGNER_TEST_H

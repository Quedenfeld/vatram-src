#ifndef SEMIGLOBALALIGNERUNITTEST_H
#define SEMIGLOBALALIGNERUNITTEST_H

//#include <QTest/QTest>

#include "align/Variant.h"
#include "align/SemiGlobalAligner.h"
#include <seqan/sequence.h>
#include <seqan/file.h>

#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <cstdlib>
#include <unordered_set>

class SemiGlobalAlignerUnitTest/*: public QObject*/ {

private:
    bool equals(Alignment &a1, Alignment &a2);
    bool matches(std::vector<Alignment> &as1, std::vector<Alignment> &as2);
    bool test(std::vector<Variant> &variants, std::string &ref, std::string &read, std::vector<Alignment> &expAlignments);
public:
    SemiGlobalAlignerUnitTest();
//private slots:
    bool testCase01();
    bool testCase02();
    bool testCase03();
    bool testCase04();
    bool testCase05();
    bool testCase06();
    bool testCase07();
    bool testCase08();
    bool testCase09();
    bool testCase10();
    bool testCase11();
    bool testCase12();
    bool testCase13();
    bool testCase14();
    bool testCase15();
    bool testCase16();
    bool testCase17();
    bool testCase18();
    bool testCase19();
    bool testCase20();
    bool testCase21();
    bool testCase22();
};

#endif // SEMIGLOBALALIGNERUNITTEST_H

#include <QString>
#include <QtTest>

#include "CollisionFreeBhtTest.h"

#include "map/CollisionFreeBHT.h"

#include <utility>
#include <unordered_set>
#include <unordered_map>
#include <random>

CollisionFreeBhtTest::CollisionFreeBhtTest() {
}

void CollisionFreeBhtTest::collisionFreeBhtTest_data() {
    QTest::addColumn< CollisionFreeBHT<>::BandHash >("bandHash");
    QTest::addColumn< std::vector<WindowIndex> >("windowIndex");
    QTest::addColumn< std::vector<WindowIndex> >("hashBucket");

    CollisionFreeBHT<>::BandHash singleEntryBandHash;
    std::vector<WindowIndex> singleEntryWindowIndex;
    std::vector<WindowIndex> singleEntryHashBucket;
    singleEntryBandHash = 20;
    singleEntryWindowIndex.push_back(30);
    singleEntryHashBucket.push_back(30);

    CollisionFreeBHT<>::BandHash multiWindowBandHash;
    std::vector<WindowIndex> multiWindowWindowIndex;
    std::vector<WindowIndex> multiWindowHashBucket;
    multiWindowBandHash = 40;
    multiWindowWindowIndex.push_back(70);
    multiWindowWindowIndex.push_back(80);
    multiWindowHashBucket.push_back(70);
    multiWindowHashBucket.push_back(80);

    QTest::newRow("Single entry") << singleEntryBandHash << singleEntryWindowIndex << singleEntryHashBucket;
    QTest::newRow("Multi window entry") << multiWindowBandHash << multiWindowWindowIndex << multiWindowHashBucket;
}

void CollisionFreeBhtTest::collisionFreeBhtTest() {
    QFETCH(CollisionFreeBHT<>::BandHash, bandHash);
    QFETCH(std::vector<WindowIndex>, windowIndex);
    QFETCH(std::vector<WindowIndex>, hashBucket);

    CollisionFreeBHT<> table(3);
    for (unsigned int i = 0; i < windowIndex.size(); i++) {
        table.add(bandHash, windowIndex.at(i));
    }
    std::vector<WindowIndex> result = table.find(bandHash, maximumOfReturnedWindows);
//    for (WindowIndex windowIndex : result) {
//        qDebug("%d", windowIndex);
//    }
    QCOMPARE(result, hashBucket);
}

// Test the hash table with random entries
void CollisionFreeBhtTest::randomTest() {
    typedef CollisionFreeBHT<>::BandHash Key;
    typedef WindowIndex Value;
    std::unordered_map<Key, std::unordered_set<Value>> check;
    CollisionFreeBHT<> cfbht(10);
    const size_t n = 100000;
    const size_t keyMax = n;
    std::mt19937 rnd;

    for (size_t i = 0; i < n; i++) {
        Key key = rnd() % keyMax;
        Value value = rnd() & MultiWindowManager::WindowIndexEntry::VALUE_MASK;
        cfbht.add(key, value);
        check[key].insert(value);
    }

    for (size_t i = 0; i < keyMax; i++) {
        std::vector<Value> res = cfbht.find(i, maximumOfReturnedWindows);
        std::unordered_set<Value> resSet(res.begin(), res.end());

        QCOMPARE(check[i], resSet);
    }
}

void CollisionFreeBhtTest::removeTest() {
    typedef CollisionFreeBHT<>::BandHash Key;
    typedef WindowIndex Value;
    std::unordered_map<Key, std::unordered_set<Value>> check;
    CollisionFreeBHT<> cfbht(10);
    const size_t n = 100000;
    const size_t keyMax = n * 5 / 4;
    std::mt19937 rnd;

    for (size_t i = 0; i < n; i++) {
        Key key = rnd() % keyMax;
        Value value = rnd() & MultiWindowManager::WindowIndexEntry::VALUE_MASK;
        cfbht.add(key, value);
        check[key].insert(value);
    }

//    for (size_t i = 0; i < n/2; i++) {
//        Key key = rnd() % keyMax;
//        std::vector<Value> vec = cfbht.find(key, maximumOfReturnedWindows);
//        if (vec.size()) {
//            Value value = vec[rnd() % vec.size()];
//            cfbht.remove(key, value);
//            check[key].erase(value);
//        }
//    }

    for (size_t i = 0; i < keyMax; i++) {
        std::vector<Value> res = cfbht.find(i, maximumOfReturnedWindows);
        std::unordered_set<Value> resSet(res.begin(), res.end());

        if (check[i] != resSet) {
            std::cerr << "testCFBHT() : err at = " << i << std::endl;
            for (Value v: resSet) {
                std::cerr << v << ",";
            }
            std::cerr << std::endl;
            for (Value v: check[i]) {
                std::cerr << v << ",";
            }
            std::cerr << std::endl;
            std::cerr << std::endl;
        }
        QCOMPARE(check[i], resSet);
    }
}

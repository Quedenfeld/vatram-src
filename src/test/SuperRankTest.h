#ifndef SUPERRANKTEST_H
#define SUPERRANKTEST_H
#include <QtTest>
#include "map/SuperRank.h"

//Q_DECLARE_METATYPE(SuperRank::WindowIndex)

class SuperRankTest : public QObject {
    Q_OBJECT
    const size_t maximumOfReturnedWindows = 10;
public:
    SuperRankTest();
private Q_SLOTS:
    void superRankTest();
    void testNoCollision();
    void testRandomNumbers();
};

#endif // SUPERRANKTEST_H

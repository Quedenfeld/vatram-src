#include "LSHTest.h"
#include "map/LSHimpl.h"

#include <ctime>
#include <unordered_set>
#include <unordered_map>

void LSHTest::testLSH() {
    // TODO: was soll man hier testen??
    //LSH(decltype(initialRandomSeed) initialRandomSeed);
}

void LSHTest::testConfigureInitialState() {
    // TODO: was soll man hier testen??
    //template <class Random>
    //void configureInitialState(Random &random, size_t);
}

void LSHTest::testAddGenome() {
    LSH lsh(0);
    lsh.qGramSize = 3;
    lsh.windowSize = 4;
    lsh.windowOffset = 4;
    lsh.signatureLength = 6;
    lsh.initialize(3);
    LSH lsh2(0);
    lsh2.qGramSize = lsh.qGramSize;
    lsh2.windowSize = lsh.windowSize;
    lsh2.windowOffset = lsh.windowOffset;
    lsh2.signatureLength = lsh.signatureLength;
    lsh2.initialize(3);
    Genome genome;
    genome.emplace_back("1", "ACGT");
    genome.emplace_back("2", "AAAA");
    genome.emplace_back("3", "CGCT");


    lsh.addGenome(genome.begin(), genome.end());
    for (const Chromosome &c: genome) {
        lsh2.addChromosome(c);
    }

    QCOMPARE(lsh.chromosomes.size(), genome.size());
    QCOMPARE(lsh2.chromosomes.size(), genome.size());

    for (size_t i = 0; i < genome.size(); i++) {
        QCOMPARE(&lsh.chromosomes[i].get(), &lsh2.chromosomes[i].get());
    }
    // * @brief Adds all chromosomes between begin_it and end_it.
    // */
    //template <class Iterator>
    //void addGenome(Iterator begin_it, Iterator end_it);
}

void LSHTest::testAddChromosome() {
    LSH lsh(0);
    lsh.qGramSize = 3;
    lsh.windowSize = 7;
    lsh.windowOffset = 3;
    lsh.signatureLength = 10;
    const int lng = 1000;
    std::mt19937 rnd;
    lsh.initialize(lng);


    char sym[4] = {'A', 'C', 'G', 'T'};
    ReferenceString data = "";
    for (size_t i = 0; i < lng; i++) {
        data += sym[rnd() % 4];
    }
    Chromosome ch("1", data);

    lsh.addChromosome(ch);
    size_t windows = (lng - (lsh.windowSize - lsh.windowOffset)) / lsh.windowOffset;
    QCOMPARE(lsh.referenceWindows.size(), windows);
    // * @brief
    // * @details Creates overlapping windows of the chromosome (using the attributes windowSize and windowOffset).
    // *      Each window is stored as a Reference object and is added to the hash table via addReference().
    // * @param chromosome
    // */
    //void addChromosome(const Chromosome &chromosome);
}


void LSHTest::testAddReferenceWindow() {
    LSH lsh(0);
    lsh.qGramSize = 3;
    lsh.windowSize = 5;
    lsh.windowOffset = 5;
    lsh.signatureLength = 10;
    std::mt19937 rnd;
    lsh.initialize(20);

    Chromosome ch("1", "AACCGGTTAGGTACGTACGT");
    ReadString rs =         "GTTAG";

    lsh.chromosomes.push_back(ch);

    QVERIFY(false); // FIXME: LSH::addReferenceWindow has been removed
//    WindowIndex wi = lsh.addReferenceWindow(0, 0);
//    if (wi != 0) {
//        return false;
//    }

//    wi = lsh.addReferenceWindow(0, 10);
//    if (wi != 1) {
//        return false;
//    }

//    std::vector<WindowIndex> out, out_rv;
//    lsh.findRead(rs, out, out_rv);
//    if (out.size() != 0 || out_rv.size() != 0) {
//        std::cerr << "testAddReferenceWindow() : found unknown read" << std::endl;
//        return false;
//    }

//    wi = lsh.addReferenceWindow(0, 5);
//    if (wi != 2) {
//        return false;
//    }

//    lsh.findRead(rs, out, out_rv);
//    if (out.size() == 0) {
//        std::cerr << "testAddReferenceWindow() : read not found" << std::endl;
//        return false;
//    }

//    return true;

    // * @brief Adds one window of the reference genome.
    // * @details This function adds the reference to the attribute references, calculates the q-grams via createQGrams_iupac,
    // *      calculates their signatures and adds the corresponding band values to the bandHashTable
    // * @param reference a window of the reference genome
    // * @return the reference ID of the added reference.
    // */
    //WindowIndex addReferenceWindow(ChromosomeIndex chromosomeIndex, Position beginPosition);
}

void LSHTest::testCreateQGramHashes() {
    LSH lsh(0);
    lsh.qGramSize = 4;
    lsh.windowSize = 8;

    ReadString ref = "AACCACCA";

    std::vector<LSH::QGramHash> out;
    lsh.createQGramHashes(seqan::begin(ref), seqan::end(ref), out);

    QCOMPARE(out.size(), size_t(4));

    ref = "AAAAAAAA";
    lsh.createQGramHashes(seqan::begin(ref), seqan::end(ref), out);
    QCOMPARE(out.size(), size_t(1));

    // * @brief creates the q-gram hashes of a DnaString or Dna5String
    // * @param window_begin
    // * @param window_end
    // * @param out_hashes After execution of this function, this variable contains the q-grams (respectively their hash values)
    // */
    //template <class Iterator>
    //void createQGramHashes(Iterator window_begin, Iterator window_end, std::vector<QGramHash> &out_hashes);
}

void LSHTest::testCreateQGramHashes_iupac() {
    LSH lsh(0);
    lsh.qGramSize = 4;
    lsh.windowSize = 8;
    lsh.limit = 100;
    lsh.limit_skip = 200;

    ReferenceString ref = "AACNTTGA";

    std::vector<LSH::QGramHash> out;
    lsh.createQGramHashes_iupac(seqan::begin(ref), seqan::end(ref), out);
    QCOMPARE(out.size(), size_t(17)); // 4+4+4+4+1

    lsh.limit = 2;
    int succ = 0;
    for (size_t i = 0; i < 100; i++) {  // 100 Versuche!
        lsh.createQGramHashes_iupac(seqan::begin(ref), seqan::end(ref), out);
        if (out.size() == 9) { // 2+2+2+2+1
            succ++;
        }
    }
    QVERIFY(succ >= 10); // mind. 10 mal Erfolg

    lsh.limit_skip = 3;
    lsh.createQGramHashes_iupac(seqan::begin(ref), seqan::end(ref), out);
    QCOMPARE(out.size(), size_t(1)); // 0+0+0+0+1
    // * @brief creates the q-gram hashes of a IupacString
    // * @param window_begin
    // * @param window_end
    // * @param out_hashes After execution of this function, this variable contains the q-grams (respectively their hash values)
    // */
    //template <class Iterator>
    //void createQGramHashes_iupac(Iterator window_begin, Iterator window_end, std::unordered_set<QGramHash> &out_hashes);
}

void LSHTest::testCreateSignatures() {
    LSH lsh(0);
    std::mt19937 rnd;
    lsh.signatureLength = 17;
    lsh.initialize(100);
    std::vector<LSH::QGramHash> qgrams;
    for (size_t i = 0; i < 100; i++) {
        qgrams.push_back(rnd());
    }
    std::vector<LSH::SignatureHash> out;
    lsh.createSignatures(qgrams.begin(), qgrams.end(), out);
    QCOMPARE(out.size(), lsh.signatureLength);
    std::unordered_set<LSH::SignatureHash> signatures(out.begin(), out.end());

    std::vector<LSH::QGramHash> qgrams2;
    for (size_t i = 0; i < 50; i++) {
        qgrams2.push_back(rnd());
    }
    for (size_t i = 50; i < 100; i++) {
        qgrams2.push_back(qgrams[i]);
    }
    std::vector<LSH::SignatureHash> out2;
    lsh.createSignatures(qgrams2.begin(), qgrams2.end(), out2);
    size_t collisions = 0;
    for (auto sig: out2) {
        if (signatures.find(sig) != signatures.end()) {
            collisions++;
        }
    }
    QVERIFY(collisions != 0);
    //template <class Iterator>
    //void createSignatures(Iterator qGramHashes_begin, Iterator qGramHashes_end, std::vector<SignatureHash> &out_signatures);
}

void LSHTest::testCreateBandHashes() {
    LSH lsh(0);
    lsh.bandSize = 5;
    const size_t input_length = 10;
    std::vector<LSH::BandHash> output;
    std::vector<LSH::SignatureHash> input;
    std::mt19937 rnd;
    for (size_t i = 0; i < input_length; i++) {
        input.push_back(rnd());
    }
    lsh.createBandHashes(input.begin(), input.end(), output);
    QCOMPARE(output.size(), input_length / lsh.bandSize);
    QCOMPARE(output[0], lsh.bandHashFunction(&input[0], &input[lsh.bandSize]));
    //template <class Iterator>
    //void createBandHashes(Iterator signature_begin, Iterator signature_end, std::vector<BandHash> &out_bandHashes);
}

void LSHTest::testFindRead() {
    LSH lsh(0);
    lsh.qGramSize = 5;
    lsh.windowSize = 10;
    lsh.windowOffset = 5;
    lsh.signatureLength = 10;
    lsh.bandSize = 2;
    lsh.initialize(20);
    Chromosome ch("M");
    //         .....-----.....-----.....-----.....
    ch.data = "AATTACCCGTGTGGACGCGTGCTTAGTCGATTTGATGG";
    lsh.addChromosome(ch);
    lsh.buildIndex();
    ReadString read =   "GTGGACGCGT";
    ReadString reversedRead(read);
    seqan::reverseComplement(reversedRead, seqan::Serial());
    std::vector<WindowIndex> out, out_rc;
    lsh.findRead(read, reversedRead, lsh._mapOptions, out, out_rc);
    QCOMPARE(out_rc.size(), size_t(0));
    QVERIFY(out.size() > 0 && out.size() < 2);
    QCOMPARE(out[0], WindowIndex(2));
    // * @brief Finds the read and its reverse complement in the genome.
    // * @param out_referenceIDs Positions where the read was found.
    // * @param out_referenceIDs_revComplement Positions where the reverse complement of the read was found.     */
    //void findRead(ReadString read,
    //              std::vector<WindowIndex> &out_referenceWindowIndices,
    //              std::vector<WindowIndex> &out_referenceWindowIndices_revComplement);
}

void LSHTest::testFindRead_revCompl() {
    LSH lsh(0);
    lsh.qGramSize = 5;
    lsh.windowSize = 10;
    lsh.windowOffset = 5;
    lsh.signatureLength = 10;
    lsh.bandSize = 2;
    lsh.initialize(20);
    Chromosome ch("1");
    //         .....-----.....-----.....-----.....
    ch.data = "AATTACCCGTGTGGACGCGTGCTTAGTCGATTTGATGG";
    lsh.addChromosome(ch);
    lsh.buildIndex();
    ReadString read =   "ACGCGTCCAC";// "GTGGACGCGT";
    ReadString reversedRead(read);
    seqan::reverseComplement(reversedRead, seqan::Serial());
    std::vector<WindowIndex> out, out_rc;
    lsh.findRead(read, reversedRead, lsh._mapOptions, out, out_rc);
    QCOMPARE(out.size(), size_t(0));
    QVERIFY(out_rc.size() > 0 && out_rc.size() < 2);
    QCOMPARE(out_rc[0], WindowIndex(2));
}

void LSHTest::testQGramHashFunction() {
    // Checks if there are collisions for very short q-grams
    LSH lsh(0);
    const size_t qlength = 10;
    std::vector<seqan::Dna> currentQgram(qlength);
    std::unordered_set<LSH::QGramHash> hashes;
    for (size_t i = 0; i < (1 << (2*qlength)); i++) {
        size_t rest = i;
        for (size_t j = 0; j < qlength; j++) {
            currentQgram[j] = seqan::Dna(rest % 4);
            rest /= 4;
        }
        LSH::QGramHash h = lsh.qGramHashFunction(currentQgram.begin(), currentQgram.end());
        QCOMPARE(hashes.find(h), hashes.end());
        hashes.insert(h);
    }
    //template <class Iterator> // an iterator pointing to seqan::Dna or seqan::Dna5
    //QGramHash qGramHashFunction(Iterator qgram_begin, Iterator qgram_end);
}

void LSHTest::testBandHashFunction() {
    // Checks if there are collisions for random signatures
    const size_t testCases = 1 << 16;
    const size_t bandSize = 2;
    LSH lsh(0);
    std::mt19937 rnd(std::time(nullptr));
    std::unordered_set<LSH::BandHash> hashes;

    size_t collisions = 0;
    for (size_t i = 0; i < testCases; i++) {
        std::vector<LSH::QGramHash> input(bandSize);
        for (size_t j = 0; j < bandSize; j++) {
            input[j] = rnd();
        }
        LSH::BandHash h = lsh.bandHashFunction(input.begin(), input.end());
        if (hashes.find(h) != hashes.end()) {
            collisions++;
        }
        hashes.insert(h);
    }

    QVERIFY(collisions <= 2); // höchstens 2 Kollisionen

    //template <class Iterator>
    //BandHash bandHashFunction(Iterator signature_begin, Iterator signature_end);
}

namespace {
seqan::Iupac toIupac(int t, int a, int c, int g) {
    return seqan::Iupac(t + (a << 1) + (c << 2) + (g << 3));
}
}

void LSHTest::testFindCombinationsOfQGrams() {
    LSH lsh(0);

    std::vector<seqan::Iupac> iupacString;
    iupacString.push_back(toIupac(0, 0, 1, 1));
    iupacString.push_back(toIupac(0, 0, 0, 1));
    iupacString.push_back(toIupac(0, 0, 1, 0));
    iupacString.push_back(toIupac(0, 1, 0, 0));
    iupacString.push_back(toIupac(1, 1, 1, 1));
    iupacString.push_back(toIupac(1, 0, 0, 0));
    iupacString.push_back(toIupac(0, 1, 1, 1));
    iupacString.push_back(toIupac(0, 0, 1, 1));
    const size_t combinations = 48;

    size_t count = 0;

    typedef typename std::array<seqan::Dna, MAX_Q_GRAM_SIZE>::iterator OpIt;
    std::vector<std::vector<seqan::Dna>> qgrams;
    auto func = [&count, &qgrams](OpIt beginIt, OpIt endIt) {
        count++;
        qgrams.emplace_back(beginIt, endIt);
//        for (; beginIt != endIt; ++beginIt) {
//            std::cout << seqan::Dna(*beginIt);
//        }
//        std::cout << std::endl;
    };


    lsh.qGramSize = iupacString.size();
    lsh.findCombinationsOfQGram(iupacString.begin(), func);

    QCOMPARE(count, combinations);

    // check if there are equal qgrams in the container
    for (size_t i = 0; i < qgrams.size(); i++) {
        for (size_t j = i+1; j < qgrams.size(); j++) {
            // check if qgrams have the correct length
            QCOMPARE(qgrams[i].size(), iupacString.size());
            QCOMPARE(qgrams[j].size(), iupacString.size());

            // check if qgrams are inequal
            bool unequal = false;
            for (size_t k = 0; k < qgrams[i].size(); k++) {
                if (qgrams[i][k] != qgrams[j][k]) {
                    unequal = true;
                }
            }
            QVERIFY(unequal);
        }
    }

    // check if every qgram is in the base iupac-string
    for (size_t i = 0; i < qgrams.size(); i++) {
        auto& qgram = qgrams[i];
        for (size_t k = 0; k < qgram.size(); k++) {
            int dna = seqan::Iupac(qgram[k]).value;
            int iupac = iupacString[k].value;

            QVERIFY((dna & iupac) != 0);
        }
    }

    // * Find all variant combinations of the substring "iupacString[beginPos...beginPos+qGramSize]"
    // *  and execute the function operation on the founds.
    // *  @param curQGram arbitrary space of length qGramSize
    // *  @param curVariant arbitrary space of length qGramSize+1
    // *  @param operation a object that overloads operator()(Iterator, Iterator) with Iterator = std::vector<seqan::DNA>::iterator
    // */
    //template <class Iterator, class Function>
    //void findCombinationsOfQGram(Iterator iupacQgram_begin, Function &operation);
}

void LSHTest::testCFBHT() {
    typedef CollisionFreeBHT<>::BandHash Key;
    typedef WindowIndex Value;
    std::unordered_map<Key, std::unordered_set<Value>> check;
    const size_t maximumOfReturnedWindows = 10;
    CollisionFreeBHT<> cfbht(10);
    const size_t n = 100000;
    const size_t keyMax = n * 5 / 4;
    std::mt19937 rnd;


    for (size_t i = 0; i < n; i++) {
        Key key = rnd() % keyMax;
        Value value = rnd() & MultiWindowManager::WindowIndexEntry::VALUE_MASK;
        cfbht.add(key, value);
        check[key].insert(value);
    }

    size_t counter = 0;
    for (auto bandHash: cfbht) {
        counter++;
        QVERIFY(check.count(bandHash));
    }
    QCOMPARE(counter, check.size());

    size_t correct = 0;
    for (size_t i = 0; i < keyMax; i++) {
        auto& correctResult = check[i];
        std::vector<Value> res = cfbht.find(i, maximumOfReturnedWindows);
        std::unordered_set<Value> resSet(res.begin(), res.end());

        QCOMPARE(correctResult, resSet);
#if 0
            std::cerr << "testCFBHT() : last correct = " << correct << std::endl;
            for (Value v: resSet) {
                std::cerr << v << ",";
            }
            std::cerr << std::endl;
            for (Value v: correctResult) {
                std::cerr << v << ",";
            }
            std::cerr << std::endl;
            std::cerr << std::endl;
#endif
        correct++;
    }
}

void LSHTest::testFindIntervals() {
    LSH lsh(0);
    lsh.qGramSize = 3;
    lsh.windowSize = 3;
    lsh.windowOffset = 3;
    lsh.signatureLength = 10;
    lsh.initialize(1000);

    Chromosome ch("1");
    ch.data = "AAACGTCGATGCCGTATATAT";
    lsh.addChromosome(ch);
    lsh.buildIndex();

    ReadString read = "CGT";
    ReadString reversedRead(read);
    seqan::reverseComplement(reversedRead, seqan::Serial());
    std::vector<MapInterval> results;
    std::cout << "_test" << std::endl;
    lsh.findInterval_singleEnd(read, reversedRead, lsh._mapOptions, results);

    QCOMPARE(results.size(), size_t(2));

    QCOMPARE(results[0].chromosome, ChromosomeIndex(0));
    QCOMPARE(results[0].startPosition, Position(0));
    QCOMPARE(results[0].endPosition, Position(14));
    QCOMPARE(results[0].inverted, false);

    QCOMPARE(results[1].chromosome, ChromosomeIndex(0));
    QCOMPARE(results[1].startPosition, Position(4));
    QCOMPARE(results[1].endPosition, Position(21));
    QCOMPARE(results[1].inverted, false);
}

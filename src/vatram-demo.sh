#! /bin/bash

refUrlPrefix='ftp://ftp.ncbi.nih.gov/genomes/Homo_sapiens/ARCHIVE/ANNOTATION_RELEASE.105/Assembled_chromosomes/seq/hs_ref_GRCh37.p13_chr'
refUrlSuffix='.fa.gz'
varUrl='ftp://ftp.ncbi.nih.gov/snp/organisms/human_9606_b142_GRCh37p13/VCF/00-common_all.vcf.gz'
readsUrl='ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data/HG00096/alignment/HG00096.mapped.ILLUMINA.bwa.GBR.low_coverage.20120522.bam'

readsFile='reads.bam'
outputNoVars='output-no-vars.bam'
outputWithVars='output-with-vars.bam'

chromosomes=()
intervals=()
regions=()

if [ ${#regions[@]} -eq 0 ]; then
	if [ ${#chromosomes[@]} -eq 0 ]; then
		chromosomes=('21' '22')
	fi
	declare -A cs
	for c in ${chromosomes[@]}; do
		cs[$c]=1
	done
	unset -v chromosomes
	chromosomes=${!cs[@]}
	if [ ${#intervals} -eq 0 ]; then
		for i in `seq 8 2 32`; do
			intervalStartMultiplier=1000000
			intervalStartOffset=0
			intervalSize=100000
			intervalStart=$[intervalStartMultiplier * i + intervalStartOffset]
			intervalEnd=$[intervalStart + intervalSize]
			intervals+=("${intervalStart}-${intervalEnd}")
		done
	fi
	for chr in ${chromosomes[@]}; do
		for interval in ${intervals[@]}; do
			regions+=("${chr}:${interval}")
		done
	done
else
	if [ ${#chromosomes[@]} -eq 0 ]; then
		for r in ${regions[*]}; do
			chromosomes+=${r%:*}
		done
	fi
	declare -A cs
	for c in ${chromosomes[@]}; do
		cs[$c]=1
	done
	unset -v chromosomes
	chromosomes=${!cs[@]}
fi


varFiles=()
refFiles=()
for chr in ${chromosomes[@]}; do
	refUrl="${refUrlPrefix}${chr}${refUrlSuffix}"
	refFile="ref${chr}.fa.gz"
	varFile="var${chr}.vcf"
	if [ ! -f $refFile ]; then
		echo 
		echo Downloading reference chromosome $chr...
		curl -o $refFile $refUrl
		if [ ! $? -eq 0 ]; then
			echo Could not download reference.
			exit $?
		fi
	fi
	if [ ! -f $varFile ]; then
		echo 
		echo Downloading variants for chromosome $chr...
		bcftools view $varUrl $chr >$varFile
		if [ ! $? -eq 0 ]; then
			echo Could not download variants.
			exit $?
		fi
	fi
	refFiles+=($refFile)
	varFiles+=($varFile)
done

if [ ! -f $readsFile ]; then
	echo 
	echo Downloading reads...
	samtools view -b -1 -o $readsFile $readsUrl ${regions[*]}
	if [ ! $? -eq 0 ]; then
		echo Could not download reads.
		exit $?
	fi
fi

echo 
echo Running VATRAM without variants...
cmd="vatram align -o $outputNoVars `for c in ${chromosomes[@]}; do echo -f $c; done` `for r in ${refFiles[@]}; do echo -r $r; done` $readsFile"
echo $cmd
echo 
eval $cmd
echo
echo Running VATRAM with variants...
cmd="vatram align -o $outputWithVars `for c in ${chromosomes[@]}; do echo -f $c; done` `for v in ${varFiles[@]}; do echo -v $v; done` `for r in ${refFiles[@]}; do echo -r $r; done` $readsFile"
echo $cmd
echo 
eval $cmd

echo
echo Running \`samtools flagstat\` for VATRAM without variants output...
samtools flagstat $outputNoVars | grep -m2 'total\|mapped'
echo 
echo Running \`samtools flagstat\` for VATRAM with variants output...
samtools flagstat $outputWithVars | grep -m2 'total\|mapped'


#include "experiments/VaryParameters.h"
#include "experiments/HumanGenomeExperiment.h"
#include "experiments/SpeedTest.h"
#include "map/CollisionFreeBHT.h"
#include "GenomeGenerator.h"
#include "Timer.h"
#include "io/VariantsReader.h"
#include "io/ReferenceReader.h"
#include "experiments/RealReadExperiment.h"

#include "ReadGenerator.h"
#include "VariantGenerator.h"
#include "experiments/ReadLengthExp.h"

#include "compare/RMExperiment.h"
#include "compare/RMExperimentPaired.h"

#include "FilterPacBioReads.h"

#include <seqan/seq_io.h>

#include <ctime>
#include <iostream>
#include <unordered_map>
#include <unordered_set>

void createHumanGenomeReads(std::ostream& file, std::vector<std::string>& refPaths) {
    Genome genome;
    std::unordered_set<std::string> filter;

    std::cout << "Read genome" << std::endl;
    Timer<> timer_read;
    ReferenceReader rr(genome, filter);
    rr.readReferences(refPaths);
    std::cout << "  time: " << timer_read << std::endl;

    GenomeGenerator gen;
    size_t length = 0;
    for (const Chromosome &chr : genome) {
        length += seqan::length(chr.data);
    }
    std::vector<GenomeGenerator::Read> allreads;
    for (const Chromosome &chr : genome) {
       std::vector<GenomeGenerator::Read> reads = gen.createReads(chr, (seqan::length(chr.data) / (double)length)*1000000 , 100, 0.0, 0.0);
       for (GenomeGenerator::Read & current : reads){
           allreads.push_back(current);
       }
    }
//    std::ofstream file("/scratch/PG583/createdReads.fastq");
    for (size_t i = 0; i < allreads.size(); i++) {
        ReadString read = allreads[i].readString;
        seqan::writeRecord(file, std::to_string(i), read, seqan::Fastq());
    }
    file.flush();
}

template <class Function>
void createReads(const std::vector<std::string> &refPaths,
                 const std::vector<std::string> &varPaths,
                 const std::string &samPath,
                 uint64_t seed,
                 Function rgFunc) {
    Genome genome;//
    std::unordered_set<std::string> filter;

    std::cout << "readReferences()" << std::endl;
    ReferenceReader rr(genome, filter);
    rr.readReferences(refPaths);

//    std::cout << "VariantGenerator()" << std::endl;
//    VariantGenerator vg(genome, seed);
//    auto variants = vg.createSNPs(1000000, [](){return 0.4;});
//    vg.writeVariants(variants, varPaths[0]);


    std::cout << "readVariants()" << std::endl;
    VariantsReader vr(genome, filter);
    vr.readVariants(varPaths);

//    GenomeGenerator gg(seed);
//    gg.createReadsAndSamWithVariants(genome, 100, 0.25, 0.001, 0.001, 0.018, 0.0, numReads, samPath);


    std::cout << "ReadGenerator()" << std::endl;
    ReadGenerator rg(genome, varPaths, seed);
    rg.readVariants();
//    auto reads = rg.createReads(numReads, 100, 0.018*4, 0.002*4, 0.3);
//    auto reads = rg.createReads_pairedEnd(numReads/2, 100, 500, 0.018*4, 0.002*4, 0.3);
    auto reads = rgFunc(rg);
    rg.writeReads(reads, samPath);



}

std::vector<UnalignedRead> convert(const std::vector<GenomeGenerator::Read>& reads) {
    std::vector<UnalignedRead> res;
    size_t count = 0;
    for (const GenomeGenerator::Read& r: reads) {
        std::string id = std::string("read_") + std::to_string(++count);
        res.push_back(UnalignedRead{id, r.readString});
        if (r.next) {
            res.push_back(UnalignedRead{id, r.next->readString});
        }
    }
    return res;
}

int main() {
    std::cout << "Hello World!" << std::endl;

    if (false) {
        Genome genome;
        ReferenceReader rr(genome);
        std::vector<std::string> refPaths;
        refPaths.push_back("D:/Eigene Dateien/Studium/MA/CD/data/chr22.fna.gz");
        rr.readReferences(refPaths);
        std::vector<std::string> varPaths;
        varPaths.push_back("D:/Eigene Dateien/Studium/MA/CD/data/chr22.vcf.gz");
        ReadGenerator rg(genome, varPaths, 1337);
        writeReadFile(convert(rg.createReads(40000, 250, 0.036, 0.004, 0.3)), "D:/Eigene Dateien/Studium/MA/CD/data/constantLength_n250_f0.04.fastq");
        std::mt19937 rnd;
        std::normal_distribution<double> distrGap(300, 50);
        writeReadFile_paired(convert(rg.createReads_pairedEnd(50000, 100, std::bind(std::ref(distrGap), std::ref(rnd)), 0.036, 0.004, 0.3)),
                             "D:/Eigene Dateien/Studium/MA/CD/data/pairedEnd_n100_f0.04_1.fastq",
                             "D:/Eigene Dateien/Studium/MA/CD/data/pairedEnd_n100_f0.04_2.fastq");
        std::uniform_int_distribution<size_t> distrVar(100, 900);
        writeReadFile(convert(rg.createReads_differentLength(20000, std::bind(std::ref(distrVar), std::ref(rnd)), 0.036, 0.004, 0.3)), "D:/Eigene Dateien/Studium/MA/CD/data/variableLength_n100-900_f0.04.fastq");
        return 0;
    }

    filter_54x();

//    SuperRank<> sr(1000);
//    sr.add(1337, 1);
//    sr.add(1337, 2);
//    sr.add(1338, 3);
//    sr.add(42, 4);
//    sr.add(4242, 5);
//    sr.sort();
//    sr.build();
//    std::cout << sr.findIterators(1337, 1000).first[0] << std::endl;
//    for (auto h: sr) {
//        std::cout << h << std::endl;
//    }
//    return 0;



//    ReferenceString str = "ACCCGGTTTAGATTTAC";
//    ReferenceString cpy;
//    std::cout << str[0] << std::endl;
//    std::cout << sizeof(str) << std::endl;
//    std::cout << seqan::length(str) << std::endl;
//    std::cout << sizeof(str.data_host) << std::endl;
//    std::cout << typeid(str.data_host).name() << std::endl;
//    std::cout << &(str.data_host) << std::endl;
//    std::cout << seqan::length(str.data_host) << std::endl;
//    std::cout << sizeof(str.data_host[0]) << std::endl;
//    std::cout << typeid(str.data_host[0]).name() << std::endl;
//    std::cout << &(str.data_host[0]) << std::endl;
//    std::cout << &(str.data_host[1]) << std::endl;
//    std::cout << &(str.data_host[2]) << std::endl;
//    seqan::resize(cpy, seqan::length(str));
//    std::memcpy(&cpy.data_host[0], &str.data_host[0], seqan::length(str.data_host) * sizeof(str.data_host[0]));
//    std::cout << str << std::endl;
//    std::cout << cpy << std::endl;
//    return 0;


#ifdef _WIN32
    // small values for my slow quadcore
    //const size_t n = 1024*1024;
    const size_t rnum = 20000;//
    //const size_t err = 3;
#else
    // big values for the compute server
    //const size_t n = 1024*1024*1024*3ull;
    const size_t rnum = 1000*1000; // TODOJQ: das ist vermutlich zu wenig? Streuung?
    //const size_t err = 3;
#endif

    std::vector<std::string> refPaths;
    std::vector<std::string> varPaths;
//    std::string samPathGCAT;
//    std::string samPathReal;
    std::string samPathPrefix;
//    std::string samPathTest, samPathTestPE;
#ifdef _WIN32
    // the paths for my PC
    std::string refPath_prefix = "D:/Eigene Dateien/Studium/MA/Data/GRCh37.p13/fa/GCA_000001405.14_GRCh37.p13_no_alt_analysis_set.chr";
    std::string refPath_suffix = ".fna.gz";
    std::string varPath_prefix = "D:/Eigene Dateien/Studium/MA/Data/GRCh37.p13/vcf/common_all.";
    std::string varPath_suffix = ".vcf.gz";
    for (size_t i = 1; i <= 22; i++) {
        refPaths.push_back(refPath_prefix + std::to_string(i) + refPath_suffix);
        varPaths.push_back(varPath_prefix + std::to_string(i) + varPath_suffix);
    }
    for (std::string str: {"X", "Y"}) {
        refPaths.push_back(refPath_prefix + str + refPath_suffix);
        varPaths.push_back(varPath_prefix + str + varPath_suffix);
    }
//    varPaths.push_back("D:/Eigene Dateien/Studium/MA/Data/GRCh37.p13/vcf/common_all.22.vcf.gz");
//    refPaths.push_back("D:/Eigene Dateien/Studium/MA/Data/GRCh37.p13/fa/GCA_000001405.14_GRCh37.p13_no_alt_analysis_set.chr22.fna.gz");

//    samPathTest = "D:/Eigene Dateien/Studium/MA/Data/Reads/test.sam";
//    samPathTestPE = "D:/Eigene Dateien/Studium/MA/Data/Reads/testPE.sam";
    samPathPrefix = "D:/Eigene Dateien/Studium/MA/Data/Reads/rlng";
#else
    // the paths for the compute server
    std::string refPath_prefix = "/scratch/JQ/genome/split/GCA_000001405.14_GRCh37.p13_no_alt_analysis_set.chr";
    std::string refPath_suffix = ".fna.gz";
    std::string varPath_prefix = "/scratch/JQ/vcf/split/common_all.";
    std::string varPath_suffix = ".vcf.gz";
    for (size_t i = 1; i <= 22; i++) {
        refPaths.push_back(refPath_prefix + std::to_string(i) + refPath_suffix);
        varPaths.push_back(varPath_prefix + std::to_string(i) + varPath_suffix);
    }
    for (std::string str: {"X", "Y"}) {
        refPaths.push_back(refPath_prefix + str + refPath_suffix);
        varPaths.push_back(varPath_prefix + str + varPath_suffix);
    }
//    samPathGCAT = "/scratch/PG583/benchmark/results/BWA/GCAT_25Dez/output.sam";
//    samPathReal = "/scratch/PG583/reads/SRR062634/shuffled_paired_end1.sam";
    samPathPrefix = "/scratch/JQ/sam/";

#endif

    auto seed = std::time(nullptr);
    seed = 1445088900;
    std::cout << "Seed: " << seed << std::endl;

    global_LSH = LSH_PE_FOR | LSH_PE_MEAN | LSH_SPLIT_EQUAL;

//    runReadLengthExperiments("./", refPaths, varPaths, samPathPrefix, rnum, seed);
//    runRMexperimentPaired(refPaths, varPaths, seed);
    runRMexperiment(refPaths, varPaths, seed);
    runRMexperimentPaired(refPaths, varPaths, seed);

    return 0;

    if (false) {
        Aligner aligner(0,0);
        Genome genome;
        ReferenceReader rr(genome);
        rr.readReferences(refPaths);
        VariantsReader vr(genome);
        vr.readVariants(varPaths);

        MapInterval ival;
        ival.chromosome = rr.getChrIdxByName("5");
        ival.startPosition = 133365365;
        ival.endPosition = 133366585;
        ival.inverted = false;
        ival.score = 0;
        VariantIndex varIdx(genome[ival.chromosome].variants, -1);
        ReadString pattern = "TGAGACCAGCCTGACCAACAAGGAGAAACCCTATCTCTACTAAAAACACAAAATTAGCCGGGCGTGGTGGTGCTTGACTGTTAATCCCAGCTACTTGGGAGGATGAGGCAGGAGAATCGCTTGAACCTGGGAGGTGGAGGTCCGCGGTGAGCAAGATCGTGCCATTATACTCCAGCCTGGGCAACAAGAGTGAAACTCCATCTCAAAAAAAAAAAGAGAAAAAAATGTTAATATGAAATAAGCATTAAATAGCACATATGGCTAGTGCCCATCATGTTGGCCAGGGAGGCCCCAGTATTGCATAGAGCCTTCTGCAAAAACTCCTAGTCATTCAGAACTTCCATTGGCTGAGAGCTCACCTCCACACAAGGTTCTGTTTGAGAGATCTAATAATGAGAAAGCTCAGAGCAACAGGCTTCACTAAGGGTAGAGAAGTTTTCCCCTCAGCATTCTTTCTTTCTTTCTTTTTGTTTTTTTGAGATGGACTCTCACTCTGTCGC";
        ErrorCount maxError = 20;
        ErrorCount maxClip = 0;
        Alignment a = aligner.align(genome[ival.chromosome], ival.startPosition, ival.endPosition, varIdx, pattern, maxError, maxClip);
        std::cout << a.isValid() << std::endl;
//        std::cout << a.getCigar() << std::endl;

        return 0;
    }
    if (false) {
        Aligner aligner(0,0);
        Chromosome chr("11", "AGGTCCGCCTGATGAGAGCTGAGTTCAGGTCCTGAATATCTTTGTTAATTTTTTGTCTCAGTGATCTGTCTAATATTGTCAGTGGGGTATTAAAGTCTCCCACCAYTATTGCGTGAGGGTCTAAGTCTCTTTGAAGGTCTCTAAGAACTTGCTTTATGAATCTGGGTGTTCCTGTATTGGCTGCATATATATATTTATGATAGTTAGATCTTCTTGTTGAATTGAACGCTTTACCATTA");
        chr.addDeletion(216, 3);
        VariantIndex varIdx(chr.variants, -1);
        ReadString pattern = "ATCTGATATTGTCAGTGGGGTGATAAAGTCTCCTGCTATTATTGTGTGTCAGACTAAGTCTCTTTGAAGGTCTCTAAGAACTTGCTTTATGAATCTGGAT";
        ErrorCount maxError = 65534;
        ErrorCount maxClip = 0;
        Alignment a = aligner.align(chr, 0, 238, varIdx, pattern, maxError, maxClip);
        std::cout << a.isValid() << std::endl;
//        std::cout << a.getCigar() << std::endl;

        return 0;
    }
    if (false) {
        Aligner aligner(0,0);
        Chromosome chr("5", "GTAGGATTCCATTTATGTAACATTCTTGAAATGACAAAAGTAGGGAGATAGAAAACAGATTAGTGGTTACCAAGGGTTAGGGATGGTGGGAAAAGGGCGGTGGGAGTGACTATAAAAGGATAGCCTGTGGGAGGTCTTTGAGATGCTGGGATAGTTCTGTATCACGATTAGGTGGTAGTTACATAAATCTACACATGTGAGAAAATGGCACAGAACTATACACACACATTGTACTAACATCAACTTCCTGGTTTTGATATTGCGCTACAGTAATGTAAAATGTAACCCCTTGGGAAAACTGGGTGACAGGTACGTGAGACCTCTCTCAACTACTTTTTGCAACTTCCTGGAAATCTATAATTATTTCAATAGAAAAAGGAAAAAGGCCGGGCACGGTGGCTCACACCTGTAATCCCAGCASTTTGGGAGGCTGAGGCTGGTGGATCACCTGAGGTCAGGACTTTGAGACCAGCCTGACCAACATGGAGAAACCCTATCTCTACTAAAAACACAAAATTAGCCGGGCGTGGTGGTGCTTGACTGTAATCCCAGCTACTTGGGAGGATGAGGCAGGAGAATCGCTTGAACCTGGGAGGTGGAGGTCYGCGGTGAGCAAGRTCGTGCCATTATACTCCAGCCTGGGCAACAAGAGTGAAACTCCATCTCAAAAAAAAAAAGAGAAAAAAATGTTAATATGAAATAAGCATTACATAGCACATATGGCTAGTGCCCATCATGTTGGACAGGGAGGCCCCAGTAATGCATAGAGCCTTCTGCAAAATCTCCTAGTCATTCAGAACTTCCAGTGGCAGAGAGCTCACCTCCTCACAAGGTTCTGTTTGAAAGCTCTAATAATAAGAAAGCTCAGACCAGCAGGTTTCAGTAAGGGTAGAAAAGTTTTCCCCTCAGCATTCTTTCTTTCTTTCTTTCTTTTTTTTTTGAGATGGAGTCTCACTCTGTCGCCCAAGCTGGAAGCTGGAGTCCAGTGGTGACAGCGATCTCAGCTCACCGCAACCTCCATCTCCCAGGTTCAAGTGATTCTCCTGCCTCAGCCTCCTGAGAATCTGGGATTACAGGCCTCTGCCACCACATCTGGCTAATTTTGTATTTTTAGTAGGGGTTTTTTAGGGGTTTTACCATGATGGTCAGGCTGGTCTTGAACTCCTGACCTCAAGTGATCTGCCCACCTTGGCCTCCCAAAGTGCTGGGATTACAGCCGTG");
        chr.addDeletion(930, 1);
        chr.addInsertion(930, "T");
        VariantIndex varIdx(chr.variants, -1);
        ReadString pattern = "TGAGACCAGCCTGACCAACAAGGAGAAACCCTATCTCTACTAAAAACACAAAATTAGCCGGGCGTGGTGGTGCTTGACTGTTAATCCCAGCTACTTGGGAGGATGAGGCAGGAGAATCGCTTGAACCTGGGAGGTGGAGGTCCGCGGTGAGCAAGATCGTGCCATTATACTCCAGCCTGGGCAACAAGAGTGAAACTCCATCTCAAAAAAAAAAAGAGAAAAAAATGTTAATATGAAATAAGCATTAAATAGCACATATGGCTAGTGCCCATCATGTTGGCCAGGGAGGCCCCAGTATTGCATAGAGCCTTCTGCAAAAACTCCTAGTCATTCAGAACTTCCATTGGCTGAGAGCTCACCTCCACACAAGGTTCTGTTTGAGAGATCTAATAATGAGAAAGCTCAGAGCAACAGGCTTCACTAAGGGTAGAGAAGTTTTCCCCTCAGCATTCTTTCTTTCTTTCTTTTTGTTTTTTTGAGATGGACTCTCACTCTGTCGC";
        ErrorCount maxError = 20;
        ErrorCount maxClip = 0;
        Alignment a = aligner.align(chr, 0, 1220, varIdx, pattern, maxError, maxClip);
        std::cout << a.isValid() << std::endl;
//        std::cout << a.getCigar() << std::endl;

        return 0;
    }


    return 0;

//    std::ofstream file_rlng("./rlng.csv");
//    file_rlng << "Read length, with variants\n";
//    runVaryReadLengthExperiment(file_rlng, refPaths, varPaths, samPathPrefix + std::string("rlng"), rnum, seed);

//    global_LSH = LSH_PE_PLUS;


//    varPaths.clear();
//    varPaths.push_back(samPathTest.substr(0, samPathTest.find_last_of("/")+1) + "test.vcf");
//    std::cout << varPaths.back() << std::endl;

//    createReads(refPaths, varPaths, samPathTest, seed, [rnum](ReadGenerator& rg){
//        return rg.createReads(rnum, 1000, 0.018*2, 0.002*2, 0.3);
//    });

//    std::mt19937 rnd;
//    std::normal_distribution<double> peDist(300, 50);
//    auto distr = [&peDist, &rnd](){return peDist(rnd);};
//    createReads(refPaths, varPaths, samPathTestPE, seed, [rnum, &samPathTest, &distr](ReadGenerator& rg){
//        auto reads = rg.createReads_pairedEnd(rnum/2, 100, distr, 0.018*2, 0.002*2, 0.3);
//        std::vector<ReadGenerator::Read> se;
//        for (ReadGenerator::Read rd: reads) {
//            se.push_back(*rd.next);
//            rd.next.reset();
//            se.push_back(rd);
//        }
//        rg.writeReads(se, samPathTest);
//        return reads;
//    });


////    runRealReadExperiment(file_test, refPaths, varPaths, samPathTest, seed);

//    std::ofstream file_pe("./pe.csv");
//    file_pe << "Paired End, with variants\n";
//    runRealReadExperiment(file_test, refPaths, varPaths, samPathTestPE, seed);


//    std::ofstream file_real("./real.csv");
//    file_real << "Real, with variants\n";
//    runRealReadExperiment(file_real, refPaths, varPaths, samPathReal, seed);

//    std::ofstream file_our2("./our100_e2.csv");
//    file_our2 << "our reads (length = 100, errors = 2, snp = 90%), with variants\n";
//    runHumanGenomeExperiment(file_our2, refPaths, varPaths, rnum, 100, 1.8, 0.2, seed);

//    std::ofstream file_gcat("./gcat_noVariants.csv");
//    file_gcat << "GCAT, no variants\n";
//    runRealReadExperiment(file_gcat, refPaths, std::vector<std::string>(), samPathGCAT, seed);

//    std::ofstream file_real_noVar("./real_noVariants.csv");
//    file_real_noVar << "Real, no variants\n";
//    runRealReadExperiment(file_real_noVar, refPaths, std::vector<std::string>(), samPathReal, seed);

//    std::ofstream file_our2_noVar("./our100_e2_noVariants.csv");
//    file_our2_noVar << "our reads (length = 100, errors = 2, snp = 90%), no variants\n";
//    runHumanGenomeExperiment(file_our2_noVar, refPaths, std::vector<std::string>(), rnum, 100, 1.8, 0.2, seed);

//    std::ofstream file_ourLong10("./our1k_e10.csv");
//    file_ourLong10 << "our reads (length = 1000, errors = 2, snp = 90%), with variants\n";
//    runHumanGenomeExperiment(file_ourLong10, refPaths, varPaths, rnum, 1000, 9, 1, seed);

    return 0;
}




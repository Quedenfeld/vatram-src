#ifndef FILTERPACBIOREADS_H
#define FILTERPACBIOREADS_H

#include "compare/ReadSamIo.h"

inline
void filterPacBio(const std::string& inputFile,
                  const std::string& outputFile,
                  size_t minLng, size_t maxLng) {
    std::vector<UnalignedRead> all = readReadFile(inputFile);
    std::vector<UnalignedRead> good;
    for (UnalignedRead& r: all) {
        size_t lng = seqan::length(r.seq);
        if (minLng <= lng && lng <= maxLng) {
            good.emplace_back(std::move(r));
        } else {
            // Split Read
            size_t count = 0;
            while (minLng <= lng) {
                size_t newLng = std::min(lng, maxLng);
                UnalignedRead newRead;
                newRead.id = r.id + "_" + std::to_string(count++);
                newRead.seq = seqan::infix(r.seq, 0, newLng);
                good.emplace_back(std::move(newRead));
                r.seq = seqan::infix(r.seq, newLng, lng);
                lng = seqan::length(r.seq);
            }
        }
    }
    writeReadFile(good, outputFile);
}

inline
void filter_54x() {
    {
        std::string out = "/scratch/JQ/reads/PacBio/54x/first_split3k.fasta";
        if (!exists(out)) {
            filterPacBio("/scratch/JQ/reads/PacBio/54x/first.fasta", out, 100, 3000);
        }
    }
    {
        std::string out = "/scratch/JQ/reads/PacBio/54x/first_split1k.fasta";
        if (!exists(out)) {
            filterPacBio("/scratch/JQ/reads/PacBio/54x/first.fasta", out, 100, 1000);
        }
    }
    std::cout << "finished: filter_54x()" << std::endl;
}

#endif // FILTERPACBIOREADS_H

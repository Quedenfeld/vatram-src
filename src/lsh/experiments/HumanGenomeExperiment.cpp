#include "experiments/HumanGenomeExperiment.h"
#include "map/LSHimpl.h"
#include "io/ReferenceReader.h"
#include "io/VariantsReader.h"
#include "GenomeGenerator.h"
#include "Timer.h"
#include "experiments/VaryParameters.h"
#include "Experiments.h"
#include "classes/QGramHashFunctions2.h"
#include "classes/BandHashFunctions.h"
#include "experiments/SpeedTest.h"
#include "experiments/VaryParameters.h"

void runHumanGenomeExperiment(std::ostream& out,
                              const std::vector<std::string>& refPaths,
                              const std::vector<std::string>& varPaths,
                              size_t rnum, size_t rlng, double snpErrors, double indelErrors,
                              uint64_t seed) {
    std::cout << "\n=== HUMAN GENOME EXPERIMENT ===" << std::endl;

    Genome genome;
    std::unordered_set<std::string> filter;

    std::cout << "Read genome" << std::endl;
    Timer<> timer_read;
    ReferenceReader rr(genome, filter);
    rr.readReferences(refPaths);
    std::cout << "  time: " << timer_read << std::endl;

    std::cout << "Read variants" << std::endl;
    Timer<> timer_var;
    VariantsReader vr(genome, filter);
    vr.readVariants(varPaths);
    std::cout << "  time: " << timer_var << std::endl;

    std::cout << "  Create reads" << std::endl;
    Timer<> timer_reads;
    GenomeGenerator gg(seed);
    std::vector<GenomeGenerator::Read> reads;
    createGenomeReads(genome, gg, rnum, rlng, snpErrors, indelErrors, reads);
    std::cout << "    time: " << timer_reads << std::endl;

    if (rlng == 100) {
        runVaryParameters_rlng100(out, genome, reads, seed, varPaths);
        runVaryErrors(out, genome, rnum, seed);
    } else if (rlng == 1000) {

    } else {
        std::cerr << "error: no suitable function [at runHumanGenomeExperiment()]" << std::endl;
    }
}

void createGenomeReads(const Genome& genome,
                 GenomeGenerator& gg,
                 size_t rnum, size_t rlng, double snpErrors, double indelErrors,
                 std::vector<GenomeGenerator::Read>& out_reads) {
    out_reads.clear();
    size_t genomeLength = 0;
    for (const Chromosome &chr: genome) {
        genomeLength += seqan::length(chr.data);
    }
    for (const Chromosome &chr: genome) {
        size_t numberOfReads = rnum * double(seqan::length(chr.data)) / double(genomeLength);
        std::vector<GenomeGenerator::Read> rs = gg.createReads(chr, numberOfReads, rlng, snpErrors / double(rlng), indelErrors / double(rlng));
        for (GenomeGenerator::Read& r: rs) {
            out_reads.push_back(r);
        }
    }
}


void runVaryErrors(std::ostream& out, Genome& genome, size_t rnum, uint64_t seed) {
    LSH lsh(seed);
    GenomeGenerator gg(seed);
    std::vector<GenomeGenerator::Read> readsGenerated;
    clearAfterGapped(lsh);
    const size_t rlng = 100;

    typedef Experiment<LSH> Exp;
    Exp exp(genome, lsh, [](Exp& e){clearAfterGapped(e.lsh);}, out);
    exp.addReadSet(readsGenerated);
    bool doAlign = true;


    std::vector<double> verr = {0, 1, 2, 3, 4, 5};
    auto seterr = [&genome, &gg, &rnum, &readsGenerated](Exp&, double err) {
        createGenomeReads(genome, gg, rnum, rlng, err, 0, readsGenerated);
    };
    exp.varyParameter<double>("errors (100% SNP)", verr, seterr, "\nERRORS-PER-READ (SNPs only)\n", doAlign, ParameterType::Query);
    readsGenerated.clear();

    std::vector<double> verr2 = {0, 1, 2, 3, 4, 5};
    auto seterr2 = [&genome, &gg, &rnum, &readsGenerated](Exp&, double err) {
        createGenomeReads(genome, gg, rnum, rlng, err * 0.9, err * 0.1, readsGenerated);
    };
    exp.varyParameter<double>("errors (90% SNP)", verr2, seterr2, "\nERRORS-PER-READ (90% SNPs)\n", doAlign, ParameterType::Query);
    readsGenerated.clear();

    std::vector<double> verr3 = {0, 1, 2, 3, 4, 5};
    auto seterr3 = [&genome, &gg, &rnum, &readsGenerated](Exp&, double err) {
        createGenomeReads(genome, gg, rnum, rlng, err * 0.5, err * 0.5, readsGenerated);
    };
    exp.varyParameter<double>("errors (50% SNP)", verr3, seterr3, "\nERRORS-PER-READ (50% SNPs)\n", doAlign, ParameterType::Query);
    readsGenerated.clear();
}

#ifndef VARYPARAMETERS_H
#define VARYPARAMETERS_H

#include <iosfwd>
#include "map/LSH.h"
#include "GenomeGenerator.h"

void runVaryParameters_rlng100(std::ostream& out,
                               Genome &genome,
                               const std::vector<GenomeGenerator::Read>& reads,
                               uint64_t seed,
                               const std::vector<std::string>& varPaths);

void runVaryParameters_rlng1k(std::ostream& out,
                              Genome& genome,
                              const std::vector<GenomeGenerator::Read>& reads,
                              uint64_t seed,
                              bool containsVariants);

typedef LSH_Generic<uint32_t, uint64_t, SuperRank<uint32_t>, GappedQGramHashFunction<uint64_t>> LSH_Q64;
//typedef LSH_Generic<uint32_t, uint64_t, CollisionFreeBHT<uint32_t>, GappedQGramHashFunction<uint64_t>> LSH_Q64_CFBHT;
//typedef LSH_Generic<uint32_t, uint32_t, CollisionFreeBHT<uint32_t>, GappedQGramHashFunction<uint32_t>> LSH_CFBHT;
typedef LSH_Generic<uint64_t, uint64_t, CollisionFreeBHT<uint64_t>, GappedQGramHashFunction<uint64_t>> LSH_64bBands;

struct WindowBandsConfig {
    unsigned windowSize;
    unsigned windowOffset;
    unsigned bandNumber;
};

std::ostream& operator<<(std::ostream& strm, const WindowBandsConfig& wb);

struct WindowBandsFactorConfig {
    double windowSizeFactor;
    double windowOffsetFactor;
//    unsigned bandNumber;
};

std::ostream& operator<<(std::ostream& strm, const WindowBandsFactorConfig& wb);

struct MaxSeletNextFactor {
    unsigned maxSelect;
    double nextFactor;
};

std::ostream& operator<<(std::ostream& strm, const MaxSeletNextFactor& wb);

struct LimitConfig {
    unsigned limit;
    unsigned limitSkip;
};

std::ostream& operator<<(std::ostream& strm, const LimitConfig& wb);

struct MinCountConfig {
    unsigned minCountSelection;
    unsigned minCountPrefiltering;
};

std::ostream& operator<<(std::ostream& strm, const MinCountConfig& wb);

struct ExtendConfig {
    double extendFactor;
    double extendBandMultiplicator;
};

std::ostream& operator<<(std::ostream& strm, const ExtendConfig& ec);

template <class LSH_>
void printLSHParameters(LSH_& lsh, std::ostream& out) {
    out << "q = " << lsh.qGramSize << '\n';
    out << "gapString = " << lsh.qGramHashObject.getGapVector() << '\n';
    out << "winSize = " << lsh.windowSize << '\n';
    out << "winOff = " << lsh.windowOffset << '\n';
    out << "sigLng = " << lsh.signatureLength << '\n';
    out << "bandSize = " << lsh.bandSize << '\n';
    out << "limit = " << lsh.limit << '\n';
    out << "limitSkip = " << lsh.limit_skip << '\n';
    out << "minCountSelection = " << lsh._mapOptions.minCountSelection << '\n';
    out << "minCountPrefiltering = " << lsh._mapOptions.minCountPrefiltering << '\n';
    out << "maxSelect = " << lsh._mapOptions.maxSelect << '\n';
    out << "nextFactor = " << lsh._mapOptions.nextFactor << '\n';
    out << "contractBandMultiplicator = " << lsh._mapOptions.contractBandMultiplicator << '\n';
    out << "extendBandMultiplicator = " << lsh._mapOptions.extendBandMultiplicator << '\n';
    out << "extendFactor = " << lsh._mapOptions.extendFactor << '\n';
    out << "maxReturnedWindows = " << lsh._mapOptions.maximumOfReturnedWindows << '\n';
}

template <class LSH_>
void clearAfter(LSH_& lsh) {
    lsh.qGramSize = LSH::DEFAULT_QGRAM_SIZE;
    lsh.windowSize = LSH::DEFAULT_WINDOW_SIZE;
    lsh.windowOffset = LSH::DEFAULT_WINDOW_OFFSET;
    lsh.signatureLength = LSH::DEFAULT_SIGNATURE_LENGTH;
    lsh.bandSize = LSH::DEFAULT_BAND_SIZE;
    lsh.limit = LSH::DEFAULT_LIMIT;
    lsh.limit_skip = LSH::DEFAULT_LIMIT_SKIP;
    lsh._mapOptions.minCountSelection = LshMapOptions::DEFAULT_MIN_COUNT_SELECTION;
    lsh._mapOptions.minCountPrefiltering = LshMapOptions::DEFAULT_MIN_COUNT_PREFILTERING;
    lsh._mapOptions.maxSelect = 64; //LshMapOptions::DEFAULT_MAX_SELECT;
    lsh._mapOptions.nextFactor = 4; //LshMapOptions::DEFAULT_NEXT_FACTOR;
    lsh._mapOptions.contractBandMultiplicator = LshMapOptions::DEFAULT_CONTRACT_BAND_MULTIPLICATOR;
    lsh._mapOptions.extendBandMultiplicator = LshMapOptions::DEFAULT_EXTEND_BAND_MULTIPLICATOR;
    lsh._mapOptions.extendFactor = LshMapOptions::DEFAULT_EXTEND_FACTOR;
    lsh._mapOptions.maximumOfReturnedWindows = LshMapOptions::DEFAULT_MAXIMUM_OF_RETURNED_WINDOWS;
}

template <class TLsh>
void clearAfterGapped(TLsh& lsh) {
    clearAfter(lsh);
    std::string qstr = "####-#-####--##-#####";
    lsh.qGramHashObject.setGapVector(qstr);
    lsh.qGramSize = qstr.size();
}

//template <class LSH_>
//void clearAfter_1k(LSH_& lsh) {
//    lsh.qGramSize = LSH::DEFAULT_QGRAM_SIZE;
//    lsh.windowSize = 10 * LSH::DEFAULT_WINDOW_SIZE;
//    lsh.windowOffset = 10 * LSH::DEFAULT_WINDOW_OFFSET;
//    lsh.signatureLength = 10 * LSH::DEFAULT_SIGNATURE_LENGTH;
//    lsh.bandSize = LSH::DEFAULT_BAND_SIZE;
//    lsh.limit = LSH::DEFAULT_LIMIT;
//    lsh.limit_skip = LSH::DEFAULT_LIMIT_SKIP;
//    lsh._mapOptions.minCount = 3; // LshMapOptions::DEFAULT_MIN_COUNT;
//    lsh._mapOptions.maxSelect = 64; //LshMapOptions::DEFAULT_MAX_SELECT;
//    lsh._mapOptions.nextFactor = 4; //LshMapOptions::DEFAULT_NEXT_FACTOR;
//    lsh._mapOptions.contractBandMultiplicator = LshMapOptions::DEFAULT_CONTRACT_BAND_MULTIPLICATOR;
//    lsh._mapOptions.extendBandMultiplicator = LshMapOptions::DEFAULT_EXTEND_BAND_MULTIPLICATOR;
//    lsh._mapOptions.extendFactor = LshMapOptions::DEFAULT_EXTEND_FACTOR;
//    lsh._mapOptions.maximumOfReturnedWindows = LshMapOptions::DEFAULT_MAXIMUM_OF_RETURNED_WINDOWS;
//}

//template <class TLsh>
//void clearAfterGapped_1k(TLsh& lsh) {
//    clearAfter(lsh);
//    std::string qstr = "####-#-####--##-#####";
//    lsh.qGramHashObject.setGapVector(qstr);
//    lsh.qGramSize = qstr.size();
//}

#endif // VARYPARAMETERS_H

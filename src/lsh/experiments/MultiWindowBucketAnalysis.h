#ifndef MULTIWINDOWBUCKETANALYSIS_H
#define MULTIWINDOWBUCKETANALYSIS_H

#include "map/LSH.h"
#include "Experiments.h"

struct WindowIndexData {
    typedef LSH::BandHash BandHash;
    struct Entry {
        size_t bhtIndex;
        BandHash bandHash;
    };
    std::map<size_t, size_t> wiCount; // #WI in bucket -> Häufigkeit
    std::map<size_t, std::vector<Entry>> entries;
};

template <class TLsh>
WindowIndexData calcWindowIndexData(TLsh& lsh, size_t countDiv = 1, bool createEntries = true) {
    WindowIndexData wid;
    auto& wiCount = wid.wiCount;
    auto& entries = wid.entries;
    Timer<> timer;
    for (size_t i = 0; i < lsh.bandHashTables.size(); i++) {
        auto& bht = lsh.bandHashTables[i];
        for (auto bandHash: bht) {
            std::vector<WindowIndex> v;
            bht.find(bandHash, lsh._mapOptions.maximumOfReturnedWindows, [&v](WindowIndex wi) {v.push_back(wi);});
            size_t key = v.size() / countDiv;
            wiCount[key]++;
            if (createEntries) {
                entries[key].push_back({i, bandHash});
            }
        }
    }
    std::cout << "calcWindowIndexData : " << timer << " ms" << std::endl;
    return wid;
}

template <class TLsh>
void hitDistribution(std::ostream& out, TLsh& lsh, const std::vector<GenomeGenerator::Read>& reads) {
    out << "\nHIT DISTRIBUTION\n";
    std::vector<std::vector<size_t>> vecHitDistr(2 * lsh.signatureLength);
    std::vector<size_t> vecHitSum;
    Timer<> timer;
    timer.resetTimer();
    for (const GenomeGenerator::Read& read: reads) {
        const ReadString& rs = read.readString;
        ReadString rs_revCompl = rs;
        seqan::reverseComplement(rs_revCompl);
        std::vector<size_t> returnSizes;
        std::vector<typename TLsh::BandHash> bandHashes;
        lsh.calcReadBandHashes(seqan::begin(rs), seqan::end(rs), bandHashes);
        for (size_t i = 0; i < bandHashes.size(); i++) {
            std::vector<WindowIndex> v;
            lsh.bandHashTables[i].find(bandHashes[i], lsh._mapOptions.maximumOfReturnedWindows, [&v](WindowIndex wi) {v.push_back(wi);});
            returnSizes.push_back(v.size());
        }
        lsh.calcReadBandHashes(seqan::begin(rs_revCompl), seqan::end(rs_revCompl), bandHashes);
        for (size_t i = 0; i < bandHashes.size(); i++) {
            std::vector<WindowIndex> v;
            lsh.bandHashTables[i].find(bandHashes[i], lsh._mapOptions.maximumOfReturnedWindows, [&v](WindowIndex wi) {v.push_back(wi);});
            returnSizes.push_back(v.size());
        }
        std::sort(returnSizes.begin(), returnSizes.end());
        vecHitSum.push_back(0);
        for (size_t i = 0; i < returnSizes.size(); i++) {
            vecHitSum.back() += returnSizes[i];
            vecHitDistr[i].push_back(returnSizes[i]);
        }
    }
    std::cout << "hitDistribution : " << timer << " ms" << std::endl;
    Stats hits(vecHitSum.begin(), vecHitSum.end());
    std::vector<Stats> hitDistr;
    for (std::vector<size_t>& v: vecHitDistr) {
        hitDistr.emplace_back(v.begin(), v.end());
    }
    out << "index; mean; deviation; min; q1; median; q3; max;\n";
    out << ";" << hits.mean << ";" << hits.deviation << ";";
    out << hits.min << ";" << hits.q1 << ";" << hits.median << ";" << hits.q3 << ";" << hits.max << ";";
    out << "\n";
    for (size_t i = 0; i < hitDistr.size(); i++) {
        Stats& st = hitDistr[i];
        out << i << ";" << st.mean << ";" << st.deviation << ";";
        out << st.min << ";" << st.q1 << ";" << st.median << ";" << st.q3 << ";" << st.max << ";";
        out << "\n";
    }
    out << std::endl;
}

template <class TLsh, class OrderedMap, class Map>
void evilQgrams(std::ostream& out, TLsh& lsh, const OrderedMap& wiCount, Map& entries, size_t countDiv = 1, size_t minQuantity = 100000) {
    out << "\nEVIL Q-GRAMS\n";
    out << "number of window indices; qgram; \n";
    for (auto wicEntry: wiCount) {
        if (wicEntry.first >= minQuantity / countDiv) {
            for (const WindowIndexData::Entry& entry: entries[wicEntry.first]) {
                out << wicEntry.first * countDiv << ";";
                typename TLsh::QGramHash qgram = entry.bandHash ^ lsh.hashFunctions[entry.bhtIndex];
                typename TLsh::QGramHash qgramTmp = qgram;
                for (size_t j = 0; j < lsh.qGramSize; j++) {
                    out << seqan::Dna(qgramTmp & 3);
                    qgramTmp >>= 2;
                }
                out << ";";
                out << "\n";
            }

        }
    }
    out << std::endl;
}

template <class OrderedMap>
void multiWindowBuckets(std::ostream& out, const OrderedMap& wiCount, size_t countDiv = 1, size_t maxValue = std::numeric_limits<size_t>::max()) {
    out << "\nMULTI WINDOW BUCKETS\n";
    out << "#windowIndices (min); #windowIndices (max); occurrences;\n";
    for (auto entry: wiCount) {
        size_t count = entry.first * countDiv;
        if (count < maxValue) {
            out << count << ";" << (count + countDiv - 1) << ";" << entry.second << ";\n";
        }
    }
    out << std::endl;
}



/*
template <class TLsh>
void multiWindowBucketAnalysis(std::ostream& out, TLsh& lsh, const std::vector<GenomeGenerator::Read>& reads) {
    typedef LSH::BandHash BandHash;
    struct Entry {
        size_t bhtIndex;
        BandHash bandHash;
    };
    std::map<size_t, size_t> wiCount; // #WI in bucket -> Häufigkeit
    std::map<size_t, std::vector<Entry>> entries;
    Timer<> timer;
    const size_t COUNT_DIV = 1000;
    for (size_t i = 0; i < lsh.bandHashTables.size(); i++) {
        auto& bht = lsh.bandHashTables[i];
        for (auto bandHash: bht) {
            size_t key = bht.find(bandHash, lsh._mapOptions.maximumOfReturnedWindows).size() / COUNT_DIV;
            wiCount[key]++;
            entries[key].push_back({i, bandHash});
        }
    }
    std::cout << "multiWindowBucketAnalysis : count :" << timer << " ms" << std::endl;
//        out << "Multi Window Buckets\n";
//        for (auto entry: wiCount) {
//            out << entry.first << ";" << entry.second << "\n";
//        }
//        out << std::endl;

//        out << "Evil Q-Grams\n";
//        out << "number of window indices; qgram; \n";
//        for (auto wicEntry: wiCount) {
//            if (wicEntry.first >= 10000 / COUNT_DIV) {
//                for (Entry& entry: entries[wicEntry.first]) {
//                    out << wicEntry.first * COUNT_DIV << ";";
//                    QGramHash qgram = entry.bandHash ^ lsh.hashFunctions[entry.bhtIndex];
//                    QGramHash qgramTmp = qgram;
//                    for (size_t j = 0; j < lsh.qGramSize; j++) {
//                        out << seqan::Dna(qgramTmp & 3);
//                        qgramTmp >>= 2;
//                    }
//                    out << ";";
//                    out << "\n";
//                }

//            }
//        }
//        out << std::endl;

    out << "Hit Distribution\n";
    std::vector<std::vector<size_t>> vecHitDistr(2 * lsh.signatureLength);
    std::vector<size_t> vecHitSum;
    timer.resetTimer();
    for (const GenomeGenerator::Read& read: reads) {
        const ReadString& rs = read.readString;
        ReadString rs_revCompl = rs;
        seqan::reverseComplement(rs_revCompl);
        std::vector<size_t> returnSizes;
        std::vector<BandHash> bandHashes;
        lsh.calcReadBandHashes(seqan::begin(rs), seqan::end(rs), bandHashes);
        for (size_t i = 0; i < bandHashes.size(); i++) {
            returnSizes.push_back(lsh.bandHashTables[i].find(bandHashes[i], lsh._mapOptions.maximumOfReturnedWindows).size());
        }
        lsh.calcReadBandHashes(seqan::begin(rs_revCompl), seqan::end(rs_revCompl), bandHashes);
        for (size_t i = 0; i < bandHashes.size(); i++) {
            returnSizes.push_back(lsh.bandHashTables[i].find(bandHashes[i], lsh._mapOptions.maximumOfReturnedWindows).size());
        }
        std::sort(returnSizes.begin(), returnSizes.end());
        vecHitSum.push_back(0);
        for (size_t i = 0; i < returnSizes.size(); i++) {
            vecHitSum.back() += returnSizes[i];
            vecHitDistr[i].push_back(returnSizes[i]);
        }
    }
    std::cout << "multiWindowBucketAnalysis : count :" << timer << " ms" << std::endl;
    Stats hits(vecHitSum.begin(), vecHitSum.end());
    std::vector<Stats> hitDistr;
    for (std::vector<size_t>& v: vecHitDistr) {
        hitDistr.emplace_back(v.begin(), v.end());
    }
    out << "index; mean; deviation; min; q1; median; q3; max;\n";
    out << ";" << hits.mean << ";" << hits.deviation << ";";
    out << hits.min << ";" << hits.q1 << ";" << hits.median << ";" << hits.q3 << ";" << hits.max << ";";
    out << "\n";
    for (size_t i = 0; i < hitDistr.size(); i++) {
        Stats& st = hitDistr[i];
        out << i << ";" << st.mean << ";" << st.deviation << ";";
        out << st.min << ";" << st.q1 << ";" << st.median << ";" << st.q3 << ";" << st.max << ";";
        out << "\n";
    }
    out << std::endl;


//        size_t keymax = wiCount.rbegin()->first;

//        std::ofstream lmwb("./largeMultiWindowBucket.txt");
//        for (size_t i = 0; i < lsh.bandHashTables.size(); i++) {
//            auto& bht = lsh.bandHashTables[i];
//            for (auto bandHash: bht) {
//                size_t key = bht.find(bandHash, lsh._mapOptions.maximumOfReturnedWindows).size() / 100;
//                if (key == keymax) {
//                    uint32_t qgram = bandHash ^ lsh.hashFunctions[i];
//                    std::cout << "q-gram:";
//                    for (size_t j = 0; j < 16; j++) {
//                        std::cout << seqan::Dna(qgram & 3);
//                        qgram >>= 2;
//                    }
//                    std::cout << std::endl;
//                    for (uint32_t wi: bht.find(bandHash, lsh._mapOptions.maximumOfReturnedWindows)) {
//                        lmwb << lsh.referenceWindows[wi].chromosomeIndex << ": " << lsh.referenceWindows[wi].beginPosition << "\n";
//                    }
//                }
//            }
//        }

}*/




#endif // MULTIWINDOWBUCKETANALYSIS_H

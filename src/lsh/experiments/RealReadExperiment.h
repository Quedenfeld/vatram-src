#ifndef REALREADEXPERIMENT_H
#define REALREADEXPERIMENT_H

#include <iosfwd>
#include <string>
#include <vector>
#include "GenomeGenerator.h"

void runRealReadExperiment(std::ostream& out,
                           const std::vector<std::string>& refPath,
                           const std::vector<std::string>& varPath,
                           const std::string& samPath,
                           uint64_t seed);




#endif // REALREADEXPERIMENT_H

#ifndef SPEEDTEST_H
#define SPEEDTEST_H

#include <iostream>
#include "Timer.h"
#include <vector>
#include "GenomeGenerator.h"
#include "seqan/modifier.h"
#include <unordered_map>
#include "map/HashDataStructure.h"

void runSpeedTest(std::ostream& out, size_t n, size_t rnum, size_t errors);

template <class LSH_>
void speedTest(std::ostream& out, LSH_& lsh, const std::vector<GenomeGenerator::Read>& reads) {
    size_t rnum = reads.size();

    out << "\nSPEED TEST (max-returned-windows = " << lsh._mapOptions.maximumOfReturnedWindows << ")\n";
    out << "method;time (sum);time (only)\n";
    unsigned doNotOptimize = 0;

    Timer<> timer_band;
    for (auto& read: reads) {
        std::vector<typename LSH_::BandHash> bh;
        lsh.calcReadBandHashes(seqan::begin(read.readString), seqan::end(read.readString), bh);
        doNotOptimize += bh.size();
    }
    timer_band.stopTimer();
    double time_band = timer_band.getDuration().count() * 1000.0 / rnum;
    out << "calcReadBandHashes;" << time_band << ";" << time_band << std::endl;

    Timer<> timer_rc;
    for (auto& read: reads) {
        auto rs = read.readString;
        seqan::reverseComplement(rs);
        doNotOptimize += unsigned(rs[0]);
    }
    timer_rc.stopTimer();
    double time_rc = timer_rc.getDuration().count() * 1000.0 / rnum;
    out << "reverseComplement;" << time_rc << ";" << time_rc << std::endl;

    std::vector<std::vector<typename LSH_::BandHash>> bandHashes;
    for (auto& read: reads) {
        bandHashes.emplace_back();
        lsh.calcReadBandHashes(seqan::begin(read.readString), seqan::end(read.readString), bandHashes.back());
    }
    Timer<> timer_bht_search;
    for (std::vector<typename LSH_::BandHash> v: bandHashes) {
        for (size_t i = 0; i < v.size(); i++) {
             doNotOptimize += (unsigned) (size_t)
                     lsh.bandHashTables[i].findIterators(v[i], lsh._mapOptions.maximumOfReturnedWindows).first;
        }
    }
    timer_bht_search.stopTimer();
    double time_bht_search = timer_bht_search.getDuration().count() * 1000.0 / rnum;
    out << "bht-search;" << time_bht_search << ";" << time_bht_search << std::endl;

    Timer<> timer_bht_acc;
    for (std::vector<typename LSH_::BandHash> v: bandHashes) {
        for (size_t i = 0; i < v.size(); i++) {
             lsh.bandHashTables[i].find(v[i], lsh._mapOptions.maximumOfReturnedWindows, [&doNotOptimize](uint32_t){doNotOptimize++;});
        }
    }
    timer_bht_acc.stopTimer();
    double time_bht_acc = timer_bht_acc.getDuration().count() * 1000.0 / rnum;
    out << "bht-access;" << time_bht_acc << ";" << (time_bht_acc - time_bht_search) << std::endl;

    Timer<> timer_occ;
    for (auto& read: reads) {
        ReadString revCompl = read.readString;
        seqan::reverseComplement(revCompl);
        doNotOptimize +=
                lsh.findReadOccurrences(read.readString, revCompl, lsh._mapOptions).size();
    }
    timer_occ.stopTimer();
    double time_occ = timer_occ.getDuration().count() * 1000.0 / rnum;
    out << "findReadOccurrences;" << time_occ << ";" << (time_occ - 2*time_band - time_rc - time_bht_acc) << std::endl;

    Timer<> timer_compr;
    for (auto& read: reads) {
        ReadString revCompl = read.readString;
        seqan::reverseComplement(revCompl);
        doNotOptimize +=
                lsh.compressOccurrences(
                    lsh.findReadOccurrences(read.readString, revCompl, lsh._mapOptions)).size();
    }
    timer_compr.stopTimer();
    double time_compr = timer_compr.getDuration().count() * 1000.0 / rnum;
    out << "compressOccurrences;" << time_compr << ";" << (time_compr - time_occ) << std::endl;

    Timer<> timer_find;
    for (auto& read: reads) {
        ReadString revCompl = read.readString;
        seqan::reverseComplement(revCompl);
        std::vector<MapInterval> res;
        lsh.findInterval_singleEnd(read.readString, revCompl, lsh._mapOptions, res);
        doNotOptimize += res.size();
    }
    timer_find.stopTimer();
    double time_find = timer_find.getDuration().count() * 1000.0 / rnum;
    out << "findInterval;" << time_find << ";" << (time_find - time_compr) << std::endl;

    std::cout << "doNotOptimize: " << doNotOptimize << std::endl;
}

#endif // SPEEDTEST_H

#include "experiments/RealReadExperiment.h"
#include "io/ReferenceReader.h"
#include "io/VariantsReader.h"
#include "GenomeGenerator.h"
#include "Timer.h"
#include "experiments/VaryParameters.h"
#include "Experiments.h"
#include "classes/QGramHashFunctions2.h"
#include "classes/BandHashFunctions.h"
#include "experiments/HumanGenomeExperiment.h"
#include "experiments/MultiWindowBucketAnalysis.h"

void runRealReadExperiment(std::ostream &out,
                           const std::vector<std::string> &refPaths,
                           const std::vector<std::string> &varPaths,
                           const std::string &samPath,
                           uint64_t seed) {
    std::cout << "\n=== REAL READ EXPERIMENT ===" << std::endl;

    Genome genome;
    std::unordered_set<std::string> filter;

    std::cout << "Read genome" << std::endl;
    Timer<> timer_read;
    ReferenceReader rr(genome, filter);
    rr.readReferences(refPaths);
    std::cout << "  time: " << timer_read << std::endl;

    std::cout << "Read variants" << std::endl;
    Timer<> timer_var;
    VariantsReader vr(genome, filter, 0.25);
    vr.readVariants(varPaths);
    std::cout << "  time: " << timer_var << std::endl;

//    GenomeGenerator gg(seed);
//    gg.createReadsAndSamWithVariants(genome, 100, 0.25, 0.001, 0.001, 0.018, 0.0, 1000000, "./reads.sam");
//    return;

    std::cout << "Read reads" << std::endl;
    Timer<> timer_reads;
    std::vector<GenomeGenerator::Read> reads;
    extractAlignedReadsFromSAM(samPath, genome, reads);

    std::cout << "  time: " << timer_reads << std::endl;

    runVaryParameters_rlng100(out, genome, reads, seed, varPaths);


}



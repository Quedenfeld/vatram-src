#include "experiments/VaryParameters.h"
#include "Experiments.h"
#include "GenomeGenerator.h"
#include "classes/QGramHashFunctions2.h"
#include "classes/BandHashFunctions.h"
#include "MultiWindowBucketAnalysis.h"
#include "SpeedTest.h"
#include "io/VariantsReader.h"
#include "map/LSHimpl.h"

#include <ctime>

void varyQGramHashFunctions(const Chromosome& chr, const std::vector<GenomeGenerator::Read>& reads, std::ostream& out, unsigned long long seed);
void varyBandHashFunctions(const Chromosome& chr, const std::vector<GenomeGenerator::Read>& reads, std::ostream& out, unsigned long long seed);

std::ostream& operator<<(std::ostream& strm, const WindowBandsConfig& wb) {
    return strm << wb.windowSize << ";" << wb.windowOffset << ";" << wb.bandNumber;
}

std::ostream& operator<<(std::ostream& strm, const WindowBandsFactorConfig& wb) {
//    const double defaultMemory = calcMemory(LSH::DEFAULT_READ_LENGTH, LSH::DEFAULT_SIGNATURE_LENGTH);
//    double seemingRlng = wb.windowOffsetFactor * LSH:: / (LSH::DEFAULT_WINDOW_OFFSET / LSH::DEFAULT_READ_LENGTH);
//    e.lsh.signatureLength = calcSignatureLength(seemingRlng, defaultMemory);
    return strm << (wb.windowSizeFactor-wb.windowOffsetFactor) << ";" << wb.windowOffsetFactor << ";" << wb.windowSizeFactor;// << ";" << wb.bandNumber;
}

std::ostream& operator<<(std::ostream& strm, const MaxSeletNextFactor& mn) {
    return strm << mn.maxSelect << ";" << mn.nextFactor;
}

std::ostream& operator<<(std::ostream& strm, const LimitConfig& lc) {
    return strm << lc.limit << ";" << lc.limitSkip;
}

std::ostream& operator<<(std::ostream& strm, const ExtendConfig& ec) {
    return strm << ec.extendFactor << ";" << ec.extendBandMultiplicator;
}

std::ostream& operator<<(std::ostream& strm, const MinCountConfig& mcc) {
    return strm << mcc.minCountSelection << ";" << mcc.minCountPrefiltering;
}

void runVaryParameters_rlng100(std::ostream& out,
                               Genome& genome,
                               const std::vector<GenomeGenerator::Read>& reads,
                               uint64_t seed,
                               const std::vector<std::string>& varPaths) {
    bool containsVariants = varPaths.size() > 0;
    double alignProbability = 0.1;

    std::cout << "Initilaize LSH" << std::endl;
    LSH lsh(seed);
    typedef Experiment<LSH> Exp;
    Exp exp(genome, lsh, [](Exp& e){clearAfterGapped(e.lsh);}, out);
    exp.addReadSet(reads);
    printLSHParameters(lsh, out);

    std::cout << "Experiments" << std::endl;

    std::cout << "  max-select" << std::endl;
    for (int cfg: {LSH_PE_PLUS, LSH_PE_PLUS | LSH_PE_NEXTFACTOR_MAX, /*LSH_PE_FOR | LSH_PE_PLUS, LSH_PE_FOR | LSH_PE_PLUS_D2,*/ LSH_PE_FOR | LSH_PE_MEAN, LSH_PE_FOR | LSH_PE_MEAN | LSH_PE_NEXTFACTOR_MAX}){
        global_LSH = cfg | LSH_PE_MINCOUNT_ZERO;
        out << "cfg: " << std::hex << global_LSH << std::dec << std::endl;
        std::vector<unsigned> v = {1,2,3,5,7,10,15,20,30,50,70,100};
        auto set = [](Exp& e, unsigned v) {e.lsh._mapOptions.maxSelect = v;};
        exp.varyParameter<unsigned>("max select", v, set, "\nMAX-SELECT\n", alignProbability, ParameterType::Query);
    }

    std::cout << "we just exit here..." << std::endl;
    return;

    // =======================================================================
    // DEFAULT
    // =======================================================================
    global_LSH = LSH_PE_FOR | LSH_PE_PLUS_D2 | LSH_PE_MINCOUNT_ZERO;
    std::cout << "  default" << std::endl;
    out << "ORIGINAL\n";
    out << RUN_EXPERIMENT_HEADLINE; // skip the first ';'
    exp.runExperiment(alignProbability);
    out << std::endl;

    std::cout << "we just exit here..." << std::endl;
    return;


    // =======================================================================
    // VARIANT PROBABILITY THRESHOLD
    // =======================================================================
    if (containsVariants) {
        std::cout << "  vpt" << std::endl;
        std::vector<double> vpt = {0.0, 0.25, 0.5};//{0.0, 0.05, 0.1, 0.15, 0.25, 0.35, 0.5};
        Genome genomeCopy = genome;
        auto setpt = [&varPaths, &genomeCopy](Exp& e, double pt) {
            for (Chromosome& chr: genomeCopy) {
                chr.data = chr.noVariantsString;
                chr.variants.clear();
            }
            VariantsReader vr(genomeCopy, pt);
            vr.readVariants(varPaths);
            e.createVariantIndexes();
        };
        exp.varyParameter<double>("variant probability threshold", vpt, setpt, "\nVARIANT-PROBABILITY-THRESHOLD\n", alignProbability, ParameterType::Index);
    }




    // =======================================================================
    // INITIALIZATION
    // =======================================================================
    std::cout << "  initialization" << std::endl;
    out << "\nHASH-INITIALIZATION\n";
    out << "hash-initialization; " << RUN_EXPERIMENT_HEADLINE;
    typedef std::function<void(Exp&)> InitFunction;
    clearAfterGapped(lsh);

    std::mt19937_64 rnd(std::time(nullptr));
    InitFunction rndHash = [&rnd](Exp& e) {
        for (size_t i = 0; i < e.lsh.signatureLength; i++) {
            e.lsh.hashFunctions[i] = rnd() % (1ull << 2*e.lsh.qGramSize);
        }
    };
    InitFunction splitHash = [&rnd](Exp& e) {
        double big = exp2(2 * e.lsh.qGramSize);
        double div = big / e.lsh.signatureLength;

        for (size_t i = 0; i < e.lsh.signatureLength; i++) {
            e.lsh.hashFunctions[i] = uint64_t(i*div) + rnd() % uint64_t(div);
        }
    };
    InitFunction splitHashTriangle = [&rnd](Exp& e) {
        double big = exp2(2 * e.lsh.qGramSize);
        double div = big / e.lsh.signatureLength;
        for (size_t i = 0; i < e.lsh.signatureLength; i++) {
            e.lsh.hashFunctions[i] = uint64_t(i*div) + rnd() % uint64_t(div/2) + rnd() % uint64_t(div/2);
        }
    };

    lsh.signatureLength = LSH::DEFAULT_SIGNATURE_LENGTH;
    for (int i = 0; i < 5; i++) {
        out << "rndHash" << ";";
        lsh.clear();
        exp.afterInitializaton = rndHash;
        exp.runExperiment(alignProbability);
        out << std::endl;
    }
    for (int i = 0; i < 5; i++) {
        out << "splitHash" << ";";
        lsh.clear();
        exp.afterInitializaton = splitHash;
        exp.runExperiment(alignProbability);
        out << std::endl;
    }
    for (int i = 0; i < 5; i++) {
        out << "splitHashTriangle" << ";";
        lsh.clear();
        exp.afterInitializaton = splitHashTriangle;
        exp.runExperiment(alignProbability);
        out << std::endl;
    }
    exp.afterInitializaton = [](Exp&){};

    // =======================================================================
    // Q-GRAMS
    // =======================================================================
    std::cout << "  q-grams" << std::endl;
    {
        lsh.clear();
        LSH_Q64 lsh_q64(seed);
        Experiment<LSH_Q64> e64(genome, lsh_q64, [&exp](Experiment<LSH_Q64>& e){clearAfter(e.lsh); e.alignerOptions = exp.alignerOptions;}, out);
        e64.addReadSet(reads);
        std::vector<unsigned> vq = {14,15,16,17,18,19,20,21,22};
        auto setq = [](Experiment<LSH_Q64>& e, unsigned q) {e.lsh.qGramSize = q;};
        e64.varyParameter<unsigned>("q-gram size", vq, setq, "\nQ-GRAM-SIZE (64-bit q-grams, no gaps)\n", alignProbability, ParameterType::Index);
    }

                                   //----||||----||||----
    std::vector<std::string> vgv = {"################",
                                    "##############----##",
                                    "########-########",
                                    "########--########",
                                    "########----########",
                                    "###-###-####-###-###",
                                    "##-##-##-##-##-##-##-##",
                                    "####-##-###--##-####",
                                    "####-####-##-####-##",
                                    "#--##-####-#####-####",
                                    "#--##-####-##-#-##-####",
                                    "#-###-#-###-##-#-###-#-#",
                                    "####-#-####--##-#####"};
    auto setgv = [](Exp& e, std::string gs) {e.lsh.qGramHashObject.setGapVector(gs); e.lsh.qGramSize = gs.size();};
    exp.varyParameter<std::string>("q-gram", vgv, setgv, "\nGAPPED-Q-GRAMS (32-bit q-grams, same length)\n", alignProbability, ParameterType::Index);

    /*
    {
        lsh.clear();
        LSH_Q64 lsh_q64(seed);
        clearAfter(lsh_q64);
        std::vector<std::string> vgv = {"################",
                                        "##################",
                                        "####################",
                                        "########--########",
                                        "#########--#########",
                                        "##########--##########",
                                        "####-#-####--##-#####",
                                        "#####-#-####--##-######",
                                        "######-#-####--##-#######",
                                        "##-####-#-####--##-#####",
                                        "####-####-#-####--##-#####"};
        auto setgv = [](LSH_Q64& lsh, const std::string& gs) {
            lsh.qGramHashObject.setGapVector(gs);
            lsh.qGramSize = gs.size();
        };
        varyParameter<LSH_Q64, std::string>(genome, reads, lsh_q64, out, "q-gram", vgv, setgv, clearAfter<LSH_Q64>, "\nGAPPED-Q-GRAMS (64-bit q-grams, different length)\n", ParameterType::Index);
    }

                                    //----||||----||||----||||
    std::vector<std::string> vgv2 = {"################",
                                     "########--------########",
                                     "###-##-#####--###-###"};
    out << "\nGAPPED-Q-GRAM-HASH-FUNCTIONS\n";
    out << "q-gram; hash-function" << RUN_EXPERIMENT_HEADLINE;
    lsh.clear();
    for (const std::string& qgram: vgv2) {
        {
            LSH_GappedQGram lsh_gapped(seed, qgram);
            clearAfter(lsh_gapped);
            lsh_gapped.qGramSize = qgram.size();
            out << qgram << ";";
            out << "<<2" << ";";
            runExperiment(genome, reads, lsh_gapped, out, ParameterType::Index);
            out << std::endl;
        }{
            LSH_GappedQGram_m5 lsh_gapped_m5(seed, qgram);
            clearAfter(lsh_gapped_m5);
            lsh_gapped_m5.qGramSize = qgram.size();
            lsh_gapped_m5.qGramHashObject.multiplicator = 5;
            out << qgram << ";";
            out << "*5" << ";";
            runExperiment(genome, reads, lsh_gapped_m5, out, ParameterType::Index);
            out << std::endl;
        }{
            LSH_GappedQGram_m5 lsh_gapped_m5(seed, qgram);
            clearAfter(lsh_gapped_m5);
            lsh_gapped_m5.qGramSize = qgram.size();
            lsh_gapped_m5.qGramHashObject.multiplicator = 33767;
            out << qgram << ";";
            out << "*33767" << ";";
            runExperiment(genome, reads, lsh_gapped_m5, out, ParameterType::Index);
            out << std::endl;
        }{
            LSH_GappedQGram_m5 lsh_gapped_m5(seed, qgram);
            clearAfter(lsh_gapped_m5);
            lsh_gapped_m5.qGramSize = qgram.size();
            lsh_gapped_m5.qGramHashObject.multiplicator = 19937;
            out << qgram << ";";
            out << "*19937" << ";";
            runExperiment(genome, reads, lsh_gapped_m5, out, ParameterType::Index);
            out << std::endl;
        }{
            LSH_GappedQGram_std lsh_gapped_std(seed, qgram);
            clearAfter(lsh_gapped_std);
            lsh_gapped_std.qGramSize = qgram.size();
            out << qgram << ";";
            out << "std" << ";";
            runExperiment(genome, reads, lsh_gapped_std, out, ParameterType::Index);
            out << std::endl;
        }
    }

    // =======================================================================
    // WINDOW SIZE ; WINDOW OFFSET ; BAND NUMBER
    // =======================================================================
    std::cout << "  windows" << std::endl;
    std::vector<unsigned> vws = {125, 130, 135, 140, 145, 150, 155, 160};
    auto setws = [](LSH& lsh, unsigned ws) {lsh.windowSize = ws;};
    varyParameter<LSH, unsigned>(genome, reads, lsh, out, "window size", vws, setws, clearAfterGapped<LSH>, "\nWINDOW-SIZE\n", ParameterType::Index);


    std::vector<WindowBandsConfig> vwb;
    for (unsigned offset: {100, 125, 175, 225}) {
        for (unsigned overlap: {0, 15, 25, 35, 50}) {
            vwb.push_back(WindowBandsConfig{offset + overlap, offset, (32 * offset) / 100}); // always use the same amount of memory!
        }
    }
    auto setwb = [](LSH& lsh, WindowBandsConfig wb) {
        lsh.windowSize = wb.windowSize;
        lsh.windowOffset = wb.windowOffset;
        lsh.signatureLength = lsh.bandSize * wb.bandNumber;
    };
    varyParameter<LSH, WindowBandsConfig>(genome, reads, lsh, out, "window size; window offset; band number", vwb, setwb, clearAfterGapped<LSH>, "\nWINDOW-SIZE & OVERLAPPING\n", ParameterType::Index);

    std::vector<unsigned> vbn = {8,16,32,48,64,80,96};
    auto setbn = [](LSH& lsh, unsigned bn) {lsh.signatureLength = lsh.bandSize * bn;};
    varyParameter<LSH, unsigned>(genome, reads, lsh, out, "band number", vbn, setbn, clearAfterGapped<LSH>, "\nBAND-NUMBER\n", ParameterType::Index);
    {
        lsh.clear();
        LSH_Q64_CFBHT lsh_super(seed);
        clearAfterGapped(lsh_super);
        auto setbn = [](LSH_Q64_CFBHT& lsh, unsigned bn) {lsh.signatureLength = lsh.bandSize * bn;};
        varyParameter<LSH_Q64_CFBHT, unsigned>(genome, reads, lsh_super, out, "band number", vbn, setbn, clearAfterGapped<LSH_Q64_CFBHT>, "\nBAND-NUMBER (CFBHT)\n", ParameterType::Index);
    }

    // cfbht-load-factor
    if (false) {
        lsh.clear();
        LSH_CFBHT lsh_cfbht(seed);
        clearAfterGapped(lsh_cfbht);
        std::vector<double> vlf = {0.4, 0.5, 0.6, 0.7, 0.8};
        auto setlf = [](LSH_CFBHT& lsh, double lf) {
            for (auto &bht: lsh.bandHashTables) {
                bht.maximalLoadFactor = lf;
                bht.initialSizeMultiplicator = 1.005 / lf;

            }
        };
        out << "\nCFBHT-LOAD-FACTOR\n";
//        std::string myHeadLine(RUN_EXPERIMENT_HEADLINE);
//        myHeadLine.pop_back();
        out << "load factor; real load factor" << RUN_EXPERIMENT_HEADLINE;
        for (double lf: vlf) {
            lsh.clear();
            out << lf << ";";
            std::ostringstream oss;
            std::function<void(LSH_CFBHT&)> set = [lf, setlf](LSH_CFBHT& lsh){setlf(lsh, lf);};
            runExperiment(genome, reads, lsh_cfbht, set, oss);
            double realLoadFactor = 0;
            for (auto &bht: lsh_cfbht.bandHashTables) {
                if (bht.hashTable.size()) {
                    realLoadFactor += bht.getUniqueBandHashCount() / bht.hashTable.size();
                }
            }
            realLoadFactor /= lsh_cfbht.bandHashTables.size();
            out << realLoadFactor << ";";
            out << oss.str();
            out << std::endl;
        }
    }

    // =======================================================================
    // BAND SIZE ; BAND HASH FUNCTIONS
    // =======================================================================
    std::cout << "  bands" << std::endl;
    std::vector<unsigned> vbs = {1,2,3};
    const unsigned bandNumber = LSH::DEFAULT_SIGNATURE_LENGTH;
    auto setbs = [bandNumber](LSH& lsh, unsigned bs) {lsh.bandSize = bs; lsh.signatureLength = bandNumber * bs;};
    varyParameter<LSH, unsigned>(genome, reads, lsh, out, "band size", vbs, setbs, clearAfterGapped<LSH>, "\nBAND-SIZE\n", ParameterType::Index);

    out << "\nBAND-HASH-FUNCTIONS (band size = 2)\n";
    out << "band-hash-function" << RUN_EXPERIMENT_HEADLINE;
    lsh.clear();
    {
        LSH_bhf_m33767 lsh_m33767(seed);
        clearAfterGapped(lsh_m33767);
        lsh_m33767.bandSize = 2;
        lsh_m33767.signatureLength *= 2; // twice as much as default
        out << "*33767" << ";";
        runExperiment(genome, reads, lsh_m33767, out, ParameterType::Index);
        out << std::endl;
    }{
        LSH_bhf_stdHash lsh_std(seed);
        clearAfterGapped(lsh_std);
        lsh_std.bandSize = 2;
        lsh_std.signatureLength *= 2;
        out << "std" << ";";
        runExperiment(genome, reads, lsh_std, out, ParameterType::Index);
        out << std::endl;
    }{
        LSH_bhf_64 lsh_64(seed);
        clearAfterGapped(lsh_64);
        lsh_64.bandSize = 2;
        lsh_64.signatureLength *= 2;
        out << "64bit" << ";";
        runExperiment(genome, reads, lsh_64, out, ParameterType::Index);
        out << std::endl;
    }



    // =======================================================================
    // LIMIT ; LIMIT SKIP
    // =======================================================================
    std::cout << "  limit" << std::endl;
    if (containsVariants) {
        std::vector<unsigned> vl = {1, 2, 3, 4, 6, 8};
        auto setl = [](LSH& lsh, unsigned l) {lsh.limit = l;};
        varyParameter<LSH, unsigned>(genome, reads, lsh, out, "limit", vl, setl, clearAfterGapped<LSH>, "\nLIMIT\n", ParameterType::Index);

        std::vector<unsigned> vls = {1, 2, 4, 8, 16, 32};
        auto setls = [](LSH& lsh, unsigned ls) {lsh.limit_skip = ls; lsh.limit = std::min<unsigned>(lsh.limit, ls);};
        varyParameter<LSH, unsigned>(genome, reads, lsh, out, "limit-skip", vls, setls, clearAfterGapped<LSH>, "\nLIMIT-SKIP\n", ParameterType::Index);

        std::vector<LimitConfig> vlc = {{1,1}, { 1,16}, { 1,256},
                                               { 4,16}, { 4,256},
                                               {16,16}, {16,256},
                                                        {64,256}};
        auto setlc = [](LSH& lsh, LimitConfig lc) {
            lsh.limit = lc.limit;
            lsh.limit_skip = lc.limitSkip;
        };
        varyParameter<LSH, LimitConfig>(genome, reads, lsh, out, "limit; limit-skip", vlc, setlc, clearAfterGapped<LSH>, "\nLIMIT & LIMIT-SKIP\n", ParameterType::Index);
    }

    // =======================================================================
    // MIN COUNT ; MAX SELECT ; NEXT FACTOR
    // =======================================================================
    std::cout << "  count" << std::endl;
    std::vector<unsigned> vmc = {1,2,3,4};
    auto setmc = [](LSH& lsh, unsigned mc) {lsh._mapOptions.minCount = mc;};
    varyParameter<LSH, unsigned>(genome, reads, lsh, out, "min count", vmc, setmc, clearAfterGapped<LSH>, "\nMIN-COUNT\n", ParameterType::Query);

    std::vector<MaxSeletNextFactor> vmn;
    for (unsigned maxSelect: {8, 16, 32, 64, 128, 256}) {
        for (double nextFactor: {2., 4., 8., 16.}) {
            vmn.push_back(MaxSeletNextFactor{maxSelect, nextFactor});
        }
    }
    auto setmn = [](LSH& lsh, MaxSeletNextFactor mn) {lsh._mapOptions.maxSelect = mn.maxSelect;
                                                      lsh._mapOptions.nextFactor = mn.nextFactor;};
    varyParameter<LSH, MaxSeletNextFactor>(genome, reads, lsh, out, "max select; next factor", vmn, setmn, clearAfterGapped<LSH>, "\nMAX-SELECT & NEXT-FACTOR\n", ParameterType::Query);

    // =======================================================================
    // EXTEND NUMBER ; BAND MULTIPLICATOR
    // =======================================================================
    std::cout << "  extend" << std::endl;
    std::vector<ExtendConfig> vec;
    const unsigned windowSize = LSH::DEFAULT_WINDOW_SIZE;
    for (int minExtend: {-8, 0, 16, 32, 48}) {
        for (double mult: {0., 0.15, 0.3, 0.45}) {
            if (minExtend + windowSize * mult >= 0) {
                vec.push_back(ExtendConfig{unsigned(minExtend + windowSize * mult), mult});
            }
        }
    }
    auto setec = [](LSH& lsh, ExtendConfig ec) {lsh._mapOptions.extendFactor = ec.extendFactor;
                                                lsh._mapOptions.extendBandMultiplicator = ec.extendBandMultiplicator;};
    varyParameter<LSH, ExtendConfig>(genome, reads, lsh, out, "extend-number; extend-band-multiplicator", vec, setec, clearAfterGapped<LSH>, "\nEXTEND-NUMBER & EXTEND-BAND-MULTIPLICATOR\n", ParameterType::Query);

    std::vector<double> vcbm = {0.0, 0.1, 0.2, 0.3, 0.4, 0.5};
    auto setcbm = [](LSH& lsh, double cbm) {lsh._mapOptions.contractBandMultiplicator = cbm;};
    varyParameter<LSH, double>(genome, reads, lsh, out, "contract-band-multiplicator", vcbm, setcbm, clearAfterGapped<LSH>, "\nCONTRACT-BAND-MULTIPLICATOR\n", ParameterType::Query);

    // =======================================================================
    // ITERATIONS
    // =======================================================================
    std::cout << "  iterations" << std::endl;
    std::vector<int> vit = {1,2,3,4,5,6,7,8,9,10};
    auto setit = [](LSH& lsh, int) {
        size_t seed = std::time(nullptr);
        lsh.initialRandomSeed = seed;
        lsh.rngHashFunctions.seed(seed);
        lsh.rngLimits.clear();
        lsh.rngLimits.emplace_back(seed);
    };
    varyParameter<LSH, int>(genome, reads, lsh, out, "iteration", vit, setit, clearAfterGapped<LSH>, "\nITERATIONS\n", ParameterType::Index);

    // =======================================================================
    // HIT-DISTRIBUTION ; EVIL Q-GRAMS ; MULTI WINDOW BUCKETS
    // =======================================================================
    std::cout << "  multi analysis" << std::endl;
    {
        lsh.clear();
        LSH_CFBHT lsh_cfbht(seed);
        clearAfter(lsh_cfbht);
        lsh_cfbht.qGramSize = 16;
        lsh_cfbht.qGramHashObject.setGapVector("################");
        lsh_cfbht._mapOptions.maximumOfReturnedWindows = std::numeric_limits<uint32_t>::max();
        addChromosomeOrGenome(lsh_cfbht, genome);

        WindowIndexData wiData_1 = calcWindowIndexData(lsh_cfbht, 1);
        evilQgrams(out, lsh_cfbht, wiData_1.wiCount, wiData_1.entries, 1, 100000);
    }
    {
        lsh.clear();
        LSH_CFBHT lsh_cfbht(seed);
        clearAfterGapped(lsh_cfbht);

        lsh_cfbht._mapOptions.maximumOfReturnedWindows = std::numeric_limits<uint32_t>::max();
        addChromosomeOrGenome(lsh_cfbht, genome);

        hitDistribution(out, lsh_cfbht, reads);

        WindowIndexData wiData_1k = calcWindowIndexData(lsh_cfbht, 1000);
        multiWindowBuckets(out, wiData_1k.wiCount, 1000);


    }



    // =======================================================================
    // MAX-RETURNED-WINDOWS
    // =======================================================================
    std::cout << "  returned windows" << std::endl;
    std::vector<unsigned> vmrw = {100, 200, 400, 800, 1600, 3200, 6400, 12800, 25600, 51200, 102400, 204800, 409600};
    auto setmrw = [](LSH& lsh, unsigned mrw) {lsh._mapOptions.maximumOfReturnedWindows = mrw;};
    varyParameter<LSH, unsigned>(genome, reads, lsh, out, "max returned windows", vmrw, setmrw, clearAfterGapped<LSH>, "\nMAXIMUM-OF-RETURNED-WINDOWS\n", ParameterType::Query);

    std::vector<unsigned> vmrws = {1000, std::numeric_limits<unsigned>::max()};
    for (unsigned mrw: vmrws) {
        lsh.clear();
        clearAfterGapped(lsh);
        lsh._mapOptions.maximumOfReturnedWindows = mrw;
        addChromosomeOrGenome(lsh, genome);
        lsh.buildIndex();
        speedTest(out, lsh, reads);
    }
    */
}

#ifndef HUMANGENOMEEXPERIMENT_H
#define HUMANGENOMEEXPERIMENT_H

#include <iosfwd>
#include <string>
#include <vector>
#include "Chromosome.h"
#include "GenomeGenerator.h"

void createGenomeReads(const Genome &genome,
                       GenomeGenerator& gg,
                       size_t rnum,
                       size_t rlng,
                       double snpErros,
                       double indelErrors,
                       std::vector<GenomeGenerator::Read>& out_reads);

void runHumanGenomeExperiment(std::ostream& out,
                              const std::vector<std::string>& refPath,
                              const std::vector<std::string>& varPath,
                              size_t rnum, size_t rlng, double snpErrors, double indelErrors,
                              uint64_t seed);

void runVaryErrors(std::ostream& out,
                   Genome& genome,
                   size_t rnum,
                   uint64_t seed);

#endif // HUMANGENOMEEXPERIMENT_H

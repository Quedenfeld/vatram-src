#ifndef RMCOMPARATORPAIRED_H
#define RMCOMPARATORPAIRED_H

#include "SamCompare.h"
#include "ExternalProgram.h"
#include "DistanceComparator.h"
#include "CompareStructs.h"
#include "io/SamWriter.h"
#include "ReadSamIo.h"



class RMComparatorPaired {
public:
    struct ReadSet {
        std::string name;
        std::string samFile;
        std::string readFile1;
        std::string readFile2;
        ReadSet(const std::string& name, const std::string& samFile) : name(name), samFile(samFile) {}
        ReadSet(const std::string& name, const std::string& readFile1, const std::string& readFile2) : name(name), readFile1(readFile1), readFile2(readFile2) {}

        bool isSamFile() const {
            return readFile1 == "" && readFile2 == "" && samFile != "";
        }
        bool isReadFile() const {
            return readFile1 != "" && readFile2 != "" && samFile == "";
        }
    };
//    struct Result {
//        const ReadSet&
//    };

    std::vector<ReadSet> readSets;
    std::vector<ReadmapperCmdPaired> readmapper;

    SamCompare samCompare;
    DistanceComparator distCompare;

    RMComparatorPaired(const Genome& genome, ErrorCount maxError) :
        distCompare(genome, maxError) {
    }

    void addReadmapper(const std::string& name, const std::string& cmdPrefix, const std::string& cmdBetween, const std::string& cmdSuffix,  const std::string& samFile, const std::string& memoryPrgmName = "") {
        readmapper.emplace_back(name, cmdPrefix, cmdBetween, cmdSuffix, samFile, memoryPrgmName);
    }

    void addReadSet(const ReadSet& rs) {
        readSets.push_back(rs);
    }

//    void addReadSet(const std::string& name, const std::string& readFile1, const std::string& readFile2) {
//        readSets.emplace_back(name, readFile1, readFile2);
//    }

    void evaluate(std::ostream& out, bool removeReadFile) {
        out << "ReadSet;Readmapper;Time;Memory;CorrectRate;WrongRate\n";
        for (const ReadSet& rs: readSets) {
            std::cout << "ReadSet: " << rs.name << std::endl;
            if (rs.isSamFile()) {
                evaluateSAM(rs, out, removeReadFile);
            }
            if (rs.isReadFile()) {
                evaluateRead(rs, out);
            }
        }
    }


    void evaluateRead(const ReadSet& rs, std::ostream& out) {
        typedef DistanceComparator::ReadmapperResult ReadmapperResult;
        std::vector<ReadmapperResult> rmResults;
        std::cout << "  Run mappers" << std::endl;
        for (const ReadmapperCmdPaired& rm: readmapper) {
            std::cout << "    Readmapper: " << rm.name << std::endl;
            std::string cmdString = rm.commandPrefix + " " + rs.readFile1 + " " + rm.commandBetweenReads + " " + rs.readFile2 + " " + rm.commandSuffix;
            std::cout << "      Command: " << cmdString << std::endl;
            ExternalResult exres =
            runProgram(cmdString.c_str(), rm.memoryPrgmName);
            std::cout << "    Finished mapping (maxMemory = " << exres.maxMemory << ")" << std::endl;
            rmResults.emplace_back(rm, exres);
        }
        std::cout << "  Collect SAM" << std::endl;
        for (ReadmapperResult& rmRes: rmResults) {
            try {
                rmRes.reads = std::move(readSamFile(rmRes.readmapper.samFile));
            } catch (std::exception& e) {
                std::cout << "    Exception: " << e.what() << std::endl;
            }
        }
        std::cout << "  Read FASTQ: " << std::flush;
        std::vector<UnalignedRead> unaligned = readReadFile(rs.readFile1);
        for (UnalignedRead& r: readReadFile(rs.readFile2)) {
            unaligned.emplace_back(std::move(r));
        }
        std::sort(unaligned.begin(), unaligned.end());
        size_t readCount = unaligned.size();
        std::cout << readCount << std::endl;
        std::cout << "  Compare" << std::endl;
        std::vector<AlignedRead> opt;
        distCompare.compare(unaligned, rmResults, opt);
        writeSamFile(opt, std::string("opt") + rs.name + ".sam");
        std::cout << "  Print" << std::endl;
        for (const ReadmapperResult& rmRes: rmResults) {
            out << rs.name << ";" << rmRes.readmapper.name << ";"
                << (rmRes.externalResult.time.count() / 1000.) << ";"
                << (rmRes.externalResult.maxMemory / 1024. / 1024. / 1024.) << ";"
                << (rmRes.correct / (double) readCount) << ";"
                << (rmRes.wrong / (double) readCount) << ";";
            out << std::endl;
        }
    }

    void evaluateSAM(const ReadSet& rs, std::ostream& out, bool removeReadFile) {
        std::cout << "  Read SAM" << std::endl;
        std::vector<AlignedRead> correct = readSamFile(rs.samFile);
        std::cout << "  Write FASTQ" << std::endl;
        std::string readFile1 = rs.name + "_1.fastq";
        std::string readFile2 = rs.name + "_2.fastq";
        writeReadFile_paired(correct, readFile1, readFile2);
        for (const ReadmapperCmdPaired& rm: readmapper) {
            std::cout << "  Readmapper: " << rm.name << std::endl;
            std::cout << "    Start mapping" << std::endl;
            std::string cmdString = rm.commandPrefix + " " + readFile1 + " " + rm.commandBetweenReads + " " + readFile2 + " " + rm.commandSuffix;
            std::cout << "      Command: " << cmdString << std::endl;
            ExternalResult exres =
            runProgram(cmdString.c_str(), rm.memoryPrgmName); // exec the readmapper
            std::cout << "    Finished mapping (maxMemory = " << exres.maxMemory << ")" << std::endl;

            try {
                std::cout << "    Read SAM" << std::endl;
                auto aligned = readSamFile(rm.samFile);
                std::cout << "    Compare SAM" << std::endl;
                std::vector<AlignedRead> unmappedReads;
                SamCompare::Result result = samCompare.compareSam(correct, aligned, 5, &unmappedReads);

                std::cout << "    Write Result" << std::endl;
                out << rs.name << ";" << rm.name << ";"
                    << (exres.time.count() / 1000.) << ";"
                    << (exres.maxMemory / 1024. / 1024. / 1024.) << ";"
                    << (result.correct / (double) result.readCount) << ";"
                    << (result.wrong / (double) result.readCount) << ";";

                out << std::endl;
                std::cout << "    Finished" << std::endl;
            } catch (std::exception& e) {
                std::cout << "    Exception: " << e.what() << std::endl;
            }
        }
        if (removeReadFile) {
            // clear read file
            std::remove(readFile1.c_str());
            std::remove(readFile2.c_str());

        }
    }

    void writeSamFile(const std::vector<AlignedRead>& reads, const std::string& path) {
        seqan::BamStream bamstream(path.c_str(), seqan::BamStream::WRITE);
        ChromosomeReferences cr;
        for (const Chromosome& chr: distCompare.genome) {
            cr.push_back(chr);
        }
        SamWriter sw(bamstream);
        sw.writeHeader(cr);

        for (const AlignedRead& read: reads) {
            ReadString revComplStr = read.seq;
            seqan::reverseComplement(revComplStr);

            CigarString cs;
            seqan::CigarElement<> manyElements('M', seqan::length(read.seq));
            seqan::append(cs, manyElements);

            sw.writeRead(read.id, read.seq, revComplStr, read.revCompl, distCompare.chrName2idx[read.chr], read.beginPos, cs);
        }

    }





};

#endif // RMCOMPARATORPAIRED_H

#ifndef READMAPPER_H
#define READMAPPER_H

#include <string>
#include "Chromosome.h"

inline bool endsWith(const std::string& value, const std::string& ending) {
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

struct ReadmapperCmd {
    std::string name;
    std::string commandPrefix;
    std::string commandSuffix;
    std::string samFile;
    std::string memoryPrgmName;
    ReadmapperCmd(const std::string& name, const std::string& commandPrefix, const std::string& commandSuffix, const std::string& samFile, const std::string& memoryPrgmName) :
        name(name),
        commandPrefix(commandPrefix),
        commandSuffix(commandSuffix),
        samFile(samFile),
        memoryPrgmName(memoryPrgmName) {

    }
};

struct ReadmapperCmdPaired : public ReadmapperCmd {
    std::string commandBetweenReads;
    ReadmapperCmdPaired(const std::string& name,
                        const std::string& commandPrefix,
                        const std::string& commandBetweenReads,
                        const std::string& commandSuffix,
                        const std::string& samFile,
                        const std::string& memoryPrgmName) :
        ReadmapperCmd(name, commandPrefix, commandSuffix, samFile, memoryPrgmName),
        commandBetweenReads(commandBetweenReads) {

    }
};

struct UnalignedRead {
    std::string id;
    ReadString seq;

    bool operator<(const UnalignedRead& r) const {
        if (id != r.id) {
            return id < r.id;
        } else {
            return seq < r.seq;
        }
    }
    bool operator==(const UnalignedRead& r) const {
        return id == r.id && seq == r.seq;
    }
};

struct AlignedRead : public UnalignedRead {
    std::string chr;
    Position beginPos;
    bool revCompl;


};

#endif // READMAPPER_H

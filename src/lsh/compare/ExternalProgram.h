#ifndef EXTERNALPROGRAM_H
#define EXTERNALPROGRAM_H

#include <string>
#include <stdexcept>
#include <chrono>
#include <thread>
#include <vector>
#include <iostream>
#include <fstream>
#include <thread>
#include "Timer.h"

#ifndef _WIN32
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#endif

struct ExternalResult {
    size_t maxMemory = 0;
    std::chrono::milliseconds time;
//    int returnedValue = 0;
};

struct ExternalCommand {
    std::vector<std::string> argv;
    std::string outPath;

    ExternalCommand(const std::string& line) {
        size_t lastBegin = 0;
        bool lock = false;
        for (size_t i = 0; i < line.size(); i++) {
            if (line[i] == '"') {
                lock = !lock;
            } else if (!lock) {
                if (line[i] == ' ') {
                    if (lastBegin < i) {
                        if (line[lastBegin] == '"' && line[i-1] == '"') {
                            argv.push_back(line.substr(lastBegin+1, i-lastBegin-2));
                        } else {
                            argv.push_back(line.substr(lastBegin, i - lastBegin));
                        }
                    }
                    lastBegin = i+1;
                } else if (line[i] == '>') {
                    i++;
                    while (i < line.size() && line[i] == ' ') i++;
                    outPath = line.substr(i);
                    lastBegin = line.size();
                    break;
                }
            }
        }
        if (lastBegin < line.size()) {
            argv.push_back(line.substr(lastBegin, line.size() - lastBegin));
        }
    }

    char** getArgvCStrings() {
        if (!cstringsCreated) {
            createArgvCStrings();
        }
        return argvCStrings_pointer.data();
    }

private:
    bool cstringsCreated = false;
    std::vector<char> argvCStrings_concatString;
    std::vector<char*> argvCStrings_pointer;

    void createArgvCStrings() {
        std::vector<size_t> idxs;
        for (const std::string& str: argv) {
            idxs.push_back(argvCStrings_concatString.size());
            for (char c: str) {
                argvCStrings_concatString.push_back(c);
            }
            argvCStrings_concatString.push_back('\0');
        }
        for (size_t idx: idxs) {
            argvCStrings_pointer.push_back(&argvCStrings_concatString[idx]);
        }
        argvCStrings_pointer.push_back(NULL);

        cstringsCreated = true;
    }
};

#ifndef _WIN32
void waitForPid(pid_t pid, ExternalResult* outTime) {
    Timer<> timer;
    int status;
    waitpid(pid, &status, 0);
    outTime->time = timer.stopTimer();
}
#else
void waitForPid(int, ExternalResult*) {}
#endif


ExternalResult runProgram(const std::string& prgm, const std::string& memoryPrgmName = "") {
    ExternalCommand cmd(prgm);
    ExternalResult res;

#ifndef _WIN32
    pid_t pid = fork();
    pid_t memPID = pid;
#else
    int pid = 0; // win32-compatibility
    int memPID = 0;
#endif
    if (pid < 0) {
        std::cout << "Error: " << strerror(errno) << std::endl;
//        std::system(prgm.c_str());
        res.maxMemory = -1;
        return res;
    } else if (pid == 0) {
        // Child
//        std::cout << "Exec: " << cmd.argv[0].c_str() << std::endl;
//        char** pt = cmd.getArgvCStrings();
//        while (*pt) {
//            std::cout << "  " << *pt++ << "\n";
//        }
//        std::cout << "Redirect: " << (cmd.outPath != std::string("") ? cmd.outPath : "-") << std::endl;
#ifndef _WIN32
        if (cmd.outPath != std::string("")) {
            int fd = open(cmd.outPath.c_str(), O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
            dup2(fd, 1);
            close(fd);
        }

        execv(cmd.argv[0].c_str(), cmd.getArgvCStrings());
#endif
    } else {
        std::thread thread(waitForPid, pid, &res);
        // Parent
#ifndef _WIN32
        size_t pageSize = sysconf( _SC_PAGESIZE);
#else
        size_t pageSize = 1;
#endif

        if (memoryPrgmName != "") {
            std::chrono::milliseconds waitTime(1000);
            std::this_thread::sleep_for(waitTime);
            std::system((std::string("pidof ") + memoryPrgmName + " > tmp.txt").c_str());
            std::ifstream f("tmp.txt");
            f >> memPID;
            std::cout << "    New PID: " << memPID << std::endl;
        }


        while (true) {
            std::chrono::milliseconds waitTime(500);
            std::this_thread::sleep_for(waitTime);
            std::ifstream f(std::string("/proc/") + std::to_string(memPID) + "/statm");
            if (!f.good()) {
                break; // datei fehlt -> programm vermutlich fertig
            }
            size_t memPages;
            f >> memPages;
            if (memPages == 0) {
                break;
            }
            res.maxMemory = std::max(res.maxMemory, memPages * pageSize);
        }
//        std::cout << "Max memory: " << res.maxMemory << std::endl;

        thread.join();
    }


    return res;
}


#endif // EXTERNALPROGRAM_H

// just a little test
//int main() {
//	runProgram("grep \"12 34\" bigFile.blub > test.txt");
//}



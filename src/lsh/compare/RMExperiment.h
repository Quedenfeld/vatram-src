#ifndef RMEXPERIMENT_H
#define RMEXPERIMENT_H

#include "RMComparator.h"
#include "ReadGenerator.h"
#include "VatramCmdStrings.h"
#include "io/ReferenceReader.h"

#include <functional>

class RMExperiment {
public:



    struct Experiment {
        std::string name;
        std::string inputFile;
        std::string vatramExtraParameters;
        double meanError;
//        std::function<void()> createSamFile
        Experiment(const std::string& name, const std::string& samFile, const std::string& vatramExtraParameters, double meanError) :
            name(name), inputFile(samFile), vatramExtraParameters(vatramExtraParameters), meanError(meanError) {}
    };

    std::ofstream resultFile;
    std::vector<Experiment> experiments;
    uint64_t baseCountPerExperiment;
    uint64_t seed;
    std::mt19937 rnd;

    Genome genome;

    RMExperiment(uint64_t baseCount, uint64_t seed) :
        resultFile("result.csv"),
        baseCountPerExperiment(baseCount),
        seed(seed),
        rnd(seed) {

    }


    void createExperiments(const std::vector<std::string> &refPaths,
                           const std::vector<std::string> &varPaths) {

        std::unordered_set<std::string> filter;
        ReferenceReader rr(genome, filter);
        rr.readReferences(refPaths);
        VariantsReader vr(genome, filter, 0.2);
        vr.readVariants(varPaths);
        ReadGenerator rg(genome, varPaths, seed);


//        createEqualLength(rg, 100, 0.04); // just for testing everything




        double realErrorFactor = 0.1;
        std::vector<double> verr = {0.02, 0.04, 0.08};


//        createRealRazers3("c_50_ERR385811", "/scratch/JQ/reads/Illumina/ERR385811/ERR385811_1M.fastq", "-e 6", 50*realErrorFactor); // 50 c
//        createRealRazers3("c_100_ERR281333a", "/scratch/JQ/reads/Illumina/ERR281333/ERR281333_1_2M.fastq", "-e 10", 100*realErrorFactor); // 100 c pe
//        createRealRazers3("c_150_SRR1374471", "/scratch/JQ/reads/Illumina/SRR1374471/SRR1374471.fastq.gz", "-e 15 -w 210 -W 187 -s 54", 150*realErrorFactor); // 150 c
//        createRealRazers3("c_250_ERR967952a", "/scratch/JQ/reads/Illumina/ERR967952/ERR967952_1.fastq.gz", "-e 25 -w 350 -W 312 -s 90", 250*realErrorFactor); // 250 c pe
//        createRealRazers3("v_180_DRR003760", "/scratch/JQ/reads/Illumina/DRR003760/DRR003760.fastq.gz", "-e 20", 180*realErrorFactor); // 180 v
//        createRealRazers3("v_266_SRR003025", "/scratch/JQ/reads/GS_FLX/SRR003025/SRR003025.fastq.gz", "-e 30", 266*realErrorFactor); // 266 v
//        createRealRazers3("v_565_SRR003174", "/scratch/JQ/reads/GS_FLX/SRR003174/SRR003174.fastq.gz", "-e 60", 565*realErrorFactor); // 565 v


        for (size_t lng: {100, 150, 200, 300, 400, 500}) {
            createIndexSet(rg, lng);
            for (double err: verr) {
                createEqualLength(rg, lng, err);
            }
        }



        for (size_t lng: {100, 150, 200, 300, 400, 500}) {//{100, 300, 500}) {
            std::vector<double> verr = {0.04};
            for (double err: verr) {
                createHighVariantDensity_high(rg, lng, err);
            }
            for (double err: verr) {
                createHighVariantDensity_all(rg, lng, err);
            }
        }

//        for (double err: verr) {
//            createVaryLength(rg, 100, 500, err); // 300
//        }
//        for (double err: verr) {
//            createVaryLength(rg, 50, 950, err); // 500
//        }
//        for (double err: verr) {
//            createVaryLength(rg, 50, 1950, err); // 1000
//        }
//        for (double err: verr) {
//            createVaryLength(rg, 1000, 2000, err); // 1500
//        }
//        for (double err: verr) {
//            createVaryLength(rg, 1500, 2500, err); // 2000
//        }
//        for (double err: verr) {
//            createVaryLength(rg, 500, 3500, err); // 2000
//        }
//        for (double err: verr) {
//            createVaryLength(rg, 2500, 3500, err); // 3000
//        }

        std::string varLngCmdPart = " --split-length 200 -s 68 -w 280 -W 200 ";

        createFastq("c_50_ERR385811", "/scratch/JQ/reads/Illumina/ERR385811/ERR385811_1M.fastq", "-e 6", 50*realErrorFactor); // 50 c
        createFastq("c_100_ERR281333a", "/scratch/JQ/reads/Illumina/ERR281333/ERR281333_1_2M.fastq", "-e 10", 100*realErrorFactor); // 100 c pe
        createFastq("c_150_SRR1374471", "/scratch/JQ/reads/Illumina/SRR1374471/SRR1374471.fastq.gz", "-e 15 -w 210 -W 187 -s 52", 150*realErrorFactor); // 150 c
        createFastq("c_250_ERR967952a", "/scratch/JQ/reads/Illumina/ERR967952/ERR967952_1_1M.fastq", "-e 25 -w 350 -W 312 -s 83", 250*realErrorFactor); // 250 c pe
        createFastq("v_180_DRR003760", "/scratch/JQ/reads/Illumina/DRR003760/DRR003760.fastq.gz", varLngCmdPart+"-e 20", 180*realErrorFactor); // 180 v
        createFastq("v_266_SRR003025", "/scratch/JQ/reads/GS_FLX/SRR003025/SRR003025.fastq.gz", varLngCmdPart+"-e 30", 266*realErrorFactor); // 266 v
        createFastq("v_565_SRR003174", "/scratch/JQ/reads/GS_FLX/SRR003174/SRR003174.fastq.gz", varLngCmdPart+"-e 60", 565*realErrorFactor); // 565 v

        createFastq("pacbio_54x", "/scratch/JQ/reads/PacBio/54x/first_split3k.fasta", varLngCmdPart+"-e 450", 3000*realErrorFactor*2); // 565 v
//        createFastq("pacbio_54x", "/scratch/JQ/reads/PacBio/54x/first_split1k.fasta", varLngCmdPart+"-e 150", 1000*realErrorFactor*2); // 565 v



        // clear stuff that is not needed
        for (Chromosome& chr: genome) {
            chr.insertLowProbabilitySnps();
            decltype(chr.noVariantsString) empty;
            seqan::swap(chr.noVariantsString, empty);
        }




    }




    void createEqualLength(ReadGenerator& rg, size_t rlng, double errorRate) {
        std::ostringstream oss;
        oss << "const_" << rlng << "_" << std::setprecision(3) << errorRate;
        std::string file = oss.str() + ".sam";
        if (!exists(file)) {
            auto reads = rg.createReads(baseCountPerExperiment / rlng, rlng, .95*errorRate, .05*errorRate, 0.3);
            rg.writeReads(reads, file);
        }
        std::ostringstream cmd;
        cmd << " -w " << int(std::round(1.4*rlng)) << " -W " << int(std::round(1.25*rlng)) << " -s " << calcSignatureLength(rlng) << " -e " << int(2+std::round(2*rlng*errorRate)) << " ";
        experiments.emplace_back(oss.str(), file, cmd.str(), errorRate*rlng);
    }


    void createVaryLength(ReadGenerator& rg, size_t lngMin, size_t lngMax, double errorRate) {
        std::uniform_int_distribution<size_t> unif(lngMin, lngMax);
        size_t mean = (lngMin + lngMax) / 2;
        std::ostringstream oss;
        oss << "vary_" << lngMin << "-" << lngMax << "_" << std::setprecision(3) << errorRate;
        std::string file = oss.str() + ".sam";
        if (!exists(file)) {
            auto reads = rg.createReads_differentLength(baseCountPerExperiment / mean, std::bind(unif, std::ref(rnd)), .95*errorRate, .05*errorRate, 0.3);
            rg.writeReads(reads, file);
        }
        std::ostringstream cmd;
        cmd << " -e " << int(std::round(2.5*mean*errorRate)) << " --split-length 200 -s 68 -w 280 -W 200 ";
        experiments.emplace_back(oss.str(), file, cmd.str(), errorRate*mean);
    }

    void createIndexSet(ReadGenerator& rg, size_t rlng) {
        std::ostringstream oss;
        oss << "indextest_" << rlng;
        std::string file = oss.str() + ".sam";
        if (!exists(file)) {
            auto reads = rg.createReads(1, rlng, 0, 0, 0);
            rg.writeReads(reads, file);
        }
        experiments.emplace_back(oss.str(), file, "", 1);
    }

    void createHighVariantDensity_high(ReadGenerator& rg, size_t rlng, double errorRate) {
        std::vector<ReadGenerator::Interval> intervals;

        auto checkChrIdx = [](ChromosomeIndex chrIdx){if (chrIdx == ChromosomeIndex(-1)) {throw std::runtime_error("negative chromsome index (createHighVariantDensity)");} else {return chrIdx;}};

        intervals.emplace_back(checkChrIdx(rg.getChromsomeIndex( "1")), 207511977, 22815);
        intervals.emplace_back(checkChrIdx(rg.getChromsomeIndex( "2")), 127413243, 30798);
        intervals.emplace_back(checkChrIdx(rg.getChromsomeIndex( "6")),  10532526, 54184);
        intervals.emplace_back(checkChrIdx(rg.getChromsomeIndex( "6")),  32630680,  4510);
        intervals.emplace_back(checkChrIdx(rg.getChromsomeIndex( "7")),  61967275,  4090);
        intervals.emplace_back(checkChrIdx(rg.getChromsomeIndex("16")),  46388873,  5233);
        intervals.emplace_back(checkChrIdx(rg.getChromsomeIndex( "X")),   2714790, 12269);
        intervals.emplace_back(checkChrIdx(rg.getChromsomeIndex( "Y")),   2600930,  5929);

        createHighVariantDensity(rg, rlng, errorRate, "high", intervals);
    }

    void createHighVariantDensity_all(ReadGenerator& rg, size_t rlng, double errorRate) {
        std::vector<ReadGenerator::Interval> intervals;

        for (size_t i = 0; i < genome.size(); i++) {
            const Chromosome& chr = genome[i];
            std::cout << "_" << chr.name << ":" << chr.variants.size() << std::endl;
            if (chr.variants.size()) {
                size_t varCount = 0;
                int64_t beginPos = chr.variants.front().getPosition();
                int64_t lastPos = 0;
                const int64_t maxDist = rlng * 0.8;
                const size_t minCount = 3;
                const int64_t extend = rlng * 0.4;
                for (const Variant& v: chr.variants) {
                    int64_t pos = v.getPosition();
                    varCount++;
//                    std::cout << "_p:" << pos << ",c:" << varCount << ",p-last:" << pos-lastPos << std::endl;
                    if (varCount == 1) {
                        beginPos = pos;
                    } else if (pos - lastPos > maxDist) {
//                        std::cout << "__if" << std::flush;
                        if (varCount >= minCount) {
//                            std::cout << "+if," << "beginPos=" << beginPos << ",extend=" << extend << ",diff=" << beginPos-extend << std::flush;
                            int64_t begin = std::max<int64_t>(beginPos - extend, 0);
//                            std::cout << "," << "pos=" << pos << ",extend=" << extend << ",plus=" << pos+extend << ",lng=" << seqan::length(chr.data) << std::flush;
                            int64_t end = std::min<int64_t>(lastPos + extend, seqan::length(chr.data));
//                            std::cout << ",b:" << begin << ",e:" << end << ",e-b:" << end-begin << std::flush;
                            if (end - begin > (int64_t) rlng) {
//                                std::cout << ",add" << std::flush;
                                intervals.emplace_back(i, begin, end - begin);
                            }
                        }
//                        std::cout << ",reset." << std::endl;

                        beginPos = pos;
                        varCount = 1;
                    }
                    lastPos = pos;
                }
            }

        }

        std::cout << "\n__size: " << intervals.size() << std::endl;
        createHighVariantDensity(rg, rlng, errorRate, "all", intervals);
    }

    void createHighVariantDensity(ReadGenerator& rg, size_t rlng, double errorRate, const std::string& name, const std::vector<ReadGenerator::Interval>& intervals) {
        std::ostringstream oss;
        oss << "var_" << name << "_" << rlng << "_" << std::setprecision(3) << errorRate;
        std::string file = oss.str() + ".sam";
        if (!exists(file)) {


            auto reads = rg.createReads_position(baseCountPerExperiment / rlng, rlng, intervals, .95*errorRate, .05*errorRate, 0.3);
            rg.writeReads(reads, file);
        }
        std::ostringstream cmd;
        size_t rlngpara = std::max<size_t>(rlng, 100);
        cmd << " -w " << int(std::round(1.4*rlngpara)) << " -W " << int(std::round(1.25*rlngpara)) << " -s " << calcSignatureLength(rlngpara) << " -e " << int(2+std::round(2*rlng*errorRate)) << " ";
        experiments.emplace_back(oss.str(), file, cmd.str(), errorRate*rlng);
    }

    void createRealRazers3(const std::string& name, const std::string& readFilePath, const std::string& vatramParameters, double meanError) {
        std::string file = "real_" + name + ".sam";
        if (!exists(file)) {
            std::string razers3 = "/home/quedenfe/razers3/bin/razers3 -tc 8 -v -rr 100 -i 94 -m 1 -dr 0 -ds -o ";
            razers3 += file + " ";
            razers3 += "/scratch/JQ/genome/GCA_000001405.14_GRCh37.p13_no_alt_analysis_set_MRN.fa ";
            razers3 += readFilePath;
            std::cout << "  Execute: " << razers3 << std::endl;
            Timer<> timer;
            ExternalResult exres =
            runProgram(razers3.c_str());
            timer.stopTimer();
            std::cout << "  Finished: " << exres.maxMemory << std::endl;
            resultFile << "createRealRazers3();" << file << ";" << timer.getDuration().count() << std::endl;
        }
        experiments.emplace_back(name, file, vatramParameters, meanError);
    }

    void createFastq(const std::string& name, const std::string& readFilePath, const std::string& vatramParameters, double meanError) {
        experiments.emplace_back(name, readFilePath, vatramParameters, meanError);
    }

    void runAll(bool clear) {
        for (const Experiment& exp: experiments) {
            run(exp, clear);
        }
    }

    void run(const Experiment& exp, bool clear) {
        RMComparator rmc(genome, 3*exp.meanError);

        rmc.addReadmapper("BWA", "/home/quedenfe/bwa/bwa mem -t 8 /scratch/JQ/genome/GCA_000001405.14_GRCh37.p13_no_alt_analysis_set_MRN.fa", " > bwa.sam", "bwa.sam");
        rmc.addReadmapper("BWA-EDIT", "/home/quedenfe/bwa/bwa mem -t 8 -A1 -B1 -O1 -E1 /scratch/JQ/genome/GCA_000001405.14_GRCh37.p13_no_alt_analysis_set_MRN.fa", " > bwaedit.sam", "bwaedit.sam");
        rmc.addReadmapper("BWA-SW", "/home/quedenfe/bwa/bwa bwasw -t 8 /scratch/JQ/genome/GCA_000001405.14_GRCh37.p13_no_alt_analysis_set_MRN.fa", " > bwasw.sam", "bwasw.sam");
        rmc.addReadmapper("VATRAM", std::string("/home/quedenfe/masterarbeit/work/vatram align ") + vatramRef + vatramVar + " -x 1337 -o vatram.sam", exp.vatramExtraParameters, "vatram.sam");
        rmc.addReadmapper("VATRAM-NOVAR", std::string("/home/quedenfe/masterarbeit/work/vatram align") + vatramRef + " -x 1337 -o vatramnovar.sam", exp.vatramExtraParameters, "vatramnovar.sam");
        rmc.addReadmapper("Bowtie2", "/home/quedenfe/bowtie2/bowtie2 -p 8 -x /scratch/JQ/genome/index_bowtie -S bowtie2.sam -U", "", "bowtie2.sam", "bowtie2-align-s");
        rmc.addReadmapper("MRSfast", "/home/quedenfe/mrsfast/mrsfast --search /scratch/JQ/genome/GCA_000001405.14_GRCh37.p13_no_alt_analysis_set_MRN.fa --threads 0 --snp /scratch/JQ/genome/snp.index -o mrsfast.sam --best --seq", "", "mrsfast.sam");
//        rmc.addReadmapper("RazerS3", "/home/quedenfe/razers3/bin/razers3 -tc 8 -v -o razers3.sam /scratch/JQ/genome/GCA_000001405.14_GRCh37.p13_no_alt_analysis_set_MRN.fa", "", "razers3.sam");
//        rmc.addReadmapper("RazerS3-Rabema", "/home/quedenfe/razers3/bin/razers3 -tc 8 -v -rr 100 -i 94 -m 1 -dr 0 -ds -o razers3rabema.sam /scratch/JQ/genome/GCA_000001405.14_GRCh37.p13_no_alt_analysis_set_MRN.fa", "", "razers3rabema.sam");


        rmc.addReadSet(exp.name, exp.inputFile);
        rmc.evaluate(resultFile, clear);
        if (clear) {
//            std::remove(exp.samFile.c_str());
        }
    }


};


inline
void runRMexperiment(const std::vector<std::string> &refPaths,
                     const std::vector<std::string> &varPaths,
                     uint64_t seed) {
    RMExperiment rme(10ULL*1000*1000, seed);
    rme.createExperiments(refPaths, varPaths);
    rme.runAll(true);
}


#endif // RMEXPERIMENT_H

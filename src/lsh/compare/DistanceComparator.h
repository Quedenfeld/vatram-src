#ifndef DISTANCECOMPARATOR_H
#define DISTANCECOMPARATOR_H

#include "Chromosome.h"
#include "align/Aligner.h"
#include "SamCompare.h"
#include "CompareStructs.h"
#include "ExternalProgram.h"
#include <unordered_map>


class DistanceComparator {
public:

    struct ReadmapperResult {
        const ReadmapperCmd& readmapper;
        ExternalResult externalResult;
        std::vector<AlignedRead> reads;
        size_t correct = 0;
        size_t wrong = 0;
        ReadmapperResult(const ReadmapperCmd& readmapper, ExternalResult externalResult) : readmapper(readmapper), externalResult(externalResult) {}
    };

    struct ReadmapperReadReference {
        ReadmapperResult& rmRes;
        const AlignedRead& read;
        ErrorCount errCount = std::numeric_limits<ErrorCount>::max();
        bool optimal = false;
        ReadmapperReadReference(ReadmapperResult& rmRes, const AlignedRead& read) : rmRes(rmRes), read(read) {}
    };

    const Genome& genome;
    std::unordered_map<std::string, size_t> chrName2idx;
    std::vector<VariantIndex> varIdxs;
    Aligner aligner;
    ErrorCount maxError;
    std::mt19937 rnd;

    DistanceComparator(const Genome& genome, ErrorCount maxError, uint64_t seed = 0) :
        genome(genome),
        aligner(0,0),
        maxError(maxError),
        rnd(seed) {
        varIdxs.reserve(genome.size());
        for (const Chromosome& chr: genome) {
            varIdxs.emplace_back(chr.variants, -1);
        }
        for (size_t i = 0; i < genome.size(); i++) {
            chrName2idx[genome[i].name] = i;
        }
    }

    ErrorCount errorCount(const AlignedRead& read) {
        auto nameIt = chrName2idx.find(read.chr);
        if (nameIt != chrName2idx.end()) {
            size_t chrIdx = nameIt->second;
//            Position beginPos = std::max(0, int(read.beginPos) - 2);
//            Position endPos = std::min(read.beginPos + seqan::length(read.seq)*2, seqan::length(genome[chrIdx].data));

//            const std::vector<SnpVariant>& lowProbSnps = genome[chrIdx].lowProbabilitySnps;
//            auto it = std::lower_bound(lowProbSnps.begin(), lowProbSnps.end(), beginPos, [](const SnpVariant& var, size_t val){return var.position < val;});
//            auto itEnd = lowProbSnps.end();
//            if (it != itEnd && it->position < endPos) {
//                // temporary ref-copy with low-probability-SNPs
//                std::vector<SnpVariant> originalBases;
//                Chromosome& chr = const_cast<Chromosome&>(genome[chrIdx]);
//                for (; it != itEnd && it->position < endPos; ++it) {
//                    originalBases.push_back(SnpVariant{it->position, chr.data[it->position]});
//                    chr.addSNP(it->position, it->snp);
//                }
//                Alignment a = aligner.align(chr, read.beginPos - 2, read.beginPos + seqan::length(read.seq)*2, varIdxs[chrIdx], read.seq, maxError, 0);
//                for (SnpVariant& snp: originalBases) {
//                    chr.data[snp.position] = snp.snp;
//                }
//                return a.getErrorCount();
//            } else {
//                // there are no low-probability-SNPs

//            }
            Alignment a = aligner.align(genome[chrIdx], read.beginPos - 2, read.beginPos + seqan::length(read.seq)*2, varIdxs[chrIdx], read.seq, maxError, 0);
            return a.getErrorCount();

        } else {
            return std::numeric_limits<ErrorCount>::max();
        }
    }

    void compare(std::vector<ReadmapperReadReference>& inout_readRefs, std::vector<AlignedRead>& out_opt) {
        ErrorCount errMin = std::numeric_limits<ErrorCount>::max();
        // TODOJQ: parallel, zwischenausgabe
        for (ReadmapperReadReference& rrr: inout_readRefs) {
            rrr.errCount = errorCount(rrr.read);
            errMin = std::min(errMin, rrr.errCount);
//            std::cout << "      RRR: " << rrr.rmRes.readmapper.name << " > " << rrr.errCount << std::endl;
        }
//        std::cout << "      Min: " << errMin << std::endl;
        std::vector<ReadmapperReadReference*> optRefs;
        for (ReadmapperReadReference& rrr: inout_readRefs) {
            if (rrr.errCount == errMin) {
                rrr.optimal = true;
                optRefs.push_back(&rrr);
            }
//            rrr.optimal = rrr.errCount == errMin;
        }
        if (optRefs.size()) {
            std::uniform_int_distribution<size_t> distr(0, optRefs.size() - 1);
            ReadmapperReadReference& opt = *optRefs[distr(rnd)];
            out_opt.push_back(opt.read);
        }
    }

    void compare(const std::vector<UnalignedRead>& reads, std::vector<ReadmapperResult>& rmResults, std::vector<AlignedRead>& out_opt) {
//        size_t count = 0;
        int percent = -1;
        for (size_t i = 0; i < reads.size(); i++) {
            const UnalignedRead& r = reads[i];
            int newPercent = std::floor((i+1) * 100. / reads.size());
            if (percent != newPercent) {
                percent = newPercent;
                std::cout << "    " << percent << "%" << std::endl;
            }
//            std::cout << "    Search for [" << count++ << "]: " << r.id << std::endl;
            std::vector<ReadmapperReadReference> refs;
            for (ReadmapperResult& rmRes: rmResults) {
//                std::cout << "      Readmapper: " << rmRes.readmapper.name;
                auto it = std::lower_bound(rmRes.reads.begin(), rmRes.reads.end(), r);
                if (it != rmRes.reads.end() && *it == r) {
//                    std::cout << " (found)";
                    refs.emplace_back(rmRes, *it);
                }
//                std::cout << std::endl;
            }
//            std::cout << "    Compare" << std::endl;
            compare(refs, out_opt);
//            std::cout << "    Add" << std::endl;
            for (ReadmapperReadReference& rrr: refs) {
                if (rrr.optimal) {
//                    std::cout << "      Readmapper: " << rrr.rmRes.readmapper.name << ", correct" << std::endl;
                    rrr.rmRes.correct++;
                } else {
//                    std::cout << "      Readmapper: " << rrr.rmRes.readmapper.name << ", wrong" << std::endl;
                    rrr.rmRes.wrong++;
                }
            }

        }
    }

};


#endif // DISTANCECOMPARATOR_H

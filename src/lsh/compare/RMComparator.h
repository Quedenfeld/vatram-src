#ifndef RMCOMPARATOR_H
#define RMCOMPARATOR_H

#include "SamCompare.h"
#include "ExternalProgram.h"
#include "DistanceComparator.h"
#include "CompareStructs.h"
#include "io/SamWriter.h"
#include "ReadSamIo.h"



class RMComparator {
public:
    struct ReadSet {
        std::string name;
        std::string file;
        ReadSet(const std::string& name, const std::string& file) : name(name), file(file) {}

        bool isSamFile() const {
            return endsWith(file, ".sam") || endsWith(file, ".sam.gz");
        }
        bool isReadFile() const {
            return endsWith(file, ".fasta") || endsWith(file, ".fastq") || endsWith(file, ".fa") || endsWith(file, ".fq") ||
                   endsWith(file, ".fasta.gz") || endsWith(file, ".fastq.gz") || endsWith(file, ".fa.gz") || endsWith(file, ".fq.gz");
        }
    };
//    struct Result {
//        const ReadSet&
//    };

    std::vector<ReadSet> readSets;
    std::vector<ReadmapperCmd> readmapper;

    SamCompare samCompare;
    DistanceComparator distCompare;

    RMComparator(const Genome& genome, ErrorCount maxError) :
        distCompare(genome, maxError) {
    }

    void addReadmapper(const std::string& name, const std::string& cmdPrefix, const std::string& cmdSuffix,  const std::string& samFile, const std::string& memoryPrgmName = "") {
        readmapper.emplace_back(name, cmdPrefix, cmdSuffix, samFile, memoryPrgmName);
    }

    void addReadSet(const std::string& name, const std::string& samFile) {
        readSets.emplace_back(name, samFile);
    }


    void evaluate(std::ostream& out, bool removeReadFile) {
        const ReadmapperCmd* vatram = NULL;
        for (const ReadmapperCmd& rm: readmapper) {
            if (rm.name == "VATRAM") {
                vatram = &rm;
                break;
            }
        }

        out << "ReadSet;Readmapper;Time;Memory;CorrectRate;WrongRate;unmappedVATRAMcorrect\n";
        for (const ReadSet& rs: readSets) {
            std::cout << "ReadSet: " << rs.name << std::endl;
            if (rs.isSamFile()) {
                evaluateSAM(rs, vatram, out, removeReadFile);
            }
            if (rs.isReadFile()) {
                evaluateRead(rs, out);
            }
        }
    }

    void evaluateRead(const ReadSet& rs, std::ostream& out) {
        typedef DistanceComparator::ReadmapperResult ReadmapperResult;
        std::vector<ReadmapperResult> rmResults;
        std::cout << "  Run mappers" << std::endl;
        for (const ReadmapperCmd& rm: readmapper) {
            std::cout << "    Readmapper: " << rm.name << std::endl;
            std::cout << "      Command: " << (rm.commandPrefix + " " + rs.file + " " + rm.commandSuffix) << std::endl;
            ExternalResult exres =
            runProgram((rm.commandPrefix + " " + rs.file + " " + rm.commandSuffix).c_str(), rm.memoryPrgmName);
            std::cout << "    Finished mapping (maxMemory = " << exres.maxMemory << ")" << std::endl;
            rmResults.emplace_back(rm, exres);
        }
        std::cout << "  Collect SAM" << std::endl;
        for (ReadmapperResult& rmRes: rmResults) {
            try {
                rmRes.reads = std::move(readSamFile(rmRes.readmapper.samFile));
            } catch (std::exception& e) {
                std::cout << "    Exception: " << e.what() << std::endl;
            }
        }
        std::cout << "  Read FASTQ: " << std::flush;
        std::vector<UnalignedRead> unaligned = readReadFile(rs.file);
        size_t readCount = unaligned.size();
        std::cout << readCount << std::endl;
        std::cout << "  Compare" << std::endl;
        std::vector<AlignedRead> opt;
        distCompare.compare(unaligned, rmResults, opt);
        writeSamFile(opt, std::string("opt") + rs.name + ".sam");
        std::cout << "  Print" << std::endl;
        for (const ReadmapperResult& rmRes: rmResults) {
            out << rs.name << ";" << rmRes.readmapper.name << ";"
                << (rmRes.externalResult.time.count() / 1000.) << ";"
                << (rmRes.externalResult.maxMemory / 1024. / 1024. / 1024.) << ";"
                << (rmRes.correct / (double) readCount) << ";"
                << (rmRes.wrong / (double) readCount) << ";";
            out << std::endl;
        }
    }

    void evaluateSAM(const ReadSet& rs, const ReadmapperCmd* /*vatram*/, std::ostream& out, bool removeReadFile) {
        std::cout << "  Read SAM" << std::endl;
        auto correct = readSamFile(rs.file);
        std::cout << "  Write FASTQ" << std::endl;
        std::string readFile = rs.name + ".fastq";
        writeReadFile(correct, readFile);
        for (const ReadmapperCmd& rm: readmapper) {
            std::cout << "  Readmapper: " << rm.name << std::endl;
            std::cout << "    Start mapping" << std::endl;
            std::cout << "      Command: " << (rm.commandPrefix + " " + readFile + " " + rm.commandSuffix) << std::endl;
//                Timer<> timer;
            ExternalResult exres =
            runProgram((rm.commandPrefix + " " + readFile + " " + rm.commandSuffix).c_str(), rm.memoryPrgmName); // exec the readmapper
//                timer.stopTimer();
            std::cout << "    Finished mapping (maxMemory = " << exres.maxMemory << ")" << std::endl;

            try {
                std::cout << "    Read SAM" << std::endl;
                auto aligned = readSamFile(rm.samFile);
                std::cout << "    Compare SAM" << std::endl;
                std::vector<AlignedRead> unmappedReads;
                SamCompare::Result result = samCompare.compareSam(correct, aligned, 5, &unmappedReads);

                std::cout << "    Write Result" << std::endl;
                out << rs.name << ";" << rm.name << ";"
                    << (exres.time.count() / 1000.) << ";"
                    << (exres.maxMemory / 1024. / 1024. / 1024.) << ";"
                    << (result.correct / (double) result.readCount) << ";"
                    << (result.wrong / (double) result.readCount) << ";";


//                if (false) if (rm.name == "BWA") {
//                    std::cout << "      Analyse Unmapped" << std::endl;
//                    std::string unmappedFile = "unmapped.fastq";
//                    std::cout << "      Write Unmapped Reads" << std::endl;
//                    writeReadFile(unmappedReads, unmappedFile);
//                    std::cout << "      Map unmapped with VATRAM" << std::endl;
//                    ExternalResult exres2 =
//                    runProgram((vatram->commandPrefix + " " + unmappedFile + " " + vatram->commandSuffix).c_str()); // exec the readmapper
//                    std::cout << "      Finished mapping (mamMemory = " << exres2.maxMemory << ")" << std::endl;
//                    try {
//                        std::cout << "      Read Unmapped SAM" << std::endl;
//                        auto unmappedAligned = readSamFile(vatram->samFile);
//                        SamCompare::Result unmappedResult = samCompare.compareSam(correct, unmappedAligned, 5);
//                        out << (unmappedResult.correct / (double) unmappedResult.readCount) << ";";
//                    } catch (std::exception& e) {
//                        std::cout << "      Exception: " << e.what() << std::endl;
//                    }
//                }

                out << std::endl;
                std::cout << "    Finished" << std::endl;
            } catch (std::exception& e) {
                std::cout << "    Exception: " << e.what() << std::endl;
            }
        }
        if (removeReadFile) {
            // clear read file
            std::remove(readFile.c_str());

        }
    }



    void writeSamFile(const std::vector<AlignedRead>& reads, const std::string& path) {
        seqan::BamStream bamstream(path.c_str(), seqan::BamStream::WRITE);
        ChromosomeReferences cr;
        for (const Chromosome& chr: distCompare.genome) {
            cr.push_back(chr);
        }
        SamWriter sw(bamstream);
        sw.writeHeader(cr);

        for (const AlignedRead& read: reads) {
            ReadString revComplStr = read.seq;
            seqan::reverseComplement(revComplStr);

            CigarString cs;
            seqan::CigarElement<> manyElements('M', seqan::length(read.seq));
            seqan::append(cs, manyElements);

            sw.writeRead(read.id, read.seq, revComplStr, read.revCompl, distCompare.chrName2idx[read.chr], read.beginPos, cs);
        }

    }






};

#endif // RMCOMPARATOR_H

#include "Experiments.h"
#include "seqan/file.h"
#include "seqan/bam_io.h"
#include <system_error>
#include <unordered_map>

//const char* RUN_EXPERIMENT_HEADLINE =
//        "; LSH build time (s); Search time per Read (ns); Time LSH per Read (ns); Time Aligner per Read (ns); LSH memory usage (GB); Correct Intersection Found; Correct Interval Found;"
//        "Wrong Intervals per Read; Wrong Columns per Read; Extra alignment calculation factor; No Interval Found;\n";

std::string convert(seqan::String<char, seqan::Alloc<>>& seqanString) {
    return std::string(seqan::begin(seqanString), seqan::end(seqanString));
}


void extractAlignedReadsFromSAM(const std::string& bamPath, const Genome& genome, std::vector<GenomeGenerator::Read> &out_reads) {
    std::unordered_map<std::string, const Chromosome*> chrNameMap;
    for (const Chromosome& chr: genome) {
        chrNameMap[chr.name] = &chr;
    }


    seqan::BamStream bam(bamPath.c_str());
    if (!seqan::isGood(bam)) {
        throw std::runtime_error("extractAlignedReadsFromSAM : error opening file");
    }
    seqan::BamAlignmentRecord record;
    size_t cigarSCount = 0;
    size_t cigarHCount = 0;
    size_t recordCount = 0;
    std::string lastID;
    while (!seqan::atEnd(bam)) {
        if (seqan::readRecord(record, bam) != 0) {
            throw std::runtime_error("extractAlignedReadsFromSAM : error reading file");
        }
        auto it = chrNameMap.find(Chromosome::generateName(convert(bam.header.sequenceInfos[record.rID].i1)));
        if (it != chrNameMap.end()) {
            recordCount++;
            bool cigarContainsS = false;
            bool cigarContainsH = false;
            for (seqan::CigarElement<> ce: record.cigar) {
                if (ce.operation == 'S') {
                    cigarContainsS = true;
                }
                if (ce.operation == 'H') {
                    cigarContainsH = true;
                }
            }
            cigarSCount += cigarContainsS;
            cigarHCount += cigarContainsH;
            if (!cigarContainsH) { // skip short reads
                std::string id(seqan::begin(record.qName), seqan::end(record.qName));
                GenomeGenerator::Read* read_pt;
                if (lastID == id && out_reads.size()) {
                    // Paired End Read (same id)
                    out_reads.back().next.reset(new GenomeGenerator::Read()); // TODOJQ: PE: extract SAM: paired end vernünftig auslesen
                    read_pt = &*out_reads.back().next;
                } else {
                    out_reads.emplace_back();
                    read_pt = &out_reads.back();
                }
                GenomeGenerator::Read& read = *read_pt;
                read.readString = record.seq;
                read.startPos = record.beginPos;
                read.chromosome = it->second;
                read.reversedComplement = seqan::hasFlagRC(record);
                if (read.reversedComplement) {

                    seqan::reverseComplement(read.readString);
                }
                lastID = id;

            }
        } // else chromosome not found -> skip
    }
    std::cout << "Alignments with S: " << cigarSCount << " / " << recordCount << std::endl;
    std::cout << "Alignments with H: " << cigarHCount << " / " << recordCount << std::endl;
}


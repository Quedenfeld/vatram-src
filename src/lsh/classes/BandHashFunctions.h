#ifndef BANDHASHFUNCTIONS_H
#define BANDHASHFUNCTIONS_H

#include "map/LSH.h"

class LSH_bhf_m33767 : public LSH {
public:
    LSH_bhf_m33767(unsigned long long seed) : LSH(seed) {}

    BandHash bandHashFunction(std::vector<SignatureHash>::iterator begin, std::vector<SignatureHash>::iterator end) {
        return std::accumulate(begin + 1, end, BandHash(*begin), [](BandHash ret, SignatureHash c) { return ret * 33767 + c; });
    }
};

class LSH_bhf_64 : public LSH_Generic<uint64_t, uint32_t, CollisionFreeBHT<uint64_t>, GappedQGramHashFunction<uint32_t>> {
public:
    LSH_bhf_64(unsigned long long seed) : LSH_Generic<uint64_t, uint32_t, CollisionFreeBHT<uint64_t>, GappedQGramHashFunction<uint32_t>>(seed) {}

    BandHash bandHashFunction(std::vector<SignatureHash>::iterator sig_begin, std::vector<SignatureHash>::iterator sig_end) {
        BandHash h = *sig_begin++;
        for (; sig_begin < sig_end; ++sig_begin) {
            h = (h << 32) ^ *sig_begin;
        }
        return h;
    }
};

class LSH_bhf_stdHash : public LSH {
public:
    LSH_bhf_stdHash(unsigned long long seed) : LSH(seed) {}

    BandHash bandHashFunction(std::vector<SignatureHash>::iterator sig_begin, std::vector<SignatureHash>::iterator sig_end) {
        std::u32string str(sig_begin, sig_end);
        std::hash<std::u32string> hf;
        return hf(str);
    }
};



#endif // BANDHASHFUNCTIONS_H

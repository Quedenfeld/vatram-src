#ifndef EXPERIMENTS_H
#define EXPERIMENTS_H

#include "types.h"
#include "Chromosome.h"
#include <utility>
#include "Timer.h"
#include "GenomeGenerator.h"
#include "seqan/seq_io.h"
#include "parallel.h"
#include "align/Aligner.h"
#include "map/LSH.h"
#include "align/AlignerDriver.h"

constexpr double DEFAULT_VARIANT_PROBABILITY_THRESHOLD = 0.2;

inline
double calcMemory(size_t readLng, size_t sigLng) {
    return sigLng * (22.595 / readLng + 0.013751) + 0.25;
}

inline
size_t calcSignatureLength(size_t readLng, double memory) {
    return std::round((memory - 0.25) / (22.595 / readLng + 0.013751));
}

inline
size_t calcSignatureLength(size_t readLng) {
    return calcSignatureLength(readLng, calcMemory(LSH::DEFAULT_READ_LENGTH, LSH::DEFAULT_SIGNATURE_LENGTH));
}

struct Stats {
public:
    double mean;
    double deviation;
    double min;
    double q1;
    double median;
    double q3;
    double max;


    Stats();

    template <class Iterator>
    Stats(Iterator it_begin, Iterator it_end) {
        calculate(it_begin, it_end);
    }

    template <class Iterator>
    void calculate(Iterator it_begin, Iterator it_end) {
        std::sort(it_begin, it_end);
        min = *it_begin;
        q1 = *(it_begin + (it_end - it_begin) / 4);
        median = *(it_begin + (it_end - it_begin) / 2);
        q3 = *(it_begin + (it_end - it_begin) * 3 / 4);
        max = *(it_end - 1);
        double n = it_end - it_begin;

        double sum = 0;
        for (auto it = it_begin; it != it_end; ++it) {
            sum += *it;
        }
        mean = sum / n;

        double dst_sq = 0;
        for (auto it = it_begin; it != it_end; ++it) {
            double d = (*it - mean);
            dst_sq += d*d;
        }
        deviation = sqrt(dst_sq / (n-1));
    }
};

template <class Integer>
Integer bits(Integer x) {
    Integer b = 0;
    while (x) {
        x >>= 1;
        b++;
    }
    return b;
}

struct ReadStatistics {
    size_t noWindowFound = 0;
    size_t correctWindowFound = 0;
    size_t sumWindows = 0;
    size_t wrongWindows = 0;
    size_t numberOfQueries = 0;

    inline
    double rate_correctWindowFound() {
        return correctWindowFound / double(numberOfQueries);
    }

    inline
    double rate_wrongWindows() {
        return wrongWindows / double(numberOfQueries);
    }

    inline
    double rate_noWindowFound() {
        return noWindowFound / double(numberOfQueries);
    }
};

struct AdvancedReadStatistics : public ReadStatistics {
    size_t correctIntervalFound = 0;
    size_t sumColumns = 0;
    size_t wrongColumns = 0;
    size_t sumReadColumns = 0;
    uint64_t timeLsh = 0;
    uint64_t timeAlign = 0;
    size_t correctAlignmentFound = 0;


    inline
    double rate_correctIntervalFound() {
        return correctIntervalFound / double(numberOfQueries);
    }

    inline
    double rate_correctAlignmentFound() {
        return correctAlignmentFound / double(numberOfQueries);
    }

    inline
    double rate_wrongColumns() {
        return wrongColumns / double(numberOfQueries);
    }

    inline
    double rate_wrongColumnsFactor() {
        return wrongColumns / double(sumReadColumns);
    }

    inline
    double rate_allColumns() {
        return sumColumns / double(numberOfQueries);
    }

    inline
    double rate_allColumnsFactor() {
        return sumColumns / double(sumReadColumns);
    }

    AdvancedReadStatistics& operator+=(const AdvancedReadStatistics& ars) {
        noWindowFound += ars.noWindowFound;
        correctWindowFound += ars.correctWindowFound;
        sumWindows += ars.sumWindows;
        wrongWindows += ars.wrongWindows;
        numberOfQueries += ars.numberOfQueries;
        correctIntervalFound += ars.correctIntervalFound;
        sumColumns += ars.sumColumns;
        wrongColumns += ars.wrongColumns;
        sumReadColumns += ars.sumReadColumns;
        timeLsh += ars.timeLsh;
        timeAlign += ars.timeAlign;
        correctAlignmentFound += ars.correctAlignmentFound;
        return *this;
    }
};

typedef std::vector<GenomeGenerator::Read> ReadSet;

struct ReadSetReference {
    const ReadSet* reads;
    size_t length;
};

namespace ParameterType {
    enum Type {UseLastIndex, Index, Query};
}

constexpr char RUN_EXPERIMENT_HEADLINE[] =
        "Read Length; LSH build time (s); Search time per Read (ns); Time LSH per Read (ns); Time Aligner per Read (ns); LSH memory usage (GB); Correct Intersection Found; Correct Interval Found; Correct Alignment Found; "
        "Wrong Intervals per Read; Columns per Read; Extra alignment calculation factor; No Interval Found;\n";

template <class TLsh>
class Experiment {
public:
    Genome& genome;
    std::vector<VariantIndex> varIdxs;
    std::vector<ReadSetReference> readSets;

    TLsh& lsh;
    std::function<void(Experiment&)> afterInitializaton;
    std::function<void(Experiment&)> clearAfter;

    AlignerOptions alignerOptions;

    std::ostream& out;

    size_t currentReadLength;

    std::mt19937 rnd;
    std::uniform_real_distribution<double> u01;

    bool multiThreading = true;

    bool extraInfoForMinCount = false;

    Experiment(Genome& genome,
               TLsh& lsh,
               std::function<void(Experiment&)> clearAfter,
               std::ostream& out,
               uint64_t seed = 0) :
        genome(genome),
        lsh(lsh),
        afterInitializaton([](Experiment&){}),
        clearAfter(clearAfter),
        alignerOptions(0, 0, 10, 0, 0, 1),
        out(out),
        rnd(seed),
        u01(0,1) {
        createVariantIndexes();
    }

    void createVariantIndexes() {
        varIdxs.clear();
        varIdxs.reserve(genome.size());
        for (const Chromosome& chr: genome) {
            varIdxs.emplace_back(chr.variants, -1);
        }
    }

    void printLSHParameters(std::ostream& out, size_t readLength) {
        currentReadLength = readLength;
        clearAfter(*this);
        out << "q = " << lsh.qGramSize << '\n';
        out << "gapString = " << lsh.qGramHashObject.getGapVector() << '\n';
        out << "winSize = " << lsh.windowSize << '\n';
        out << "winOff = " << lsh.windowOffset << '\n';
        out << "sigLng = " << lsh.signatureLength << '\n';
        out << "bandSize = " << lsh.bandSize << '\n';
        out << "limit = " << lsh.limit << '\n';
        out << "limitSkip = " << lsh.limit_skip << '\n';
        out << "minCountSelection = " << lsh._mapOptions.minCountSelection << '\n';
        out << "minCountPrefiltering = " << lsh._mapOptions.minCountPrefiltering << '\n';
        out << "maxSelect = " << lsh._mapOptions.maxSelect << '\n';
        out << "nextFactor = " << lsh._mapOptions.nextFactor << '\n';
        out << "contractBandMultiplicator = " << lsh._mapOptions.contractBandMultiplicator << '\n';
        out << "extendBandMultiplicator = " << lsh._mapOptions.extendBandMultiplicator << '\n';
        out << "extendFactor = " << lsh._mapOptions.extendFactor << '\n';
        out << "alignerMaxReturnedWindows = " << lsh._mapOptions.maximumOfReturnedWindows << '\n';
        out << "alignerMaxErrorLow = " << alignerOptions.maxError_low << '\n';
        out << "alignerMaxClipLow = " << alignerOptions.maxClip_low << '\n';
        out << "alignerMaxErrorHigh = " << alignerOptions.maxError_high << '\n';
        out << "alignerMaxClipHigh = " << alignerOptions.maxClip_high << '\n';
        out << "alignerMaxSelect = " << alignerOptions.maxSelect << '\n';
        out << "alignerNextFactor = " << alignerOptions.nextFactor << '\n';
        out << "variantProbabilityThreshold = " << DEFAULT_VARIANT_PROBABILITY_THRESHOLD << '\n';
    }

    void addReadSet(const ReadSet& readSet, size_t length) {
        readSets.emplace_back(ReadSetReference{&readSet, length});
    }

    void addReadSet(const ReadSet& readSet) {
        uint64_t lng = 0;
        uint64_t count = 0;
        for (const GenomeGenerator::Read& x: readSet) {
            const GenomeGenerator::Read* rd = &x;
            while (rd) {
                lng += seqan::length(rd->readString);
                count++;
                rd = rd->next.get();
            }
        }
        readSets.push_back(ReadSetReference{&readSet, lng / count});
    }

    bool alignRead(const GenomeGenerator::Read& read,
                   const ReadString& revCompl,
                   const std::vector<MapInterval>& intervals,
                   Aligner& aligner,
                   AdvancedReadStatistics& /*out_stats*/,
                   const MapInterval* correctInterval) {
        AlignerDriver driver(genome, varIdxs);
        AlignerDriver::Result result = driver.align(aligner, read.readString, revCompl, intervals, alignerOptions);
        bool found = correctInterval != NULL
                && result.alignment.isValid()
                && result.alignment.getErrorCount() <= read.errorCount
                && result.bestInterval == *correctInterval;

        if (correctInterval != NULL && result.bestInterval != *correctInterval ) {
//            std::cerr << "Found, but not aligned: " << correctInterval->startPosition << " vs " << result.bestInterval.startPosition;
//            std::cerr << ", " << read.errorCount << " vs " << result.alignment.getErrorCount() << std::endl;
        }
//        ErrorCount err = maxError + 1;
//        for (const MapInterval& res: result) {
//            Alignment alignment = aligner.align(lsh.chromosomes[res.chromosome].get(), res.startPosition, res.endPosition,
//                                                varIdxs[res.chromosome], res.inverted ? revCompl : read.readString, err - 1, maxClip);
//            if (alignment.isValid()) {
//                err = alignment.getErrorCount(); // -1, because next alignment should be better
//                if (err == 0) {
//                    break; // best alignment was found
//                }
//            }
//        }
//            std::cout << ",errors: " << (err) << std::endl;
        return found;
    }

    const MapInterval* evaluateResults(const GenomeGenerator::Read& read,
                                       const std::vector<MapInterval>& result,
                                       AdvancedReadStatistics& out_stats) {
        size_t rlng = seqan::length(read.readString);
        size_t rbegin = read.startPos;
        size_t rend = rbegin + rlng;

        unsigned correctWindowCount = 0;
        const MapInterval* correct = NULL;
        for (const MapInterval& hit: result) {
            out_stats.sumColumns += hit.endPosition - hit.startPosition;
            if (hit.inverted == read.reversedComplement && read.chromosome->name == lsh.chromosomes[hit.chromosome].get().name) {
                bool correctWindow = rbegin < hit.endPosition && rend > hit.startPosition;
                correctWindowCount += correctWindow;
                if (correctWindow && correctWindowCount == 1) { // avoid multiple founds of the correct window
                    correct = &hit;
                    // there is an intersection
                    out_stats.correctWindowFound++;
                    bool correctInterval = rbegin >= hit.startPosition && rend <= hit.endPosition;
                    out_stats.correctIntervalFound += correctInterval;
                    if (hit.startPosition <= rbegin) {
                        out_stats.wrongColumns += rbegin - hit.startPosition;
                    }
                    if (rend <= hit.endPosition) {
                        out_stats.wrongColumns += hit.endPosition - rend;
                    }


                } else {
                    out_stats.wrongColumns += hit.endPosition - hit.startPosition;
                    out_stats.wrongWindows += 1;
                }
            } else {
                out_stats.wrongColumns += hit.endPosition - hit.startPosition;
                out_stats.wrongWindows += 1;
            }
        }

        if (extraInfoForMinCount && correctWindowCount == 0 && rlng >= 450) {
            std::cerr << "MinCount: hash=" << std::hex << hash(read.readString) << std::dec;
            std::cerr << ", pos=" << read.startPos << ", err=" << read.errorCount;
            std::cerr  << ", results=(" << result.size() << "):[";
            bool notfirst = false;
            for (const MapInterval& hit: result) {
                std::cerr << (notfirst ? ", " : "") << "(" << hit.score << "," << hit.startPosition << "-" << hit.endPosition << ")";
                notfirst = true;
            }
            std::cerr << "]" << std::endl;
        }

        out_stats.noWindowFound += result.size() == 0;
        out_stats.sumWindows += result.size();
        out_stats.numberOfQueries++;
        out_stats.sumReadColumns += rlng;
        return correct;
    }

    uint64_t hash(const ReadString& rs) {
        uint64_t h = 0;
        for (ReadChar c: rs) {
            h += int(c);
            h *= 5;
        }
        return h;
    }

    template <class Iterator>
    AdvancedReadStatistics analyseAdvanced(Iterator reads_begin,
                                           Iterator reads_end,
                                           Aligner& aligner,
                                           double alignProbability) {
        AdvancedReadStatistics rr;
        struct ReadData {
            const GenomeGenerator::Read& read;
            ReadString revCompl[2];
            std::vector<MapInterval> result[2];
            ReadData(const GenomeGenerator::Read& read) :
                read(read) {
                revCompl[0] = read.readString;
                seqan::reverseComplement(revCompl[0]);
                const GenomeGenerator::Read* snd = pairedEnd();
                if (snd) {
                    revCompl[1] = snd->readString;
                    seqan::reverseComplement(revCompl[1]);
                }
            }
            const GenomeGenerator::Read* pairedEnd() {
                return read.next.get();
            }
        };

        // create rev-compl, etc.
        std::vector<ReadData> rdata;
        for (; reads_begin != reads_end; ++reads_begin) {
            const GenomeGenerator::Read& r = *reads_begin;
            rdata.emplace_back(r);
        }

        // LSH
        Timer<std::chrono::nanoseconds> tLsh;
        for (ReadData& r: rdata) {
            if (r.pairedEnd()) {
                lsh.findInterval_pairedEnd(r.read.readString, r.revCompl[0], r.pairedEnd()->readString, r.revCompl[1], lsh._mapOptions, r.result[0], r.result[1]);
                //, 0, r.read.startPos, r.pairedEnd()->startPos);
            } else {
                if (seqan::length(r.read.readString) > lsh.windowSize * lsh._mapOptions.longReadSplitFactor) {
                    // long read
                    lsh.findInterval_longRead(r.read.readString, r.revCompl[0], lsh._mapOptions, r.result[0]);
                } else {
                    lsh.findInterval_singleEnd(r.read.readString, r.revCompl[0], lsh._mapOptions, r.result[0]);
                }
            }
        }
        tLsh.stopTimer();
        rr.timeLsh += tLsh.getDuration().count();

        // Evaluate
        std::vector<const MapInterval*> correctIntervals[2];
        for (ReadData& r: rdata) {
            correctIntervals[0].push_back(evaluateResults(r.read, r.result[0], rr));
            correctIntervals[1].emplace_back();
            if (r.pairedEnd()) {
                correctIntervals[1].back() = evaluateResults(*r.pairedEnd(), r.result[1], rr);
            }
        }

        // Align
        if (alignProbability > 0) {
            const uint64_t ALIGN_MAX_HASH = std::numeric_limits<uint64_t>::max() * alignProbability;
            Timer<std::chrono::nanoseconds> tAlign;
            int found = 0;
            size_t i = 0;
            for (ReadData& r: rdata) {
                uint64_t h = hash(r.read.readString);
                if (h < ALIGN_MAX_HASH) { //if (u01(rnd) < alignProbability) {
                    found += alignRead(r.read, r.revCompl[0], r.result[0], aligner, rr, correctIntervals[0][i]);
                    if (r.pairedEnd()) {
                        found += alignRead(*r.pairedEnd(), r.revCompl[1], r.result[1], aligner, rr, correctIntervals[1][i]);
                    }

                }
                i++;
            }

            tAlign.stopTimer();
            rr.timeAlign += tAlign.getDuration().count() / alignProbability;
            rr.correctAlignmentFound += found / alignProbability;
        }



        return rr;
    }

    AdvancedReadStatistics analysedAdvancedParallel(const ReadSet& reads, double alignProbability) {
        Mutex mutex;
        AdvancedReadStatistics result;
        size_t chunkSize = 10000; // process chunkSize many reads in one thread
        size_t size = reads.size();
        if (multiThreading) {
            Aligner aligner(0,0);
            #pragma omp parallel for schedule(dynamic, 1) firstprivate(aligner)
            for (size_t i = 0; i < size; i += chunkSize) {

                size_t last = std::min(size, i + chunkSize);
                AdvancedReadStatistics ars = analyseAdvanced(&reads[i], &reads[last], aligner, alignProbability);
                {
                    LockGuard lock(mutex);
                    result += ars;
                }
            }
        } else {
            // single thread
            Aligner aligner(0,0);
            for (size_t i = 0; i < size; i += chunkSize) {
                size_t last = std::min(size, i + chunkSize);
                AdvancedReadStatistics ars = analyseAdvanced(&reads[i], &reads[last], aligner, alignProbability);
                result += ars;
            }
        }
        return result;
    }


    void buildIndex() {
        for (Chromosome& chr: genome) {
            chr.removeLowProbabilitySnps();
        }
        lsh.clear();
        size_t genomeLength = 0;
        for (const Chromosome& chr: genome) {
            genomeLength += seqan::length(chr.data);
        }
        lsh.initialize(genomeLength / lsh.windowOffset);
        afterInitializaton(*this);
        lsh.addGenome(genome.begin(), genome.end());
        lsh.buildIndex();
        for (Chromosome& chr: genome) {
            chr.insertLowProbabilitySnps();
        }
    }

    AdvancedReadStatistics runExperiment(const ReadSet& reads, size_t length, double alignProbability, ParameterType::Type paraType = ParameterType::Index) {
        out << length << ";";

        if (paraType == ParameterType::Index || paraType == ParameterType::UseLastIndex) {
            Timer<std::chrono::seconds> timer_lsh;
            buildIndex();
            out << timer_lsh << ";";
        } else {
            out << ";";
        }

        Timer<std::chrono::nanoseconds> timer_ana;
        AdvancedReadStatistics ars = analysedAdvancedParallel(reads, alignProbability);
        out << timer_ana.stopTimer().count() / reads.size() << ";";
        out << ars.timeLsh / reads.size() << ";";
        out << (alignProbability > 0 ? std::to_string(ars.timeAlign / reads.size()) : "") << ";";

        out << (lsh.memoryUsage() / double(1 << 30)) << ";";

        out << std::setprecision(9);
        out << ars.rate_correctWindowFound() << ";";
        out << ars.rate_correctIntervalFound() << ";";
        out << ars.rate_correctAlignmentFound() << ";";
        out << std::setprecision(6);

        out << ars.rate_wrongWindows() << ";";
        out << ars.rate_allColumns() << ";";
        out << ars.rate_allColumnsFactor() << ";";
        out << ars.rate_noWindowFound() << ";";

        return ars;
    }

    void runExperiment(double alignProbability, ParameterType::Type paraType = ParameterType::Index, const std::string& prefix = "") {
        for (const ReadSetReference& rsr: readSets) {
            currentReadLength = rsr.length;
            clearAfter(*this);
            out << prefix;
            runExperiment(*rsr.reads, rsr.length, alignProbability, readSets.size() > 1 ? ParameterType::Index : paraType);
            out << std::endl;
            if (readSets.size() > 1) {
                lsh.clear();
            }
        }
    }

    template <class Parameter>
    void varyParameter(const std::string& paraName,
                       const std::vector<Parameter>& paraList,
                       std::function<void(Experiment&, Parameter)> setFunction,
                       const std::string& headLine,
                       double alignProbability,
                       ParameterType::Type paraType = ParameterType::Index) {
        out << headLine;
        out << paraName << "; " << RUN_EXPERIMENT_HEADLINE;

        for (const ReadSetReference& rsr: readSets) {
            currentReadLength = rsr.length;
            clearAfter(*this);
            bool first = (paraType == ParameterType::UseLastIndex && readSets.size() == 1 ? false : true);
            for (const Parameter& para: paraList) {
                if (paraType == ParameterType::Index) {
                    lsh.clear();
                }
                try {
                    setFunction(*this, para);
                    out << para << ";";
                    runExperiment(*rsr.reads, rsr.length, alignProbability, first ? ParameterType::Index : paraType);
                    out << std::endl;
                } catch (...) {
                    std::cerr << "varyParameter: An error occured for " << para << "." << std::endl;
                }

                first = false;

            }
            if (readSets.size() > 1) {
                lsh.clear();
            }
        }
    }

};



void extractAlignedReadsFromSAM(const std::string& bamPath, const Genome& genome, std::vector<GenomeGenerator::Read> &out_reads);

inline
std::vector<GenomeGenerator::Read> extractAlignedReadsFromSAM(const std::string& bamPath, const Genome& genome) {
    std::vector<GenomeGenerator::Read> reads;
    extractAlignedReadsFromSAM(bamPath, genome, reads);
    return reads;
}

#endif // EXPERIMENTS_H

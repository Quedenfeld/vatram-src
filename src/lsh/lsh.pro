TARGET_FILE_NAME = lsh
include(../dep-on-lib.pri)

TEMPLATE = app
CONFIG += console

CONFIG += qt
QT += testlib

SOURCES += main.cpp \
    MemoryUsage.cpp \
    experiments/VaryParameters.cpp \
    Experiments.cpp \
    experiments/HumanGenomeExperiment.cpp \
    experiments/RealReadExperiment.cpp \

HEADERS += \
    Experiments.h \
    MemoryUsage.h \
    classes/BandHashFunctions.h \
    experiments/VaryParameters.h \
    experiments/HumanGenomeExperiment.h \
    experiments/SpeedTest.h \
    experiments/RealReadExperiment.h \
    classes/QGramHashFunctions2.h \
    experiments/MultiWindowBucketAnalysis.h \
    experiments/ReadLengthExp.h \
    compare/RMComparator.h \
    compare/SamCompare.h \
    compare/RMExperiment.h \
    compare/ExternalProgram.h \
    compare/VatramCmdStrings.h \
    compare/DistanceComparator.h \
    compare/CompareStructs.h \
    compare/RMExperimentPaired.h \
    compare/RMComparatorPaired.h \
    compare/ReadSamIo.h \
    FilterPacBioReads.h
